<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Str;

class FaqItem extends Model
{
    protected $primaryKey = 'id';

    protected $keyType = 'string';

    public $incrementing = false;

    protected $fillable = [
            'question_ru',
            'question_en',
            'answer_ru',
            'answer_en',
            'order',
        ];

    protected static function boot()
    {
        parent::boot();

        static::creating(function (Model $model) {
            $model->setAttribute($model->getKeyName(), Str::uuid()->toString());
        });
    }

    public function question()
    {
        switch (App::getLocale()) {
            case 'ru':
                return $this->question_ru;
                break;

            case 'en':
                return $this->question_en;
                break;

            default:
                return $this->question_ru;
                break;
            }

    }

    public function answer()
    {
        switch (App::getLocale()) {
            case 'ru':
                return $this->answer_ru;
                break;

            case 'en':
                return $this->answer_en;
                break;

            default:
                return $this->answer_ru;
                break;
        }
    }
}
