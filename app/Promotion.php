<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Promotion extends Model
{

    protected $primaryKey = 'id';

    protected $keyType = 'string';

    public $incrementing = false;

    public $fillable = ['user_id', 'product_id', 'available_until'];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'available_until',
    ];

    protected static function boot()
    {
        parent::boot();

        static::creating(function (Model $model) {
            $model->setAttribute($model->getKeyName(), Str::uuid()->toString());
        });
    }

    /**
     * The product that belong to the promotion.
     */
    public function product()
    {
        return $this->hasOne(Product::class,'id', 'product_id');
    }

    /**
     * The product that belong to the promotion.
     */
    public function user()
    {
        return $this->hasOne(User::class,'id', 'user_id');
    }
}
