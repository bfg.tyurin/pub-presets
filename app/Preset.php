<?php

namespace App;

use App\Price;
use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Model;

class Preset extends Model
{

    protected $price;

    protected $primaryKey = 'id';

    protected $keyType = 'string';

    public $incrementing = false;

    protected static function boot()
    {
        parent::boot();

        static::creating(function (Model $model) {
            $model->setAttribute($model->getKeyName(), Str::uuid()->toString());
        });
    }

    public static function addPreset(string $title, string $slug, Price $price)
    {
        $preset = new static;
        $preset->title = $title;
        $preset->slug = $slug;
        $preset->setPrice($price);

        $preset->save();

        return $preset;
    }

    public function setPriceAttribute(Price $price)
    {
        $this->setPrice($price);
    }

    public function setPrice(Price $price)
    {
        $this->attributes['price_original'] = $price->getOriginal();
        $this->attributes['price_discount'] = $price->getDiscount();
        $this->price = $price;
    }

    public function getPriceAttribute()
    {
        return $this->price;
    }

    public function getRouteKeyName()
    {
        return 'slug';
    }
}
