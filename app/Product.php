<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Product extends Model
{
    protected $price;

    protected $primaryKey = 'id';

    protected $keyType = 'string';

    public $incrementing = false;

    protected $fillable = [
        'title',
        'slug',
        'price_original',
        'price_discount',
        'usd_price_original',
        'usd_price_discount',
        'type',
        'category',
    ];

    protected static function boot()
    {
        parent::boot();

        static::creating(function (Model $model) {
            $model->setAttribute($model->getKeyName(), Str::uuid()->toString());
        });
    }

    public static function addProduct(Pack $pack, Price $price, string $type)
    {
        $product = new static;
        $product->title = $pack->title;
        $product->slug = $pack->slug;
        $product->setPrice($price);
        $product->type = $type;



        $product->save();

        $product->packs()->attach($pack->id);

        return $product;
    }

    public static function addBundle(array $packs, Price $price, string $title, string $slug, string $type)
    {
        $product = new static;

        $product->setPrice($price);

        $product->title = $title;
        $product->slug = $slug;
        $product->type = $type;


        $product->save();

        $product->packs()->attach($packs);

        return $product;
    }

    public function setPriceAttribute(Price $price)
    {
        $this->setPrice($price);
    }

    public function setPrice(Price $price)
    {
        $this->attributes['price_original'] = $price->getOriginal();
        $this->attributes['price_discount'] = $price->getDiscount();
        $this->price = $price;
    }

    public function getPriceAttribute()
    {
        return $this->price;
    }

    public function getRouteKeyName()
    {
        return 'slug';
    }

    /**
     * The packs that belong to the user.
     */
    public function packs()
    {
        return $this->belongsToMany(Pack::class);
    }

    // TO-DO switch to new many-to-many relation belongsToMany(Order::class);
    public function orders()
    {
        return $this->hasMany(Order::class);
    }

    public function notCampaignOrders()
    {
        return $this->belongsToMany(Order::class)
            ->where('status', 'completed')
            ->join('users', 'orders.user_id', '=', 'users.id')
            ->whereNotIn('email', Order::freeEmails())
            ->whereNull('campaign_id')
            ->select('orders.*');
    }

    public function campaignOrders()
    {
        return $this->belongsToMany(Order::class)
            ->where('status', 'completed')
            ->join('users', 'orders.user_id', '=', 'users.id')
            ->whereNotIn('email', Order::freeEmails())
            ->whereNotNull('campaign_id')
            ->select('orders.*');
    }

    /**
     * The related_promotion that belong to the user.
     */
    public function related_promotions()
    {
        return $this->belongsToMany(
            Product::class,
            'related_products',
            'related_product_id'
        )
            ->where('type', 'promotion');
    }

    public function getFileName($product)
    {
        switch ($product->slug) {
            case ('golden-caramel'):
                return 'f6a70df47ad54dd75396dcec7bf3e0e9.zip';
                break;
            case ('metallic-blues'):
                return 'fc8a95ccef877c64cce414ad4226a20c.zip';
                break;
            case ('wedding-pureness'):
                return 'fd65343df6311154dec3f6c54e3742a9.zip';
                break;
            case ('golden-metallic-wedding'):
                return 'f5f3bf383bb2ff7e472c00de97196ca3.zip';
                break;
            case ('golden-wedding'):
                return '82541ba3-55d0-46be-ad06-1e575d2abc5a.zip';
                break;
            case ('metallic-wedding'):
                return '4a7c0cc1-6da4-4270-a2a5-265c1b0c3c2e.zip';
                break;
            case ('golden-metallic'):
                return 'd13e534a-ff4e-4c04-8a0c-0be8a8bd9081.zip';
                break;
                // v2
            case ('golden-caramel-mobile'):
                return 'a7e07a1b-ca65-43e8-826c-578ba2ff6a63.zip';
                break;
            case ('metallic-blues-mobile'):
                return 'd1a0457c-8203-4ef9-99df-92cb2a7848bb.zip';
                break;
            case ('spring-burst'):
                return 'c97b5d69-5b45-4869-b74e-6007184f7db8.zip';
                break;
            case ('safari-style'):
                return '666278f4-9b86-44e9-a2fb-3857831678cd.zip';
                break;
        }
    }

    /**
     * Scope a query to only include pack type.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopePack($query)
    {
        return $query->where('type', 'pack');
    }

    /**
     * Scope a query to only include mobile category.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeMobile($query)
    {
        return $query->where('type', 'pack')->where('category', 'mobile');
    }

    /**
     * Scope a query to only include desktop category.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeDesktop($query)
    {
        return $query->where('type', 'pack')->where('category', 'desktop');
    }

    public function getFormattedRubPriceValueWithMultiplier($multiplier)
    {
        return number_format($this->price_discount * $multiplier, 0, ".", " ");
    }

    public function getNextFormattedRubPriceValueWithMultiplier($multiplier, $subTotal, $total)
    {
        $value = ($subTotal + $this->price_discount) * $multiplier - $total;
        return number_format(ceil($value), 0, ".", " ");
    }

    public function getFormattedUsdPriceValueWithMultiplier($multiplier)
    {
        return number_format($this->price_discount * $multiplier, 0, ".", " ");
    }
}
