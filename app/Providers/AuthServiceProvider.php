<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Gate::define('show-support', function ($user) {
            return $user->isSupport() || $user->isSuperAdmin() || $user->isAdmin();
        });

        Gate::define('show-dashboard', function ($user) {
            return $user->isSuperAdmin() || $user->isAdmin();
        });


        Gate::define('show-panel', function ($admin) {
            return $admin->isSuperAdmin();
        });

        Gate::define('buy-promotions', function ($user) {
            return $user->promotions()->exists();
        });
    }
}
