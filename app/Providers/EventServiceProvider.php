<?php

namespace App\Providers;

use App\Events\OrderWasCompleted;
use Illuminate\Auth\Events\Registered;
use App\Listeners\CreatePromotionListener;
use App\Listeners\CreateDownloadableListener;
use App\Listeners\SendOrderCompletedEmailListener;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],
        'App\Events\OrderCompleted' => [
            'App\Listeners\SendOrderCompletedEmail',
        ],
        OrderWasCompleted::class => [
            // CreatePromotionListener::class,
            CreateDownloadableListener::class,
            SendOrderCompletedEmailListener::class,
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
