<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class LoginToken extends Model
{
    protected $fillable = ['admin_id', 'token'];

    protected $primaryKey = 'id';

    protected $keyType = 'string';

    public $incrementing = false;

    public function getRouteKeyName()
    {
        return 'token';
    }

    protected static function boot()
    {
        parent::boot();

        static::creating(function (Model $model) {
            $model->setAttribute($model->getKeyName(), Str::uuid()->toString());
        });
    }

    public static function generateFor($admin)
    {
        return static::create([
            'admin_id' => $admin->id,
            'token'    => Str::random(64)
        ]);
    }

    public function admin()
    {
        return $this->belongsTo(Admin::class);
    }
}
