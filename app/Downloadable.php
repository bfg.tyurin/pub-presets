<?php

namespace App;

use Illuminate\Support\Str;
use Illuminate\Support\Carbon;
use Illuminate\Database\Eloquent\Model;

class Downloadable extends Model
{
    protected $primaryKey = 'id';

    protected $keyType = 'string';

    public $incrementing = false;

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = ['product_page_url', 'product_title', 'user_email', 'is_completed_order'];

    protected static function boot()
    {
        parent::boot();

        static::creating(function (Model $model) {
            $model->setAttribute($model->getKeyName(), Str::uuid()->toString());
        });
    }

    public static function validFromToken($token)
    {
        return self::where('token', $token)
            ->where('created_at', '>', Carbon::parse('-30 days'))
            ->first();
    }

    public function product()
    {
        return $this->belongsTo(Product::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function order()
    {
        return $this->belongsTo(Order::class);
    }

    /**
     * Get the products url for the Downloadable.
     *
     * @return String
     */
    public function getProductPageUrlAttribute()
    {
        return route('products.download', $this->token);
    }

    public function getProductTitleAttribute()
    {
        return $this->product ? $this->product->title : '';
    }

    public function getUserEmailAttribute()
    {
        return $this->user->email;
    }

    public function getIsCompletedOrderAttribute()
    {
        // return Order::where('user_id', $this->user->id)->where('product_id', $this->product->id)->exists();
        if ($this->order) {
            return $this->order->status === 'completed';
        }

        if ($this->product) {
            return Order::where('user_id', $this->user->id)->where('product_id', $this->product->id)->exists();
        }
    }

    public function getUrl()
    {
        // $urlWithLocale = LaravelLocalization::getLocalizedURL(LaravelLocalization::getCurrentLocale(), route('products.download', $downloadable->token));
        return url()->route('products.download', $this->token);
    }
}
