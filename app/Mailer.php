<?php

namespace App;

interface Mailer
{
    /**
     * @param User $user
     * @param Content $content
     * @return mixed
     */
    public function send(User $user, Content $content);
    public function sendByEmail(string $email, Content $content);
}
