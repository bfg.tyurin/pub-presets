<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;
use Illuminate\Support\Str;

class EmailLogin extends Model
{
    public $fillable = ['email', 'token'];

    public static function createForEmail($email)
    {
        return self::create([
            'email' => $email,
            'token' => Str::random(64)
        ]);
    }

    public static function validFromToken($token)
    {
        return self::where('token', $token)
            ->where('created_at', '>', Carbon::parse('-5 minutes'))
            ->firstOrFail();
    }

    public function user()
    {
//        dd($this->hasOne(User::class, 'email', 'email'));
        return $this->hasOne(User::class, 'email', 'email');
    }
}
