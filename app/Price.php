<?php

namespace App;

final class Price
{
    private int $original;
    private int $discount;

    public function __construct(int $original, int $discount)
    {
        $this->original = $original;
        $this->discount = $discount;
    }

    /**
     * Get the value of price
     */
    public function getOriginal(): int
    {
        return $this->original;
    }

    /**
     * Get the value of discountPrice
     */
    public function getDiscount()
    {
        return $this->discount;
    }
}
