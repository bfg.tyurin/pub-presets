<?php

namespace App\Listeners;

use App\Mailer;
use App\Content;
use App\Events\OrderWasCompleted;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Log;
use App\Services\DownloadableService;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendOrderCompletedEmailListener
{
    private Mailer $mailer;
    private DownloadableService $downloadableService;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(Mailer $mailer, DownloadableService $downloadableService)
    {
        $this->mailer = $mailer;
        $this->downloadableService = $downloadableService;
    }

    /**
     * Handle the event.
     *
     * @param  OrderWasCompleted  $event
     * @return void
     */
    public function handle(OrderWasCompleted $event)
    {
        $email = $event->order->user->email;
        $user = $event->order->user;
        $downloadable = $event->order->downloadable;
        $body = view('mail.purchase', ['downloadableLink' => $this->downloadableService->getUrl($downloadable)])->render();
        $content = new Content(__('purchase_email.title'), $body);
        $result = $this->mailer->sendByEmail($email, $content);

        if ($user->profile) {
            $body = view('mail.profile_was_created', ['downloadableLink' => $this->downloadableService->getUrl($downloadable)])->render();
            $content = new Content(__('profile_was_created_email.title'), $body);
            $this->mailer->sendByEmail($user->email, $content);
        }

        Log::info('SendOrderCompletedEmailListener', ['email' => $email, 'content title' => $content->getTitle()]);
    }
}
