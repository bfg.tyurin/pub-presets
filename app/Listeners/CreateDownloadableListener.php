<?php

namespace App\Listeners;

use App\Events\OrderWasCompleted;
use Illuminate\Support\Facades\Log;
use App\Services\CreateDownloadable;
use App\Services\DownloadableService;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class CreateDownloadableListener
{
    protected CreateDownloadable $createDownloadable;
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(CreateDownloadable $createDownloadable)
    {
        // $this->downloadableService = $downloadableService;
        $this->createDownloadable = $createDownloadable;
    }

    /**
     * Handle the event.
     *
     * @param  OrderWasCompleted  $event
     * @return void
     */
    public function handle(OrderWasCompleted $event)
    {
        $order = $event->order;

        $this->createDownloadable->call($order->user_id, $order->id);
        Log::info('Create Downloadable');
    }
}
