<?php

namespace App\Listeners;

use App\Events\OrderWasCompleted;
use App\Services\CreatePromotion;
use Illuminate\Support\Facades\Log;
use App\Services\Dto\CreatePromotionDto;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class CreatePromotionListener
{

    public $duration;
    public CreatePromotion $createPromotion;
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(CreatePromotion $createPromotion, $duration = 1)
    {
        $this->createPromotion = $createPromotion;
        $this->duration = $duration;
    }

    /**
     * Handle the event.
     *
     * @param  OrderWasCompleted  $event
     * @return void
     */
    public function handle(OrderWasCompleted $event)
    {
        $order = $event->order;
        if ($order->product->type === 'pack') {
            Log::info('Product type is a Pack. Creating promotion. with duration: ' . $this->duration);

            $promotionDto = new CreatePromotionDto($order->user_id, $order->product_id, $this->duration);
            $this->createPromotion->call($promotionDto);
        }
    }
}
