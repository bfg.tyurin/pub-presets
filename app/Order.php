<?php

namespace App;

use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletes;

class Order extends Model
{
    use SoftDeletes;

    const PAYMENT_TYPE_CLOUDPAYMENTS = 'cloudpayments';
    const PAYMENT_TYPE_PAYPAL = 'paypal';
    const STATUS_PROCESSING = 'processing';
    const STATUS_COMPLETED = 'completed';
    const CURRENCY_RUB = 'RUB';
    const CURRENCY_USD = 'USD';

    protected $dates = ['deleted_at'];

    protected $primaryKey = 'id';

    protected $keyType = 'string';

    public $incrementing = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['user_id', 'product_id', 'status', 'total', 'payment_type', 'currency'];

    protected static function boot()
    {
        parent::boot();

        static::creating(function (Model $model) {
            $model->setAttribute($model->getKeyName(), Str::uuid()->toString());
        });
    }

    public static function freeEmails()
    {
        return ['test@example.com'];
    }

    public static function discountEmails()
    {
        return [
            'test@example.com',
        ];
    }

    /**
     * Get the user that owns the order.
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Get the product that owns the order.
     */
    public function product()
    {
        return $this->belongsTo(Product::class);
    }

    public function products()
    {
        return $this->belongsToMany(Product::class);
    }

    public function downloadable()
    {
        return $this->hasOne(Downloadable::class);
    }

    /**
     * Scope a query to only include completed orders.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeCompleted($query)
    {
        return $query->where('status', 'completed');
    }

    /**
     * Scope a query to only include processing orders.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeProcessing($query)
    {
        return $query->where('status', 'processing');
    }

    /**
     * Scope a query to only include rub orders.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeRub($query)
    {
        return $query->where(function ($query) {
            $query->where('currency', 'RUB')
                ->orWhereNull('currency');
        });
    }
}
