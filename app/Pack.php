<?php

namespace App;

use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Model;

class Pack extends Model
{
    protected $price;

    protected $primaryKey = 'id';

    protected $keyType = 'string';

    public $incrementing = false;

    protected static function boot()
    {
        parent::boot();

        static::creating(function (Model $model) {
            $model->setAttribute($model->getKeyName(), Str::uuid()->toString());
        });
    }

    public static function addPack(string $title, string $slug, int $quantity)
    {
        $pack = new static;
        $pack->title = $title;
        $pack->slug = $slug;
        $pack->quantity = $quantity;

        $pack->save();

        return $pack;
    }

    public function getRouteKeyName()
    {
        return 'slug';
    }

    /**
     * The roles that belong to the user.
     */
    public function products()
    {
        return $this->belongsToMany(Product::class);
    }

    public function desktopProduct()
    {
        return $this->products()->where('category', 'desktop')->first();
    }

    public function mobileProduct()
    {
        return $this->products()->where('category', 'mobile')->first();
    }
}
