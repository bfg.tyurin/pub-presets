<?php


namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Str;

class Admin extends Authenticatable
{
    protected $fillable = ['email'];

    protected $primaryKey = 'id';

    protected $keyType = 'string';

    public $incrementing = false;

    public static function byEmail($email)
    {
        return static::where('email', $email)->firstOrFail();
    }

    protected static function boot()
    {
        parent::boot();

        static::creating(function (Model $model) {
            $model->setAttribute($model->getKeyName(), Str::uuid()->toString());
        });
    }

    public function tokens()
    {
        return $this->hasMany(LoginToken::class);
    }

    public function isSupport()
    {
        if ('test@example.com' === $this->email || 'test@example.com' === $this->email) {
            return true;
        }
        return false;
    }

    public function isAdmin()
    {
        if ('test@example.com' === $this->email) {
            return true;
        }
        return false;
    }

    public function isSuperAdmin()
    {
        if ('test@example.com' === $this->email) {
            return true;
        }
        return false;
    }
}
