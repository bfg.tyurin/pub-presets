<?php

namespace App\Jobs;

use App\Mailer;
use App\Content;
use App\Product;
use Illuminate\Bus\Queueable;
use App\Services\CreateDiscount;
use Illuminate\Support\Facades\Log;
use Illuminate\Queue\SerializesModels;
use App\Services\Dto\CreateDiscountDto;
use Illuminate\Database\QueryException;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class ProcessAddDiscountForUsersWithFreePresetPromotion implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $subscription;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($subscription)
    {
        $this->subscription = $subscription;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(CreateDiscount $createDiscount, Mailer $mailer)
    {
        $user = $this->subscription->user;
        $products = Product::where('type', 'pack')->orWhere('type', 'bundle')->get();

        foreach ($products as $product) {
            try {
                $createDiscount->call(new CreateDiscountDto($user->email, $product->id, $product->price_discount * 0.90, $product->usd_price_discount * 0.90));
            } catch (QueryException $e) {
                Log::info('user already has a discount', ['user' => $user]);
            }
        }
        $body = view('mail.add_discount_for_users_with_free_preset_promotion', ['email' => $user->email])->render();
        $content = new Content($this->subscription->email_campaign->title, $body);
        Log::info('Send add_discount_for_users_with_free_preset_promotion ' . $user->email);
        $mailer->sendByEmail($user->email, $content);

        $this->subscription->is_mail_send = true;
        $this->subscription->save();
    }
}
