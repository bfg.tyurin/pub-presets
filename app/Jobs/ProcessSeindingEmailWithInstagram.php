<?php

namespace App\Jobs;

use Exception;
use App\Mailer;
use App\Content;
use Illuminate\Bus\Queueable;
use Illuminate\Support\Facades\Log;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class ProcessSeindingEmailWithInstagram implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $email;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(string $email)
    {
        $this->email = $email;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(Mailer $mailer)
    {
        $body = view('mail.with_instagram')->render();
        $content = new Content('Как найти свой цвет?', $body);
        Log::info('Send email_with_instagram to ' . $this->email);
        $mailer->sendByEmail($this->email, $content);
        // try {
        //     $mailer->sendByEmail($this->email, $content);
        //     Log::info('SendEmailwithInstagram mail send.');
        // } catch (Exception $e) {
        //     Log::info('SendEmailwithInstagram mail failed.');
        //     return response()->json('send mail failed', 400);
        // }
    }
}
