<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GlobalString extends Model
{
    protected $fillable = ['key', 'ru', 'en'];
}
