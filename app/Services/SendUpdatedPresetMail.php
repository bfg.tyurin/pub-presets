<?php


namespace App\Services;


use App\Content;
use App\Mailer;
use App\Services\Dto\SendUpdatedPresetMailDto;
use Illuminate\Support\Facades\Log;

class SendUpdatedPresetMail
{
    /**
     * @var Mailer
     */
    private Mailer $mailer;

    /**
     * SendUpdatedPresetMail constructor.
     * @param Mailer $mailer
     */
    public function __construct(Mailer $mailer)
    {
        $this->mailer = $mailer;
    }

    /**
     * @param SendUpdatedPresetMailDto $dto
     * @return \Illuminate\Http\JsonResponse
     * @throws \Throwable
     */
    public function call(SendUpdatedPresetMailDto $dto)
    {
        $body = view('mail.updated_presets_without_promotion')->with(['downloadableUrl' => $dto->getDownloadableUrl(), 'productType' => $dto->getProductType()])->render();
        $content = new Content($dto->getTitle(), $body);
        try {
            $this->mailer->sendByEmail($dto->getUserEmail(), $content);
            Log::info('SendUpdatedPresetMail mail send.');
        } catch (Exception $e) {
            Log::info('SendUpdatedPresetMail mail failed.');
            return response()->json('send mail failed', 400);
        }
    }
}
