<?php


namespace App\Services;


use App\EmailCampaign;
use App\Order;
use App\Subscription;
use App\User;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\Log;

class SubscribeUsersToEmailCampaign
{

    public function call($id)
    {
        $emailCampaign = EmailCampaign::find($id);
        $orders = Order::latest()
            ->completed()
            ->whereDate('created_at', '<=', '2020-06-24' . ' 15:08:00')
            ->with('user')
            ->get('user_id');
        //        dd($emailCampaign->users());

        $usersWithCompletedOrders = User::whereHas('orders', function ($query) {
            $query->completed();
            $query->rub();
            // $query->whereDate('created_at', '<=', '2020-06-24' . ' 15:08:00');
        })->get();

        //        $usersWithCompletedOrders = User::join('orders', 'users.id', '=', 'orders.user_id')
        //            ->where('orders.status', 'completed')
        //            ->whereDate('orders.created_at', '<=', '2020-06-24' . ' 15:08:00')
        //            ->select('users.id')->distinct()->get();


        foreach ($usersWithCompletedOrders as $user) {
            try {
                //                dd($emailCampaign->users());
                //                $emailCampaign->users()->attach($user->id);
                Subscription::create([
                    'email_campaign_id' => $emailCampaign->id,
                    'user_id'           => $user->id,
                ]);
            } catch (QueryException $e) {
                Log::info($e);
            }
        }
    }
}
