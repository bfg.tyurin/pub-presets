<?php


namespace App\Services;


use App\Downloadable;
use Illuminate\Support\Str;

class CreateDownloadable
{
    /**
     * @param string $id
     * @param string $order_id
     * @return Downloadable
     */
    public function call(string $user_id, string $order_id)
    {
        $downloadable = new Downloadable();
        $downloadable->user_id = $user_id;
        $downloadable->order_id = $order_id;
        $downloadable->token = Str::random(64);
        $downloadable->save();
        return $downloadable;
    }
}
