<?php


namespace App\Services;


use App\Order;

class AdminTotalsService
{

    private $ordersQuery;

    public function __construct($currency = Order::CURRENCY_RUB, $campaignUsersOnly = false)
    {
        $this->currency = $currency;

        $this->ordersQuery = Order::completed()
            ->whereHas('user', function ($query) {
                return $query->whereNotIn('email', Order::freeEmails());
            });

        $this->ordersQuery->when($this->currency === Order::CURRENCY_RUB, function ($q) {
            return $q->where(function ($q) {
                $q->where('currency', 'RUB')->orWhereNull('currency');
            });
        });

        $this->ordersQuery->when($this->currency === Order::CURRENCY_USD, function ($q) {
            return $q->where('currency', $this->currency);
        });

        $this->ordersQuery->when($campaignUsersOnly === true, function ($q) {
            return $q->whereHas('user', function ($query) {
                return $query->whereNotNull('campaign_id');
            });
        });

        $this->ordersQuery->when($campaignUsersOnly === false, function ($q) {
            return $q->whereHas('user', function ($query) {
                return $query->whereNull('campaign_id');
            });
        });
    }

    public function getCount()
    {
        return $this->ordersQuery->count();
    }

    public function getAvgAmount()
    {
        $total = $this->ordersQuery->avg('total');

        return number_format($total, 2, '.', '');
    }

    public function getTotalAmount()
    {
        $total = $this->ordersQuery->sum('total');

        return number_format($total, 2, '.', '');
    }

    public function getTotalAmountWithTax()
    {
        return number_format($this->getTotalAmount() * 0.878, 2, '.', '');
    }
}
