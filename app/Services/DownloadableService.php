<?php


namespace App\Services;


use App\Downloadable;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;

class DownloadableService
{
    /**
     * @param string $id
     * @param string $product_id
     * @return Downloadable
     */
    public function createWithUserIdProductId(string $id, string $product_id)
    {
        $downloadable = new Downloadable();
        $downloadable->user_id = $id;
        $downloadable->product_id = $product_id;
        $downloadable->token = Str::random(64);
        $downloadable->save();
        return $downloadable;
    }

    public function getURl($downloadable) : String
    {
        $urlWithLocale = LaravelLocalization::getLocalizedURL(LaravelLocalization::getCurrentLocale(), route('products.download', $downloadable->token));
        Log::info(App::getLocale());
        Log::info($urlWithLocale);
        return $urlWithLocale;
    }
}
