<?php


namespace App\Services\Dto;


class CreatePromotionDto
{
    private string $userId;
    private string $productId;
    private int $days;

    public function __construct(string $userId, string $productId, int $days)
    {
        $this->userId = $userId;
        $this->productId = $productId;
        $this->days = $days;
    }

    /**
     * @return string
     */
    public function getUserId(): string
    {
        return $this->userId;
    }

    /**
     * @return string
     */
    public function getProductId(): string
    {
        return $this->productId;
    }

    /**
     * @return int
     */
    public function getDays(): int
    {
        return $this->days;
    }

}
