<?php


namespace App\Services\Dto;


class EmailCampaignSendDto
{
    /**
     * @var string
     */
    private string $userId;
    /**
     * @var string
     */
    private string $productId;
    /**
     * @var string
     */
    private string $title;
    /**
     * @var string
     */
    private string $userEmail;
    /**
     * @var string
     */
    private string $productType;

    /**
     * EmailCampaignSendDto constructor.
     * @param string $userId
     * @param string $userEmail
     * @param string $productId
     * @param string $productType
     * @param string $title
     */
    public function __construct(string $userId, string $userEmail, string $productId, string $productType, string $title)
    {
        $this->userId = $userId;
        $this->userEmail = $userEmail;
        $this->productId = $productId;
        $this->productType = $productType;
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getUserId(): string
    {
        return $this->userId;
    }

    /**
     * @return string
     */
    public function getProductId(): string
    {
        return $this->productId;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @return string
     */
    public function getUserEmail(): string
    {
        return $this->userEmail;
    }

    /**
     * @return string
     */
    public function getProductType(): string
    {
        return $this->productType;
    }

}
