<?php


namespace App\Services\Dto;


class CreateDiscountDto
{
    /**
     * @var string
     */
    private string $userEmail;
    /**
     * @var string
     */
    private string $productId;
    /**
     * @var int
     */
    private int $rubPrice;
    /**
     * @var int
     */
    private int $usdPrice;

    /**
     * CreateDiscountDto constructor.
     * @param string $userEmail
     * @param string $productId
     * @param int $rubPrice
     * @param int $usdPrice
     */
    public function __construct(string $userEmail, string $productId, int $rubPrice, int $usdPrice)
    {
        $this->userEmail = $userEmail;
        $this->productId = $productId;
        $this->rubPrice = $rubPrice;
        $this->usdPrice = $usdPrice;
    }

    /**
     * @return string
     */
    public function getUserEmail(): string
    {
        return $this->userEmail;
    }

    /**
     * @return string
     */
    public function getProductId(): string
    {
        return $this->productId;
    }

    /**
     * @return int
     */
    public function getRubPrice(): int
    {
        return $this->rubPrice;
    }

    /**
     * @return int
     */
    public function getUsdPrice(): int
    {
        return $this->usdPrice;
    }

}
