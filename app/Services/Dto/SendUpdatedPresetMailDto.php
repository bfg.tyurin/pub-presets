<?php


namespace App\Services\Dto;


class SendUpdatedPresetMailDto
{
    private string $userEmail;
    private string $title;
    private string $downloadableUrl;
    private string $productType;

    /**
     * UpdatePresetsMailDto constructor.
     * @param string $userEmail
     * @param string $title
     * @param string $downloadableUrl
     * @param string $productType
     */
    public function __construct(string $userEmail, string $title, string $downloadableUrl, string $productType)
    {
        $this->userEmail = $userEmail;
        $this->title = $title;
        $this->downloadableUrl = $downloadableUrl;
        $this->productType = $productType;
    }

    /**
     * @return string
     */
    public function getUserEmail(): string
    {
        return $this->userEmail;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @return string
     */
    public function getDownloadableUrl(): string
    {
        return $this->downloadableUrl;
    }

    /**
     * @return string
     */
    public function getProductType(): string
    {
        return $this->productType;
    }
}
