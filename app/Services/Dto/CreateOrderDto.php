<?php


namespace App\Services\Dto;


use App\Product;
use App\User;

final class CreateOrderDto
{
    /**
     * @var User
     */
    private User $user;
    /**
     * @var Product
     */
    private Product $product;
    /**
     * @var string
     */
    private string $paymentType;
    /**
     * @var string
     */
    private string $locale;

    /**
     * CreateOrderDto constructor.
     * @param User $user
     * @param Product $product
     * @param string $paymentType
     * @param string $locale
     */
    public function __construct(User $user, Product $product, string $paymentType, string $locale)
    {
        $this->user = $user;
        $this->product = $product;
        $this->paymentType = $paymentType;
        $this->locale = $locale;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @return mixed
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * @return mixed
     */
    public function getPaymentType()
    {
        return $this->paymentType;
    }

    /**
     * @return string
     */
    public function getLocale(): string
    {
        return $this->locale;
    }
}
