<?php


namespace App\Services\Dto;


class OrderStoreDto
{
    /**
     * @var string
     */
    private string $userEmail;
    /**
     * @var string
     */
    private string $productId;
    /**
     * @var string
     */
    private string $paymentType;
    /**
     * @var
     */
    private $utmData;

    /**
     * OrderStoreDto constructor.
     * @param $userEmail
     * @param $productId
     * @param $paymentType
     * @param $utmData
     */
    public function __construct(string $userEmail, string $paymentType, $utmData)
    {
        $this->userEmail = $userEmail;
        // $this->productId = $productId;
        $this->paymentType = $paymentType;
        $this->utmData = $utmData;
    }

    /**
     * @return string
     */
    public function getUserEmail(): string
    {
        return $this->userEmail;
    }

    /**
     * @return string
     */
    public function getProductId(): string
    {
        return $this->productId;
    }

    /**
     * @return string
     */
    public function getPaymentType(): string
    {
        return $this->paymentType;
    }

    /**
     * @return mixed
     */
    public function getUtmData()
    {
        return $this->utmData;
    }
}
