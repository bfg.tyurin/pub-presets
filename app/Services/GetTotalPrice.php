<?php


namespace App\Services;


use App\Product;
use App\User;

class GetTotalPrice
{

    /**
     * GetTotalPrice constructor.
     */
    public function __construct()
    {
    }

    /**
     * @param User $user
     * @param Product $product
     * @param string $locale
     * @return int
     */
    public function call(User $user, Product $product, string $locale)
    {
        $discount = $user->discountByProductId($product->id);

        switch ($locale)
        {
            case 'ru':
                return $discount ? $discount->rub_price : $product->price_discount;
                break;
            default:
                return $discount ? $discount->usd_price : $product->usd_price_discount;
                break;
        }

    }
}
