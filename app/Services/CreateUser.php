<?php


namespace App\Services;


use App\Campaign;
use App\User;
use Illuminate\Support\Facades\Log;

class CreateUser
{
    /**
     * @var CreateCampaign
     */
    private CreateCampaign $createCampaign;

    /**
     * CreateUser constructor.
     * @param CreateCampaign $createCampaign
     */
    public function __construct(CreateCampaign $createCampaign)
    {
        $this->createCampaign = $createCampaign;
    }

    public function call(string $email, $utmData = null)
    {
        $user =  User::firstOrCreate([
            'email' => $email
        ]);

        if ($utmData) {
            $campaign = $this->createCampaign->call($utmData);
            $user->campaign()->associate($campaign);
            $user->save();

            Log::info('Campaign created: ', ['campaign', $campaign]);
        }

        return $user;
    }
}
