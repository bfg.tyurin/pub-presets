<?php


namespace App\Services;


use App\Product;
use App\Promotion;
use App\Services\Dto\CreatePromotionDto;
use Illuminate\Support\Carbon;

class CreatePromotion
{
    public function call(CreatePromotionDto $dto)
    {
        $product = Product::findOrFail($dto->getProductId());

        if ($product->related_promotions()->exists()) {
            $promo = new Promotion();
            $promo->fill([
                'user_id'    => $dto->getUserId(),
                'product_id' => $product->related_promotions->first()->id,
                'available_until' => Carbon::now()->addDays($dto->getDays()),
                // 'available_until' => Carbon::parse('+' . $dto->getDays() . ' days'),
            ]);
            $promo->save();
            return $promo;
        }
    }
}
