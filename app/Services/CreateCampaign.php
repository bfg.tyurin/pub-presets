<?php


namespace App\Services;


use App\Campaign;
use Illuminate\Support\Facades\Log;

class CreateCampaign
{
    public function call($utmData)
    {
        Log::info('Cookie found', ['utm_data' => $utmData]);
        $json = json_decode($utmData);
        return Campaign::create([
            'utm_source'   => property_exists($json, 'utm_source') ? $json->utm_source : '',
            'utm_campaign' => property_exists($json, 'utm_campaign') ? $json->utm_campaign : '',
            'utm_medium'   => property_exists($json, 'utm_medium') ? $json->utm_medium : '',
            'utm_term'     => property_exists($json, 'utm_term') ? $json->utm_term : '',
            'utm_content'  => property_exists($json, 'utm_content') ? $json->utm_content : '',
        ]);
    }
}
