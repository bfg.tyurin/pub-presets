<?php


namespace App\Services;


use Mcamara\LaravelLocalization\Facades\LaravelLocalization;

class CreateDownloadableUrlWithToken
{

    /**
     * @param string $token
     * @return String
     */
    public function call(string $token) : String
    {
        return LaravelLocalization::getNonLocalizedURL(route('products.download', $token));
    }
}
