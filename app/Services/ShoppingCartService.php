<?php


namespace App\Services;

use App\Pack;
use Illuminate\Support\Facades\Log;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;

class ShoppingCartService
{
    private $discountPercent;
    private $nextDiscountPercent;
    private $fullPrice = 0;

    public function __construct()
    {

        // Reload cart when lang was changed
        // TO-DO move to another approprieate place
        $currentLocale = LaravelLocalization::getCurrentLocale();
        if (!\Cart::isEmpty()) {
            if ('ru' === $currentLocale) {
                $items = \Cart::getContent();
                foreach ($items as $row) {
                    if ($row->associatedModel) {
                        $priceDiscount = $row->associatedModel->price_discount;
                        $this->fullPrice += $row->associatedModel->price_original;
                        \Cart::update($row->id, [
                            'price' => $priceDiscount,
                        ]);
                    }
                }
            } else {
                $items = \Cart::getContent();
                foreach ($items as $row) {
                    $priceDiscount = $row->associatedModel->usd_price_discount;
                    $this->fullPrice += $row->associatedModel->usd_price_original;
                    \Cart::update($row->id, [
                        'price' => $priceDiscount,
                    ]);
                }
            }
        }

        // $discountPercent = 0;
        // $nextDiscountPercent = 15;
        // switch (\Cart::getContent()->count()) {
        //     case 1:
        //         $nextDiscountPercent = 15;
        //         break;
        //     case 2:
        //         $discountPercent = 15;
        //         $nextDiscountPercent = 20;
        //         break;
        //     case 3:
        //         $discountPercent = 20;
        //         $nextDiscountPercent = 25;
        //         break;
        //     case 4:
        //         $discountPercent = 25;
        //         $nextDiscountPercent = 30;
        //         break;
        //     case 5:
        //         $discountPercent = 30;
        //         $nextDiscountPercent = 35;
        //         break;
        //     case 6:
        //         $discountPercent = 35;
        //         $nextDiscountPercent = 40;
        //         break;
        //     case 7:
        //         $discountPercent = 40;
        //         $nextDiscountPercent = 40;
        //         break;
        // }

        // $this->discountPercent = $discountPercent;
        // $this->nextDiscountPercent = $nextDiscountPercent;

        $this->clearCartConditions();
        $this->addConditions();
    }

    public function packs()
    {
        return Pack::oldest()
            ->with(['products' => function ($query) {
                $query->where('type', 'pack');
            }])->get();
    }

    public function images()
    {
        return [
            'golden-caramel'   => 'images/basket/gm.jpg',
            'metallic-blues'   => 'images/basket/mb.jpg',
            'wedding-pureness' => 'images/basket/wp.jpg',
            'spring-burst'     => 'images/basket/sb.jpg',
            'safari-style'     => 'images/basket/ss.jpg',
        ];
    }

    public function getDiscountPercent()
    {
        return $this->discountPercent;
    }

    public function getNextDiscountPercent()
    {
        return $this->nextDiscountPercent;
    }

    public function discountPriceMultiplier()
    {
        return (100 - $this->discountPercent) / 100;
    }

    public function nextDiscountPriceMultiplier()
    {
        return (100 - $this->nextDiscountPercent) / 100;
    }

    public function discountInCurrency()
    {
        return \Cart::getContent()->count() > 1 ? \Cart::getSubTotal() - \Cart::getTotal() : 0;
    }

    public function getDiscountValue()
    {
        return \Cart::getContent()->count() > 1 ? \Cart::getSubTotal() - \Cart::getTotal() : 0;
    }

    public function getFullPrice()
    {
        $this->fullPrice = 0;
        $currentLocale = LaravelLocalization::getCurrentLocale();
        if (!\Cart::isEmpty()) {
            if ('ru' === $currentLocale) {
                $items = \Cart::getContent();
                foreach ($items as $row) {
                    if ($row->associatedModel) {
                        $this->fullPrice += $row->associatedModel->price_original;
                    }
                }
            } else {
                $items = \Cart::getContent();
                foreach ($items as $row) {
                    $this->fullPrice += $row->associatedModel->usd_price_original;
                }
            }
        }
        return $this->fullPrice;
    }

    public function getSaving()
    {
        return $this->fullPrice - intval(\Cart::getTotal());
    }

    public function add($product)
    {
        if (!\Cart::get($product->id)) {
            Log::info('Product not in a cart - adding', ['product title' => $product->title]);

            \Cart::add(
                $product->id,
                $product->title,
                LaravelLocalization::getCurrentLocale() === 'ru' ? $product->price_discount : $product->usd_price_discount,
                1,
                []
            )->associate($product);
            Log::info('Cart content:', ['content' => \Cart::getContent()]);
        }

        $this->clearCartConditions();
        $this->addConditions();
    }

    public function remove($product)
    {
        if (\Cart::get($product->id)) {
            \Cart::remove($product->id);
        }

        $this->clearCartConditions();
        $this->addConditions();
    }

    public function getContentJson()
    {
        return \Cart::getContent()->toJson();
    }

    public function getContentArray()
    {
        return \Cart::getContent()->toArray();
    }

    public function getTotal()
    {
        return intval(\Cart::getTotal());
    }

    public function getSubTotal()
    {
        return intval(\Cart::getSubTotal());
    }

    public function getContentCount()
    {
        return \Cart::getContent()->count();
    }

    public function clearCartConditions()
    {
        \Cart::clearCartConditions();
    }

    public function addConditions()
    {
        $this->discountPercent = 0;
        $this->nextDiscountPercent = 0;
        switch (\Cart::getContent()->count()) {
            case 1:
                $this->nextDiscountPercent = 15;
                break;
            case 2:
                $saleCondition = new \Darryldecode\Cart\CartCondition(array(
                    'name' => 'SALE 15%',
                    'type' => 'tax',
                    'target' => 'total',
                    'value' => '-15%',
                ));
                $this->discountPercent = 15;
                $this->nextDiscountPercent = 20;
                \Cart::condition($saleCondition);
                break;
            case 3:
                $saleCondition = new \Darryldecode\Cart\CartCondition(array(
                    'name' => 'SALE 20%',
                    'type' => 'tax',
                    'target' => 'total',
                    'value' => '-20%',
                ));
                $this->discountPercent = 20;
                $this->nextDiscountPercent = 25;
                \Cart::condition($saleCondition);
                break;
            case 4:
                $saleCondition = new \Darryldecode\Cart\CartCondition(array(
                    'name' => 'SALE 25%',
                    'type' => 'tax',
                    'target' => 'total',
                    'value' => '-25%',
                ));
                \Cart::condition($saleCondition);
                $this->discountPercent = 25;
                $this->nextDiscountPercent = 30;
                break;
            case 5:
                $saleCondition = new \Darryldecode\Cart\CartCondition(array(
                    'name' => 'SALE 30%',
                    'type' => 'tax',
                    'target' => 'total',
                    'value' => '-30%',
                ));
                \Cart::condition($saleCondition);
                $this->discountPercent = 30;
                $this->nextDiscountPercent = 35;
                break;
            case 6:
                $saleCondition = new \Darryldecode\Cart\CartCondition(array(
                    'name' => 'SALE 35%',
                    'type' => 'tax',
                    'target' => 'total',
                    'value' => '-35%',
                ));
                \Cart::condition($saleCondition);
                $this->discountPercent = 35;
                $this->nextDiscountPercent = 40;
                break;
            case 7:
                $saleCondition = new \Darryldecode\Cart\CartCondition(array(
                    'name' => 'SALE 40%',
                    'type' => 'tax',
                    'target' => 'total',
                    'value' => '-40%',
                ));
                \Cart::condition($saleCondition);
                $this->discountPercent = 40;
                $this->nextDiscountPercent = 40;
                break;
        }
    }
}
