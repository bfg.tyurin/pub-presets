<?php


namespace App\Services;

use App\Order;
use Illuminate\Support\Facades\Log;

class SetupPaypalData
{

    public function call(Order $order)
    {
        $name = $order->products->map(function ($item, $key) {
            return $item->title;
        })->join(', ');
        Log::info('name: ' . $name);

        $total = in_array($order->user->email, Order::freeEmails()) ? 1 : $order->total;

        $data = [];
        $data['items'] = [
            [
                'name'  => $name,
                'price' => $total,
                'desc'  => "Покупка курса {$name}",
                'qty'   => 1,
            ],
        ];

        $data['invoice_id'] = $order->id;
        $data['invoice_description'] = "Order #{$order->id}";
        $data['cancel_url'] = route('paypal.payment.cancel');
        $data['return_url'] = route('paypal.payment.success');

        $data['total'] = $order->total;

        return $data;
    }
}
