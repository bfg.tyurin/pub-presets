<?php


namespace App\Services;


use App\Discount;
use App\Services\Dto\CreateDiscountDto;
use App\User;

class CreateDiscount
{
    public function call(CreateDiscountDto $dto)
    {
        Discount::create([
            'user_id'    => User::firstOrCreate(['email' => $dto->getUserEmail()])->id,
            'product_id' => $dto->getProductId(),
            'rub_price'  => $dto->getRubPrice(),
            'usd_price'  => $dto->getUsdPrice(),
        ]);
    }
}
