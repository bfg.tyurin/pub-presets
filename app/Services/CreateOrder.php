<?php


namespace App\Services;


use App\Order;
use App\Services\Dto\CreateOrderDto;

final class CreateOrder
{
    /**
     * @var GetTotalPrice
     */
    private GetTotalPrice $getTotalPrice;

    /**
     * CreateOrder constructor.
     * @param GetTotalPrice $getTotalPrice
     */
    public function __construct(GetTotalPrice $getTotalPrice)
    {
        $this->getTotalPrice = $getTotalPrice;
    }

    /**
     * @param CreateOrderDto $dto
     */
    public function call(CreateOrderDto $dto)
    {
        $total = $this->getTotalPrice->call($dto->getUser(), $dto->getProduct(), $dto->getLocale());
        return Order::create([
            'user_id'      => $dto->getUser()->id,
            'product_id'   => $dto->getProduct()->id,
            'status'       => Order::STATUS_PROCESSING,
            'total'        => $total,
            'payment_type' => $dto->getPaymentType(),
            'currency'     => $dto->getLocale() === 'ru' ? Order::CURRENCY_RUB : Order::CURRENCY_USD,
        ]);
    }
}
