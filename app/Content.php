<?php


namespace App;


final class Content
{
    /** @var string */
    private string $title;

    /** @var string */
    private string $body;

    /**
     * Content constructor.
     * @param string $title
     * @param string $body
     */
    public function __construct(string $title, string $body)
    {
        $this->title = $title;
        $this->body = $body;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @return string
     */
    public function getBody(): string
    {
        return $this->body;
    }

}
