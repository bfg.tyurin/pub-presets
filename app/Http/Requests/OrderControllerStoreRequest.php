<?php

namespace App\Http\Requests;

use App\Order;
use App\Services\Dto\OrderStoreDto;
use Illuminate\Foundation\Http\FormRequest;

class OrderControllerStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'user_email'   => 'required|email',
            // 'product_id'   => 'required|exists:products,id',
            'payment_type' => 'required|in:' . Order::PAYMENT_TYPE_CLOUDPAYMENTS . ',' . Order::PAYMENT_TYPE_PAYPAL,
        ];
    }

    public function getDto()
    {
        return new OrderStoreDto(
            $this->get('user_email'),
            // $this->get('product_id'),
            $this->get('payment_type'),
            $this->cookie('utm_data'),
        );
    }
}
