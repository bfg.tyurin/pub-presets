<?php

namespace App\Http\Requests;

use App\Services\Dto\EmailCampaignSendDto;
use Illuminate\Foundation\Http\FormRequest;

class EmailCampaignSendRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'user_id'      => 'required|max:255|exists:users,id',
            'user_email'   => 'required|max:255|exists:users,email',
            'product_id'   => 'required|max:255|exists:products,id',
            'product_type' => 'required|max:255|exists:products,type',
            'title'        => 'required|max:255|exists:email_campaigns,title',
        ];
    }

    /**
     * @return EmailCampaignSendDto
     */
    public function getDto(): EmailCampaignSendDto
    {
        return new EmailCampaignSendDto(
            $this->get('user_id'),
            $this->get('user_email'),
            $this->get('product_id'),
            $this->get('product_type'),
            $this->get('title'),
        );
    }
}
