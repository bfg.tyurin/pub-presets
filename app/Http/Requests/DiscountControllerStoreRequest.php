<?php

namespace App\Http\Requests;

use App\Services\Dto\CreateDiscountDto;
use Illuminate\Foundation\Http\FormRequest;

class DiscountControllerStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'user_email' => 'required|email',
            'product_id' => 'required|max:255|exists:products,id',
            'rub_price'  => 'required|integer|min:1',
            'usd_price'  => 'required|integer|min:1',
        ];
    }

    /**
     * @return CreateDiscountDto
     */
    public function getDto() : CreateDiscountDto
    {
        return new CreateDiscountDto(
            $this->get('user_email'),
            $this->get('product_id'),
            $this->get('rub_price'),
            $this->get('usd_price'),
        );
    }
}
