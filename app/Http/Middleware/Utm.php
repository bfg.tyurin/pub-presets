<?php


namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Log;

class Utm
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $contains = Arr::hasAny(
            $request->all(),
            [
                'utm_source',
                'utm_campaign',
                'utm_medium',
                'utm_term',
                'utm_content',
            ]
        );

        if ($contains) {
            Cookie::queue(Cookie::make('utm_data', json_encode($request->all()), 60 * 24 * 360));
        }

        $containsEmail = Arr::hasAny(
            $request->all(),
            [
                'email',
            ]
        );


        if ($containsEmail) {
            Cookie::queue(Cookie::make('email', $request->input('email'), 60 * 24 * 360));
        }

        return $next($request);
    }
}
