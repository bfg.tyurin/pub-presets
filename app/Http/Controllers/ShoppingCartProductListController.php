<?php

namespace App\Http\Controllers;

use App\Product;
use App\Services\ShoppingCartService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;

class ShoppingCartProductListController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, ShoppingCartService $shoppingCartService)
    {
        $ids = $request->product_ids;
        Log::info($ids);
        foreach ($ids as $key => $value) {
            $product = Product::find($value['id']);
            if ($product) {
                // if (!\Cart::get($product->id)) {
                //     Log::info('Product not in a cart - adding', ['product title' => $product->title]);

                //     \Cart::add(
                //         $product->id,
                //         $product->title,
                //         LaravelLocalization::getCurrentLocale() === 'ru' ? $product->price_discount : $product->usd_price_discount,
                //         1,
                //         []
                //     )->associate($product);
                // }
                $shoppingCartService->add($product);
            }
        }


        // \Cart::clearCartConditions();

        $discountPercent = $shoppingCartService->getDiscountPercent();
        $nextDiscountPercent = $shoppingCartService->getNextDiscountPercent();
        // switch (\Cart::getContent()->count()) {
        //     case 1:
        //         $nextDiscountPercent = 15;
        //         break;
        //     case 2:
        //         $saleCondition = new \Darryldecode\Cart\CartCondition(array(
        //             'name' => 'SALE 15%',
        //             'type' => 'tax',
        //             'target' => 'total',
        //             'value' => '-15%',
        //         ));
        //         $discountPercent = 15;
        //         $nextDiscountPercent = 20;
        //         \Cart::condition($saleCondition);
        //         break;
        //     case 3:
        //         $saleCondition = new \Darryldecode\Cart\CartCondition(array(
        //             'name' => 'SALE 20%',
        //             'type' => 'tax',
        //             'target' => 'total',
        //             'value' => '-20%',
        //         ));
        //         $discountPercent = 20;
        //         $nextDiscountPercent = 25;
        //         \Cart::condition($saleCondition);
        //         break;
        //     case 4:
        //         $saleCondition = new \Darryldecode\Cart\CartCondition(array(
        //             'name' => 'SALE 25%',
        //             'type' => 'tax',
        //             'target' => 'total',
        //             'value' => '-25%',
        //         ));
        //         \Cart::condition($saleCondition);
        //         $discountPercent = 25;
        //         $nextDiscountPercent = 30;
        //         break;
        //     case 5:
        //         $saleCondition = new \Darryldecode\Cart\CartCondition(array(
        //             'name' => 'SALE 30%',
        //             'type' => 'tax',
        //             'target' => 'total',
        //             'value' => '-30%',
        //         ));
        //         \Cart::condition($saleCondition);
        //         $discountPercent = 30;
        //         $nextDiscountPercent = 35;
        //         break;
        //     case 6:
        //         $saleCondition = new \Darryldecode\Cart\CartCondition(array(
        //             'name' => 'SALE 35%',
        //             'type' => 'tax',
        //             'target' => 'total',
        //             'value' => '-35%',
        //         ));
        //         \Cart::condition($saleCondition);
        //         $discountPercent = 35;
        //         $nextDiscountPercent = 40;
        //         break;
        //     case 7:
        //         $saleCondition = new \Darryldecode\Cart\CartCondition(array(
        //             'name' => 'SALE 40%',
        //             'type' => 'tax',
        //             'target' => 'total',
        //             'value' => '-40%',
        //         ));
        //         \Cart::condition($saleCondition);
        //         $discountPercent = 40;
        //         $nextDiscountPercent = 40;
        //         break;
        // }

        $discountValue = $shoppingCartService->discountInCurrency();

        return response([
            'cart_content' => $shoppingCartService->getContentArray(),
            'cart_total' => $shoppingCartService->getTotal(),
            'cart_sub_total' => $shoppingCartService->getSubTotal(),
            'cart_count' => $shoppingCartService->getContentCount(),
            'cart_discount_value' => $discountValue,
            'cart_discount_percent' => $discountPercent,
            'cart_next_discount_percent' => $nextDiscountPercent,
            'cart_full_price' => $shoppingCartService->getFullPrice(),
            'cart_saving' => $shoppingCartService->getSaving(),
            'buy_button_text' => __('basket.buy_button_text_remove'),
            'basket_collections_choosen_string' => trans_choice('basket.collections_choosen', $shoppingCartService->getContentCount()),
        ]);
    }
}
