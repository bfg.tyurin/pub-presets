<?php

namespace App\Http\Controllers;

use App\Pack;
use App\User;
use App\Order;
use App\Product;
use App\Promotion;
use App\Downloadable;
use App\GlobalString;
use Jenssegers\Agent\Agent;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use App\Services\GetTotalPrice;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;

class ProductController extends Controller
{
    public function show(Request $request, Pack $pack)
    {
        // session(['product_id' => $product->id]);
        // $products = Product::where('id', '<>', $product->id)->where('type', 'pack')->paginate(5);
        // $bundle = Product::firstWhere('type', 'bundle');

        $pack = Pack::with(['products' => function ($query) {
            $query->where('type', 'pack');
        }])->find($pack->id);

        $packSaleStr = GlobalString::firstWhere('key', 'pack_sale');
        $bundleSaleStr = GlobalString::firstWhere('key', 'bundle_sale');


        // Shopping Cart logic
        // TO-DO
        $desktopProductIds = Product::desktop()->get('id')->map(function ($item) {
            return $item->id;
        });
        $cartItemIds = \Cart::getContent()->map(function ($item) {
            return $item->id;
        });
        $intersection = $desktopProductIds->intersect($cartItemIds)->all();
        $isAllDesktopProductInCart = count($intersection) === count($desktopProductIds);

        $desktopProductIdsJson = Product::desktop()->get('id')->toJson();

        $currentShoppingCartMultiplier = 0.80;

        $allDesktopRubPriceOriginal = Product::desktop()->sum('price_original');
        $allDesktopRubPriceDiscount = Product::desktop()->sum('price_discount') * $currentShoppingCartMultiplier;
        $allDesktopUsdPriceOriginal = Product::desktop()->sum('usd_price_original');
        $allDesktopUsdPriceDiscount = Product::desktop()->sum('usd_price_discount') * $currentShoppingCartMultiplier;

        $desktopProductsCount = Product::where('category', 'desktop')->count();

        return view(
            'products.show',
            [
                'pack'                       => $pack,
                'packSaleStr'                => $packSaleStr,
                'bundleSaleStr'              => $bundleSaleStr,
                'allDesktopRubPriceOriginal' => $allDesktopRubPriceOriginal,
                'allDesktopRubPriceDiscount' => $allDesktopRubPriceDiscount,
                'allDesktopUsdPriceOriginal' => $allDesktopUsdPriceOriginal,
                'allDesktopUsdPriceDiscount' => $allDesktopUsdPriceDiscount,
                'desktopProductIdsJson'      => $desktopProductIdsJson,
                'isAllDesktopProductInCart'  => $isAllDesktopProductInCart,
                'desktopProductsCount'       => $desktopProductsCount,
            ],
        );
    }

    /**
     * @param $token
     * @param GetTotalPrice $getTotalPrice
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function download($token, GetTotalPrice $getTotalPrice)
    {
        session(['promotion_url' => url()->current()]);

        $downloadable = Downloadable::validFromToken($token);
        if (!$downloadable) {

            Log::info('No downloadable for this token, redirecting');

            return redirect('/');
        }

        // Проверка заказа, редирект если нет ниодного заказ с таким мылом, продуктом и статусом "completed"
        // то редирект на главную
        $order = Order::where('user_id', $downloadable->user_id)
            ->where('product_id', $downloadable->product_id)
            ->where('status', 'completed')->first();
        if (!$order) {
            Log::info('No Orders for this token, redirecting', ['user_id', $downloadable->user_id]);
            return redirect('/');
        }

        // Если нет профиля редирект
        $user = User::firstWhere('id', $downloadable->user_id);
        if (!$user->profile) {

            Log::info('No Profile for this token, redirecting to profile.create', ['user_id', $downloadable->user_id]);
            Log::info('User email saved in Session', ['user_email', $user->email]);
            session(['email' => $user->email]);

            return redirect(route('profile.create'));
        }

        $product = Product::firstWhere('id', $downloadable->product_id);
        $promotion = [];
        $promoDuration = 0;
        $promotionTotalPrice = 0;
        if ($product) {
            if ($product->type === 'pack') {

                Log::info('Product type is "Pack", getting Promotion', ['user_id', $downloadable->user_id]);

                $promotion = Promotion::where('user_id', $downloadable->user_id)
                    ->where('product_id', $product->related_promotions->first()->id)
                    ->where('available_until', '>=', Carbon::now())
                    ->latest()->first();
                if ($promotion) {
                    $promoDuration = ceil(Carbon::now()->diffInMinutes(Carbon::parse($promotion->available_until), true) / 60);
                    $promotionTotalPrice = $getTotalPrice->call($user, $promotion->product, App::getLocale());
                }
            }
        }


        // Cart add
        // TODO!!!
        session(['user_id' => $user->id]);
        if ($promotion) {
            $cart = [
                'product_id' => $promotion->product->id,
                'quantity'   => 1,
                //                'total'      => $promotion->product->price_discount,
                'total'      => $promotionTotalPrice,
                'title'      => $promotion->product->title,

            ];

            switch (LaravelLocalization::getCurrentLocale()) {
                case ('ru'):
                    $cart['currency'] = "RUB";
                    $cart['total'] = in_array($user->email, Order::freeEmails()) ? 1 : $promotion->product->price_discount;
                    break;
                case ('en'):
                    $cart['currency'] = "USD";
                    $cart['total'] = in_array($user->email, Order::freeEmails()) ? 0.01 : $promotion->product->usd_price_discount;
                    break;
                default:
                    $cart['currency'] = "RUB";
                    $cart['total'] = in_array($user->email, Order::freeEmails()) ? 1 : $promotion->product->price_discount;
                    break;
            }

            session(['cart' => $cart]);
        }

        return view(
            'products.download',
            [
                'userEmail'    => $user->email,
                'product'    => $product,
                'products'    => is_null($downloadable->order) ? null : $downloadable->order->products,
                'promotion'  => $promotion,
                'promoDuration' => $promoDuration,
                'promotionTotalPrice' => $promotionTotalPrice,
                'downloadable' => $downloadable,
                'desktopProducts' => is_null($downloadable->order) ? null : $downloadable->order->products()->where('category', 'desktop')->get(),
                'mobileProducts'  => is_null($downloadable->order) ? null : $downloadable->order->products()->where('category', 'mobile')->get(),
            ]
        );
    }

    public function filedownload()
    {
        //TODO zipfile
        return Storage::download('public/111wedding-pureness.zip', 'yoyo');
    }

    public function pm(Request $request)
    {
        $user = null;
        $profile = null;
        if ($request->session()->has('email')) {
            $email = session()->get('email');
            $user = User::firstWhere('email', $email);
            if ($user->profile) {
                $profile = $user->profile;
            }
        }

        $agent = new Agent;
        $mobileHowToVideoUrl = '';
        if ($agent->isMobile()) {
            $mobileHowToVideoUrl = url(Storage::url('IMG_7731.MOV'));
        }

        return view(
            'products.pm',
            [
                'user_email'       => $user ? $user->email : '',
                'user_instagram'   => $profile ? $user->profile->instagram : '',
                'user_phone'       => $profile ? $user->profile->number : '',
                'mobileHowToVideoUrl' => $mobileHowToVideoUrl
            ]
        );
    }

    public function spring_burst()
    {
        $currentShoppingCartMultiplier = 0.75;

        $pack = Pack::with(['products' => function ($query) {
            $query->where('type', 'pack');
        }])->firstWhere('slug', 'spring-burst');

        $allMobileRubPriceOriginal = Product::where('category', 'mobile')->sum('price_original');
        $allMobileRubPriceDiscount = Product::where('category', 'mobile')->sum('price_discount') * $currentShoppingCartMultiplier;
        $allMobileUsdPriceOriginal = Product::where('category', 'mobile')->sum('usd_price_original');
        $allMobileUsdPriceDiscount = Product::where('category', 'mobile')->sum('usd_price_discount') * $currentShoppingCartMultiplier;

        $mobileProductIds = Product::mobile()->get('id')->map(function ($item) {
            return $item->id;
        });
        $cartItemIds = \Cart::getContent()->map(function ($item) {
            return $item->id;
        });
        $intersection = $mobileProductIds->intersect($cartItemIds)->all();
        $isAllMobileProductInCart = count($intersection) === count($mobileProductIds);

        $mobileProductIdsJson = Product::mobile()->get('id')->toJson();

        $mobileProductsCount = Product::where('category', 'mobile')->count();

        return view('products.spring_burst', compact(
            [
                'pack',
                'allMobileRubPriceOriginal',
                'allMobileRubPriceDiscount',
                'allMobileUsdPriceOriginal',
                'allMobileUsdPriceDiscount',
                'mobileProductIdsJson',
                'isAllMobileProductInCart',
                'mobileProductsCount',
            ]
        ));
    }

    public function safari_style()
    {
        $currentShoppingCartMultiplier = 0.75;

        $pack = Pack::with(['products' => function ($query) {
            $query->where('type', 'pack');
        }])->firstWhere('slug', 'safari-style');

        $allMobileRubPriceOriginal = Product::where('category', 'mobile')->sum('price_original');
        $allMobileRubPriceDiscount = Product::where('category', 'mobile')->sum('price_discount') * $currentShoppingCartMultiplier;
        $allMobileUsdPriceOriginal = Product::where('category', 'mobile')->sum('usd_price_original');
        $allMobileUsdPriceDiscount = Product::where('category', 'mobile')->sum('usd_price_discount') * $currentShoppingCartMultiplier;

        $mobileProductIds = Product::mobile()->get('id')->map(function ($item) {
            return $item->id;
        });
        $cartItemIds = \Cart::getContent()->map(function ($item) {
            return $item->id;
        });
        $intersection = $mobileProductIds->intersect($cartItemIds)->all();
        $isAllMobileProductInCart = count($intersection) === count($mobileProductIds);

        $mobileProductIdsJson = Product::mobile()->get('id')->toJson();
        $mobileProductsCount = Product::where('category', 'mobile')->count();

        return view('products.safari_style', compact(
            [
                'pack',
                'allMobileRubPriceOriginal',
                'allMobileRubPriceDiscount',
                'allMobileUsdPriceOriginal',
                'allMobileUsdPriceDiscount',
                'mobileProductIdsJson',
                'isAllMobileProductInCart',
                'mobileProductsCount',
            ]
        ));
    }
}
