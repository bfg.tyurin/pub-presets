<?php

namespace App\Http\Controllers;

use App\Content;
use App\Mailer;
use App\Order;
use App\Product;
use App\Promotion;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;

class AppleController extends Controller
{
    public function domain_association()
    {
        return Storage::get('public/apple-developer-merchantid-domain-association');
    }
}
