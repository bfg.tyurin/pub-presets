<?php

namespace App\Http\Controllers;

use App\Pack;
use App\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;

class IndexPageController extends Controller
{
    public function index(Request $request)
    {
        return view(
            'index',
            [
                'product_packs' => Product::where('type', 'pack')->latest()->get(),
                'product_bundle' =>  Product::where('type', 'bundle')->first(),
            ]
        );
    }
}
