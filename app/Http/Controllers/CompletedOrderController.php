<?php

namespace App\Http\Controllers;

use App\Order;
use Illuminate\Http\Request;
use App\Events\OrderWasCompleted;
use Illuminate\Support\Facades\Log;
use App\Services\CreateDownloadable;

class CompletedOrderController extends Controller
{
    public function store(Request $request, CreateDownloadable $createDownloadable)
    {
        Log::info($request);

        $this->validate($request, [
            'order_id' => 'required|max:255|exists:orders,id',
        ]);

        $order = Order::with('user', 'products')->findOrFail($request->input('order_id'));
        $order->status = Order::STATUS_COMPLETED;
        $order->save();

        event(new OrderWasCompleted($order));

        // Redirect
        Log::info('Redirect to magic url.');
        return ['url' => $order->downloadable->getUrl()];
    }
}
