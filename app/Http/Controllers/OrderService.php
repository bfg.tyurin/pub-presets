<?php


namespace App\Http\Controllers;


use App\Order;

class OrderService
{
    /**
     * @param string $id
     * @param string $status
     */
    public function updateStatus(string $id, string $status)
    {
        $order = Order::firstWhere('id', $id);
        $order->status = $status;
        $order->save();
        return $order;
    }
}
