<?php

namespace App\Http\Controllers;

use App\Content;
use App\EmailLogin;
use App\Mailer;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class MagicLinkController extends Controller
{
    public function index()
    {
       return redirect('/');
    }

    public function login(Request $request, Mailer $mailer)
    {
        // TODO exception
        $this->validate($request, ['email' => 'required|email|max:255|exists:users,email']);

        // Сформировать URL
        $emailLogin = EmailLogin::createForEmail($request->input('email'));
        // Сформировать URL
        $url = url(route('magiclink.login') . '/' . $emailLogin->token);
        // Отправить URL письмом
        $user = User::where('email', $emailLogin->email)->first();
        $mailer->send($user, new Content('Ссылка для входа на сайт', $url));
        // TODO Вернуть сообщение пользователю

        return redirect(route('home'));
    }

    public function authenticateEmail($token)
    {
        $emailLogin = EmailLogin::validFromToken($token);

        Auth::login($emailLogin->user);

        return redirect('home');
    }
}
