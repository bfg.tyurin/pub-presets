<?php

namespace App\Http\Controllers;

use App\Gift;
use App\User;
use App\Order;
use Exception;
use App\Mailer;
use App\Content;
use Jenssegers\Agent\Agent;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Storage;

class GiftController extends Controller
{
    public function store(Request $request, Mailer $mailer)
    {
        $this->validate($request, [
            'email' => 'required|email|max:255',
            'instagram' => 'required',
            'phone' => 'required',
        ]);

        $email = $request->input('email');
        $instagram = $request->input('instagram');
        $phone = $request->input('phone');

        $user = User::firstOrCreate(['email' => $email]);
        $gift = Gift::firstOrCreate([
            'user_id' => $user->id,
            'instagram' => $instagram,
            'phone' => $phone,
        ]);

        session(['email' => $user->email]);
        // Создать юзера
        // Создать Gift
        // Сгенерировать ссылку на пресет
        // Generate magic link
        $desktopFile = Storage::url('Zhenya_Swan_free_preset_desktop.zip');
        $mobileFile = Storage::url('FreePresetZHmobile.dng');
        // Отправить письмо
        //        $message = 'Привет! Скачайте ваш подарочный пресет Portugal Morning по ссылке<br>Десктоп пресет ' . url($desktopFile) . '<br>Мобильный пресет ' . url($mobileFile) . '<br><br>И не забывайте участвовать в конкурсе, чтобы получить шанс выиграть полную коллекцию платных пресетов:<br>1) выложить фото, обработанное пресетом<br>2) подписать хештег #zhenyaswanpresets<br>3) отметить меня @zhenyaswan в тексте поста.<br>Результаты буду публиковать в сторис по вторникам, следите!<br>';
        $message = "<!DOCTYPE html><html><head><meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\"><meta name=\"viewport\" content=\"width=device-width; initial-scale=1.0; maximum-scale=1.0;\"><title></title><style type=\"text/css\">/*<![CDATA[*/        @import url(https://fonts.googleapis.com/css2?family=Roboto:wght@400;500;700;900&display=swap);                .ReadMsgBody{                        width: 100%;                        background-color: #ede6e2;                }                .ExternalClass{                        width: 100%;                        background-color: #ede6e2;                }                body{                        width: 100%;                        background-color: #ede6e2;                        margin:0;                        padding:0;            -webkit-text-size-adjust: 100%;            text-size-adjust: 100%;                        font-family: 'Roboto', Arial, sans-serif;            font-size: 14px;            color: #4F4336;                }                table{                        border-collapse: collapse;                }        .deviceWidth {            width: 540px;        }        .text-table1 {            padding: 55px 40px 30px 40px;            background: #fff;            font-size: 16px;            line-height: 135%;            font-weight: 500;            color: #4F4336;        }        .headline1 {            font-weight: 900;            font-size: 60px;            color: #EDE6E2;        }        .bold-text1 {            font-weight: 900;            color: #4F4336;        }        .link-button {            display: block;            line-height: 52px;            border-radius: 26px;            background: #4F4336;            color: #fff !important;            text-align: center;            font-weight: bold;            font-size: 18px;            text-decoration: none;            margin: 40px 0;        }        .social-table {            padding: 0 40px 35px 40px;            background: #fff;        }        .social-link1 {            margin: 0 100px 0 0;        }        .unsubscripe {            padding: 30px 0 75px 0;            font-size: 16px;            line-height: 135%;            text-align: center;            color: #B2A8A0;            font-weight: 500;        }        @media only screen and (max-width: 580px){            .deviceWidth {                width: 320px;                padding: 0;            }            .text-table1 {                padding: 20px 20px 15px 20px;                font-size: 14px;            }            .headline1 {                font-size: 28px;            }            .social-table {                padding: 0 20px 40px 20px;            }            .social-link1 {                margin: 0 30px 0 0;            }            .unsubscripe {                font-size: 14px;            }        }/*]]>*/</style></head><body style=\"font-family: 'Roboto', Arial, sans-serif; min-width: 320px !important; -webkit-text-size-adjust: 100%; text-size-adjust: 100%;\"><!-- Wrapper --><table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" align=\"center\" style=\"-webkit-text-size-adjust: 100% !important; text-size-adjust: 100% !important; color: #4f4336; background: #ede6e2;\"><tbody><tr><td><table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" align=\"center\" class=\"deviceWidth\"><tbody><tr><td style=\"padding: 50px 0 40px 0;\"><a rel=\"noopener noreferrer\" href=\"#\" target=\"_blank\" style=\"text-decoration: none;\"><img src=\"https://login.sendpulse.com/files/emailservice/userfiles/2fa9fccde1532aa66410cd1fadb1cc987191381/h-logo2.jpg\" style=\"border: 0; max-width: 100%;\"></a></td></tr><tr><td><img src=\"https://login.sendpulse.com/files/emailservice/userfiles/2fa9fccde1532aa66410cd1fadb1cc987191381/banner3.jpg\" style=\"display: block; width: 100%; border: 0;\"></td></tr><tr><td class=\"text-table1\"><p><span class=\"headline1\" style=\"font-family: georgia, palatino;\">Ваш подарок</span></p><p><font face=\"georgia, palatino\">Очень надеюсь, что вы вдохновитесь и будете творить с моим пресетом!</font></p><p><font face=\"georgia, palatino\">Я подготовила для скачивания 2 версии пресета:</font></p><p><span style=\"font-family: georgia, palatino; font-size: 14px;\"><a rel=\"noopener noreferrer\" href=\"" . url($mobileFile) . "\" target=\"_blank\" class=\"link-button\">Скачать мобильный пресет</a></span></p><p><span style=\"font-size: 14px;\"><span style=\"font-family: georgia, palatino;\"><span style=\"font-family: georgia, palatino;\"><a rel=\"noopener noreferrer\" href=\"" . url($desktopFile) . "\" target=\"_blank\" class=\"link-button\">Скачать десктоп пресет</a></span></span><span style=\"font-family: georgia, palatino; font-size: 16px;\">И не забывайте участвовать в конкурсе, чтобы получить шанс <strong>выиграть полную коллекцию платных пресетов</strong><strong>:</strong><br> 1) выложите фото, обработанное пресетом<br> 2) подпишите хештег #zhenyaswanpresets<br> 3) отметьте меня @zhenyaswan в тексте поста.</span></span></p><p><span style=\"font-family: georgia, palatino;\">Если вы уже купили &mdash; получите полный возврат денег! Если купили только 1 коллекцию &mdash; получите возврат плюс остальные коллекции.<br> <br> Результаты буду публиковать в сторис по вторникам, следите!</span></p><p><span style=\"font-family: georgia, palatino;\"><br> <span style=\"font-family: georgia, palatino;\">Если у вас есть любые вопросы, не стесняйтесь обращаться в поддержку &mdash; мы будем рады помочь.</span></span></p></td></tr><tr><td class=\"social-table\"><span style=\"font-family: georgia, palatino; font-size: 16px;\"><a rel=\"noopener noreferrer\" href=\"https://tele.gg/denis_ayupov\" target=\"_blank\" style=\"display: inline-block; line-height: 30px; padding: 0px 0px 0px 45px; font-weight: 500; color: #4f4336; text-decoration: none; background-image: url('https://login.sendpulse.com/files/emailservice/userfiles/2fa9fccde1532aa66410cd1fadb1cc987191381/social3.jpg'); background-position: left center; background-repeat: no-repeat;\" class=\"social-link1\">telegram</a> <a rel=\"noopener noreferrer\" href=\"https://wa.me/79874749387\" target=\"_blank\" style=\"display: inline-block; line-height: 30px; padding: 0px 0px 0px 45px; font-weight: 500; color: #4f4336; text-decoration: none; background-repeat: no-repeat; background-image: url('https://login.sendpulse.com/files/emailservice/userfiles/2fa9fccde1532aa66410cd1fadb1cc987191381/social4.jpg'); background-position: left center;\">whatsapp</a></span></td></tr><tr><td class=\"unsubscripe\"><span style=\"font-family: georgia, palatino;\">Если вы не хотите получать письма от меня,</span><br> <span style=\"font-family: georgia, palatino;\">нажмите <a rel=\"noopener noreferrer\" href=\"{{unsubscribe_url}}\" style=\"color: #b2a8a0; text-decoration: underline; font-weight: 500;\">отписаться&nbsp;</a></span></td></tr></tbody></table></td></tr></tbody></table></body></html>";
        try {
            $mailer->send($user, new Content('Ссылка на скачивание пресета', $message));
            $gift->is_mail_sent = true;
            Log::info('gift mail send.');
        } catch (Exception $e) {
            $gift->is_mail_sent = false;
            Log::info('gift mail failed.');
        }

        $downloadUrl = url($desktopFile);

        $agent = new Agent;
        $gift->device = $agent->device();
        $gift->is_mobile = false;
        if ($agent->isMobile()) {
            $downloadUrl = url($mobileFile);
            $gift->is_mobile = true;
        }

        $gift->save();

        return ['url' => $downloadUrl];
    }

    public function show()
    {
        if (Gate::allows('show-dashboard')) {
            $gifts = Gift::orderBy('updated_at', 'desc')
                ->join('users', 'gifts.user_id', '=', 'users.id')
                ->whereNotIn('email', Order::freeEmails())
                ->select('gifts.*');
            return view('admin.dashboard.gifts.show', [
                'gifts' => $gifts->get(),
            ]);
        }
        return abort(404);
    }

    public function full()
    {
        if (Gate::allows('show-panel')) {
            $gifts = Gift::orderBy('updated_at', 'desc');
            return view('admin.dashboard.gifts.full', [
                'gifts' => $gifts->get(),
            ]);
        }
        return abort(404);
    }
}
