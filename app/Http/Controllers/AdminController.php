<?php


namespace App\Http\Controllers;


use App\AuthenticatesAdmin;
use App\LoginToken;
use App\Order;
use App\Product;
use App\Services\AdminTotalsService;
use App\User;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Log;
use Illuminate\Validation\ValidationException;


class AdminController extends Controller
{
    private AuthenticatesAdmin $auth;

    public function __construct(AuthenticatesAdmin $auth)
    {
        $this->auth = $auth;
    }

    public function login()
    {
        return view('admin.login');
    }


    /**
     * @param Request $request
     * @return Application|RedirectResponse|Redirector|string
     */
    public function postLogin(Request $request)
    {
        try {
            $this->auth->invite();
        } catch (ValidationException $e) {
            Log::error('postLogin | Invalid email', ['email' => $request->email]);
            return redirect(url('/'));
        }

        return 'Проверьте почту!';
    }

    public function authenticate(LoginToken $token, Request $request)
    {
        $this->auth->login($token);

        if ($request->user()->isSupport()) {
            return redirect(route('support.dashboard.index'));
        }

        return redirect(route('admin.dashboard.simple'));
    }

    public function dashboard(Request $request)
    {
        if (Gate::allows('show-panel')) {

            $orders = Order::orderBy('created_at', 'desc')
                ->where('deleted', 0)
                ->join('users', 'orders.user_id', '=', 'users.id')
                ->whereNotIn('email', Order::freeEmails());
            //                ->whereNull('campaign_id');

            $users = User::whereNotIn('email', Order::freeEmails());

            if ($request->has('order_status')) {
                $orders = $orders->where('status', $request->order_status);
            }

            if ($request->has('product_type')) {
                $orders = $orders->join('products', 'orders.product_id', '=', 'products.id')
                    ->where('type', $request->product_type);
            }

            return view('admin.dashboard', [
                'orders'   => $orders->select('orders.*')->get(),
                'products' => Product::all(),
                'users'    => $users->get(),
            ]);
        }

        abort(404);
    }

    public function simple(Request $request)
    {
        if (Gate::allows('show-dashboard')) {

            $orders = Order::orderBy('created_at', 'desc')
                ->where('status', 'completed')
                ->where('deleted', 0)
                ->join('users', 'orders.user_id', '=', 'users.id')
                // ->join('products', 'orders.product_id', '=', 'products.id')
                ->whereNull('campaign_id')
                ->whereNotIn('email', Order::freeEmails());

            if ($request->has('gifts')) {
                $orders->join('gifts', 'orders.user_id', '=', 'gifts.user_id');
                $orders->whereNotNull('gifts.user_id');
                $orders->distinct();
            }

            $orders->select('orders.*')->with('products');

            $rubRegularAdminTotalService = new AdminTotalsService(Order::CURRENCY_RUB);
            $usdRegularAdminTotalService = new AdminTotalsService(Order::CURRENCY_USD);

            return view('admin.dashboard.simple', [
                'orders'             => $orders->get(),
                'products'           => Product::all(),
                'rubRegularAdminTotalService' => $rubRegularAdminTotalService,
                'usdRegularAdminTotalService' => $usdRegularAdminTotalService,
            ]);
        }

        abort(404);
    }

    public function product(Request $request)
    {
        $product = Product::find($request->id);
        return view('admin.dashboard.product', [
            'product' => $product
        ]);
    }

    public function panel()
    {
        return abort(404);
    }

    public function panelSimple(Request $request)
    {
        if (Gate::allows('show-panel')) {
            $orders = Order::orderBy('created_at', 'desc')
                ->where('deleted', 0)
                ->join('users', 'orders.user_id', '=', 'users.id')
                // ->join('products', 'orders.product_id', '=', 'products.id')
                ->whereNotNull('campaign_id')
                ->whereNotIn('email', Order::freeEmails())
                ->select('orders.*');

            $orders->select('orders.*')->with('products');

            if ($request->has('order_status')) {
                $orders = $orders->where('status', $request->order_status);
            } else {
                $orders = $orders->where('status', 'completed');
            }

            if ($request->has('gifts')) {
                $orders->join('gifts', 'orders.user_id', '=', 'gifts.user_id');
                $orders->whereNotNull('gifts.user_id');
                $orders->distinct();
            }

            $rubCampaignAdminTotalService = new AdminTotalsService(Order::CURRENCY_RUB, true);
            $usdCampaignAdminTotalService = new AdminTotalsService(Order::CURRENCY_USD, true);

            return view('admin.panel.simple', [
                'orders'                 => $orders->get(),
                'products'               => Product::all(),
                'rubCampaignAdminTotalService' => $rubCampaignAdminTotalService,
                'usdCampaignAdminTotalService' => $usdCampaignAdminTotalService,
            ]);
        }

        abort(404);
    }

    public function orders(Request $request)
    {
        if (Gate::allows('show-panel')) {


            return view('admin.panel.orders', [
                'orders' => Order::all(),
            ]);
        }

        return abort(404);
    }

    public function logout()
    {
        Auth::logout();
        return redirect(route('admin.login'));
    }
}
