<?php


namespace App\Http\Controllers;


use App\User;
use App\Order;
use App\Mailer;
use App\Content;
use App\Product;
use App\Campaign;
use App\Promotion;
use Illuminate\Http\Request;
use App\Services\GetTotalPrice;
use App\Events\OrderWasCompleted;
use App\Services\CreatePromotion;
use App\Services\SetupPaypalData;
use Illuminate\Support\Facades\Log;
use App\Services\DownloadableService;
use App\Services\Dto\CreatePromotionDto;
use Srmklive\PayPal\Services\ExpressCheckout;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;

class PayPalController extends Controller
{
    protected $provider;
    private CreatePromotion $createPromotion;

    public function __construct(CreatePromotion $createPromotion)
    {
        $this->createPromotion = $createPromotion;
        $this->provider = new ExpressCheckout();
    }

    public function beforePayment(Request $request)
    {
        $request->validate([
            'email' => 'required|email|max:255',
            // TODO validate slug
        ]);

        Log::info('pp Email validated', ['email' => $request->input('email')]);

        session(['email' => $request->input('email')]);
        session(['product_slug' => $request->input('product_slug')]);

        return [
            'paypal_url' => LaravelLocalization::getLocalizedURL(LaravelLocalization::getCurrentLocale(), route('paypal.payment'))
        ];
    }

    public function payment(Request $request, GetTotalPrice $getTotalPrice)
    {
        Log::info('pp payment for: (session)', ['email' => session()->get('email'), 'slug' => session()->get('product_slug')]);

        $product = Product::firstWhere('slug', session()->get('product_slug'));
        Log::info('pp Product found:', ['Product title' => $product->title]);

        $user = User::firstWhere('email', session()->get('email'));

        if (!$user) {

            Log::info('pp User not exists', ['user_email' => session()->get('email')]);
            $user = User::create(['email' => session()->get('email')]);
            Log::info('pp User created', ['user' => $user]);

            if ($request->cookie('utm_data')) {

                Log::info('pp Cookie found', ['utm_data' => $request->cookie('utm_data')]);

                $utm_data = json_decode($request->cookie('utm_data'));
                $campaign = Campaign::create([
                    'utm_source'   => property_exists($utm_data, 'utm_source') ? $utm_data->utm_source : '',
                    'utm_campaign' => property_exists($utm_data, 'utm_campaign') ? $utm_data->utm_campaign : '',
                    'utm_medium'   => property_exists($utm_data, 'utm_medium') ? $utm_data->utm_medium : '',
                    'utm_term'     => property_exists($utm_data, 'utm_term') ? $utm_data->utm_term : '',
                    'utm_content'  => property_exists($utm_data, 'utm_content') ? $utm_data->utm_content : '',
                ]);

                $user->campaign()->associate($campaign);
                $user->save();

                Log::info('pp Campaign created: ', ['campaign', $campaign]);
            }
        }


        $order               = new Order();
        $order->user_id      = $user->id;
        $order->product_id   = $product->id;
        $order->status       = 'processing';
        $order->payment_type = 'paypal';

        switch (LaravelLocalization::getCurrentLocale()) {
            case ('ru'):
                $order->currency = "RUB";
                $order->total = in_array($user->email, Order::freeEmails()) ? 1 : $getTotalPrice->call($user, $product, 'ru');
                break;

            case ('en'):
                $order->currency = "USD";
                $order->total = in_array($user->email, Order::freeEmails()) ? 0.01 : $getTotalPrice->call($user, $product, 'en');
                break;
            default:
                $order->currency = "RUB";
                $order->total = in_array($user->email, Order::freeEmails()) ? 1 : $getTotalPrice->call($user, $product, 'ru');
                break;
        }

        $order->save();

        $data = [];
        $data['items'] = [
            [
                'name'  => $product->title,
                'price' => $order->total,
                'desc'  => "Покупка курса {$product->title}",
                'qty'   => 1
            ],
        ];

        $data['invoice_id'] = $order->id;
        $data['invoice_description'] = "Order #{$order->id}";
        $data['cancel_url'] = route('paypal.payment.cancel');
        $data['return_url'] = route('paypal.payment.success');

        $data['total'] = $order->total;

        Log::info('pp data', ['total' => $data['total'], 'order user email' => $order->user->email]);

        $response = $this->provider->setCurrency($order->currency)->setExpressCheckout($data);

        return redirect($response['paypal_link']);
    }

    public function cancel(Request $request)
    {
        $response = $this->provider->getExpressCheckoutDetails($request->token);
        Log::info('pp canceled responce', ['responce' => $response]);
        $orderId = $response['INVNUM'];
        $order = Order::find($orderId);

        Log::info('pp cancel', ['INVNUM' => $response['INVNUM'], 'order' => $order->id, 'user email' => $order->user->email]);

        return redirect()->back();
    }

    public function success(Request $request, SetupPaypalData $setupPaypalData, DownloadableService $downloadableService, Mailer $mailer)
    {
        Log::info('pp success');

        $response = $this->provider->getExpressCheckoutDetails($request->token);
        $token = $request->get('token');
        $PayerID = $request->get('PayerID');
        $orderId = $response['INVNUM'];

        $order = Order::with('products:title')->find($orderId);
        // $product = $order->product;
        $user = $order->user;

        $data = $setupPaypalData->call($order);

        $payment_status = $this->provider->setCurrency($order->currency)->doExpressCheckoutPayment($data, $token, $PayerID);
        Log::info('pp ', ['PAYMENTSTATUS' => $payment_status]);
        $status = $payment_status['PAYMENTINFO_0_PAYMENTSTATUS'] ?? 'null';
        $status = $status === 'Completed' ? 'completed' : $status;

        $order->status = $status;
        $order->save();

        if ($order->status === 'completed') {
            Log::info('pp order.status completed. User buy a product.', ['user_id' => $user->id, 'user_email' => $user->email, 'order_id' => $order->id]);

            event(new OrderWasCompleted($order));
            // Redirect
            Log::info('pp Redirect to magic url.');
            return redirect($order->downloadable->getUrl());
        }

        // v2 OFF
        // if ('promotion' === $order->product->type) {
        //     Log::info('pp product type: ' . $order->product->type . ' redirecting');
        //     return redirect(session('promotion_url'));
        // }

        return redirect(route('cart.show', ['name' => $order->product->slug]));
    }

    public function store(Request $request, SetupPaypalData $setupPaypalData)
    {
        $this->validate($request, [
            'orderId' => 'required|max:255|exists:orders,id',
        ]);

        $orderId = $request->orderId;
        $order = Order::with('products:title')->find($orderId);

        $name = $order->products->map(function ($item, $key) {
            return $item->title;
        })->join(', ');
        Log::info('name: ' . $name);

        $data = $setupPaypalData->call($order);

        Log::info('pp data, method STORE', ['total' => $data['total'], 'order user email' => $order->user->email]);

        $response = $this->provider->setCurrency($order->currency)->setExpressCheckout($data);

        // return redirect($response['paypal_link']);

        // return redirect(url('/'));
        return ['paypal_link' => $response['paypal_link'], 'orderId' => $orderId];
    }
}
