<?php

namespace App\Http\Controllers;

use App\Product;
use App\Services\ShoppingCartService;
use Darryldecode\Cart\Cart;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;

class ShoppingCartProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, ShoppingCartService $shoppingCartService)
    {
        $this->validate($request, [
            'product_id' => 'required|exists:products,id'
        ]);

        $id = $request->product_id;
        $product = Product::find($id);
        $shoppingCartService->add($product);

        $discountPercent = $shoppingCartService->getDiscountPercent();
        $nextDiscountPercent = $shoppingCartService->getNextDiscountPercent();

        $discountValue = $shoppingCartService->discountInCurrency();

        return response([
            'cart_content' => $shoppingCartService->getContentJson(),
            'cart_total' => $shoppingCartService->getTotal(),
            'cart_sub_total' => $shoppingCartService->getSubTotal(),
            'cart_count' => $shoppingCartService->getContentCount(),
            'cart_discount_value' => $discountValue,
            'cart_discount_percent' => $discountPercent,
            'cart_next_discount_percent' => $nextDiscountPercent,
            'cart_full_price' => $shoppingCartService->getFullPrice(),
            'cart_saving' => $shoppingCartService->getSaving(),
            'buy_button_text' => __('basket.buy_button_text_remove'),
            'basket_collections_choosen_string' => trans_choice('basket.collections_choosen', $shoppingCartService->getContentCount()),
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, ShoppingCartService $shoppingCartService)
    {
        $product = Product::firstWhere('slug', $id);
        $shoppingCartService->remove($product);

        $discountPercent = $shoppingCartService->getDiscountPercent();
        $nextDiscountPercent = $shoppingCartService->getNextDiscountPercent();

        $discountValue = $shoppingCartService->discountInCurrency();

        return response([
            'cart_content' => $shoppingCartService->getContentJson(),
            'cart_total' => $shoppingCartService->getTotal(),
            'cart_sub_total' => $shoppingCartService->getSubTotal(),
            'cart_count' => $shoppingCartService->getContentCount(),
            'cart_discount_value' => $discountValue,
            'cart_discount_percent' => $discountPercent,
            'cart_next_discount_percent' => $nextDiscountPercent,
            'cart_full_price' => $shoppingCartService->getFullPrice(),
            'cart_saving' => $shoppingCartService->getSaving(),
            'buy_button_text' => __('basket.buy_button_text_add'),
            'basket_collections_choosen_string' => trans_choice('basket.collections_choosen', $shoppingCartService->getContentCount()),
        ]);
    }
}
