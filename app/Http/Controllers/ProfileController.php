<?php

namespace App\Http\Controllers;

use App\User;
use App\Mailer;
use App\Content;
use App\Profile;
use App\Downloadable;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use App\Services\DownloadableService;

class ProfileController extends Controller
{
    public function create()
    {
        // TODO big think

        if (User::where('email', session('email'))->doesntExist()) {

            Log::info('User with email does not exist in DB: ', ['user_email_in_session', session('email')]);

            return redirect(url('/'));
        }

        return view('profile.create');
    }

    public function store(Request $request, Mailer $mailer, DownloadableService $downloadableService)
    {
        $this->validate($request, [
            'firstName'       => 'required',
            'surname'         => 'required',
            'gender'          => 'required|max:1',
            'instagram'       => 'required',
            'phone'           => 'required',
            'profession'      => 'required',
            'reason'          => 'required',
        ]);

        // TODO
        // ОБработать исключение при создании Profile с таким же email
        // ОБработать исключение если пользователя не нашлось

        $profile = Profile::create([
            'user_id'    => User::where('email', session('email'))->first()->id,
            'name'       => request('firstName'),
            'surname'    => request('surname'),
            'gender'     => request('gender'),
            'instagram'  => request('instagram'),
            'number'     => request('phone'),
            'profession' => request('profession'),
            'reason'     => request('reason'),
        ]);
        Log::info('profile created.', ['user_id' => $profile->user_id]);


        // после заполнения профиля должен быть редирект
        $downloadable = Downloadable::latest()->firstWhere('user_id', $profile->user_id);
        $user = $downloadable->user;


        // Профиль создан отправляем второе письмо
        $body = view('mail.profile_was_created', ['downloadableLink' => $downloadableService->getUrl($downloadable)])->render();
        $content = new Content(__('profile_was_created_email.title'), $body);
        $mailer->sendByEmail($user->email, $content);
        Log::info('Send profile created. email ' . $user->email);
        //

        Log::info('Redirecting to /product/download/{token}');
        return ['url' => $downloadableService->getUrl($downloadable)];
    }
}
