<?php

namespace App\Http\Controllers;

use App\Order;
use App\Product;
use App\Services\CreateUser;
use App\Services\CreateOrder;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Log;
use App\Services\Dto\CreateOrderDto;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Http;
use App\Http\Requests\OrderControllerStoreRequest;
use App\Http\Requests\OrderControllerUpdateRequest;

class OrderController extends Controller
{
    public function store(OrderControllerStoreRequest $request, CreateUser $createUser, CreateOrder $createOrder)
    {
        $dto = $request->getDto();
        $user = $createUser->call($dto->getUserEmail(), $dto->getUtmData());
        // $product = Product::findOrFail($dto->getProductId());

        // if ('promotion' === $product->type) {
        //     if (Gate::forUser($user)->denies('buy-promotions')) {
        //         return;
        //     }
        // }

        // $createOrderDto = new CreateOrderDto(
        //     $user,
        //     $product,
        //     $dto->getPaymentType(),
        //     App::getLocale()
        // );
        // return $createOrder->call($createOrderDto)->load('user', 'product');

        $total = in_array($user->email, Order::freeEmails()) ? 1 : \Cart::getTotal();

        $order = Order::create([
            'user_id'      => $user->id,
            'status'       => Order::STATUS_PROCESSING,
            'total'        => $total,
            'payment_type' => $dto->getPaymentType(),
            'currency'     => App::getLocale() === 'ru' ? Order::CURRENCY_RUB : Order::CURRENCY_USD,
        ]);

        $cartContent = \Cart::getContent()->toArray();
        $productIds = array_reduce($cartContent, function ($acc, $item) {
            return array_merge($acc, [$item['id']]);
        }, []);
        $order->products()->attach($productIds);
        return Order::with('products:id,title,price_discount,usd_price_discount')->find($order->id)->load('user')->toArray();
    }

    // TO-DO
    // public function update($id, OrderControllerUpdateRequest $request)
    // {
    //     $order = Order::findOrFail($id);

    //     $response = Http::withBasicAuth('taylor@laravel.com', 'secret')->post('https://api.cloudpayments.ru/v2/payments/find', [
    //         'InvoiceId' => $id,
    //     ]);
    //     Log::info($response);

    //     $order->status = request('status');
    //     $order->save();

    //     // event(new OrderShipped($order));
    // }
}
