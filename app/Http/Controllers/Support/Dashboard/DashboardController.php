<?php


namespace App\Http\Controllers\Support\Dashboard;


use App\Content;
use App\Downloadable;
use App\Gift;
use App\Http\Controllers\Controller;
use App\Mailer;
use App\Order;
use App\User;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Log;

class DashboardController extends Controller
{

    public function index()
    {
        if (Gate::allows('show-support')) {
            return view('support.dashboard.index');
        }
        return abort(404);
    }

    public function gifts()
    {
        if (Gate::allows('show-support')) {
            $gifts = Gift::orderBy('updated_at', 'desc')
                ->join('users', 'gifts.user_id', '=', 'users.id')
                ->whereNotIn('email', Order::freeEmails())
                ->select('gifts.*');
            return view('support.dashboard.gifts.show', [
                'gifts' => $gifts->get(),
            ]);
        }
        return abort(404);
    }

    public function findUserDownloadLink(Request $request)
    {
        if (Gate::allows('show-support')) {
            $this->validate($request, [
                'email' => 'required|max:255|exists:users,email',
            ]);

            //        route('products.download', $downloadable->token);
            $user = User::where('email', $request->email)->with('downloadables')->with('downloadables.order')->with('downloadables.order.products')->first();
            //        $d = $user->downloadables->
            //        dd($user->downloadables);
            return [
                'downloadables' => $user->downloadables ? $user->downloadables : '',
                'url' => route('support.dashboard.d.mail'),
            ];
        }
        return abort(404);
    }

    public function sendMail(Request $request, Mailer $mailer)
    {
        if (Gate::allows('show-support')) {
            $this->validate($request, [
                'email' => 'required|max:255|exists:users,email',
                'url'   => 'required',
            ]);

            // Send mail
            $user = User::where('email', $request->email)->first();
            try {
                // $mailer->send($user, new Content('Ссылка на скачивание пака пресетов', $request->url));

                $body = view('mail.purchase', ['downloadableLink' => $request->url])->render();
                $content = new Content(__('purchase_email.title'), $body);
                $mailer->send($user, $content);
                Log::info('Support send Mail to', ['email' => $request->email]);

                if ($user->profile) {
                    // Профиль создан отправляем второе письмо
                    $body = view('mail.profile_was_created', ['downloadableLink' => $request->url])->render();
                    $content = new Content(__('profile_was_created_email.title'), $body);
                    $mailer->sendByEmail($user->email, $content);
                }

                return ['status' => 'success'];
            } catch (Exception $e) {
                Log::info('Support FAILED to send mail to', ['email' => $request->email]);

                return ['status' => 'error'];
            }
        }
        return abort(404);
    }
}
