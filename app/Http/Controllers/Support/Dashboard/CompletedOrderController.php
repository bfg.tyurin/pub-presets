<?php

namespace App\Http\Controllers\Support\Dashboard;

use App\User;
use App\Order;
use App\Product;
use Illuminate\Http\Request;
use App\Events\OrderCompleted;
use App\Events\OrderWasCompleted;
use App\Listeners\CreatePromotion;
use Illuminate\Support\Facades\App;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Gate;
use Illuminate\Pagination\LengthAwarePaginator;

class CompletedOrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (Gate::denies('show-support')) {
            return abort(403);
        }

        // только без UTM!!!
        $orders = Order::when(!request()->filled('user_email'), function ($query) {
            $query->where('payment_type', 'custom');
        })
            ->when(request()->has('user_email'), function ($query) {
                $query->whereHas('user', function ($query) {
                    $query->where('email', request('user_email'));
                });
            })
            ->latest()
            ->completed()
            ->with('user')
            ->with('user.profile')
            ->with('product')
            ->paginate(5);
        return view('support.dashboard.orders.index', compact(['orders']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (Gate::denies('show-support')) {
            return abort(403);
        }

        $products = Product::where('type', '<>', 'promotion')->where('type', '<>', 'bundle')->orderBy('title')->get();

        return view('support.dashboard.orders.create', compact(['products']));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (Gate::denies('show-support')) {
            return abort(403);
        }

        $this->validate($request, []);

        $user = User::firstOrCreate(['email' => $request->email]);

        $productIds = explode(',', $request->values);

        $order = Order::create([
            'user_id'      => $user->id,
            'status'       => Order::STATUS_COMPLETED,
            'total'        => $request->total,
            'payment_type' => 'custom',
            'currency'     => $request->currency,
        ]);

        $order->products()->attach($productIds);

        event(new OrderWasCompleted($order));

        return redirect(route('support.dashboard.orders.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function show(Order $order)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function edit(Order $order)
    {
        if (Gate::denies('show-support')) {
            return abort(403);
        }

        $products = Product::where('type', '<>', 'promotion')->get();

        return view('support.dashboard.orders.edit', compact(['order', 'products']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Order $order)
    {
        if (Gate::denies('show-support')) {
            return abort(403);
        }

        $this->validate($request, [
            'email'      => 'required|email|exists:users,email',
            'product_id' => 'required|exists:products,id',
            'currency'   => 'required|in:' . Order::CURRENCY_RUB . ',' . Order::CURRENCY_USD,
            'total'      => 'required|integer|gt:0',
        ]);

        $order->fill($request->all());
        $order->save();


        if ($request->send_email) {
            event(new OrderWasCompleted($order));
        }

        return redirect()->route('support.dashboard.orders.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function destroy(Order $order)
    {
        if (Gate::denies('show-support')) {
            return abort(403);
        }

        if ($order) {
            $order->user->downloadables()->where('product_id', $order->product_id)->delete();
            $order->user->promotions()->where('product_id', $order->product_id)->delete();
            $order->delete();
        }

        return redirect()->route('support.dashboard.orders.index');
    }
}
