<?php


namespace App\Http\Controllers\Support\Dashboard;


use App\Http\Controllers\Controller;
use App\Product;
use Illuminate\Support\Facades\Gate;

class ProductController extends Controller
{
    public function show($id)
    {
        if (Gate::denies('show-support')) {
            return abort(403);
        }

        return Product::findOrFail($id);
    }
}
