<?php


namespace App\Http\Controllers\Support\Dashboard;


use App\User;
use App\Product;
use App\Discount;
use Illuminate\Http\Request;
use App\Services\CreateDiscount;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Gate;
use App\Http\Requests\DiscountControllerStoreRequest;

class DiscountController extends Controller
{
    public function index(Request $request)
    {
        if (Gate::denies('show-support')) {
            return abort(403);
        }

        $data = $this->validate($request, [
            'user_email' => 'exists:users,email',
            'product_id' => 'exists:products,id',
        ]);

        $discounts = Discount::when(request()->has('user_email'), function ($query) use ($request) {
            $query->whereHas('user', function ($query) use ($request) {
                $query->where('email', $request->input('user_email'));
            });
        })->when(request()->has('product_id'), function ($query) use ($request) {
            $query->whereHas('product', function ($query) use ($request) {
                $query->where('id', $request->input('product_id'));
            });
        })
            ->with('user')
            ->with('product')
            ->latest()
            ->paginate(8);

        return view('support.dashboard.discounts.index', compact(['discounts']));
    }

    public function create()
    {
        if (Gate::denies('show-support')) {
            return abort(403);
        }

        $products = Product::all();

        return view('support.dashboard.discounts.create', compact('products'));
    }

    public function store(DiscountControllerStoreRequest $request, CreateDiscount $createDiscount)
    {
        if (Gate::denies('show-support')) {
            return abort(403);
        }

        $createDiscount->call($request->getDto());

        return redirect(route('support.dashboard.discounts.index'));
    }

    public function destroy($id)
    {
        if (Gate::denies('show-support')) {
            return abort(403);
        }

        $discount = Discount::findOrFail($id);
        $discount->delete();
        return redirect(route('support.dashboard.discounts.index'));
    }
}
