<?php

namespace App\Http\Controllers\Support\Dashboard;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Gate;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if (Gate::denies('show-support')) {
            return abort(403);
        }

        $users = User::withoutUtm()
            ->when(request()->has('user_email'), function ($query) {
                $email = request('user_email');
                $query->where('email', $email);
            })
            ->with('profile')
            ->with('completedOrders')
            ->latest()
            ->paginate(10);

        return view('support.dashboard.users.index', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (Gate::denies('show-support')) {
            return abort(403);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (Gate::denies('show-support')) {
            return abort(403);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        if (Gate::denies('show-support')) {
            return abort(403);
        }

        return view('support.dashboard.users.show', compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        if (Gate::denies('show-support')) {
            return abort(403);
        }

        return view('support.dashboard.users.edit', compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        if (Gate::denies('show-support')) {
            return abort(403);
        }

        $request->validate([
            'user_email' => 'required|email',
        ]);

        $user->fill(['email' => request('user_email')]);
        $user->save();

        return redirect()->route('support.dashboard.users.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        if (Gate::denies('show-support')) {
            return abort(403);
        }
    }
}
