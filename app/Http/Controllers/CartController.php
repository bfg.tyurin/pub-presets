<?php

namespace App\Http\Controllers;

use App\Campaign;
use App\Content;
use App\Mailer;
use App\Order;
use App\Product;
use App\Promotion;
use App\Services\CreatePromotion;
use App\Services\DownloadableService;
use App\Services\Dto\CreatePromotionDto;
use App\Services\GetTotalPrice;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Log;

class CartController extends Controller
{
    private CreatePromotion $createPromotion;

    /**
     * CartController constructor.
     * @param CreatePromotion $createPromotion
     */
    public function __construct(CreatePromotion $createPromotion)
    {
        $this->createPromotion = $createPromotion;
    }

    public function show(Request $request)
    {
        $bundle = Product::firstWhere('type', 'bundle');
        $product = Product::firstWhere('slug', $request->name);
        return view('cart.show', [
            'user_email' => $request->session()->has('email') ? $request->session()->get('email') : '',
            'bundle'     => $bundle,
            'product'    => $product,
            'cookie_email'  => $request->cookie('email'),
        ]);
    }

    public function add(Request $request)
    {
        $productId = $request->input('product_id');
        $product = Product::findOrFail($productId);
        $cart = [
            'product_id' => $product->id,
            'quantity'   => 1,
            'price'      => $product->price_discount,
            'title'      => $product->title,

        ];
        session(['cart' => $cart]);
        //        return session('cart');
    }

    public function create(Request $request, GetTotalPrice $getTotalPrice)
    {
        // TODO переменные валидация слага или по product_id находить продукт

        //        $product = Product::find(session('product_id'));
        $slug = $request->input('slug');
        $email = $request->input('email');
        $utmData = $request->cookie('utm_data');

        $product = Product::firstWhere('slug', $slug);
        $this->validate($request, ['email' => 'required|email|max:255']);

        // TODO почему тут может не быть email?

        session(['email' => $email]);

        $user = User::firstWhere('email', $email);

        if (!$user) {

            Log::info('User not exists', ['user_email' => $email]);
            $user = User::create(['email' => $email]);
            Log::info('User created', ['user' => $user]);

            if ($utmData) {

                Log::info('Cookie found', ['utm_data' => $utmData]);

                $utm_data = json_decode($utmData);
                $campaign = Campaign::create([
                    'utm_source'   => property_exists($utm_data, 'utm_source') ? $utm_data->utm_source : '',
                    'utm_campaign' => property_exists($utm_data, 'utm_campaign') ? $utm_data->utm_campaign : '',
                    'utm_medium'   => property_exists($utm_data, 'utm_medium') ? $utm_data->utm_medium : '',
                    'utm_term'     => property_exists($utm_data, 'utm_term') ? $utm_data->utm_term : '',
                    'utm_content'  => property_exists($utm_data, 'utm_content') ? $utm_data->utm_content : '',
                ]);

                $user->campaign()->associate($campaign);
                $user->save();

                Log::info('Campaign created: ', ['campaign', $campaign]);
            }
        }

        $totalPrice = $getTotalPrice->call($user, $product, App::getLocale());

        if (in_array($user->email, Order::discountEmails())) {
            $totalPrice = (int) number_format($product->price_discount * 0.75, 0, '', '');
            Log::info('discount email checked', ['email' => $user->email, 'original discount price' => $product->price_discount, '$totalPrice' => $totalPrice]);
        }
        if (in_array($user->email, Order::freeEmails())) {
            $totalPrice = 1;
            Log::info('free email checked', ['email' => $user->email, '$totalPrice' => $totalPrice]);
        }

        Log::info('making Order', ['email' => $user->email, '$totalPrice' => $totalPrice]);

        $order             = new Order();
        $order->user_id    = $user->id;
        $order->product_id = $product->id;
        $order->status     = 'processing';
        $order->total      = $totalPrice;
        $order->save();

        Log::info('Order created: ', ['order', $order]);

        return [
            'orderId'     => $order->id,
            'accountId'   => $order->user->email,
            'amount'      => $order->total,
            'description' => $order->product->title,
        ];
    }

    public function update(Request $request, OrderService $orderService, DownloadableService $downloadableService, Mailer $mailer)
    {
        $this->validate($request, [
            'invoiceId' => 'required|max:255|exists:orders,id',
            'data.status' => 'required'
        ]);

        $order = $orderService->updateStatus($request->input('invoiceId'), $request->input('data.status'));

        // Если есть профайл
        $user = User::firstWhere('email', session('email'));
        $product = Product::find($order->product_id);

        Log::info('User buy a product.', [
            'user_id' => $user->id,
            'user_email' => $user->email,
            'product_id' => $product->id,
            'product_slug' => $product->slug
        ]);

        // Generate magic link
        Log::info('Creating magic link.');
        $downloadable = $downloadableService->createWithUserIdProductId($user->id, $order->product_id);

        // Send mail
        // Отправяем первое благодарственное письмо
        // $mailer->send($user, new Content('Ссылка на скачивание пака пресетов', $downloadableService->getUrl($downloadable)));

        $body = view('mail.purchase', ['downloadableLink' => $downloadableService->getUrl($downloadable)])->render();
        $content = new Content(__('purchase_email.title'), $body);
        $mailer->send($user, $content);
        Log::info('Mail send.');

        if ($user->profile) {
            // Профиль создан отправляем второе письмо
            $body = view('mail.profile_was_created', ['downloadableLink' => $downloadableService->getUrl($downloadable)])->render();
            $content = new Content(__('profile_was_created_email.title'), $body);
            $mailer->send($user, $content);
        }

        // Create Promotion
        if ($product->type === 'pack') {
            Log::info('Product type is a Pack. Creating promotion.');

            $promotionDto = new CreatePromotionDto($user->id, $product->id, 1);
            $this->createPromotion->call($promotionDto);
        }

        // Redirect
        Log::info('Redirect to magic url.');
        return ['url' => $downloadableService->getUrl($downloadable)];
    }
}
