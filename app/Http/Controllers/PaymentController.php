<?php


namespace App\Http\Controllers;


use App\Campaign;
use App\Content;
use App\Mailer;
use App\Order;
use App\Product;
use App\Promotion;
use App\Services\DownloadableService;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Srmklive\PayPal\Services\ExpressCheckout;

// TO-DO REMOVE
class PaymentController extends Controller
{
    protected $provider;
    public function __construct()
    {
        $this->provider = new ExpressCheckout();
    }

    public function cloudPaymentsCreate(Request $request)
    {
        // TODO переменные валидация слага или по product_id находить продукт

        //        $product = Product::find(session('product_id'));
        $user = User::findOrFail(session()->get('user_id'));
        $cart = session()->get('cart');

        $order             = new Order();
        $order->user_id    = $user->id;
        $order->product_id = $cart['product_id'];
        $order->status     = 'processing';
        $order->currency   = 'RUB';
        //        $order->total      = in_array($user->email, Order::freeEmails()) ? 1 : $cart['total'];
        $order->total      = $cart['total'];
        $order->save();

        Log::info('cloudPaymentsCreate, Order created: ', ['order', $order]);

        return [
            'order_id'      => $order->id,
            'user_email'    => $order->user->email,
            'total'         => $order->total,
            'product_title' => $order->product->title,
            'product_slug'  => $order->product->slug,
        ];
    }

    public function cloudPaymentsUpdate(Request $request, OrderService $orderService, DownloadableService $downloadableService, Mailer $mailer)
    {
        $this->validate($request, [
            'order_id' => 'required|max:255|exists:orders,id',
            'order_status' => 'required'
        ]);

        $order = $orderService->updateStatus($request->input('order_id'), $request->input('order_status'));

        // Если есть профайл
        $user = $order->user;
        $product = $order->product;

        Log::info('User buy a product.', ['user_id' => $user->id, 'user_email' => $user->email, 'product_id' => $product->id, 'product_slug' => $product->slug]);

        // Generate magic link
        Log::info('Creating magic link.');
        $downloadable = $downloadableService->createWithUserIdProductId($user->id, $order->product_id);

        // Send mail
        // $mailer->send($user, new Content('Ссылка на скачивание пака пресетов', $downloadableService->getUrl($downloadable)));

        $body = view('mail.purchase', ['downloadableLink' => $downloadableService->getUrl($downloadable)])->render();
        $content = new Content(__('purchase_email.title'), $body);
        $mailer->send($user, $content);
        Log::info('Mail send.');

        if ($user->profile) {
            // Профиль создан отправляем второе письмо
            $body = view('mail.profile_was_created', ['downloadableLink' => $downloadableService->getUrl($downloadable)])->render();
            $content = new Content(__('profile_was_created_email.title'), $body);
            $mailer->send($user, $content);
        }

        // Redirect
        Log::info('Redirect to magic url.');
        return ['url' => $downloadableService->getUrl($downloadable)];
    }

    public function paypal(Request $request)
    {
        // user_id уже должен быть в сессии

        $user = User::findOrFail(session()->get('user_id'));
        $cart = session()->get('cart');

        Log::info('PaymentController@paypal buying promotion', ['cart' => $cart]);

        $order               = new Order();
        $order->user_id      = $user->id;
        $order->product_id   = $cart['product_id'];
        $order->status       = 'processing';
        $order->payment_type = 'paypal';
        $order->currency     = $cart['currency'];
        $order->total        = $cart['total'];
        $order->save();

        $data = [];
        $data['items'] = [
            [
                'name'  => $order->product->title,
                'price' => $order->total,
                'desc'  => "Покупка курса {$order->product->title}",
                'qty'   => 1
            ],
        ];

        $data['invoice_id'] = $order->id;
        $data['invoice_description'] = "Order #{$order->id}";
        $data['cancel_url'] = route('paypal.payment.cancel');
        $data['return_url'] = route('paypal.payment.success');

        $data['total'] = $order->total;

        Log::info('PaymentController@paypal pp data', ['data' => $data]);

        $response = $this->provider->setCurrency($order->currency)->setExpressCheckout($data);

        $request->session()->forget(['cart', 'user_id']);

        return redirect($response['paypal_link']);
    }
}
