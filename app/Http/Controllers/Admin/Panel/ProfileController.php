<?php


namespace App\Http\Controllers\Admin\Panel;


use App\Http\Controllers\Controller;
use App\Profile;
use Illuminate\Support\Facades\Gate;

class ProfileController extends Controller
{

    public function show()
    {
        if (Gate::allows('show-panel')) {
            $profiles = Profile::all();
            return view(
                'admin.panel.profiles.show', ['profiles' => $profiles]
            );
        }

        return abort(404);
    }
}
