<?php


namespace App\Http\Controllers\Admin\Panel;


use App\User;
use Exception;
use App\Mailer;
use App\Content;
use App\Discount;
use App\Promotion;
use Carbon\Carbon;
use App\Downloadable;
use App\Subscription;
use App\EmailCampaign;
use Dotenv\Result\Success;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Services\CreateDiscount;
use App\Services\CreatePromotion;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Gate;
use App\Services\DownloadableService;
use App\Services\Dto\CreateDiscountDto;
use App\Services\SendUpdatedPresetMail;
use Illuminate\Database\QueryException;
use App\Services\Dto\CreatePromotionDto;
use App\Jobs\ProcessGoldenCaramelPromotion;
use App\Services\Dto\SendUpdatedPresetMailDto;
use App\Http\Requests\EmailCampaignSendRequest;
use App\Jobs\ProcessSeindingEmailWithInstagram;
use App\Services\SubscribeUsersToEmailCampaign;
use App\Services\CreateDownloadableUrlWithToken;
use App\Jobs\ProcessAddDiscountForUsersWithFreePresetPromotion;

class EmailCampaignController extends Controller
{

    private SubscribeUsersToEmailCampaign $subscribeUsersToEmailCampaign;

    /**
     * EmailCampaignController constructor.
     * @param SubscribeUsersToEmailCampaign $subscribeUsersToEmailCampaign
     */
    public function __construct(SubscribeUsersToEmailCampaign $subscribeUsersToEmailCampaign)
    {
        $this->subscribeUsersToEmailCampaign = $subscribeUsersToEmailCampaign;
    }

    public function index()
    {
        if (Gate::allows('show-panel')) {
            $emailCampaigns = EmailCampaign::all();
            return view('admin.panel.email_campaigns.index', compact('emailCampaigns'));
        }

        return abort('404');
    }

    public function show($id)
    {
        if (Gate::allows('show-panel')) {

            $emailCampaign = EmailCampaign::with(
                [
                    'users',
                    'users.orders' => function ($query) {
                        $query->completed();
                        $query->rub();
                    },
                    'users.promotions' => function ($query) {
                        return $query->latest();
                    },
                    'users.orders.product',
                    'users.promotions.product',
                    'users.email_campaigns',
                ]
            )->findOrFail($id);


            $users = $emailCampaign->users;

            return view(
                'admin.panel.email-campaigns.show',
                [
                    'mailing' => $emailCampaign,
                    'users'   => $users,
                ]
            );
        }
        return abort(404);
    }

    /**
     * @param EmailCampaignSendRequest $request
     * @param CreateDownloadableUrlWithToken $createDownloadableUrlWithToken
     * @param DownloadableService $downloadableService
     * @param SendUpdatedPresetMail $sendUpdatedPresetMail
     * @param CreatePromotion $createPromotion
     * @return \Illuminate\Http\JsonResponse|void
     * @throws \Throwable
     */
    public function send(
        EmailCampaignSendRequest $request,
        CreateDownloadableUrlWithToken $createDownloadableUrlWithToken,
        DownloadableService $downloadableService,
        SendUpdatedPresetMail $sendUpdatedPresetMail
    ) {
        if (Gate::denies('show-support')) {
            return abort(403);
        }

        $emailCampaignSendDto = $request->getDto();
        $downloadable = $downloadableService->createWithUserIdProductId($emailCampaignSendDto->getUserId(), $emailCampaignSendDto->getProductId());
        $downloadableUrl = $createDownloadableUrlWithToken->call($downloadable->token);

        $sendUpdatedPresetMailDto = new SendUpdatedPresetMailDto(
            $emailCampaignSendDto->getUserEmail(),
            $emailCampaignSendDto->getTitle(),
            $downloadableUrl,
            $emailCampaignSendDto->getProductType()
        );
        $sendUpdatedPresetMail->call($sendUpdatedPresetMailDto);

        return response()->json('success', 200);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|void
     */
    public function beforeSend($id)
    {
        if (Gate::allows('show-panel')) {
            Log::info('beforeSend');

            $this->subscribeUsersToEmailCampaign->call($id);

            return back();
        }

        return abort(404);
    }

    public function rollback($id)
    {
        if (Gate::allows('show-panel')) {
            Log::info('rollback');
            $mailing = EmailCampaign::find($id);
            $mailing->users()->detach();

            return back();
        }

        return abort(404);
    }


    public function viewmail()
    {
        if (Gate::allows('show-panel')) {
            return view('mail.updated-presets', ['downloadableUrl' => url("/"), 'productType' => 'bundle']);
        }

        return abort(404);
    }

    public function viewUpdatedPresetsMailWithoutPromotion()
    {
        if (Gate::denies('show-panel')) {
            return abort(403);
        }

        return view('mail.updated_presets_without_promotion', ['downloadableUrl' => url("/"), 'productType' => 'bundle']);
    }

    public function viewMailWithInstagram()
    {
        if (Gate::denies('show-panel')) {
            return abort(403);
        }

        return view('mail.with_instagram', ['downloadableUrl' => url("/"), 'productType' => 'bundle']);
    }

    public function viewMailGoldenCaramelPromotion()
    {
        if (Gate::denies('show-panel')) {
            return abort(403);
        }

        return view('mail.golden_caramel_promotion');
    }

    public function viewAddDiscountForUsersWithFreePresetPromotion()
    {
        if (Gate::denies('show-panel')) {
            return abort(403);
        }

        return view('mail.add_discount_for_users_with_free_preset_promotion', ['email' => 'nebuhad@yandex.ru']);
    }

    public function goldenCaramelPromotionShow()
    {
        $emailCampaign = EmailCampaign::with('subscriptions')
            ->with('subscriptions.user')
            ->with('subscriptions.user.promotions')
            ->with('subscriptions.user.promotions.product')
            ->with('subscriptions.user.orders')
            ->with('subscriptions.user.orders.product')
            ->firstWhere('title', 'golden caramel акция');

        return view('admin.panel.email_campaigns.golden_caramel_promotion.show', compact('emailCampaign'));
    }

    public function goldenCaramelPromotionSubscribe(Request $request)
    {
        if (Gate::denies('show-support')) {
            return abort(403);
        }

        // $usersWithCompletedOrders = User::whereHas('orders', function ($query) {
        //     $query->completed();
        //     $query->rub();
        //     $query->whereHas('product', function ($query) {
        //         $query->where('slug', 'golden-caramel');
        //     });
        // })->whereDoesntHave('orders', function ($query) {
        //     $query->completed();
        //     $query->rub();
        //     $query->whereHas('product', function ($query) {
        //         $query->where('type', 'bundle');
        //     });
        // })->get();

        $usersWithCompletedOrders = User::has('orders', '=', 1)->whereHas('orders', function ($query) {
            $query->completed();
            $query->rub();
            $query->whereHas('product', function ($query) {
                $query->where('slug', 'golden-caramel');
            });
        })->get();

        $emailCampaign = EmailCampaign::find($request->input('id'));

        foreach ($usersWithCompletedOrders as $user) {
            try {
                Subscription::create([
                    'email_campaign_id' => $emailCampaign->id,
                    'user_id'           => $user->id,
                ]);
            } catch (QueryException $e) {
                Log::info($e);
            }
        }

        // $emailCampaign = EmailCampaign::firstWhere('title', 'golden caramel акция');
        return response()->json();
    }



    public function goldenCaramelPromotionSend(Request $request)
    {
        if (Gate::denies('show-support')) {
            return abort(403);
        }

        $emailCampaign = EmailCampaign::with('subscriptions')
            ->with('subscriptions.user')
            ->with('subscriptions.user.orders.product')
            ->find($request->input('id'));

        foreach ($emailCampaign->subscriptions as $subscription) {
            ProcessGoldenCaramelPromotion::dispatch($subscription);
        }


        // $promotions = [];
        // $discounts = [];
        // $downoloadables = [];

        // foreach ($emailCampaign->subscriptions as $subscription) {
        //     $user = $subscription->user;
        //     $product = $subscription->user->orders->first()->product;
        //     $productPromo = $product->related_promotions->first();

        //     array_push($promotions, [
        //         'id' => Str::uuid()->toString(),
        //         'user_id'    => $user->id,
        //         'product_id' => $productPromo->id,
        //         'available_until' => Carbon::now()->addDays(1),
        //         'created_at' => Carbon::now(),
        //         'updated_at' => Carbon::now(),
        //     ]);

        //     array_push($discounts, [
        //         'id' => Str::uuid()->toString(),
        //         'user_id'    => $user->id,
        //         'product_id' => $productPromo->id,
        //         'rub_price'  => 2300,
        //         'usd_price'  => $product->usd_price_discount,
        //         'created_at' => Carbon::now(),
        //         'updated_at' => Carbon::now(),
        //     ]);

        //     array_push($downoloadables, [
        //         'id' => Str::uuid()->toString(),
        //         'user_id'    => $user->id,
        //         'product_id' => $product->id,
        //         'token' => Str::random(64),
        //         'created_at' => Carbon::now(),
        //         'updated_at' => Carbon::now(),
        //     ]);

        //     $subscription->is_mail_send = true;
        //     $subscription->save();
        // }

        // Promotion::insert($promotions);
        // Discount::insert($discounts);
        // Downloadable::insert($downoloadables);
    }

    public function addDiscountForUsersWithFreePresetPromotionShow(Request $request)
    {
        if (Gate::denies('show-support')) {
            return abort(403);
        }

        $this->validate($request, [
            'order_status' => 'exists:orders,status',
        ]);

        $emailCampaign = EmailCampaign::with('subscriptions')
            // ->with('subscriptions.user')
            // ->with('subscriptions.user.promotions')
            // ->with('subscriptions.user.promotions.product')
            // ->with('subscriptions.user.orders')
            // ->with('subscriptions.user.orders.product')
            // ->with('subscriptions.user.discounts')
            // ->with('subscriptions.user.gifts')
            // ->with('subscriptions.user.discounts.product')
            // ->with('subscriptions.user.downloadables')
            ->firstWhere('title', 'Распродажа пресетов до 29 июля + дополнительная скидка');

        $subscriptions = $emailCampaign->subscriptions()
            ->when(request()->has('order_status'), function ($query) use ($request) {
                $query->whereHas('user.orders', function ($query) use ($request) {
                    $query->where('status', $request->input('order_status'));
                });
            })
            ->with('user')
            ->with('user.promotions')
            ->with('user.promotions.product')
            ->with('user.orders')
            ->with('user.orders.product')
            ->with('user.discounts')
            ->with('user.gifts')
            ->with('user.discounts.product')
            ->with('user.downloadables')
            ->paginate(5);

        return view('admin.panel.email_campaigns.add_discount_for_users_with_free_preset_promotion.show', compact(['emailCampaign', 'subscriptions']));
    }

    public function addDiscountForUsersWithFreePresetPromotionSubscribe(Request $request)
    {
        if (Gate::denies('show-support')) {
            return abort(403);
        }

        $usersWithoutOrders = User::whereHas('gifts', function ($query) {
            $query->whereHas('user.orders', function ($query) {
                $query->processing();
            })->whereDoesntHave('user.orders', function ($query) {
                $query->completed();
            })->orWhereDoesntHave('user.orders');
        })->get();

        $emailCampaign = EmailCampaign::find($request->input('id'));

        $subscriptions = [];
        foreach ($usersWithoutOrders as $user) {
            // try {
            //     Subscription::create([
            //         'email_campaign_id' => $emailCampaign->id,
            //         'user_id'           => $user->id,
            //     ]);
            // } catch (QueryException $e) {
            //     Log::info($e);
            // }

            array_push($subscriptions, [
                'id' => Str::uuid()->toString(),
                'email_campaign_id' => $emailCampaign->id,
                'user_id'           => $user->id,
                'is_mail_send'           => false,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ]);
        }

        Subscription::insert($subscriptions);
    }

    public function addDiscountForUsersWithFreePresetPromotionSend(Request $request)
    {
        if (Gate::denies('show-support')) {
            return abort(403);
        }

        $emailCampaign = EmailCampaign::with('subscriptions')
            ->with('subscriptions.user')
            ->with('subscriptions.user.orders.product')
            ->find($request->input('id'));

        foreach ($emailCampaign->subscriptions as $subscription) {
            ProcessAddDiscountForUsersWithFreePresetPromotion::dispatch($subscription);
        }
    }

    public function sendInstagramEmail(Mailer $mailer)
    {
        if (Gate::denies('show-support')) {
            return abort(403);
        }

        $emails = [];

        $users = User::all();
        Log::info('Users count - ' . count($users));

        foreach ($users as $user) {
            ProcessSeindingEmailWithInstagram::dispatch($user->email);
        }



        return response()->json('success', 200);
    }

    public function unsubscribe(EmailCampaign $emailCampaign)
    {
        if (Gate::denies('show-support')) {
            return abort(403);
        }

        // $emailCampaign = EmailCampaign::find($request->input('id'));
        $emailCampaign->users()->detach();

        return redirect()->back();
    }
}
