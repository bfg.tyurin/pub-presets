<?php


namespace App\Http\Controllers\Admin\Panel;


use App\Http\Controllers\Controller;
use App\EmailCampaign;
use App\Product;
use App\Promotion;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Log;

class CreatedMailingPromotionsController extends Controller
{

    public function store(Request $request)
    {
        if (Gate::allows('show-panel')) {
//            $createPromotionForUsersWithDuration->execute($getMailingUsersbyId->execute(), 2);

            $mailing = EmailCampaign::with(
                [
                    'users' => function($query) {
                        $query->whereHas('orders', function ($query) {
                            $query->rub();
                            $query->completed();

                            $query->whereHas('product', function ($query) {
                                $query->pack();
                            });
                        });
                    },
                    'users.orders',
                    'users.orders.product'
                ]
            )->findOrFail(request('id'));
            $users = $mailing->users->dd();

            foreach ($users as $user) {

                foreach ($user->orders as $order) {
                    $product = $order->product;
                    if ('pack' === $product->type) {
                        $user->promotions()->save(Promotion::create([
                            'user_id' => $user->id,
                            'product_id' => $product->related_promotions()->first()->id,
                        ]));
                    }
                }



//                foreach($user->orders as $order) {
//                    $product = Product::find($order->product_id);
//                    if ('pack' === $product->type) {
////                        $user->promotions()->save(Promotion::create([
////                            'user_id'      => $user->id,
////                            'product_id' => $product->first()->id,
////                        ]));
////                        $user->promotions()->latest()->first()->delete();
//                    }
//                }
            }

            return redirect()->back();
        }

        return abort(404);
    }
}
