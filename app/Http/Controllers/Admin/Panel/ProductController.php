<?php


namespace App\Http\Controllers\Admin\Panel;

use App\Product;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Gate;

class ProductController extends Controller
{

    public function index()
    {
        if (Gate::denies('show-panel')) {
            return abort(403);
        }

        $products = Product::paginate();
        return view('admin.panel.product.index', compact('products'));
    }

    public function update(Request $request, Product $product)
    {
        if (Gate::denies('show-panel')) {
            return abort(403);
        }

        $data = $this->validate($request, [
            'title' => 'required|unique:products,title,' . $product->id,
            'price_discount' => 'required|integer|gt:0',
            'usd_price_discount' => 'required|integer|gt:0',
        ]);

        $product->fill($data);
        $product->save();

        return redirect()
            ->route('admin.panel.products.index');
    }

    public function edit(Product $product)
    {
        if (Gate::denies('show-panel')) {
            return abort(403);
        }

        // $article = Product::findOrFail($id);
        return view('admin.panel.product.edit', compact('product'));
    }
}
