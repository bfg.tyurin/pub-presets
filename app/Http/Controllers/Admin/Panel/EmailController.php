<?php


namespace App\Http\Controllers\Admin\Panel;

use Exception;

use Illuminate\Support\Facades\App;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Gate;


class EmailController extends Controller
{
    public function viewPurchase()
    {
        if (Gate::denies('show-panel')) {
            return abort(403);
        }

        return view('mail.purchase');
    }

    public function viewProfileCreated()
    {
        if (Gate::denies('show-panel')) {
            return abort(403);
        }

        App::setLocale('ru');

        return view('mail.profile_was_created');
    }
}
