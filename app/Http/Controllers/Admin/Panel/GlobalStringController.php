<?php

namespace App\Http\Controllers\Admin\Panel;

use App\GlobalString;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Gate;

class GlobalStringController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (Gate::denies('show-panel')) {
            return abort(403);
        }

        $globalStrings = GlobalString::paginate();
        return view('admin.panel.global_string.index', compact('globalStrings'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (Gate::denies('show-panel')) {
            return abort(403);
        }

        $globalString = new GlobalString();
        return view('admin.panel.global_string.create', compact('globalString'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (Gate::denies('show-panel')) {
            return abort(403);
        }

        $data = $this->validate($request, [
            'key' => 'required|unique:global_strings',
            'ru' => 'required',
            'en' => 'required',
        ]);
        $globalString = new GlobalString();
        // Заполнение статьи данными из формы
        $globalString->fill($data);
        // При ошибках сохранения возникнет исключение
        $globalString->save();

        // Редирект на указанный маршрут
        return redirect()
            ->route('admin.panel.global_strings.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\GlobalString  $string
     * @return \Illuminate\Http\Response
     */
    public function show(GlobalString $globalString)
    {
        if (Gate::denies('show-panel')) {
            return abort(403);
        }
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\GlobalString  $string
     * @return \Illuminate\Http\Response
     */
    public function edit(GlobalString $globalString)
    {
        if (Gate::denies('show-panel')) {
            return abort(403);
        }

        return view('admin.panel.global_string.edit', compact('globalString'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\GlobalString  $string
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, GlobalString $globalString)
    {
        if (Gate::denies('show-panel')) {
            return abort(403);
        }
        // dd($globalString);
        $globalString->fill($request->all());
        $globalString->save();
        return redirect()
            ->route('admin.panel.global_strings.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\GlobalString  $string
     * @return \Illuminate\Http\Response
     */
    public function destroy(GlobalString $globalString)
    {
        if (Gate::denies('show-panel')) {
            return abort(403);
        }

        if ($globalString) {
            $globalString->delete();
        }

        return redirect()->route('admin.panel.global_strings.index');
    }
}
