<?php


namespace App\Http\Controllers\Admin\Panel;


use App\Content;
use App\FaqItem;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Log;
use App\Http\Controllers\Controller;

class FaqItemController extends Controller
{
    public function index()
    {
        if (Gate::allows('show-support')) {
            $items = FaqItem::orderBy('order')->get();
            return view('admin.panel.faq-items.index', [
                'items' =>$items
            ]);
        }
        return abort(404);
    }

    public function create()
    {
        if (Gate::allows('show-support')) {
            return view('admin.panel.faq-items.create');
        }
        return abort(404);
    }

    public function store(Request $request)
    {
        if (Gate::allows('show-support')) {
            $this->validate($request, [
                'question_ru' => 'required',
                'question_en' => 'required',
                'answer_ru'   => 'required',
                'answer_en'   => 'required',
                'order'       => 'required',
            ]);
            FaqItem::create($request->all());
            return redirect()->back()->with('success', 'Create Successfully');
        }
        return abort(404);
    }

    public function edit(FaqItem $faqItem)
    {
        if (Gate::allows('show-support')) {
            return view('admin.panel.faq-items.edit', [
                'item' =>$faqItem
            ]);
        }
        return abort(404);
    }

    public function update(Request $request, FaqItem $faqItem)
    {

        if (Gate::allows('show-support')) {
            $this->validate($request, [
                'question_ru' => 'required',
                'question_en' => 'required',
                'answer_ru'   => 'required',
                'answer_en'   => 'required',
                'order'       => 'required',
            ]);
            $faqItem->update($request->all());

            return redirect()->back()->with('success', 'Update Successfully');
        }
        return abort(404);
    }
}
