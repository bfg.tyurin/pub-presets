<?php

namespace App\Http\Controllers;

use ZipArchive;
use App\Downloadable;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;

class ZipController extends Controller
{
    public function show(Request $request)
    {
        // Log::info('file name: ', ['fileName' => $request]);
        $downloadable = Downloadable::validFromToken($request->token);
        if (!$downloadable) {

            Log::info('ZipController: No downloadable for this token, redirecting');

            return redirect()->back();
        }
        $products = $downloadable->order->products()->where('category', $request->category)->get();

        $zipFile  = 'temp/' . 'Zhenya_Swan_presets_' . $request->category . '_' . Str::random(10) .  '.zip';
        $zip = new ZipArchive;
        if ($zip->open(Storage::disk('private')->path($zipFile), ZipArchive::CREATE | ZipArchive::OVERWRITE)) {
            foreach ($products as $product) {
                if (Storage::disk('private')->exists($product->getFileName($product))) {
                    $fileName = $product->getFileName($product);
                    Log::info('file name: ', ['fileName' => $fileName]);
                    Log::info('path of zip file: ', ['path' => Storage::disk('private')->path($zipFile)]);
                    Log::info('file name exists file? : ' . Storage::disk('private')->exists($product->getFileName($product)) ? 'true' : 'false');
                    $result = $zip->addFile(Storage::disk('private')->path($fileName), $product->title . '.zip');
                    Log::info('file added? : ', ['result' => $result ? 'true' : 'false']);
                }
            }
        }


        $zip->close();

        // We return the file immediately after download
        return response()->download(Storage::disk('private')->path($zipFile))->deleteFileAfterSend(true);
    }
}
