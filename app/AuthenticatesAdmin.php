<?php


namespace App;


use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Validation\ValidationException;


class AuthenticatesAdmin
{
    use ValidatesRequests;

    protected Request $request;

    private Mailer $mailer;

    public function __construct(Request $request, Mailer $mailer)
    {
        $this->mailer = $mailer;
        $this->request = $request;
    }

    public function invite()
    {
        $this->validateRequest();
        $token = $this->createToken();
        $this->send($token);
    }
    private function validateRequest()
    {
        $this->validate($this->request, [
            'email' => 'required|email|exists:admins'
        ]);
    }

    private function createToken()
    {
        $admin = Admin::byEmail($this->request->email);
        $loginToken = LoginToken::generateFor($admin);

        Log::info('Generate token for email', ['email' => $admin->email]);

        return $loginToken;
    }

    protected function send($token)
    {
        $admin = Admin::findOrFail($token->admin_id);
        $this->mailer->sendByEmail($admin->email,  new Content('Ссылка для входа в Пресеты', route('admin.token', $token->token)));
        Log::info('Sending email to admin', ['email' => $admin->email]);
    }

    public function login(LoginToken $token)
    {
        Auth::login($token->admin, true);
//        $token->delete();
    }
}
