<?php

namespace App;

use App\Gift;
use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    protected $primaryKey = 'id';

    protected $keyType = 'string';

    public $incrementing = false;

    protected static function boot()
    {
        parent::boot();

        static::creating(function (Model $model) {
            $model->setAttribute($model->getKeyName(), Str::uuid()->toString());
        });
    }

    public function profile()
    {
        return $this->hasOne(Profile::class);
    }

    public function orders()
    {
        return $this->hasMany(Order::class);
    }

    public function completedOrders()
    {
        return $this->hasMany(Order::class)->completed();
    }

    public function campaign()
    {
        return $this->belongsTo(Campaign::class);
    }

    public function downloadables()
    {
        return $this->hasMany(Downloadable::class);
    }

    public function email_campaigns()
    {
        return $this->belongsToMany(EmailCampaign::class, 'subscriptions')
            ->using(Subscription::class)
            ->withPivot('is_mail_send')
            ->withTimestamps();
    }

    public function promotions()
    {
        return $this->hasMany(Promotion::class);
    }

    public function discounts()
    {
        return $this->hasMany(Discount::class);
    }

    public function gifts()
    {
        return $this->hasMany(Gift::class);
    }

    /**
     * @param $productId
     * @return Model|\Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function discountByProductId($productId)
    {
        return $this->hasMany(Discount::class)->firstWhere('product_id', $productId);
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['email'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function isAdmin()
    {
        return false;
    }

    public function isSuperAdmin()
    {
        return false;
    }

    public function scopeWithoutUtm()
    {
        return User::doesntHave('campaign');
    }
}
