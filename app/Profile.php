<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Profile extends Model
{
    protected $primaryKey = 'id';

    protected $keyType = 'string';

    public $incrementing = false;

    protected static function boot()
    {
        parent::boot();

        static::creating(function (Model $model) {
            $model->setAttribute($model->getKeyName(), Str::uuid()->toString());
        });
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        // имя, фамилия, ник, гендер, номер, профессия, причина
        'user_id', 'name', 'surname', 'gender', 'instagram', 'number', 'profession', 'reason'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
