<?php

namespace App\View\Components;

use App\Pack;
use Illuminate\View\Component;

class ShoppingCartBlock extends Component
{
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|string
     */
    public function render()
    {
        return view('components.shopping-cart-block');
    }
}
