<?php

namespace App\View\Components;

use App\Order;
use Illuminate\View\Component;

class AdminTotalsComponent extends Component
{

    public $service;
    public $count;
    public $avg;
    public $total;
    public $totalWithTax;
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($service)
    {
        $this->count = $service->getCount();
        $this->avg = $service->getAvgAmount();
        $this->total = $service->getTotalAmount();
        $this->totalWithTax = $service->getTotalAmountWithTax();
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|string
     */
    public function render()
    {
        // Всего заказов
        // Средний чек
        // Выручка
        // Выручка с вычетом налогов
        // $orders = Order::completed()->rub()->count();
        return view('components.admin-totals-component');
    }
}
