down:
	php artisan down --message="Скоро вернемся"

up:
	php artisan up

install:
	composer install --optimize-autoloader --no-dev
	npm run prod

cache:
	php artisan cache:clear
	php artisan config:cache
	php artisan view:cache
