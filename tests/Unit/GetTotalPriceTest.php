<?php

namespace Tests\Unit;

use App\Discount;
use App\Product;
use App\Services\GetTotalPrice;
use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\App;
use Tests\TestCase;

class GetTotalPriceTest extends TestCase
{
    use WithFaker, RefreshDatabase;

    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testGetRubTotalPriceWithDiscount()
    {
        $user = factory(User::class)->create();
        $product = factory(Product::class)->create();
        $discount = factory(Discount::class)->create(['user_id' => $user->id, 'product_id' => $product->id]);
        $locale = 'ru';
        App::setLocale($locale);

        $expectedPrice = $discount->rub_price;
        $totalPrice = (new GetTotalPrice())->call($user, $product, $locale);
        $this->assertEquals($expectedPrice, $totalPrice);
    }

    public function testGetRubTotalPriceWithoutDiscount()
    {
        $this->withoutExceptionHandling();

        $user = factory(User::class)->create();
        $product = factory(Product::class)->create();
        $locale = 'ru';
        App::setLocale($locale);

        $expectedPrice = $product->price_discount;
        $totalPrice = (new GetTotalPrice())->call($user, $product, $locale);
        $this->assertEquals($expectedPrice, $totalPrice);
    }

    public function testGetUsdTotalPriceWithDiscount()
    {
        $user = factory(User::class)->create();
        $product = factory(Product::class)->create();
        $discount = factory(Discount::class)->create(['user_id' => $user->id, 'product_id' => $product->id]);
        $locale = 'en';
        App::setLocale($locale);

        $expectedPrice = $discount->usd_price;
        $totalPrice = (new GetTotalPrice())->call($user, $product, $locale);
        $this->assertEquals($expectedPrice, $totalPrice);
    }

    public function testGetUsdTotalPriceWithoutDiscount()
    {
        $this->withoutExceptionHandling();

        $user = factory(User::class)->create();
        $product = factory(Product::class)->create();
        $locale = 'en';
        App::setLocale($locale);

        $expectedPrice = $product->usd_price_discount;
        $totalPrice = (new GetTotalPrice())->call($user, $product, $locale);
        $this->assertEquals($expectedPrice, $totalPrice);
    }

    public function testGetRubTotalPriceWithDiscountOtherProduct()
    {
        $user = factory(User::class)->create();
        $product = factory(Product::class)->create();
        $discount = factory(Discount::class)->create(['user_id' => $user->id, 'product_id' => $product->id]);
        $locale = 'ru';
        App::setLocale($locale);

        $currentProduct = factory(Product::class)->create();

        $expectedPrice = $currentProduct->price_discount;
        $totalPrice = (new GetTotalPrice())->call($user, $currentProduct, $locale);
        $this->assertEquals($expectedPrice, $totalPrice);
    }

    public function testGetUsdTotalPriceWithDiscountOtherProduct()
    {
        $user = factory(User::class)->create();
        $product = factory(Product::class)->create();
        $discount = factory(Discount::class)->create(['user_id' => $user->id, 'product_id' => $product->id]);
        $locale = 'en';
        App::setLocale($locale);

        $currentProduct = factory(Product::class)->create();

        $expectedPrice = $currentProduct->usd_price_discount;
        $totalPrice = (new GetTotalPrice())->call($user, $currentProduct, $locale);
        $this->assertEquals($expectedPrice, $totalPrice);
    }
}
