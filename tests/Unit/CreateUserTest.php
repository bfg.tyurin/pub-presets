<?php

namespace Tests\Unit;

use App\Campaign;
use App\Services\CreateCampaign;
use App\Services\CreateUser;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class CreateUserTest extends TestCase
{
    use WithFaker, RefreshDatabase;

    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function testUserCreatedWithUtm()
    {
        $this->withoutExceptionHandling();

        $fieldsCampaign = [
            'utm_source'   => $this->faker->sentence,
            'utm_campaign' => $this->faker->sentence,
            'utm_medium'   => $this->faker->sentence,
            'utm_term'     => $this->faker->sentence,
            'utm_content'  => $this->faker->sentence,
        ];
        $json = json_encode($fieldsCampaign);

        $fields = [
            'email' => $this->faker->safeEmail,
        ];
        $createUser = new CreateUser(new CreateCampaign());
        $user = $createUser->call($fields['email'], $json);

        $this->assertDatabaseHas('users', $fields);
        $this->assertDatabaseHas('campaigns', $fieldsCampaign);
        $this->assertInstanceOf(Campaign::class, $user->campaign);
        $this->assertEquals($user->campaign->utm_source, $fieldsCampaign['utm_source']);
        $this->assertEquals($user->campaign->utm_campaign, $fieldsCampaign['utm_campaign']);
        $this->assertEquals($user->campaign->utm_medium, $fieldsCampaign['utm_medium']);
        $this->assertEquals($user->campaign->utm_term, $fieldsCampaign['utm_term']);
        $this->assertEquals($user->campaign->utm_content, $fieldsCampaign['utm_content']);

        $this->assertNotNull($user->campaign_id);
    }

    public function testUserCreatedWithoutUtmWithEmptyStringUtm()
    {
        $this->withoutExceptionHandling();

        $fields = [
            'email' => $this->faker->safeEmail,
        ];
        $createUser = new CreateUser(new CreateCampaign());
        $user = $createUser->call($fields['email'], '');

        $this->assertDatabaseHas('users', $fields);
        $this->assertNotInstanceOf(Campaign::class, $user->campaign);
        $this->assertNull($user->campaign);
        $this->assertNull($user->campaign_id);

    }

    public function testUserCreatedWithoutUtm()
    {
        $this->withoutExceptionHandling();
        $fields = [
            'email' => $this->faker->safeEmail,
        ];
        $createUser = new CreateUser(new CreateCampaign());
        $createUser->call($fields['email']);

        $this->assertDatabaseHas('users', $fields);
    }
}
