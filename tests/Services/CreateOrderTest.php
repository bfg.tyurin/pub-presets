<?php

namespace Tests\Services;

use App\Order;
use App\Product;
use App\Services\CreateOrder;
use App\Services\Dto\CreateOrderDto;
use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\App;
use Mcamara\LaravelLocalization\LaravelLocalization;
use Tests\TestCase;

/**
 * @method withoutExceptionHandling()
 */
class CreateOrderTest extends TestCase
{
    use WithFaker, RefreshDatabase;

    protected function refreshApplicationWithLocale($locale)
    {
        self::tearDown();
        putenv(LaravelLocalization::ENV_ROUTE_KEY . '=' . $locale);
        self::setUp();
    }

    protected function tearDown() : void
    {
        putenv(LaravelLocalization::ENV_ROUTE_KEY);
        parent::tearDown();
    }

    public function testOrderCanBeCreatedWithRuLocale()
    {
        $this->refreshApplicationWithLocale('ru');
        $this->withoutExceptionHandling();

        $user = factory(User::class)->create();
        $product = factory(Product::class)->create();
        $attributes = [
            'user_id'      => $user->id,
            'product_id'   => $product->id,
            'payment_type' => Order::PAYMENT_TYPE_CLOUDPAYMENTS,
            'locale'       => App::getLocale(),

        ];

        $fillable = [
            'user_id' => $user->id,
            'product_id' => $product->id,
            'status' => Order::STATUS_PROCESSING,
            'payment_type' => Order::PAYMENT_TYPE_CLOUDPAYMENTS,
            'currency' => Order::CURRENCY_RUB,
        ];

        $order = (new CreateOrder)->call(new CreateOrderDto(
            $attributes['user_id'],
            $attributes['product_id'],
            $attributes['payment_type'],
            $attributes['locale'],
        ));
        $this->assertDatabaseHas('orders', $fillable);
    }

    public function testOrderCanBeCreatedWithEnLocale()
    {
        $this->refreshApplicationWithLocale('en');
        $this->withoutExceptionHandling();

        $user = factory(User::class)->create();
        $product = factory(Product::class)->create();
        $attributes = [
            'user_id'      => $user->id,
            'product_id'   => $product->id,
            'payment_type' => Order::PAYMENT_TYPE_CLOUDPAYMENTS,
            'locale'       => App::getLocale(),

        ];

        $fillable = [
            'user_id' => $user->id,
            'product_id' => $product->id,
            'status' => Order::STATUS_PROCESSING,
            'payment_type' => Order::PAYMENT_TYPE_CLOUDPAYMENTS,
            'currency' => Order::CURRENCY_USD,
        ];

        $order = (new CreateOrder)->call(new CreateOrderDto(
            $attributes['user_id'],
            $attributes['product_id'],
            $attributes['payment_type'],
            $attributes['locale'],
        ));
        $this->assertDatabaseHas('orders', $fillable);
    }

    public function testOrderCanBeCreatedWithOtherLocale()
    {
        $this->refreshApplicationWithLocale('sp');
        $this->withoutExceptionHandling();

        $user = factory(User::class)->create();
        $product = factory(Product::class)->create();
        $attributes = [
            'user_id'      => $user->id,
            'product_id'   => $product->id,
            'payment_type' => Order::PAYMENT_TYPE_CLOUDPAYMENTS,
            'locale'       => App::getLocale(),

        ];

        $fillable = [
            'user_id' => $user->id,
            'product_id' => $product->id,
            'status' => Order::STATUS_PROCESSING,
            'payment_type' => Order::PAYMENT_TYPE_CLOUDPAYMENTS,
            'currency' => Order::CURRENCY_USD,
        ];

        $order = (new CreateOrder)->call(new CreateOrderDto(
            $attributes['user_id'],
            $attributes['product_id'],
            $attributes['payment_type'],
            $attributes['locale'],
        ));
        $this->assertDatabaseHas('orders', $fillable);
    }
}
