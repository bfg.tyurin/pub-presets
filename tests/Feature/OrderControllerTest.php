<?php

namespace Tests\Feature;

use App\User;
use App\Order;
use App\Product;
use App\Discount;
use Tests\TestCase;
use Illuminate\Support\Facades\App;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Mcamara\LaravelLocalization\LaravelLocalization;

class OrderControllerTest extends TestCase
{
    use WithFaker, RefreshDatabase;

    public function setUp(): void
    {
        parent::setUp();
        // $this->artisan('db:seed');
    }

    protected function refreshApplicationWithLocale($locale)
    {
        self::tearDown();
        putenv(LaravelLocalization::ENV_ROUTE_KEY . '=' . $locale);
        self::setUp();
    }

    protected function tearDown(): void
    {
        putenv(LaravelLocalization::ENV_ROUTE_KEY);
        parent::tearDown();
    }

    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_user_can_create_order_with_2_products_with_currency_rub()
    {
        $this->refreshApplicationWithLocale('ru');
        $this->withoutExceptionHandling();

        $email = $this->faker->safeEmail;
        $product1 = factory(Product::class)->create();
        $product2 = factory(Product::class)->create();

        $currentDiscount = 20;
        \Cart::add(
            $product1->id,
            $product1->title,
            App::getLocale() === 'ru' ? $product1->price_discount : $product1->usd_price_discount,
            1,
            []
        )->associate($product1);
        \Cart::add(
            $product2->id,
            $product2->title,
            App::getLocale() === 'ru' ? $product2->price_discount : $product2->usd_price_discount,
            1,
            []
        )->associate($product2);
        $saleCondition = new \Darryldecode\Cart\CartCondition(array(
            'name' => 'SALE 20%',
            'type' => 'tax',
            'target' => 'total',
            'value' => '-20%',
        ));
        \Cart::condition($saleCondition);

        $attributes = [
            'user_email'   => $email,
            'payment_type' => Order::PAYMENT_TYPE_CLOUDPAYMENTS,
        ];

        $url = route('orders.store');
        $response = $this->post($url, $attributes);


        $currentDiscountMultiplier = (100 - $currentDiscount) / 100;
        $totalWithDiscount = number_format(($product1->price_discount + $product2->price_discount) * $currentDiscountMultiplier, 0, '', '');

        $this->assertDatabaseHas('users', ['email' => $email]);
        $user = User::firstWhere('email', $email);
        $responceJson = $response->assertStatus(200)->assertJsonFragment([
            'user_id'      => $user->id,
            // 'products'     => json_encode([['id' => $product1->id], ['id' => $product2->id]]),
            'status'       => Order::STATUS_PROCESSING,
            'total'        => $totalWithDiscount,
            'payment_type' => Order::PAYMENT_TYPE_CLOUDPAYMENTS,
            'currency'     => Order::CURRENCY_RUB,
        ]);
        $fields = [
            'user_id'      => $user->id,
            'status'       => Order::STATUS_PROCESSING,
            'total'        => $totalWithDiscount,
            'payment_type' => Order::PAYMENT_TYPE_CLOUDPAYMENTS,
            'currency'     => Order::CURRENCY_RUB,
        ];

        $this->assertDatabaseHas('orders', $fields);
    }

    /**
     * A basic feature test example.
     *
     * @return void
     */

    public function test_user_can_create_order_with_currency_usd()
    {
        $this->refreshApplicationWithLocale('en');
        $this->withoutExceptionHandling();

        $email = $this->faker->safeEmail;
        $product = factory(Product::class)->create();

        $attributes = [
            'user_email'   => $email,
            'product_id'   => $product->id,
            'payment_type' => Order::PAYMENT_TYPE_CLOUDPAYMENTS,
        ];

        $url = route('orders.store');

        $response = $this->post($url, $attributes);

        $this->assertDatabaseHas('users', ['email' => $email]);

        $user = User::firstWhere('email', $email);

        $response->assertStatus(201)->assertJson([
            'user_id'      => $user->id,
            'product_id'   => $product->id,
            'status'       => Order::STATUS_PROCESSING,
            'total'        => $product->usd_price_discount,
            'payment_type' => Order::PAYMENT_TYPE_CLOUDPAYMENTS,
            'currency'     => Order::CURRENCY_USD,
            'user' => ['email' => $email],
            'product' => ['title' => $product->title],
        ]);

        $fields = [
            'user_id'      => $user->id,
            'product_id'   => $product->id,
            'status'       => Order::STATUS_PROCESSING,
            'total'        => $product->usd_price_discount,
            'payment_type' => Order::PAYMENT_TYPE_CLOUDPAYMENTS,
            'currency'     => Order::CURRENCY_USD,
        ];

        $this->assertDatabaseHas('orders', $fields);
    }

    public function test_order_requires_user_email()
    {
        $this->refreshApplicationWithLocale('ru');

        $attributes = [
            'user_email' => '',
            'product_id' => $this->faker->uuid,
            'payment_type'  => Order::PAYMENT_TYPE_CLOUDPAYMENTS,
        ];

        $this->post(route('orders.store'), $attributes)->assertSessionHasErrors('user_email');
    }

    public function test_order_requires_product_id()
    {
        $this->refreshApplicationWithLocale('ru');

        $attributes = [
            'user_email' => $this->faker->safeEmail,
            'product_id' => '',
            'payment_type'  => Order::PAYMENT_TYPE_CLOUDPAYMENTS,
        ];

        $this->post(route('orders.store'), $attributes)->assertSessionHasErrors('product_id');
    }

    public function test_order_requires_existing_product_id()
    {
        $this->refreshApplicationWithLocale('ru');

        $attributes = [
            'user_email' => $this->faker->safeEmail,
            'product_id' => $this->faker->uuid,
            'payment_type'  => Order::PAYMENT_TYPE_CLOUDPAYMENTS,
        ];

        $this->post(route('orders.store'), $attributes)->assertSessionHasErrors('product_id');
    }

    public function test_order_requires_payment_type()
    {
        $this->refreshApplicationWithLocale('ru');

        $attributes = [
            'user_email' => $this->faker->safeEmail,
            'product_id' => $this->faker->uuid,
            'payment_type'  => '',
        ];

        $this->post(route('orders.store'), $attributes)->assertSessionHasErrors('payment_type');
    }

    public function test_order_requires_existing_payment_type()
    {
        $this->refreshApplicationWithLocale('ru');

        $attributes = [
            'user_email' => $this->faker->safeEmail,
            'product_id' => $this->faker->uuid,
            'payment_type'  => $this->faker->sentence(),
        ];

        $this->post(route('orders.store'), $attributes)->assertSessionHasErrors('payment_type');
    }

    public function test_ru_user_have_discount()
    {
        $this->refreshApplicationWithLocale('ru');

        $product = factory(Product::class)->create();
        $user = factory(User::class)->create();
        $discount = factory(Discount::class)->create(['user_id' => $user->id, 'product_id' => $product->id]);

        $attributes = [
            'user_email' => $user->email,
            'product_id' => $product->id,
            'payment_type'  => Order::PAYMENT_TYPE_CLOUDPAYMENTS,
        ];

        $this->post(route('orders.store'), $attributes)
            ->assertSee($user->email)
            ->assertSee($product->title)
            ->assertSee($discount->rub_price);
        $this->assertDatabaseHas('orders', [
            'product_id' => $product->id,
            'currency' => Order::CURRENCY_RUB,
            'total' => $discount->rub_price
        ]);
    }
}
