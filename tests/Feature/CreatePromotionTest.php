<?php

namespace Tests\Feature;

use App\User;
use App\Product;
use Carbon\Carbon;
use Tests\TestCase;
use App\Services\CreatePromotion;
use App\Services\Dto\CreatePromotionDto;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CreatePromotionTest extends TestCase
{
    use WithFaker, RefreshDatabase;
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_create_promotion_for_pack()
    {
        $product = factory(Product::class)->create(['type' => 'pack']);
        $promoProduct = factory(Product::class)->create(['type' => 'promotion']);

        $product->related_promotions()->attach($promoProduct->id);

        $user = factory(User::class)->create();
        $this->assertFalse($user->promotions()->exists());

        $createPromotionDto = new CreatePromotionDto($user->id, $product->id, 2);
        $createPromotion = new CreatePromotion();
        $createPromotion->call($createPromotionDto);

        // where('available_until', '>=', Carbon::now())
        // 'available_until' => Carbon::parse('+' . $dto->getDays() . ' days'),

        // dd($user->promotions()->latest()->first());
        $this->assertTrue($user->promotions()->exists());
        $this->assertDatabaseHas('promotions', ['user_id' => $user->id, 'product_id' => $promoProduct->id]);

        $userPromo = $user->promotions()->latest()->first();
        $this->assertEquals($userPromo->product_id, $promoProduct->id);
        $this->assertTrue(Carbon::parse($userPromo->available_until)->isFuture());
    }

    public function test_not_create_promotion_for_product_with_has_not_related_products()
    {
        $product = factory(Product::class)->create(['type' => 'bundle']);


        $user = factory(User::class)->create();

        $createPromotionDto = new CreatePromotionDto($user->id, $product->id, 2);
        $createPromotion = new CreatePromotion();
        $createPromotion->call($createPromotionDto);

        $this->assertFalse($user->promotions()->exists());
    }
}
