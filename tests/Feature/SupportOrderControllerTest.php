<?php

namespace Tests\Feature;

use Mockery;
use App\User;
use App\Admin;
use App\Order;
use App\Mailer;
use Tests\TestCase;
use Illuminate\Support\Str;
use App\Events\OrderWasCompleted;
use Illuminate\Support\Facades\Event;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class SupportOrderControllerTest extends TestCase
{
    use WithFaker, RefreshDatabase;
    /**
     * A basic feature test example.
     *
     * @return void
     */

    public function test_edit_not_user_get_302()
    {
        $user = factory(User::class)->make();
        $order = factory(Order::class)->create();

        $responce = $this->get(route('support.dashboard.orders.edit', $order));
        $responce->assertStatus(302);
    }

    public function test_edit_not_support_get_403()
    {
        $admin = Admin::create(['email' => 'test@example.com']);
        $user = factory(User::class)->make();
        $order = factory(Order::class)->create();

        $responce = $this->actingAs($admin, 'web')->get(route('support.dashboard.orders.edit', $order));
        $responce->assertStatus(403);
    }

    public function test_edit_support_see_user_email()
    {
        $admin = Admin::create(['email' => 'nebuhad@yandex.ru']);
        $order = factory(Order::class)->create();

        $responce = $this->actingAs($admin, 'web')->get(route('support.dashboard.orders.edit', $order));
        $responce->assertSee($order->user->email);
    }

    public function test_edit_support_see_user_product_title()
    {
        $admin = Admin::create(['email' => 'nebuhad@yandex.ru']);
        $order = factory(Order::class)->create();

        $responce = $this->actingAs($admin, 'web')->get(route('support.dashboard.orders.edit', $order));
        $responce->assertSee($order->product->title);
    }

    public function test_edit_support_see_user_product_price()
    {
        $admin = Admin::create(['email' => 'nebuhad@yandex.ru']);
        $order = factory(Order::class)->create();

        $responce = $this->actingAs($admin, 'web')->get(route('support.dashboard.orders.edit', $order));
        $responce->assertSee($order->total);
    }

    public function test_edit_support_see_user_order_currency()
    {
        $admin = Admin::create(['email' => 'nebuhad@yandex.ru']);
        $order = factory(Order::class)->create();

        $responce = $this->actingAs($admin, 'web')->get(route('support.dashboard.orders.edit', $order));
        $responce->assertSee($order->currency);
    }


    public function test_update_support_can_change_order_price()
    {
        $this->withoutExceptionHandling();

        $admin = Admin::create(['email' => 'nebuhad@yandex.ru']);
        $order = factory(Order::class)->create();

        $attributes = [
            'email'      => $order->user->email,
            'product_id' => $order->product_id,
            'currency'   => $order->currency,
            'total'      => 999,
        ];


        $responce = $this->actingAs($admin, 'web')->patch(route('support.dashboard.orders.update', $order), $attributes);

        $this->assertDatabaseHas('orders', ['id' => $order->id, 'total' => 999]);
    }

    public function test_edit_support_can_change_order_currency()
    {
        $this->withoutExceptionHandling();

        $admin = Admin::create(['email' => 'nebuhad@yandex.ru']);
        $order = factory(Order::class)->create();

        $attributes = [
            'email'      => $order->user->email,
            'product_id' => $order->product_id,
            'currency'   => 'USD',
            'total'      => $order->total,
        ];


        $responce = $this->actingAs($admin, 'web')->patch(route('support.dashboard.orders.update', $order), $attributes);

        $this->assertDatabaseHas('orders', ['id' => $order->id, 'currency' => 'USD',]);
    }

    public function test_edit_support_should_recive_validation_error_total_0()
    {
        $admin = Admin::create(['email' => 'nebuhad@yandex.ru']);
        $order = factory(Order::class)->create();

        $attributes = [
            'email'      => $order->user->email,
            'product_id' => $order->product_id,
            'currency'   => $order->currency,
            'total'      => 0,
        ];


        $responce = $this->actingAs($admin, 'web')->patch(route('support.dashboard.orders.update', $order), $attributes);
        $responce->assertSessionHasErrors(['total']);

        $this->assertDatabaseHas('orders', ['id' => $order->id, 'total' => $order->total,]);
    }

    public function test_edit_support_should_recive_validation_error_total_minus()
    {
        $admin = Admin::create(['email' => 'nebuhad@yandex.ru']);
        $order = factory(Order::class)->create();

        $attributes = [
            'email'      => $order->user->email,
            'product_id' => $order->product_id,
            'currency'   => $order->currency,
            'total'      => -1,
        ];


        $responce = $this->actingAs($admin, 'web')->patch(route('support.dashboard.orders.update', $order), $attributes);
        $responce->assertSessionHasErrors(['total']);

        $this->assertDatabaseHas('orders', ['id' => $order->id, 'total' => $order->total,]);
    }

    public function test_edit_support_can_send_email()
    {
        $this->withoutExceptionHandling();

        Event::fake([
            OrderWasCompleted::class,
        ]);

        $admin = Admin::create(['email' => 'nebuhad@yandex.ru']);
        $order = factory(Order::class)->create();

        $attributes = [
            'email'      => $order->user->email,
            'product_id' => $order->product_id,
            'currency'   => 'USD',
            'total'      => 999,
            'send_email' => 'on',
        ];


        $responce = $this->actingAs($admin, 'web')->patch(route('support.dashboard.orders.update', $order), $attributes);

        Event::assertDispatched(OrderWasCompleted::class);
        $this->assertDatabaseHas('orders', ['id' => $order->id, 'currency' => 'USD',]);
    }
}
