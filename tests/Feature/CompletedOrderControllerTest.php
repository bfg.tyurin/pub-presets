<?php

namespace Tests\Feature;

use App\User;
use App\Order;
use App\Profile;
use Tests\TestCase;
use App\Events\OrderCompleted;
use Illuminate\Support\Facades\Event;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Mcamara\LaravelLocalization\LaravelLocalization;

class CompletedOrderControllerTest extends TestCase
{
    use WithFaker, RefreshDatabase;

    public function setUp(): void
    {
        parent::setUp();
        // $this->artisan('db:seed');
    }

    protected function refreshApplicationWithLocale($locale)
    {
        self::tearDown();
        putenv(LaravelLocalization::ENV_ROUTE_KEY . '=' . $locale);
        self::setUp();
    }

    protected function tearDown(): void
    {
        putenv(LaravelLocalization::ENV_ROUTE_KEY);
        parent::tearDown();
    }
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_can_update_order()
    {
        $this->refreshApplicationWithLocale('ru');

        $user = factory(User::class)->create(['email' => 'test@example.com']);
        $order = factory(Order::class)->create(['user_id' => $user->id]);
        $user = $order->user;

        $attributes = [
            'order_id' => $order->id,
        ];

        Event::fake([
            OrderCompleted::class,
        ]);

        $url = route('completed-orders.store');
        $response = $this->withSession(['email' => $user->email])->json('POST', $url, $attributes);
        $response->assertJsonMissingValidationErrors('status');
        $this->assertDatabaseHas('orders', ['id' => $order->id, 'status' => Order::STATUS_COMPLETED]);
        $this->assertDatabaseHas('downloadables', ['user_id' => $user->id]);
        $this->get($response['url'])->assertStatus(302);
    }

    public function test_can_update_order_user_has_profile()
    {
        $this->refreshApplicationWithLocale('ru');

        $user = factory(User::class)->create(['email' => 'test@example.com']);
        $order = factory(Order::class)->create(['user_id' => $user->id]);
        $user = $order->user;

        $profile = Profile::create([
            'user_id'    => $user->id,
            'name'       => '123',
            'surname'    => '123',
            'gender'     => '123',
            'instagram'  => '123',
            'number'     => '123',
            'profession' => '123',
            'reason'     => '123',
        ]);

        $attributes = [
            'order_id' => $order->id,
        ];

        // https://laravel.com/docs/7.x/mocking#event-fake
        Event::fake([
            OrderCompleted::class,
        ]);

        $url = route('completed-orders.store');
        $response = $this->withSession(['email' => $user->email])->json('POST', $url, $attributes);
        $response->assertJsonMissingValidationErrors('status');
        $this->assertDatabaseHas('orders', ['id' => $order->id, 'status' => Order::STATUS_COMPLETED]);
        $this->assertDatabaseHas('downloadables', ['user_id' => $user->id]);
        $this->get($response['url'])->assertStatus(200);

        Event::assertDispatched(OrderCompleted::class);
    }
}
