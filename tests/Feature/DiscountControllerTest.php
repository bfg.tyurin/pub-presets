<?php

namespace Tests\Feature;

use App\Admin;
use App\Discount;
use App\Product;
use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Auth;
use Tests\TestCase;

class DiscountControllerTest extends TestCase
{

    use WithFaker, RefreshDatabase;

    public function setUp() :void
    {
        parent::setUp();
        $this->artisan('db:seed');
    }


    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testSupportCanCreateADiscountForExistingUser()
    {
        $this->withoutExceptionHandling();

        $admin = Admin::create(['email' => 'nebuhad@yandex.ru']);
        $response = $this->actingAs($admin, 'web');

        $user = factory(User::class)->create();
        $product = factory(Product::class)->create();
        $rubPrice = $this->faker->randomNumber(4);
        $usdPrice = $this->faker->randomNumber(2);

        $attributes = [
            'user_email' => $user->email,
            'product_id' => $product->id,
            'rub_price'  => $rubPrice,
            'usd_price'  => $usdPrice,
        ];

        $fields = [
            'user_id' => $user->id,
            'product_id' => $product->id,
            'rub_price' => $rubPrice,
            'usd_price' => $usdPrice,
        ];

        $response->post(route('support.dashboard.discounts.store'), $attributes);

        $this->assertDatabaseHas('discounts', $fields);
    }

    public function testSupportCanCreateADiscountNewUser()
    {
        $this->withoutExceptionHandling();

        $admin = Admin::create(['email' => 'nebuhad@yandex.ru']);
        $response = $this->actingAs($admin, 'web');

        $product = factory(Product::class)->create();
        $rubPrice = $this->faker->randomNumber(4);
        $usdPrice = $this->faker->randomNumber(2);

        $newEmail = $this->faker->safeEmail;

        $attributes = [
            'user_email' => $newEmail,
            'product_id' => $product->id,
            'rub_price'  => $rubPrice,
            'usd_price'  => $usdPrice,
        ];

        $response->post(route('support.dashboard.discounts.store'), $attributes);

        $fields = [
            'user_id' => User::firstWhere('email', $newEmail)->id,
            'product_id' => $product->id,
            'rub_price'  => $rubPrice,
            'usd_price'  => $usdPrice,
        ];

        $this->assertDatabaseHas('discounts', $fields);
    }

    public function testSupportCanDeleteDiscount()
    {
        $this->withoutExceptionHandling();
        $this->withoutMiddleware();
        $admin = Admin::create(['email' => 'nebuhad@yandex.ru']);

        $user = factory(User::class)->create();
        $product = factory(Product::class)->create();
        $discount = factory(Discount::class)->create(['user_id' => $user->id, 'product_id' => $product->id]);

        $attributes = [
            'id' => $discount->id,
        ];

        $url = route('support.dashboard.discounts.destroy', $discount->id);
        $response = $this->actingAs($admin, 'web')->delete($url);


        $this->assertDatabaseMissing('discounts', $attributes);
    }

    public function testDiscountRequiresUserEmail()
    {
        $admin = Admin::create(['email' => 'nebuhad@yandex.ru']);

        $attributes = [
            'user_email' => '',
            'product_id' => $this->faker->uuid,
            'rub_price'  => $this->faker->randomNumber(4),
            'usd_price'  => $this->faker->randomNumber(2),
        ];

        $this->actingAs($admin, 'web')->post(route('support.dashboard.discounts.store'), $attributes)->assertSessionHasErrors('user_email');
    }

    public function testDiscountRequiresProductId()
    {
        $admin = Admin::create(['email' => 'nebuhad@yandex.ru']);

        $attributes = [
            'user_email' => $this->faker->safeEmail,
            'product_id' => '',
            'rub_price'  => $this->faker->randomNumber(4),
            'usd_price'  => $this->faker->randomNumber(2),
        ];

        $this->actingAs($admin, 'web')->post(route('support.dashboard.discounts.store'), $attributes)->assertSessionHasErrors('product_id');
    }
}
