<?php

namespace Tests\Feature;

use App\Product;
use App\Services\CreateDiscount;
use App\Services\Dto\CreateDiscountDto;
use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class CreateDiscountTest extends TestCase
{
    use WithFaker, RefreshDatabase;

    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test()
    {
        $this->withoutExceptionHandling();

        $user = factory(User::class)->create();
        $product = factory(Product::class)->create();
        $rubPrice = $this->faker->randomNumber(4);
        $usdPrice = $this->faker->randomNumber(2);

        $fields = [
            'user_id' => $user->id,
            'product_id' => $product->id,
            'rub_price'  => $rubPrice,
            'usd_price'  => $usdPrice,
        ];

        $createDiscount = new CreateDiscount();
        $createDiscount->call(new CreateDiscountDto($user->email, $product->id, $rubPrice, $usdPrice));

        $this->assertDatabaseHas('discounts', $fields);
    }
}
