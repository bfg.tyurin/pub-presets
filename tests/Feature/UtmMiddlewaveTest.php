<?php

namespace Tests\Feature;

use App\Http\Middleware\Utm;
use Tests\TestCase;
use Illuminate\Http\Request;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class UtmMiddlewaveTest extends TestCase
{

    public function testExample()
    {
        $request = new Request;

        $request->merge([
            'utm_source' => 'test source',
            'utm_campaign' => 'test campaign',
        ]);

        $middleware = new Utm;

        $middleware->handle($request, function ($req) {
        });
        $this->get(url('/'))->assertCookie('utm_data');
    }
}
