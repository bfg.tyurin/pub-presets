<?php

namespace Tests\Feature;

use App\User;
use App\Product;
use App\Discount;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Mcamara\LaravelLocalization\LaravelLocalization;

class CartControllerTest extends TestCase
{
    use WithFaker, RefreshDatabase;
    /**
     * A basic feature test example.
     *
     * @return void
     */

    protected function refreshApplicationWithLocale($locale)
    {
        self::tearDown();
        putenv(LaravelLocalization::ENV_ROUTE_KEY . '=' . $locale);
        self::setUp();
    }

    protected function tearDown(): void
    {
        putenv(LaravelLocalization::ENV_ROUTE_KEY);
        parent::tearDown();
    }

    public function test_ru_user_without_discount_and_utm_can_create_order()
    {
        $this->refreshApplicationWithLocale('ru');

        $product = factory(Product::class)->create();
        $email = $this->faker->safeEmail;

        $attributes = [
            'slug' => $product->slug,
            'email' => $email,
            'utm_data' => '',
        ];

        $fields = [
            'email' => $email,
        ];

        $response = $this->post(route('cart.create'), $attributes)->assertStatus(200)->assertSee($email)->assertSee($product->title)->assertSee($product->price_discount);
        $this->assertDatabaseHas('users', $fields);
        $this->assertDatabaseHas('orders', ['product_id' => $product->id, 'total' => $product->price_discount]);

        $this->assertNull(User::firstWhere('email', $response['accountId'])->campaign);
    }

    public function testEnUserWithoutDiscountAndUtmCanCreateOrder()
    {
        $this->refreshApplicationWithLocale('en');

        $product = factory(Product::class)->create();
        $email = $this->faker->safeEmail;

        $attributes = [
            'slug' => $product->slug,
            'email' => $email,
            'utm_data' => '',
        ];

        $fields = [
            'email' => $email,
        ];

        $this->post(route('cart.create'), $attributes)->assertStatus(200)->assertSee($email)->assertSee($product->title)->assertSee($product->usd_price_discount);
        $this->assertDatabaseHas('users', $fields);
        $this->assertDatabaseHas('orders', ['product_id' => $product->id, 'total' => $product->usd_price_discount]);
    }

    public function testRuUserWithUtmWithoutDiscountCanCreateOrder()
    {
        $this->withoutExceptionHandling();
        $this->withoutMiddleware();

        $this->refreshApplicationWithLocale('ru');

        $product = factory(Product::class)->create();
        $email = $this->faker->safeEmail;

        $attributes = [
            'slug' => $product->slug,
            'email' => $email,
        ];

        $fields = [
            'email' => $email,
        ];

        $response = $this->withCookie('utm_data', json_encode(['utm_source' => 'test source']))->post(route('cart.create'), $attributes)->assertStatus(200)->assertSee($email)->assertSee($product->title)->assertSee($product->price_discount);
        $this->assertDatabaseHas('users', $fields);
        $this->assertDatabaseHas('orders', ['product_id' => $product->id, 'total' => $product->price_discount]);


        $this->assertNotNull(User::firstWhere('email', $response['accountId'])->campaign);
        $user = User::firstWhere('email', $email)->load('campaign');
        $this->assertEquals('test source', $user->campaign->utm_source);
    }

    public function testRuFreeEmailsHasDiscount()
    {
        $this->refreshApplicationWithLocale('ru');

        $product = factory(Product::class)->create();
        $email = 'test@example.com';

        $attributes = [
            'slug' => $product->slug,
            'email' => $email,
            'utm_data' => '',
        ];

        $fields = [
            'email' => $email,
        ];

        $this->post(route('cart.create'), $attributes)->assertStatus(200)->assertSee($email)->assertSee($product->title);
        $this->assertDatabaseHas('users', $fields);
        $this->assertDatabaseHas('orders', ['product_id' => $product->id, 'total' => 1]);
    }

    public function test_ru_user_have_discount()
    {
        $this->refreshApplicationWithLocale('ru');

        $product = factory(Product::class)->create();
        $user = factory(User::class)->create();
        $discount = factory(Discount::class)->create(['user_id' => $user->id, 'product_id' => $product->id]);

        $attributes = [
            'slug' => $product->slug,
            'email' => $user->email,
            'utm_data' => '',
        ];

        $fields = [
            'email' => $user->email,
        ];

        $this->post(route('cart.create'), $attributes)->assertStatus(200)
            ->assertSee($user->email)
            ->assertSee($product->title)
            ->assertSee($discount->rub_price);
        $this->assertDatabaseHas('orders', ['product_id' => $product->id, 'total' => $discount->rub_price]);
    }

    public function test_en_user_have_discount()
    {
        $this->refreshApplicationWithLocale('en');

        $user = factory(User::class)->create();
        $product = factory(Product::class)->create();
        $discount = factory(Discount::class)->create(['user_id' => $user->id, 'product_id' => $product->id]);

        $attributes = [
            'slug' => $product->slug,
            'email' => $user->email,
            'utm_data' => '',
        ];

        $fields = [
            'email' => $user->email,
        ];

        $this->post(route('cart.create'), $attributes)->assertStatus(200)
            ->assertSee($user->email)
            ->assertSee($product->title)
            ->assertSee($discount->usd_price);
        $this->assertDatabaseHas('orders', ['user_id' => $user->id, 'product_id' => $product->id, 'total' => $discount->usd_price]);
    }
}
