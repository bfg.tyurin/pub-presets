<?php

namespace Tests\Feature;

use App\User;
use App\Order;
use App\Product;
use App\Profile;
use Carbon\Carbon;
use Tests\TestCase;
use App\Services\CreateOrder;
use App\Services\GetTotalPrice;
use App\Services\CreatePromotion;
use Illuminate\Support\Facades\App;
use App\Services\Dto\CreateOrderDto;
use App\Services\DownloadableService;
use App\Services\Dto\CreatePromotionDto;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Mcamara\LaravelLocalization\LaravelLocalization;

class ProductdownloadTest extends TestCase
{
    use WithFaker, RefreshDatabase;

    protected function refreshApplicationWithLocale($locale)
    {
        self::tearDown();
        putenv(LaravelLocalization::ENV_ROUTE_KEY . '=' . $locale);
        self::setUp();
    }

    protected function tearDown(): void
    {
        putenv(LaravelLocalization::ENV_ROUTE_KEY);
        parent::tearDown();
    }

    public function testDownloadViewHasProperVariables()
    {
        $this->refreshApplicationWithLocale('ru');
        $this->withoutExceptionHandling();

        $user = factory(User::class)->create();
        $product = factory(Product::class)->create(['type' => 'pack']);
        $related = factory(Product::class)->create(['type' => 'promotion']);
        $product->related_promotions()->attach($related->id);
        // $downloadable = factory(Downloadable::class)->create(['user_id' => $user->id, 'product_id' => $product->id]);
        $downloadable = (new DownloadableService)->createWithUserIdProductId($user->id, $product->id);
        $order = (new CreateOrder(new GetTotalPrice()))->call(new CreateOrderDto($user, $product, Order::PAYMENT_TYPE_CLOUDPAYMENTS, App::getLocale()));
        $order->status = Order::STATUS_COMPLETED;
        $order->save();
        Profile::create(['user_id' => $user->id, 'name' => 'test', 'surname' => 'test', 'gender' => 'test', 'instagram' => 'test', 'number' => 'test', 'profession' => 'test', 'reason' => 'test']);
        $promotion = (new CreatePromotion())->call(new CreatePromotionDto($user->id, $product->id, 2));
        $promoDuration = ceil(Carbon::now()->diffInMinutes(Carbon::parse($promotion->available_until), true) / 60);
        $promotionTotalPrice = (new GetTotalPrice())->call($user, $promotion->product, App::getLocale());

        $this->get(route('products.download', $downloadable->token))
            ->assertViewHas(['userEmail' => $user->email])
            ->assertViewHas(['product' => $product])
            ->assertViewHas(['promotion' => $promotion])
            ->assertViewHas(['promoDuration' => $promoDuration])
            ->assertViewHas(['promotionTotalPrice' => $promotionTotalPrice]);
    }
}
