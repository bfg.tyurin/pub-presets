<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group([
    'prefix' => LaravelLocalization::setLocale(),
    'middleware' => ['localeSessionRedirect', 'localizationRedirect']
], function () {
    /** ADD ALL LOCALIZED ROUTES INSIDE THIS GROUP **/
    Route::get('/', 'IndexPageController@index')->name('index');

    Route::prefix('/products')->group(function () {

        Route::get('/spring-burst', 'ProductController@spring_burst')->name('products.spring_burst');
        Route::get('/safari-style', 'ProductController@safari_style')->name('products.safari_style');

        Route::get('/download/{token}', 'ProductController@download')->name('products.download');
        Route::get('/portugal-morning', 'ProductController@pm')->name('products.pm');
        Route::get('/{pack}', 'ProductController@show')->name('products.show');
    });

    Route::get('/cart', 'CartController@show')->name('cart.show');
    Route::get('/cart/all', 'CartController@all')->name('cart.all');
    Route::post('/cart', 'CartController@create')->name('cart.create');
    Route::post('/cart/add', 'CartController@add')->name('cart.add');
    Route::patch('/cart', 'CartController@update')->name('cart.update');

    Route::get('/paypal/payment', 'PayPalController@payment')->name('paypal.payment');
    Route::post('/paypal/payment', 'PayPalController@beforePayment')->name('paypal.beforePayment');
    Route::get('/paypal/cancel', 'PayPalController@cancel')->name('paypal.payment.cancel');
    Route::get('/paypal/payment/success', 'PayPalController@success')->name('paypal.payment.success');
    Route::post('/paypal', 'PayPalController@store')->name('paypal.store');

    Route::get('/payment/paypal', 'PaymentController@paypal')->name('payment.paypal');
    Route::post('/payment/cp', 'PaymentController@cloudPaymentsCreate')->name('payment.cloudpayments.create');
    Route::patch('/payment/cp', 'PaymentController@cloudPaymentsUpdate')->name('payment.cloudpayments.update');

    Route::post('/profile', 'ProfileController@store')->name('profile.store');
    Route::get('/profile/create', 'ProfileController@create')->name('profile.create');

    Route::post('/orders', 'OrderController@store')->name('orders.store');
    Route::patch('/orders/{id}', 'OrderController@update')->name('orders.update');

    Route::post('/completed-orders', 'CompletedOrderController@store')->name('completed-orders.store');

    Route::resource('/shopping_cart_products', 'ShoppingCartProductController');
    Route::resource('/shopping_cart_product_list', 'ShoppingCartProductListController');

    // Route::resource('zip', 'ZipController', ['only' => ['show']]);
    Route::get('/zip/{token}/category/{category}', 'ZipController@show')->name('zip.show');
});


//Route::get('/products', 'ProductController@index')->name('products.index');

Route::post('/gifts', 'GiftController@store')->name('gifts.store');

Route::get('/magiclink/{token}', 'MagicLinkController@authenticateEmail')->name('magiclink.authenticate-email');
Route::get('/magiclink', 'MagicLinkController@index');
Route::post('/magiclink', 'MagicLinkController@login')->name('magiclink.login');

//Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/.well-known/apple-developer-merchantid-domain-association', 'AppleController@domain_association');

Route::get('/admin/login', 'AdminController@login')->name('admin.show');
Route::post('/admin/login', 'AdminController@postLogin')->name('admin.login');;
Route::get('/admin/token/{token}', 'AdminController@authenticate')->name('admin.token');
Route::get('/admin/dashboard', 'AdminController@dashboard')->middleware('auth')->name('admin.dashboard');
Route::get('/admin/dashboard/simple', 'AdminController@simple')->middleware('auth')->name('admin.dashboard.simple');
Route::get('/admin/dashboard/gifts', 'GiftController@show')->middleware('auth')->name('admin.dashboard.gifts.show');
Route::get('/admin/dashboard/gifts/full', 'GiftController@full')->middleware('auth')->name('admin.dashboard.gifts.full');
Route::get('/admin/dashboard/products/{id}', 'AdminController@product')->middleware('auth')->name('admin.dashboard.product');
Route::get('/admin/panel', 'AdminController@panel')->middleware('auth')->name('admin.panel');
Route::get('/admin/panel/simple', 'AdminController@panelSimple')->middleware('auth')->name('admin.panel.simple');
Route::get('/admin/panel/orders', 'AdminController@orders')->middleware('auth')->name('admin.panel.orders');
Route::get('/admin/logout', 'AdminController@logout')->name('admin.logout');

Route::prefix('admin/panel')->namespace('Admin\Panel')->name('admin.panel.')->middleware('auth')->group(function () {
    // Controllers Within The "App\Http\Controllers\Admin\Panel" Namespace
    Route::get('/profiles', 'ProfileController@show')->name('profiles.show');
    Route::get('/faq-items', 'FaqItemController@index')->name('faq-items.index');
    Route::get('/faq-items/create', 'FaqItemController@create')->name('faq-items.create');
    Route::post('/faq-items', 'FaqItemController@store')->name('faq-items.store');
    Route::get('/faq-items/{faqItem}/edit', 'FaqItemController@edit')->name('faq-items.edit');
    Route::put('/faq-items/{faqItem}', 'FaqItemController@update')->name('faq-items.update');

    Route::get('/email_campaigns', 'EmailCampaignController@index')->middleware('auth')->name('email_campaigns.index');
    // Route::get('/email_campaigns/{id}', 'EmailCampaignController@show')->middleware('auth')->name('email_campaigns.show');


    Route::post('/email_campaigns', 'EmailCampaignController@send')->middleware('auth')->name('email_campaigns.send');
    Route::post('/email_campaigns/before/{id}', 'EmailCampaignController@beforeSend')->middleware('auth')->name('email_campaigns.before');
    //    Route::post('/mail/updatePromotions', 'EmailCampaignController@updatePromotions')->middleware('auth')->name('mail.updatePromotions');
    Route::post('/promotions', 'CreatedMailingPromotionsController@store')->middleware('auth')->name('promotions.store');

    Route::post('/mail/rollback/{id}', 'EmailCampaignController@rollback')->middleware('auth')->name('mail.rollback');
    Route::get('/mail/send_instagram_email', 'EmailCampaignController@sendInstagramEmail')->middleware('auth')->name('mail.send_instagram_email');


    // Mail Views
    Route::get('/mail/viewmail', 'EmailCampaignController@viewmail')->middleware('auth')->name('mail.viewmail');
    Route::get('/mail/updated_presets_without_promotion', 'EmailCampaignController@viewUpdatedPresetsMailWithoutPromotion')->middleware('auth');
    Route::get('/mail/with_instagram', 'EmailCampaignController@viewMailWithInstagram')->middleware('auth')->name('mail.rollback');
    Route::get('/mail/purchase', 'EmailController@viewPurchase')->middleware('auth')->name('mail.purchase');
    Route::get('/mail/profile_created', 'EmailController@viewProfileCreated')->middleware('auth')->name('mail.profile_created');

    // Golden Caramel Promotion Email Campaign
    Route::get('/email_campaigns/golden_caramel_promotion', 'EmailCampaignController@goldenCaramelPromotionShow')->middleware('auth')->name('email_campaigns.golden_caramel_promotion.show');
    Route::post('/email_campaigns/golden_caramel_promotion', 'EmailCampaignController@goldenCaramelPromotionSubscribe')->middleware('auth')->name('email_campaigns.golden_caramel_promotion_subscribe');
    Route::delete('/email_campaigns/{emailCampaign}', 'EmailCampaignController@unsubscribe')->middleware('auth')->name('email_campaigns.unsubscribe');
    Route::post('/email_campaigns/golden_caramel_promotion/send', 'EmailCampaignController@goldenCaramelPromotionSend')->middleware('auth')->name('email_campaigns.golden_caramel_promotion_send');
    Route::get('/mail/golden_caramel_promotion', 'EmailCampaignController@viewMailGoldenCaramelPromotion')->middleware('auth')->name('mail.golden_caramel_promotion');

    // Free Preset Discount
    Route::get('/email_campaigns/add_discount_for_users_with_free_preset_promotion', 'EmailCampaignController@addDiscountForUsersWithFreePresetPromotionShow')->middleware('auth')->name('email_campaigns.add_discount_for_users_with_free_preset_promotion.show');
    Route::post('/email_campaigns/add_discount_for_users_with_free_preset_promotion', 'EmailCampaignController@addDiscountForUsersWithFreePresetPromotionSubscribe')->middleware('auth')->name('email_campaigns.add_discount_for_users_with_free_preset_promotion_subscribe');
    Route::post('/email_campaigns/add_discount_for_users_with_free_preset_promotion/send', 'EmailCampaignController@addDiscountForUsersWithFreePresetPromotionSend')->middleware('auth')->name('email_campaigns.add_discount_for_users_with_free_preset_promotion_send');
    Route::get('/mail/add_discount_for_users_with_free_preset_promotion', 'EmailCampaignController@viewAddDiscountForUsersWithFreePresetPromotion')->middleware('auth')->name('mail.add_discount_for_users_with_free_preset_promotion');


    Route::resource('/products', 'ProductController')->middleware('auth');
    Route::resource('/global_strings', 'GlobalStringController')->middleware('auth');
});

Route::prefix('/support/dashboard')->namespace('Support\Dashboard')->name('support.dashboard.')->middleware('auth')->group(function () {
    // Controllers Within The "App\Http\Controllers\Admin\Panel" Namespace
    Route::get('/', 'DashboardController@index')->name('index');
    Route::get('/gifts', 'DashboardController@gifts')->name('gifts');
    Route::post('/d', 'DashboardController@findUserDownloadLink')->name('d');
    Route::post('/mail', 'DashboardController@sendMail')->name('d.mail');

    Route::get('/discounts', 'DiscountController@index')->name('discounts.index');
    Route::get('/discounts/create', 'DiscountController@create')->name('discounts.create');
    Route::post('/discounts', 'DiscountController@store')->name('discounts.store');
    Route::delete('/discounts/{id}', 'DiscountController@destroy')->name('discounts.destroy');

    Route::get('/products/{id}', 'ProductController@show')->name('products.show');

    Route::resource('/orders', 'CompletedOrderController')->middleware('auth');
    Route::resource('/users', 'UserController')->middleware('auth');
});
