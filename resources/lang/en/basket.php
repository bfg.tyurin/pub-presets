<?php

return [
    'title' => 'Menu-basket',
    'desktop' => 'Desktop',
    'mobile' => 'Mobile',
    'see' => 'Have a look',
    'add_another' => 'Add one more collection and get',
    'discount' => 'discount',
    'discount_2' => 'Discount ',
    'total' => 'Total Price',
    'selected' => 'Are chosen',
    'buy_button_text_add' => 'buy it now',
    'buy_button_text_remove' => 'In cart',
    'max_discount_text' => 'You got the maximum',
    'sum' => 'for total',
    'discount_3' => 'Extra discount ',
    'discount_until' => 'Discount until 20 september ',
    'saving' => 'Saving ',
    'collections_choosen' => '{0} None selected collections|{1} <b>:count collection</b> is chosen|[2,*] <b>:count collections</b> are chosen',
];
