window._ = require('lodash');

/**
 * We'll load jQuery and the Bootstrap jQuery plugin which provides support
 * for JavaScript based Bootstrap features such as modals and tabs. This
 * code may be modified to fit the specific needs of your application.
 */

try {
    window.Popper = require('popper.js').default;
    window.$ = window.jQuery = require('jquery');

    require('bootstrap');

  window.pay = function () {
    // TODO remove
    console.log('debug');
    form = document.querySelector('.js-cart-form');
    // TODO remove
    console.log( form.elements.email.value);
    $.ajax({
      url: form.action,
      type: 'POST',
      // to-do replace with formData or ...
      data: {
        email: form.elements.email.value,
      },
      dataType: 'json',
      success: function (data) {
        var widget = new cp.CloudPayments();
        // TODO remove
        console.log('success PUT');
        console.log(data);
        widget.charge({ // options
            publicId: 'test_api_00000000000000000000001',  //id из личного кабинета
            description: data.description, //назначение
            amount: data.amount, //сумма
            currency: 'RUB', //валюта
            invoiceId: data.orderId, //номер заказа  (необязательно)
            accountId: data.accountId, //идентификатор плательщика (необязательно)
            skin: "mini", //дизайн виджета,
            data: {
              status: 'processing' //произвольный набор параметров
            }
          },

          function (options) { // success
            //действие при успешной оплате
            // TODO remove
            console.log('success charge');
            console.log(options);
            options.data.status = 'completed';
            if( paymentResult['success'] == true ) {
              $.ajax({
                url: document.querySelector('.js-cart-form').action,
                type: 'PATCH',
                data: options,
                dataType: 'json',
                success: function(data) {
                  // TODO remove
                  console.log('success PATCH');
                  console.log(data);
                  window.location.replace(data.url);
                }
              });
            }
          },
          function (reason, options) { // fail
            //действие при неуспешной оплате
            // TODO remove
            console.log(reason);
            console.log(options);
          });
      }
    });


  };
} catch (e) {}

/**
 * We'll load the axios HTTP library which allows us to easily issue requests
 * to our Laravel back-end. This library automatically handles sending the
 * CSRF token as a header based on the value of the "XSRF" token cookie.
 */

window.axios = require('axios');

window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';

/**
 * Echo exposes an expressive API for subscribing to channels and listening
 * for events that are broadcast by Laravel. Echo and event broadcasting
 * allows your team to easily build robust real-time web applications.
 */

// import Echo from 'laravel-echo';

// window.Pusher = require('pusher-js');

// window.Echo = new Echo({
//     broadcaster: 'pusher',
//     key: process.env.MIX_PUSHER_APP_KEY,
//     cluster: process.env.MIX_PUSHER_APP_CLUSTER,
//     forceTLS: true
// });
