require('./bootstrap');

// Mobile video modal
$(document).ready(function () {

  var $videoSrc;
  $('.js-video-btn').click(function () {
    $videoSrc = $(this).data("src");
  });

  // when the modal is opened autoplay it
  $('#myModal').on('shown.bs.modal', function (e) {

    // set the video src to autoplay and not to show related video. Youtube related video is like a box of chocolates... you never know what you're gonna get
    $("#video").attr('src', $videoSrc);
  })



  // stop playing the youtube video when I close the modal
  $('#myModal').on('hide.bs.modal', function (e) {
    // a poor man's stop video
    $("#video").attr('src', $videoSrc);
  })

  // document ready

  $.ajaxSetup({
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    },
  });

  $('.js-basket_item__button').on("click", (function (e) {
    e.preventDefault();
    const btn = $(this);
    const payment_block = $(this).closest('.preset_payment_block');
    const productId = btn.data('product_id');
    console.log(productId);

    if (btn.hasClass('basket_item__dark')) {
      $.ajax({
        url: btn.data('resource_destroy'),
        type: 'DELETE',
        success: function (data) {
          console.log(data);
          console.log(data.cart_count);

          const total = Math.ceil(parseFloat(data.cart_total));
          const subTotal = Math.ceil(parseFloat(data.cart_sub_total));
          btn.toggleClass('basket_item__dark');
          payment_block.toggleClass('preset_payment_block__selected');
          // $('.js-cart-count').text(data.cart_count);
          $('.js-discount-percent').text(data.cart_discount_percent);
          $('.js-cart-total').text(total);

          // Всех выбранным обновить цену с текущей скидкой
          // Выбрать всех детей с классом js-basket_item__button_price_old кнопки с классом basket_item__dark

          const currentProductMultiplier = (100 - data.cart_discount_percent) / 100;
          const nextProductMultiplier = (100 - data.cart_next_discount_percent) / 100;
          $('.js-basket_item__button').each(function (index) {
            const price = $(this).find('.js-basket_item__button_price');
            const oldValue = parseInt(price.data('value'));
            if ($(this).hasClass('basket_item__dark')) {
              const newValue = Math.ceil(oldValue * currentProductMultiplier);
              price.text(newValue);
            } else {
              const futureTotalWithNextDiscount = Math.ceil((subTotal + oldValue) * nextProductMultiplier);
              const newValue = futureTotalWithNextDiscount - total;
              price.text(newValue);
            }

          });
          $('.js-next-discount-percent').text(data.cart_next_discount_percent);
          $('.js-discount-value').text(Math.ceil(data.cart_discount_value));
          // $('.js-basket_item__button_price_old').css('text-decoration', 'line-through');

          if (data.cart_count < 1) {
            $('.js-red_point').removeClass('red_point');
          }

          $(`.js-button_buy[data-id="${productId}"]`).text(data.buy_button_text);
          // document.querySelector('.js_buy_buton_text').textContent = data.buy_button_text;
          $(`.js-button_buy[data-id="${productId}"]`).closest('.preset_payment_block').toggleClass('preset_payment_block__selected');

          // update cart warn message
          // TO-DO
          const warnDiscountMessage = document.querySelector('.js-warn_discount_message');
          const regularMessage = warnDiscountMessage.dataset.regular_message;
          const newMessage = warnDiscountMessage.dataset.cart_is_full_message;
          if (data.cart_count == 0) {
            document.querySelector('.js-warn_discount_block').style.display = 'none';
          } else {
            document.querySelector('.js-warn_discount_block').style.display = 'block';
            if (data.cart_next_discount_percent === data.cart_discount_percent) {
              warnDiscountMessage.textContent = newMessage;
            } else {
              warnDiscountMessage.textContent = regularMessage;
            }
          }

          // update Saving
          document.querySelector('.js_basket_saving').textContent = data.cart_saving;

          //update collections string
          document.querySelector('.js_basket_collections_choosen_string').innerHTML = data.basket_collections_choosen_string;

          // update Cart full price
          document.querySelector('.js_cart_full_price').textContent = data.cart_full_price;

          // update Cart sub total
          document.querySelector('.js_cart_sub_total').textContent = data.cart_sub_total;
        }
      });
    } else {
      $.post($(this).data('resource'),
        {
          product_id: productId
        },
        function (data) {
          console.log(data);
          console.log(data.cart_count);
          const total = Math.ceil(parseFloat(data.cart_total));
          const subTotal = Math.ceil(parseFloat(data.cart_sub_total));
          btn.toggleClass('basket_item__dark');
          // $('.js-cart-count').text(data.cart_count);
          $('.js-discount-percent').text(data.cart_discount_percent);
          $('.js-cart-total').text(total);

          const currentProductMultiplier = (100 - data.cart_discount_percent) / 100;
          const nextProductMultiplier = (100 - data.cart_next_discount_percent) / 100;
          $('.js-basket_item__button').each(function (index) {
            const price = $(this).find('.js-basket_item__button_price');
            if ($(this).hasClass('basket_item__dark')) {
              const oldValue = parseInt(price.data('value'));
              const newValue = Math.ceil(oldValue * currentProductMultiplier);

              price.text(newValue);
            } else {
              const oldValue = parseInt(price.data('value'));

              const futureTotalWithNextDiscount = Math.ceil((subTotal + oldValue) * nextProductMultiplier);
              const newValue = futureTotalWithNextDiscount - total;
              price.text(newValue);
            }

          });

          $('.js-next-discount-percent').text(data.cart_next_discount_percent);
          $('.js-discount-value').text(Math.ceil(data.cart_discount_value));
          // $('.js-basket_item__button_price_old').css('text-decoration', 'line-through');

          if (data.cart_count > 0 && !$('.js-red_point').hasClass('red-point')) {
            $('.js-red_point').addClass('red_point');
          }

          $(`.js-button_buy[data-id="${productId}"]`).text(data.buy_button_text);
          // document.querySelector('.js_buy_buton_text').textContent = data.buy_button_text;
          $(`.js-button_buy[data-id="${productId}"]`).closest('.preset_payment_block').toggleClass('preset_payment_block__selected');

          // update cart warn message
          // TO-DO
          const warnDiscountMessage = document.querySelector('.js-warn_discount_message');
          const regularMessage = warnDiscountMessage.dataset.regular_message;
          const newMessage = warnDiscountMessage.dataset.cart_is_full_message;
          if (data.cart_count == 0) {
            console.log(data.cart_count);
            document.querySelector('.js-warn_discount_block').style.display = 'none';
          } else {
            document.querySelector('.js-warn_discount_block').style.display = 'block';
            if (data.cart_next_discount_percent === data.cart_discount_percent) {
              warnDiscountMessage.textContent = newMessage;
            } else {
              warnDiscountMessage.textContent = regularMessage;
            }
          }

          // update Saving
          document.querySelector('.js_basket_saving').textContent = data.cart_saving;

          // update collections string
          document.querySelector('.js_basket_collections_choosen_string').innerHTML = data.basket_collections_choosen_string;

          // update Cart full price
          document.querySelector('.js_cart_full_price').textContent = data.cart_full_price;

          // update Cart sub total
          document.querySelector('.js_cart_sub_total').textContent = data.cart_sub_total;
        },
      );
    }
  }));

  // $('.js-button_buy').on("click", (function (e) {
  //   e.preventDefault();
  //   const productId = $(this).data('id');
  //   console.log(productId);
  //   $(`.js-basket_item__button[data-product_id="${productId}"]`).trigger('click');
  //   $('.js_toggle_basket').trigger('click');
  // }));

  // $('body').on('click', 'a.js-preset_payment_block', function (e) {
  //   e.preventDefault();
  //   var $this = $(this).find('.js-button_buy');
  //   const productId = $this.data('id');
  //   console.log(productId);

  //   $(`.js-basket_item__button[data-product_id="${productId}"]`).trigger('click');
  //   $('.js_toggle_basket').trigger('click');
  // }));

  $('body').on('click', 'a.preset_payment_block', function (e) {
    e.preventDefault();
    var this_target = $(e.target);
    if (!this_target.hasClass('js_toggle_basket')) {
      var $this = $(this).find('.js-button_buy');
      const productId = $this.data('id');
      console.log(productId);
      $(`.js-basket_item__button[data-product_id="${productId}"]`).trigger('click');

      $('.js_toggle_basket').trigger('click');
    }


    return false;
  });

  function writeCookie(name, val, expires) {
    var date = new Date;
    date.setDate(date.getDate() + expires);
    document.cookie = name + "=" + val + "; path=/; expires=" + date.toUTCString();
  }

  $('body').on('click', '.js_write_lang_cookie', function () {
    writeCookie('lang', 'yes', 30);
  });

  $('body').on('click', '.basket_item__info', function(){
    if($(window).width()*1 < 768){
      var href = $(this).prev().attr('href');
        window.location.href = href;
      }
  });


  const isValidMail = function (email) {
  // const emailRegExp = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
  const emailRegExp = /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/;
  return email.length !== 0 && emailRegExp.test(email);
};

const isValidInstagram = function (str) {
  const emailRegExp = /^(?!.*\.\.)(?!.*\.$)[^\W][\w.]{0,29}$/;
  return str.length !== 0 && emailRegExp.test(str);
};

const isValidPhone = function (phone) {
  const regexp = /^\s*(?:\+?(\d{1,3}))?([-. (]*(\d{3})[-. )]*)?((\d{3})[-. ]*(\d{2,4})(?:[-.x ]*(\d+))?)\s*$/;
  return phone.length >= 7 && regexp.test(phone);
}

$.ajaxSetup({
  headers: {
    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
  },
});

$(".js-profile-form-btn").on("click", (function (e) {
  e.preventDefault();
  $('.p_input_error').css('display', 'none');
  $('.custom_textarea').removeClass('red-border-input-error');
  $('.custom-control-label').removeClass('radio-form-error');
  $.ajax({
    url: document.querySelector('.js-profile-form').action,
    type: 'POST',
    // to-do replace with formData or ...
    data: $('.js-profile-form').serialize(),
    dataType: 'json',
    success: function (data) {
      window.location.replace(data.url);
    },
    error: function (data) {
      $.each(data.responseJSON.errors, function (key, value) {
        $('input[name=' + key + ']').next('.p_input_error').css('display', 'block');
        if ('reason' === key) {
          $('.custom_textarea').addClass('red-border-input-error');
        }
        if ('gender' === key) {
          $('.custom-control-label').addClass('radio-form-error');
        }
      });

    }
  });
}));

$(document).ready(function () {

  $('.js_toggle_basket').click(function () {
    $('.basket_block').addClass('open');

    const emailInput = document.querySelector('.js-user-email');
    emailInput.closest('.js-input-error-wrapper').classList.remove('red-border-input-error');
    emailInput.nextElementSibling.style.display = 'none';

    return false;
  });

  $('.js_close_basket').click(function () {
    $('.basket_block').removeClass('open');



    return false;
  });



  $('body').click(function (e) {
    var target = $(e.target);
    if ($('.basket_block').hasClass('open')) {
      if (!target.hasClass('basket_block') && target.closest('.basket_block').length == 0 && !target.hasClass('js-button_buy') && !target.hasClass('js-button_buy_list')) {
        $('.basket_block').removeClass('open');
      }
    }

  });

  $('.js-input-email').on('input', function () {
    if (isValidMail($(this).val())) {
      $(this).closest('.js-input-error-wrapper').removeClass('red-border-input-error');
      $(this).next('.js-p-input-error').hide();
    } else {
      $(this).closest('.js-input-error-wrapper').addClass('red-border-input-error');
      $(this).next('.js-p-input-error').show();
    }
  });

  $('.js-input-instagram').on('input', function () {
    if (isValidInstagram($(this).val())) {
      $(this).closest('.js-input-error-wrapper').removeClass('red-border-input-error');
      $(this).next('.js-p-input-error').hide();
    } else {
      $(this).closest('.js-input-error-wrapper').addClass('red-border-input-error');
      $(this).next('.js-p-input-error').show();
    }
  });

  $('.js-input-phone').on('input', function () {
    if (isValidPhone($(this).val())) {
      $(this).closest('.js-input-error-wrapper').removeClass('red-border-input-error');
      $(this).next('.js-p-input-error').hide();
    } else {
      $(this).closest('.js-input-error-wrapper').addClass('red-border-input-error');
      $(this).next('.js-p-input-error').show();
    }
  });
});

$(".js-gift-form-btn").on("click", (function (e) {
  e.preventDefault();
  const form = $('.js-gift-form');
  const action = form.attr('action');
  const email = $('.js-input-email');
  const instagram = $('.js-input-instagram');
  const phone = $('.js-input-phone');

  const validate = function () {
    const arr = [];

    let valid = isValidMail(email.val());
    arr.push(valid);
    if (!valid) {
      email.closest('.js-input-error-wrapper').addClass('red-border-input-error');
      email.next('.js-p-input-error').show();
    } else {
      console.log('1');
      email.closest('.js-input-error-wrapper').removeClass('red-border-input-error');
    }

    valid = isValidInstagram(instagram.val());
    arr.push(valid);
    if (!valid) {
      instagram.closest('.js-input-error-wrapper').addClass('red-border-input-error');
      instagram.next('.js-p-input-error').show();
    } else {
      console.log('2');
      instagram.closest('.js-input-error-wrapper').removeClass('red-border-input-error');
      instagram.closest('.js_p_input_error').hide();
    }

    valid = isValidPhone(phone.val())
    arr.push(valid);
    if (!valid) {
      phone.closest('.js-input-error-wrapper').addClass('red-border-input-error');
      phone.next('.js-p-input-error').show();
    } else {
      console.log('3');
      phone.closest('.js-input-error-wrapper').removeClass('red-border-input-error');
    }

    return !arr.includes(false);
  }

  if (!validate()) {
    console.error('Form is not valid, return ' + form.serialize());
    return false;
  }

  $.ajax({
    url: action,
    type: 'POST',
    // to-do replace with formData or ...
    data: form.serialize(),
    dataType: 'json',
    success: function (data) {
      $('.pm_form').hide();
      $('.pm_thanks').show();
      const downloadUrl = data.url;
      const downloadBtn = $('.js-pm-download-btn');
      downloadBtn.attr('href', downloadUrl);
    },
    error: function (data) {
      console.log(data);
    }
  });
}));

$(".js-paypal-form").submit(function (e) {
  e.preventDefault();
  const selectedTarif = $('.payment_tarif.selected');
  $.ajax({
    url: document.querySelector('.js-paypal-form').action,
    type: 'POST',
    data: {
      email: $('#email_for_pay').val(),
      product_slug: selectedTarif.data('slug'),
    },
    success: function (data) {
      window.location.replace(data.paypal_url);
    },
  });
});
/*
* Акции
 */
$(".js-cloudpayments").submit(function (e) {
  e.preventDefault();
  const action = $(this).attr('action');
  const nextUrl = $(this).data('nexturl');
  $.ajax({
    url: action,
    // data: $(this).serialize(),
    data: $(this).serialize(),
    type: 'POST',
    dataType: 'json',
    success: function (data) {
      const widget = new cp.CloudPayments();
      const options = { // options
        publicId: '',  //id из личного кабинета
        description: 'Покупка курса ' + data.product.title,
        invoiseId: data.id,
        amount: data.total,
        currency: data.currency,
        accountId: data.user.email, //идентификатор плательщика (необязательно)
        email: data.user.email,
        skin: "classic", //дизайн виджета
        data: {
          email: data.user.email,
          slug: data.product.slug
        }
      };

      widget.pay('auth', options, {
        onSuccess: function (paymentResult) { // success
          console.log('onSuccess:' + JSON.stringify(paymentResult));
          //window.location.replace('success.php?email='+w_email);

        },
        onFail: function (paymentResult) {
          //console.log('fail');
          console.log('onFail:' + JSON.stringify(paymentResult));
        },
        onComplete: function (paymentResult) {
          if (paymentResult['success'] == true) {

            console.log('OnComplete:' + JSON.stringify(paymentResult));

            window.dataLayer.push({
              event: 'payment',
              ecommerce: {
                currencyCode: 'RUB',
                purchase: {
                  actionField: {
                    id: data.id, // номер заказа
                    revenue: data.total + '.00', // сумма покупки
                    tax: '0.00', // налог, если есть
                    shipping: '0'
                  },
                  products: [{
                    id: data.product_id, // id купленного продукта из cms сайта, если нет, то название продукта
                    name: data.title, // название продукта из cms
                    category: 'пресет', // категория продукта, если нет, оставить пустым
                    quantity: 1, // количество купленного продукта
                    price: data.total + '.00' // цена единицы продукта
                  }]
                }
              }
            });
            $.post({
              url: nextUrl,
              data: {
                order_id: data.id,
              },
              dataType: 'json',
              success: function (data) {
                // TODO remove
                console.log('success PATCH');
                window.location.replace(data.url);
              }
            });
          }
        }
      }
      );

    },
  });
});

$(document).ready(function () {

  checkinput = function (elem, type) {
    if (type == "email") {
      var pattern = /^[a-z0-9][a-z0-9\._-]*[a-z0-9]*@([a-z0-9]+([a-z0-9-]*[a-z0-9]+)*\.)+[a-z]+/i;
    }
    else {
      var pattern = /^(\s*)?(\+)?([- _():=+]?\d[- _():=+]?){8,14}(\s*)?$/;
    }
    var mail = elem,
      input_wrapper = mail.parent('.p_input_wrapper'),
      span = $('.p_input_error');

    if (mail.val() != '') {
      if (mail.val().search(pattern) == 0) {
        span.text('').removeClass('active');
        mail.removeClass('error');
        input_wrapper.removeClass('input_error');
        return true;
      } else {
        // span.text('Введен неверно').addClass('active');
        mail.addClass('error');
        input_wrapper.addClass('input_error');
        return false;
      }
    }
    else {
      span.text('Заполните поле').addClass('active');
      mail.addClass('error');
      input_wrapper.addClass('input_error');
      return false;
    }
  }
  // $('input#email_for_pay').on('input', function () {
  //   checkinput($(this), "email");
  // });


  const form = document.querySelector('.js-cart-form');
  // TODO remove
  if ($('#pay_by_card').length != 0) {
    $('#pay_by_card').on('click', function (e) {
      e.preventDefault();

      const selectedTarif = $('.payment_tarif.selected');
      const w_slug = selectedTarif.data('slug');

      if (checkinput($('#email_for_pay'), "email")) {
        $.ajax({
          url: form.action,
          type: 'POST',
          data: {
            email: $('#email_for_pay').val().trim().toLowerCase(),
            slug: w_slug
          },
          dataType: 'json',
          success: function (data) {
            console.log(data);

            const orderId = data.orderId;
            const accountId = data.accountId;
            const amount = data.amount;
            const description = data.description;

            const widget = new cp.CloudPayments();

            widget.pay('auth', { // options
              publicId: '',  //id из личного кабинета
              description: 'Покупка курса ' + description,
              invoiseId: orderId,
              amount: amount,
              currency: 'RUB',
              accountId: accountId, //идентификатор плательщика (необязательно)
              email: accountId,
              skin: "classic", //дизайн виджета
              data: {
                email: accountId,
                slug: w_slug
              }
            }, {
              onSuccess: function () { // success
                //window.location.replace('success.php?email='+w_email);

              },
              onFail: function () {
                //console.log('fail');
              },
              onComplete: function (paymentResult) {
                if (paymentResult['success'] == true) {
                  window.dataLayer.push({
                    event: 'payment',
                    ecommerce: {
                      currencyCode: 'RUB',
                      purchase: {
                        actionField: {
                          id: orderId, // номер заказа
                          revenue: amount + '.00', // сумма покупки
                          tax: '0.00', // налог, если есть
                          shipping: '0'
                        },
                        products: [{
                          id: w_slug, // id купленного продукта из cms сайта, если нет, то название продукта
                          name: w_slug, // название продукта из cms
                          category: 'пресет', // категория продукта, если нет, оставить пустым
                          quantity: 1, // количество купленного продукта
                          price: amount + '.00' // цена единицы продукта
                        }]
                      }
                    }
                  });
                  $.ajax({
                    url: document.querySelector('.js-cart-form').action,
                    type: 'PATCH',
                    data: {
                      email: accountId,
                      slug: w_slug,
                      invoiceId: orderId,
                      status: 'completed',
                      data: { status: 'completed' }
                    },
                    dataType: 'json',
                    success: function (data) {
                      // TODO remove
                      console.log('success PATCH');
                      window.location.replace(data.url);
                    }
                  });
                }
              }
            }
            );

          }
        });
      } // ЕСЛИ ЕМЕЙЛ ПРОВЕРЕН
    });
  }
});

const ujs = require('@rails/ujs');
ujs.start();

const getDatalayerProducts = (arr, currency = 'RUB', discount = 0) => {
  const products = arr.map((product) => {
    const productPrice = currency === 'RUB' ? product.price_discount : product.usd_price_discount;
    return {
      id: product.id, // id купленного продукта из cms сайта, если нет, то название продукта
      name: product.title, // название продукта из cms
      category: 'пресет', // категория продукта, если нет, оставить пустым
      quantity: 1, // количество купленного продукта
      price: productPrice + '.00' // цена единицы продукта
    };
  });

  return products;
};

const setupJsonHeaders = () => {
  const token = document.querySelector('meta[name="csrf-token"]').getAttribute('content');
  return {
    'Content-Type': 'application/json;charset=utf-8',
    "Accept": "application/json, text-plain, */*",
    'X-CSRF-TOKEN': token
  }
};
