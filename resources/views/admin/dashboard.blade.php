<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta name="robots" content="noindex, nofollow" />
    <meta charset="utf-8">
    <meta content="width=device-width,initial-scale=1" name="viewport">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Пресеты Жени Савиной</title>
    <meta content="Пресеты Жени Савиной" name="description">

    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@400;500;600;700;800;900&display=swap"
        rel="stylesheet">

    <link href="{{ mix('css/app.css')}}" rel="stylesheet">
</head>

<body>
    <nav class="navbar navbar-dark sticky-top bg-dark flex-md-nowrap p-0">
        <a class="navbar-brand col-sm-3 col-md-2 mr-0" href="{{ url('/') }}">Пресеты Жени Савиной</a>
        <ul class="navbar-nav px-3">
            <li class="nav-item text-nowrap">
                <a class="nav-link" href="{{ route('admin.logout') }}">Выйти</a>
            </li>
        </ul>
    </nav>



    <div class="container-fluid">
        <div class="row">
            <div class="col-md-2 d-none d-md-block bg-light sidebar">
                <div class="sidebar-sticky">
                    <ul class="nav flex-column">
                        <li class="nav-item">
                            <span class="nav-link active">Заказы ({{ count($orders) }})

                            </span>
                        </li>
                        <li class="nav-item">
                            <span class="nav-link active">Пользователи ({{ count($users) }})
                            </span>
                        </li>
                        @foreach($products as $product)
                        <li class="nav-item" style="margin-top: 10px">
                            <div class="form-signin card">
                                <div class="card-body">
                                    <h5 class="card-title">{{ $product->title }}
                                        ({{ count($product->notCampaignOrders) }})</h5>
                                </div>
                                <ul class="list-group list-group-flush">
                                    <li class="list-group-item">Цена: {{ $product->price_original }}</li>
                                    <li class="list-group-item">Цена со скидкой: {{ $product->price_discount }}</li>
                                    <li class="list-group-item">Слаг: {{ $product->slug }}</li>
                                </ul>
                                <div class="card-body">
                                    <a href="{{ route('products.show', $product) }}" class="card-link">Посмотреть на
                                        сайте</a>
                                </div>
                            </div>
                        </li>
                        @endforeach
                    </ul>


                </div>
            </div>
            <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
                <div
                    class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom">
                    <h1 class="h2">Заказы</h1>
                    <a class="ml-2 mr-auto" href="{{ route('admin.dashboard') }}">
                        <button type="button" class="btn btn-sm btn-outline-secondary" data-dismiss="toast"
                            aria-label="Close">
                            Сбросить
                        </button>
                    </a>
                    {{-- TO-DO --}}
                    {{-- <a class="ml-2 mr-auto" href="{{ route('admin.dashboard', ['category' => 'desktop']) }}">Только для
                        компьютера</a>
                    <a class="ml-2 mr-auto" href="{{ route('admin.dashboard', ['category' => 'mobile']) }}">Только
                        мобильные</a> --}}

                    <div class="btn-toolbar mb-2 mb-md-0">
                        <div class="btn-group mr-2">
                            <button class="btn btn-sm btn-outline-secondary" disabled>Export</button>
                        </div>
                        <div class="dropdown">
                            <button class="btn btn-sm btn-outline-secondary dropdown-toggle" type="button"
                                id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                    fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                    stroke-linejoin="round" class="feather feather-calendar">
                                    <rect x="3" y="4" width="18" height="18" rx="2" ry="2"></rect>
                                    <line x1="16" y1="2" x2="16" y2="6"></line>
                                    <line x1="8" y1="2" x2="8" y2="6"></line>
                                    <line x1="3" y1="10" x2="21" y2="10"></line>
                                </svg>
                                Показать
                            </button>
                            <div class="dropdown-menu" aria-labelledby="dropdownMenu2">
                                <ul class="">
                                    <li class="pl-2 pt-1"><input class="js-dashboard-checkbox" type="checkbox" checked
                                            data-class="js-dashboard-order-id"> № заказа</li>
                                    <li class="pl-2 pt-1"><input class="js-dashboard-checkbox" type="checkbox" checked
                                            data-class="js-dashboard-client-email"> Клиент</li>
                                    <li class="pl-2 pt-1"><input class="js-dashboard-checkbox" type="checkbox" checked
                                            data-class="js-dashboard-product-title"> Товар</li>
                                    <li class="pl-2 pt-1"><input class="js-dashboard-checkbox" type="checkbox" checked
                                            data-class="js-dashboard-product-type"> Тип товара</li>
                                    <li class="pl-2 pt-1"><input class="js-dashboard-checkbox" type="checkbox" checked
                                            data-class="js-dashboard-order-total"> Стоимость</li>
                                    <li class="pl-2 pt-1"><input class="js-dashboard-checkbox" type="checkbox" checked
                                            data-class="js-dashboard-order-status"> Статус</li>
                                    <li class="pl-2 pt-1"><input class="js-dashboard-checkbox" type="checkbox" checked
                                            data-class="js-dashboard-order-payment-type"> Тип платежа</li>
                                    <li class="pl-2 pt-1"><input class="js-dashboard-checkbox" type="checkbox" checked
                                            data-class="js-dashboard-order-currency"> Валюта</li>
                                    <li class="pl-2 pt-1"><input class="js-dashboard-checkbox" type="checkbox" checked
                                            data-class="js-dashboard-has-utm"> has_utm?</li>
                                    <li class="pl-2 pt-1"><input class="js-dashboard-checkbox" type="checkbox" checked
                                            data-class="js-dashboard-user-name"> Имя</li>
                                    <li class="pl-2 pt-1"><input class="js-dashboard-checkbox" type="checkbox" checked
                                            data-class="js-dashboard-user-surname"> Фамилия</li>
                                    <li class="pl-2 pt-1"><input class="js-dashboard-checkbox" type="checkbox" checked
                                            data-class="js-dashboard-user-gender"> Пол</li>
                                    <li class="pl-2 pt-1"><input class="js-dashboard-checkbox" type="checkbox" checked
                                            data-class="js-dashboard-user-instagram"> Инстаграм</li>
                                    <li class="pl-2 pt-1"><input class="js-dashboard-checkbox" type="checkbox" checked
                                            data-class="js-dashboard-user-phone"> Телефон</li>
                                    <li class="pl-2 pt-1"><input class="js-dashboard-checkbox" type="checkbox" checked
                                            data-class="js-dashboard-user-profession"> Профессия</li>
                                    <li class="pl-2 pt-1"><input class="js-dashboard-checkbox" type="checkbox" checked
                                            data-class="js-dashboard-user-reason"> Причина</li>
                                    <li class="pl-2 pt-1"><input class="js-dashboard-checkbox" type="checkbox" checked
                                            data-class="js-dashboard-created_at"> Создан</li>
                                    <li class="pl-2 pt-1"><input class="js-dashboard-checkbox" type="checkbox" checked
                                            data-class="js-dashboard-updated_at"> Обновлен</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="table-responsive" style="font-size: .875rem">
                    <table class="table table-striped table-sm">
                        <thead class="thead-dark">
                            {{--                    <tr>--}}
                            {{--                        <th class="js-dashboard-order-id">#</th>--}}
                            {{--                        <th>Клиент <input class="js-dashboard-checkbox" type="checkbox" checked data-class="js-dashboard-client-email"></th>--}}
                            {{--                        <th>Товар <input class="js-dashboard-checkbox" type="checkbox" checked data-class="js-dashboard-product-title"></th>--}}
                            {{--                        <th>Тип товара <input class="js-dashboard-checkbox" type="checkbox" checked data-class="js-dashboard-product-type"></th>--}}
                            {{--                        <th>Стоимость <input class="js-dashboard-checkbox" type="checkbox" checked data-class="js-dashboard-order-total"></th>--}}
                            {{--                        <th>Статус <input class="js-dashboard-checkbox" type="checkbox" checked data-class="js-dashboard-order-status"></th>--}}
                            {{--                        <th>Тип платежа <input class="js-dashboard-checkbox" type="checkbox" checked data-class="js-dashboard-order-payment-type"></th>--}}
                            {{--                        <th>Валюта <input class="js-dashboard-checkbox" type="checkbox" checked data-class="js-dashboard-order-currency"></th>--}}
                            {{--                        <th>has_utm? <input class="js-dashboard-checkbox" type="checkbox" checked data-class="js-dashboard-has-utm"></th>--}}
                            {{--                        <th>Имя <input class="js-dashboard-checkbox" type="checkbox" checked data-class="js-dashboard-user-name"></th>--}}
                            {{--                        <th>Фамилия <input class="js-dashboard-checkbox" type="checkbox" checked data-class="js-dashboard-user-surname"></th>--}}
                            {{--                        <th>Пол <input class="js-dashboard-checkbox" type="checkbox" checked data-class="js-dashboard-user-gender"></th>--}}
                            {{--                        <th>Инстаграм <input class="js-dashboard-checkbox" type="checkbox" checked data-class="js-dashboard-user-instagram"></th>--}}
                            {{--                        <th>Телефон <input class="js-dashboard-checkbox" type="checkbox" checked data-class="js-dashboard-user-phone"></th>--}}
                            {{--                        <th>Профессия <input class="js-dashboard-checkbox" type="checkbox" checked data-class="js-dashboard-user-profession"></th>--}}
                            {{--                        <th>Причина <input class="js-dashboard-checkbox" type="checkbox" checked data-class="js-dashboard-user-reason"></th>--}}
                            {{--                        <th>Создан <input class="js-dashboard-checkbox" type="checkbox" checked data-class="js-dashboard-created_at"></th>--}}
                            {{--                        <th>Обновлен <input class="js-dashboard-checkbox" type="checkbox" checked data-class="js-dashboard-updated_at"></th>--}}
                            {{--                    </tr>--}}
                            <tr>
                                <th class="js-dashboard-order-id">№ заказа</th>
                                <th class="js-dashboard-client-email">Клиент</th>
                                <th class="js-dashboard-product-title">Товар</th>
                                <th class="js-dashboard-product-type">Тип товара</th>
                                <th class="js-dashboard-order-total">Стоимость</th>
                                <th class="js-dashboard-order-status">Статус</th>
                                <th class="js-dashboard-order-payment-type">Тип платежа</th>
                                <th class="js-dashboard-order-currency">Валюта</th>
                                <th class="js-dashboard-has-utm">has_utm?</th>
                                <th class="js-dashboard-user-name">Имя</th>
                                <th class="js-dashboard-user-surname">Фамилия</th>
                                <th class="js-dashboard-user-gender">Пол</th>
                                <th class="js-dashboard-user-instagram">Инстаграм</th>
                                <th class="js-dashboard-user-phone">Телефон</th>
                                <th class="js-dashboard-user-profession">Профессия</th>
                                <th class="js-dashboard-user-reason">Причина</th>
                                <th class="js-dashboard-created_at">Создан</th>
                                <th class="js-dashboard-updated_at">Обновлен</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($orders as $order)
                            <tr>
                                <th class="js-dashboard-order-id">{{ $order->id }}</th>
                                <td class="js-dashboard-client-email">{{ $order->user->email }}</td>
                                <td class="js-dashboard-product-title">
                                    @foreach ($order->products as $product)
                                        {{ $product->title }}<br>
                                    @endforeach</td>
                                <td class="js-dashboard-product-type">
                                    @foreach ($order->products as $product)
                                    <a
                                        href="{{ request()->fullUrlWithQuery(['product_type' => $product->type]) }}">{{ $product->type }}
                                    </a><br>
                                    @endforeach

                                </td>
                                <td class="js-dashboard-order-total">{{ $order->total }}</td>
                                <td class="js-dashboard-order-status"><a
                                        href="{{ request()->fullUrlWithQuery([ 'order_status' => $order->status]) }}">{{ $order->status }}</a>
                                </td>
                                <td class="js-dashboard-order-payment-type">{{ $order->payment_type }}</td>
                                <td class="js-dashboard-order-currency">
                                    {{ $order->currency ? $order->currency : 'RUB' }}</td>
                                <td class="js-dashboard-has-utm">{{ $order->user->campaign ? 'да' : 'нет' }}</td>
                                <td class="js-dashboard-user-name">
                                    {{ $order->user->profile ? $order->user->profile->name : 'Нет' }}</td>
                                <td class="js-dashboard-user-surname">
                                    {{ $order->user->profile ? $order->user->profile->surname : 'Нет' }}</td>
                                <td class="js-dashboard-user-gender">
                                    {{ $order->user->profile ? $order->user->profile->gender : 'Нет' }}</td>
                                <td class="js-dashboard-user-instagram">
                                    {{ $order->user->profile ? $order->user->profile->instagram : 'Нет' }}</td>
                                <td class="js-dashboard-user-phone">
                                    {{ $order->user->profile ? $order->user->profile->number : 'Нет' }}</td>
                                <td class="js-dashboard-user-profession">
                                    {{ $order->user->profile ? $order->user->profile->profession : 'Нет' }}</td>
                                <td class="js-dashboard-user-reason">
                                    {{ $order->user->profile ? $order->user->profile->reason : 'Нет' }}</td>
                                <td class="js-dashboard-created_at">{{ $order->created_at->timezone('Europe/Moscow') }}
                                </td>
                                <td class="js-dashboard-updated_at">{{ $order->updated_at->timezone('Europe/Moscow') }}
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </main>
        </div>
    </div>


    <script src="{{ mix('js/app.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/select2.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/floatingFormLabels.min.js') }}"></script>
    <script src="{{ asset('js/jquery.event.move.js') }}"></script>
    <script src="{{ asset('js/jquery.twentytwenty.js') }}"></script>
    <script src="{{ asset('js/libs/jquery.mousewheel.js') }}"></script>
    <script src="{{ asset('js/libs/SmoothScroll.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/app_custom.js') }}"></script>

    <script>
        $(document).ready(function () {
        if (!localStorage.getItem('columnsState')) {
            localStorage.setItem('columnsState', JSON.stringify(
                {
                    columns: [
                        {
                            data: 'js-dashboard-order-id',
                            state: 'checked',
                        },
                        {
                            data: 'js-dashboard-client-email',
                            state: 'checked',
                        },
                        {
                            data: 'js-dashboard-product-title',
                            state: 'checked',
                        },
                        {
                            data: 'js-dashboard-product-type',
                            state: 'checked',
                        },
                        {
                            data: 'js-dashboard-order-total',
                            state: 'checked',
                        },
                        {
                            data: 'js-dashboard-order-status',
                            state: 'checked',
                        },
                        {
                            data: 'js-dashboard-order-payment-type',
                            state: 'checked',
                        },
                        {
                            data: 'js-dashboard-order-currency',
                            state: 'checked',
                        },
                        {
                            data: 'js-dashboard-has-utm',
                            state: 'checked',
                        },
                        {
                            data: 'js-dashboard-user-name',
                            state: 'checked',
                        },
                        {
                            data: 'js-dashboard-user-surname',
                            state: 'checked',
                        },
                        {
                            data: 'js-dashboard-user-gender',
                            state: 'checked',
                        },
                        {
                            data: 'js-dashboard-user-instagram',
                            state: 'checked',
                        },
                        {
                            data: 'js-dashboard-user-phone',
                            state: 'checked',
                        },
                        {
                            data: 'js-dashboard-user-profession',
                            state: 'checked',
                        },
                        {
                            data: 'js-dashboard-user-reason',
                            state: 'checked',
                        },
                        {
                            data: 'js-dashboard-created_at',
                            state: 'checked',
                        },
                        {
                            data: 'js-dashboard-updated_at',
                            state: 'checked',
                        },
                    ],
                }
            ));
        } else {
            console.log('columnsState avalable');
            const columnsState  = JSON.parse(localStorage.getItem('columnsState'));
            const columns  = columnsState.columns;
            columns.forEach(function (item) {
                const select = $("[data-class=" + item.data + "]");
                select.prop('checked', item.state ? true : false);
                select.trigger('change');
            });
        }
    });

    $('.js-dashboard-checkbox').change(function () {
        const columnToHideClassName = $(this).data('class');
        console.log(columnToHideClassName);
        if(this.checked) {
            console.log('checked');
            const columnsState  = JSON.parse(localStorage.getItem('columnsState'));
            const r = columnsState.columns.find(({ data }) => data === columnToHideClassName);
            console.log(r);
            r.state = 'checked';
            localStorage.setItem('columnsState', JSON.stringify(columnsState));
            $('.' + columnToHideClassName).show();
        } else {
            const columnsState  = JSON.parse(localStorage.getItem('columnsState'));
            const r = columnsState.columns.find(({ data }) => data === columnToHideClassName);
            console.log(r);
            r.state = '';
            localStorage.setItem('columnsState', JSON.stringify(columnsState));
            $('.' + columnToHideClassName).hide();
            console.log('unchecked');
        }
    });
    </script>


</body>

</html>
