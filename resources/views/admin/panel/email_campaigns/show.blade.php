<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta name="robots" content="noindex, nofollow" />
    <meta charset="utf-8">
    <meta content="width=device-width,initial-scale=1" name="viewport">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Пресеты Жени Савиной</title>
    <meta content="Пресеты Жени Савиной" name="description">

    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@400;500;600;700;800;900&display=swap" rel="stylesheet">

    <link href="{{ mix('css/app.css')}}" rel="stylesheet">
</head>

<body>
<nav class="navbar navbar-dark sticky-top bg-dark flex-md-nowrap p-0">
    <a class="navbar-brand col-sm-3 col-md-2 mr-0" href="{{ url('/') }}">Пресеты Жени Савиной</a>
    <p class="navbar-brand col-sm-3 col-md-2 mr-0" href="{{ url('/') }}">{{ $mailing->title }}</p>
    <ul class="navbar-nav px-3">
        <li class="nav-item text-nowrap">
            <a class="nav-link" href="{{ route('admin.logout') }}">Выйти</a>
        </li>
    </ul>
</nav>



<div class="container-fluid">
    <div class="row">
        <div class="col-md-2 d-md-block bg-light sidebar">
            <div class="sidebar-sticky">
                <br>
                <p>Пользователей в рассылке: {{ $users->count() }}</p>
                <form method="POST" action="{{ route('admin.panel.email-campaigns.before', $mailing->id) }}">
                    @csrf
                    <button type="submit">Добавить в рассылку</button>
                </form>
                <form method="POST" action="{{ route('admin.panel.promotions.store') }}">
                    @csrf
                    <input type="hidden" name="id" value="{{ $mailing->id }}">
                    <input type="hidden" name="users" value="{{ $users }}">
                    <button type="submit">Добавить акции</button>
                </form>
                <form method="POST" action="{{ route('admin.panel.mail.rollback', $mailing->id) }}">
                    @csrf
                    <button type="submit">Убрать</button>
                </form>
                <p></p>
                <form method="POST" action="#">
                    @csrf
                    <button type="submit" disabled>Отправить всем</button>
                </form>
            </div>
        </div>
        <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
            <div class="table-responsive" style="font-size: .875rem">
                <table class="table table-striped table-sm">
                    <thead class="thead-dark">
                    <tr>
                        <th></th>
                        <th>email</th>
                        <th>заказы</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($users as $user)
                        <tr>
{{--                            <td>--}}
{{--                                <form method="POST" action="{{ route('admin.panel.email-campaigns.send') }}" class="js-send-mail-form">--}}
{{--                                    @csrf--}}
{{--                                    <input name="user_id" type="hidden" value="{{ $user->id }}">--}}
{{--                                    <button type="submit" class="btn btn-sm {{ $user->email_campaigns->find($mailing->id)->pivot->is_mail_send ? 'btn-success' : 'btn-danger' }} js-send-mail-btn" {{ $user->email_campaigns->find($mailing->id)->pivot->is_mail_send ? 'disabled' : '' }}>Отправить</button>--}}
{{--                                </form>--}}
{{--                            </td>--}}
                            <td>{{ $user->email }}</td>
                            <td>
                                <table class="table table-striped table-sm">
                                    <thead class="thead-dark">
                                    <tr>
                                        <th></th>
                                        <th>номер</th>
                                        <th>дата</th>
                                        <th>total</th>
                                        <th>статус</th>
                                        <th>валюта</th>
                                        <th>тип</th>
                                        <th>название</th>
                                        <th>удален?</th>
                                        <th>Акции</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($user->orders as $order)
                                        <tr>
                                            <td>
                                                <form method="POST" action="{{ route('admin.panel.email-campaigns.send') }}" class="js-send-mail-form">
                                                    @csrf
                                                    <input name="user_id" type="hidden" value="{{ $user->id }}">
                                                    <input name="user_email" type="hidden" value="{{ $user->email }}">
                                                    <input name="product_id" type="hidden" value="{{ $order->product->id }}">
                                                    <input name="product_type" type="hidden" value="{{ $order->product->type }}">
                                                    <input name="title" type="hidden" value="{{ $mailing->title }}">
                                                    <button type="submit" class="btn btn-sm {{ $user->email_campaigns->find($mailing->id)->pivot->is_mail_send ? 'btn-success' : 'btn-danger' }} js-send-mail-btn" {{ $user->email_campaigns->find($mailing->id)->pivot->is_mail_send ? 'disabled' : '' }}>Отправить</button>
                                                </form>
                                            </td>
                                            <td> <a href="{{ $user->downloadables()->firstWhere('product_id', $order->product->id) ? (new \App\Services\CreateDownloadableUrlWithToken())->call($user->downloadables()->latest()->firstWhere('product_id', $order->product->id)->token) : '#' }}">{{ $order->id }}</a></td>
                                            <td>{{ $order->created_at }}</td>
                                            <td>{{ $order->total }}</td>
                                            <td>{{ $order->status }}</td>
                                            <td>{{ $order->currency ? $order->currency : 'RUB' }}</td>
                                            <td>{{ $order->product->type ?? ''}}</td>
                                            <td>{{ $order->product->title ?? ''}}</td>
                                            <td>{{ $order->deleted ? 'Да' : 'Нет' }}</td>

                                            <td>
                                                <table class="table table-striped table-sm">
                                                    <thead class="thead-dark">
                                                    <tr>
                                                        <th>Продукт</th>
                                                        <th>Создана</th>
                                                        <th>Обновлена</th>
                                                        <th>Доступна до</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    @if($order->product->related_promotions()->exists())
                                                    @foreach($user->promotions()->where('product_id', $order->product->related_promotions->first()->id)->latest('available_until')->get() as $promotion)
                                                        <tr>
                                                            <th>{{ $promotion ? $promotion->product->title : '' }}</th>
                                                            <td>{{ $promotion ? $promotion->created_at->timezone('Europe/Moscow') : '' }}</td>
                                                            <td>{{ $promotion ? $promotion->updated_at->timezone('Europe/Moscow') : '' }}</td>
                                                            <td>{{ $promotion ? $promotion->available_until->timezone('Europe/Moscow') : '' }}</td>
                                                        </tr>
                                                    @endforeach
                                                    @endif
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </main>
    </div>
</div>


    <script src="{{ mix('js/app.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/select2.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/floatingFormLabels.min.js') }}"></script>
    <script src="{{ asset('js/jquery.event.move.js') }}"></script>
    <script src="{{ asset('js/jquery.twentytwenty.js') }}"></script>
    <script src="{{ asset('js/libs/jquery.mousewheel.js') }}"></script>
    <script src="{{ asset('js/libs/SmoothScroll.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/app_custom.js') }}"></script>

    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
        });

        $(document).on('submit', '.js-send-mail-form', function(e) {
            e.preventDefault();
            const that = $(this);
            const submitBtn = that.find('.js-send-mail-btn');
            submitBtn.prop('disabled', true);
            $.ajax({
                type: 'POST',
                url: that.attr('action'),
                data: that.serialize(),
                dataType: 'json',
                success: function (data) {
                    console.log(data);
                    submitBtn.removeClass('btn-danger');
                    submitBtn.addClass('btn-success');
                },
                error: function (data) {
                    submitBtn.addClass('btn-success');
                    submitBtn.prop('disabled', false);
                }
            });
        });
    </script>

    </body>
    </html>
