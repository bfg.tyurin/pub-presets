<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta name="robots" content="noindex, nofollow" />
    <meta charset="utf-8">
    <meta content="width=device-width,initial-scale=1" name="viewport">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="csrf-param" content="_token">
    <title>Пресеты Жени Савиной</title>
    <meta content="Пресеты Жени Савиной" name="description">

    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@400;500;600;700;800;900&display=swap" rel="stylesheet">

    <link href="{{ mix('css/app.css')}}" rel="stylesheet">
</head>

<body>
<nav class="navbar navbar-dark sticky-top bg-dark flex-md-nowrap p-0">
    <a class="navbar-brand col-sm-3 col-md-2 mr-0" href="{{ url('/') }}">Пресеты Жени Савиной</a>
    <p class="navbar-brand col-sm-3 col-md-2 mr-0" href="{{ url('/') }}">{{ $emailCampaign->title }}</p>
    <ul class="navbar-nav px-3">
        <li class="nav-item text-nowrap">
            <a class="nav-link" href="{{ route('admin.logout') }}">Выйти</a>
        </li>
    </ul>
</nav>



<div class="container-fluid">
    <div class="row">
        <div class="col-md-2 d-md-block bg-light sidebar">
            <div class="sidebar-sticky">
                <br>
                <p>Пользователей в рассылке: {{ $emailCampaign->users->count() }}</p>
                {{-- <form method="POST" action="{{ route('admin.panel.email_campaigns.before', $emailCampaign->id) }}">
                    @csrf
                    <button type="submit">Добавить в рассылку</button>
                </form> --}}
                <a href="{{ route('admin.panel.email_campaigns.add_discount_for_users_with_free_preset_promotion_subscribe') }}"
                data-type="json"
                data-method="post"
                data-remote="true"
                data-params="id={{ $emailCampaign->id }}"
                rel="nofollow">
                    <input type="submit" value="Добавить пользователей" data-disable-with="Добавляем" disabled>
                </a>
                <a href="{{ route('admin.panel.email_campaigns.unsubscribe', $emailCampaign) }}"
                data-type="json"
                data-method="delete"
                rel="nofollow">
                    <input type="submit" value="Отписать пользователей" data-disable-with="Отписываем" disabled>
                </a>
                <a href="{{ route('admin.panel.email_campaigns.add_discount_for_users_with_free_preset_promotion_send') }}"
                data-type="json"
                data-method="post"
                data-remote="true"
                data-params="id={{ $emailCampaign->id }}"
                rel="nofollow">
                    <input type="submit" value="Отправить" data-disable-with="Отправляю" disabled>
                </a>
                <div class="mt-5">
                    Фильтры
                    {{Form::open(['url' => route('admin.panel.email_campaigns.add_discount_for_users_with_free_preset_promotion.show'), 'method' => 'GET'])}}
                        {{-- Имя пользователя передано из экшена --}}
                        {{ Form::label('order_status', 'Статус заказа:') }}
                        {{ Form::select('order_status', ['processing' => 'processing', 'completed' => 'completed'], null, ['placeholder' => 'выбрать...']) }}
                        {{ Form::submit('Найти') }}
                    {{Form::close()}}

                </div>
                <div><a href="{{  route('admin.panel.email_campaigns.add_discount_for_users_with_free_preset_promotion.show') }}">Очистить</a></div>

                {{--
                <form method="POST" action="{{ route('admin.panel.promotions.store') }}">
                    @csrf
                    <input type="hidden" name="id" value="{{ $emailCampaign->id }}">
                    <input type="hidden" name="users" value="{{ $users }}">
                    <button type="submit">Добавить акции</button>
                </form>
                <form method="POST" action="{{ route('admin.panel.mail.rollback', $emailCampaign->id) }}">
                    @csrf
                    <button type="submit">Убрать</button>
                </form>
                <p></p>
                <form method="POST" action="#">
                    @csrf
                    <button type="submit" disabled>Отправить всем</button>
                </form> --}}
            </div>
        </div>
        <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
            <div class="table-responsive" style="font-size: .875rem">
                <table class="table table-striped table-sm">
                    <thead class="thead-dark">
                    <tr>
                        <th>id</th>
                        <th>email</th>
                        <th>Gift?</th>
                        <th>заказы</th>
                        <th>скидки</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($subscriptions as $subscription)
                        <tr>
{{--                            <td>--}}
{{--                                <form method="POST" action="{{ route('admin.panel.email_campaigns.send') }}" class="js-send-mail-form">--}}
{{--                                    @csrf--}}
{{--                                    <input name="user_id" type="hidden" value="{{ $user->id }}">--}}
{{--                                    <button type="submit" class="btn btn-sm {{ $user->email_campaigns->find($emailCampaign->id)->pivot->is_mail_send ? 'btn-success' : 'btn-danger' }} js-send-mail-btn" {{ $user->email_campaigns->find($emailCampaign->id)->pivot->is_mail_send ? 'disabled' : '' }}>Отправить</button>--}}
{{--                                </form>--}}
{{--                            </td>--}}
                            <td>{{ $subscription->user->id }}</td>
                            <td>{{ $subscription->user->email }}</td>
                            <td>{{ $subscription->user->gifts ? 'yes' : 'no' }}</td>
                            <td>
                                <table class="table table-striped table-sm">
                                    <thead class="thead-dark">
                                    <tr>
                                        <th></th>
                                        <th>номер</th>
                                        <th>дата</th>
                                        <th>total</th>
                                        <th>статус</th>
                                        <th>валюта</th>
                                        <th>тип</th>
                                        <th>название</th>
                                        <th>удален?</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($subscription->user->orders as $order)
                                        <tr>
                                            <td>
                                                <form method="POST" action="{{ route('admin.panel.email_campaigns.send') }}" class="js-send-mail-form">
                                                    @csrf
                                                    <input name="user_id" type="hidden" value="{{ $subscription->user->id }}">
                                                    <input name="user_email" type="hidden" value="{{ $subscription->user->email }}">
                                                    <input name="product_id" type="hidden" value="{{ $order->product->id }}">
                                                    <input name="product_type" type="hidden" value="{{ $order->product->type }}">
                                                    <input name="title" type="hidden" value="{{ $emailCampaign->title }}">
                                                    {{-- <button type="submit" class="btn btn-sm {{ $subscription->user->email_campaigns->find($emailCampaign->id)->pivot->is_mail_send ? 'btn-success' : 'btn-danger' }} js-send-mail-btn" {{ $user->email_campaigns->find($emailCampaign->id)->pivot->is_mail_send ? 'disabled' : '' }}>Отправить</button> --}}
                                                </form>
                                            </td>
                                            {{-- <td> <a href="{{ $order->user->downloadables ? (new \App\Services\CreateDownloadableUrlWithToken())->call($order->user->downloadables()->latest()->firstWhere('product_id', $order->product->id)->token ?? '') : '#' }}">{{ $order->id }}</a></td> --}}
                                            <td> <a href="#">{{ $order->id }}</a></td>
                                            <td>{{ $order->created_at }}</td>
                                            <td>{{ $order->total }}</td>
                                            <td>{{ $order->status }}</td>
                                            <td>{{ $order->currency ? $order->currency : 'RUB' }}</td>
                                            <td>{{ $order->product->type ?? ''}}</td>
                                            <td>{{ $order->product->title ?? ''}}</td>
                                            <td>{{ $order->deleted ? 'Да' : 'Нет' }}</td>

                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </td>
                            <td>
                                <table class="table table-striped table-sm">
                                    <thead class="thead-dark">
                                    <tr>
                                        <th>Продукт</th>
                                        <th>Создана</th>
                                        <th>Обновлена</th>
                                        <th>цена рубли</th>
                                        <th>цена доллары</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($subscription->user->discounts as $discount)
                                        <tr>
                                            <th><a href="{{ $discount->product ? route('products.show', $discount->product) : '' }}">{{ $discount ? $discount->product->title : '' }}</a></th>
                                            <td>{{ $discount ? $discount->created_at->timezone('Europe/Moscow') : '' }}</td>
                                            <td>{{ $discount ? $discount->updated_at->timezone('Europe/Moscow') : '' }}</td>
                                            <td>{{ $discount ? $discount->rub_price: '' }}</td>
                                            <td>{{ $discount ? $discount->usd_price: '' }}</td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                {{ $subscriptions->withQueryString()->links() }}
            </div>
        </main>
    </div>
</div>


    <script src="{{ mix('js/app.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/select2.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/floatingFormLabels.min.js') }}"></script>
    <script src="{{ asset('js/jquery.event.move.js') }}"></script>
    <script src="{{ asset('js/jquery.twentytwenty.js') }}"></script>
    <script src="{{ asset('js/libs/jquery.mousewheel.js') }}"></script>
    <script src="{{ asset('js/libs/SmoothScroll.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/app_custom.js') }}"></script>

    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
        });

        $(document).on('submit', '.js-send-mail-form', function(e) {
            e.preventDefault();
            const that = $(this);
            const submitBtn = that.find('.js-send-mail-btn');
            submitBtn.prop('disabled', true);
            $.ajax({
                type: 'POST',
                url: that.attr('action'),
                data: that.serialize(),
                dataType: 'json',
                success: function (data) {
                    console.log(data);
                    submitBtn.removeClass('btn-danger');
                    submitBtn.addClass('btn-success');
                },
                error: function (data) {
                    submitBtn.addClass('btn-success');
                    submitBtn.prop('disabled', false);
                }
            });
        });
    </script>

    </body>
    </html>
