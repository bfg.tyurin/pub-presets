<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta name="robots" content="noindex, nofollow" />
    <meta charset="utf-8">
    <meta content="width=device-width,initial-scale=1" name="viewport">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Пресеты Жени Савиной</title>
    <meta content="Пресеты Жени Савиной" name="description">

    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@400;500;600;700;800;900&display=swap" rel="stylesheet">

    <link href="{{ mix('css/app.css')}}" rel="stylesheet">
</head>

<body>
<nav class="navbar navbar-dark sticky-top bg-dark flex-md-nowrap p-0">
    <a class="navbar-brand col-sm-3 col-md-2 mr-0" href="{{ url('/') }}">Пресеты Жени Савиной</a>
    <ul class="navbar-nav px-3">
        <li class="nav-item text-nowrap">
            <a class="nav-link" href="{{ route('admin.logout') }}">Выйти</a>
        </li>
    </ul>
</nav>



<div class="container-fluid">
    <div class="row">
        <div class="col-md-2 d-none d-md-block bg-light sidebar">
            <div class="sidebar-sticky">

            </div>
        </div>
        <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
            <div class="table-responsive" style="font-size: .875rem">
                <table class="table table-striped table-sm">
                    <thead class="thead-dark">
                    <tr>
                        <th>#</th>
                        <th>Пользователь</th>
                        <th>Товар</th>
                        <th>Тип товара</th>
                        <th>Стоимость</th>
                        <th>Статус</th>
                        <th>Тип платежа</th>
                        <th>Создан</th>
                        <th>Обновлен</th>
                        <th>Действия</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($orders as $order)
                        <tr>
                            <th>{{ $order->id }}</th>
                            <td>{{ $order->user->email }}</td>
                            <td>{{ $order->product->title }}</td>
                            <td>{{ $order->product->type }}</td>
                            <td>{{ $order->total }}</td>
                            <td>{{ $order->status }}</td>
                            <td>{{ $order->payment_type }}</td>
                            <td>{{ $order->created_at->timezone('Europe/Moscow') }}</td>
                            <td>{{ $order->updated_at->timezone('Europe/Moscow') }}</td>
                            <td><a href="#editEmployeeModal" class="edit" data-toggle="modal"><i class="material-icons" data-toggle="tooltip" title="Edit">&#xE254;</i></a></td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </main>
    </div>
</div>


<script src="{{ mix('js/app.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/select2.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/floatingFormLabels.min.js') }}"></script>
<script src="{{ asset('js/jquery.event.move.js') }}"></script>
<script src="{{ asset('js/jquery.twentytwenty.js') }}"></script>
<script src="{{ asset('js/libs/jquery.mousewheel.js') }}"></script>
<script src="{{ asset('js/libs/SmoothScroll.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/app_custom.js') }}"></script>


</body>
</html>

