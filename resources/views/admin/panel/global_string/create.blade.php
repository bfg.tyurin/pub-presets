<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta name="robots" content="noindex, nofollow" />
    <meta charset="utf-8">
    <meta content="width=device-width,initial-scale=1" name="viewport">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Пресеты Жени Савиной</title>
    <meta content="Пресеты Жени Савиной" name="description">

    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@400;500;600;700;800;900&display=swap" rel="stylesheet">

    <link href="{{ mix('css/app.css')}}" rel="stylesheet">
</head>

<body>
<nav class="navbar navbar-dark sticky-top bg-dark flex-md-nowrap p-0">
    <a class="navbar-brand col-sm-3 col-md-2 mr-0" href="{{ url('/') }}">Пресеты Жени Савиной</a>
    <p class="navbar-brand col-sm-3 col-md-2 mr-0" href="{{ url('/') }}">Products</p>
<ul class="navbar-nav px-3">
    <li class="nav-item text-nowrap">
        <a class="nav-link" href="{{ route('admin.logout') }}">Выйти</a>
    </li>
</ul>
</nav>



<div class="container-fluid">
    <div class="row">
        <div class="col-md-2 d-md-block bg-light sidebar">
            <div class="sidebar-sticky">

            </div>
        </div>
        <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
            <a href="{{ route('admin.panel.global_strings.create') }}"><button class="btn-sm">Add</button></a>
            {{ Form::model($globalString, ['url' => route('admin.panel.global_strings.store')]) }}
            {{ Form::label('key', 'Ключ') }}
            {{ Form::text('key') }}<br>
            {{ Form::label('ru', 'Ru') }}
            {{ Form::text('ru') }}<br>
            {{ Form::label('en', 'En') }}
            {{ Form::text('en') }}<br>
            {{ Form::submit('Создать') }}
        {{ Form::close() }}
        </main>
    </div>
</div>
<script src="{{ mix('js/app.js') }}"></script>
</body>
</html>
