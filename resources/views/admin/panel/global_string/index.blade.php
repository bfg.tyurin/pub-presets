<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta name="robots" content="noindex, nofollow" />
    <meta charset="utf-8">
    <meta content="width=device-width,initial-scale=1" name="viewport">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="csrf-param" content="_token">
    <title>Пресеты Жени Савиной</title>
    <meta content="Пресеты Жени Савиной" name="description">

    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@400;500;600;700;800;900&display=swap" rel="stylesheet">

    <link href="{{ mix('css/app.css')}}" rel="stylesheet">
</head>

<body>
<nav class="navbar navbar-dark sticky-top bg-dark flex-md-nowrap p-0">
    <a class="navbar-brand col-sm-3 col-md-2 mr-0" href="{{ url('/') }}">Пресеты Жени Савиной</a>
    <p class="navbar-brand col-sm-3 col-md-2 mr-0" href="{{ url('/') }}">Global Strings</p>
<ul class="navbar-nav px-3">
    <li class="nav-item text-nowrap">
        <a class="nav-link" href="{{ route('admin.logout') }}">Выйти</a>
    </li>
</ul>
</nav>



<div class="container-fluid">
    <div class="row">
        <div class="col-md-2 d-md-block bg-light sidebar">
            <div class="sidebar-sticky">

            </div>
        </div>
        <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
            <a href="{{ route('admin.panel.global_strings.create') }}"><button class="btn-sm">Add</button></a>
            <div class="table-responsive" style="font-size: .875rem">
                <table class="table table-striped table-sm">
                    <thead class="thead-dark">
                    <tr>
                        <th></th>
                        <th>Ключ</th>
                        <th>Ru</th>
                        <th>En</th>
                        <th></th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($globalStrings as $globalString)
                        <tr>
                            <td></td>
                            <td>{{ $globalString->key }}</td>
                            <td>{{ $globalString->ru }}</td>
                            <td>{{ $globalString->en }}</td>
                            <td><a href="{{ route('admin.panel.global_strings.edit', $globalString) }}"><button class="btn-sm">edit</button></a></td>
                            <td><a href="{{ route('admin.panel.global_strings.destroy', $globalString) }}" data-confirm="Вы уверены?" data-method="delete" rel="nofollow">delete</a></td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </main>
    </div>
</div>

<script src="{{ mix('js/app.js') }}"></script>

</body>
</html>
