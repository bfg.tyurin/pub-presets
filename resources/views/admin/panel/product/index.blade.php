<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta name="robots" content="noindex, nofollow" />
    <meta charset="utf-8">
    <meta content="width=device-width,initial-scale=1" name="viewport">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Пресеты Жени Савиной</title>
    <meta content="Пресеты Жени Савиной" name="description">

    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@400;500;600;700;800;900&display=swap" rel="stylesheet">

    <link href="{{ mix('css/app.css')}}" rel="stylesheet">
</head>

<body>
<nav class="navbar navbar-dark sticky-top bg-dark flex-md-nowrap p-0">
    <a class="navbar-brand col-sm-3 col-md-2 mr-0" href="{{ url('/') }}">Пресеты Жени Савиной</a>
    <p class="navbar-brand col-sm-3 col-md-2 mr-0" href="{{ url('/') }}">Products</p>
<ul class="navbar-nav px-3">
    <li class="nav-item text-nowrap">
        <a class="nav-link" href="{{ route('admin.logout') }}">Выйти</a>
    </li>
</ul>
</nav>



<div class="container-fluid">
    <div class="row">
        <div class="col-md-2 d-md-block bg-light sidebar">
            <div class="sidebar-sticky">

            </div>
        </div>
        <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
            <div class="table-responsive" style="font-size: .875rem">
                <table class="table table-striped table-sm">
                    <thead class="thead-dark">
                    <tr>
                        <th></th>
                        <th>Название</th>
                        <th>Цена в рублях</th>
                        <th>Цена в долларах</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($products as $product)
                        <tr>
                            <td></td>
                            <td>{{ $product->title }}</td>
                            <td>{{ $product->price_discount }}</td>
                            <td>{{ $product->usd_price_discount }}</td>
                            <td><a href="{{ route('admin.panel.products.edit', $product) }}"><button class="btn-sm">edit</button></a></td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </main>
    </div>
</div>

</body>
</html>
