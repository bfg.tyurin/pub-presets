{{ Form::model($product, ['url' => route('admin.panel.products.update', $product), 'method' => 'PATCH']) }}
    @if ($errors->any())
        <div>
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
        </div>
    @endif

    {{ Form::label('title', 'Название') }}
    {{ Form::text('title') }}<br>
    {{ Form::label('price_discount', 'Цена в рублях') }}
    {{ Form::number('price_discount') }}<br>
    {{ Form::label('usd_price_discount', 'Цена в долларах') }}
    {{ Form::number('usd_price_discount') }}<br>
    {{ Form::submit('Сохранить') }}
{{ Form::close() }}
