<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta name="robots" content="noindex, nofollow" />
    <meta charset="utf-8">
    <meta content="width=device-width,initial-scale=1" name="viewport">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Пресеты Жени Савиной</title>
    <meta content="Пресеты Жени Савиной" name="description">

    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@400;500;600;700;800;900&display=swap"
        rel="stylesheet">

    <link href="{{ mix('css/app.css')}}" rel="stylesheet">
</head>

<body>
    <nav class="navbar navbar-dark sticky-top bg-dark flex-md-nowrap p-0">
        <a class="navbar-brand col-sm-3 col-md-2 mr-0" href="{{ url('/') }}">Пресеты Жени Савиной</a>
        <ul class="navbar-nav px-3">
            <li class="nav-item text-nowrap">
                <a class="nav-link" href="{{ route('admin.logout') }}">Выйти</a>
            </li>
        </ul>
    </nav>



    <div class="container-fluid">
        <div class="row">
            <div class="col-md-2 col-lg-8"></div>
            <main role="main" class="col-md-8 ml-sm-auto col-lg-8 px-4">
                <div class="container">
                    <p></p>
                    <p>Всего покупок: {{ count($orders) }}</p>
                    <table class="table table-sm table-responsive" style="padding-bottom: 10px">
                        <thead>
                            <tr>
                                <th scope="col">Название</th>
                                <th scope="col">Количество</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($products as $product)
                            <tr>
                                <td>{{ $product->title }}</td>
                                <td>{{ count($product->campaignOrders) }}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="row">
                    <x-admin-totals-component :service="$rubCampaignAdminTotalService" />
                    <x-admin-totals-component :service="$usdCampaignAdminTotalService" />
                </div>
                <div class="table-responsive" style=" font-size: .875rem">
                    <table class="table table-striped table-sm">
                        <thead class="thead-dark">
                            <tr>
                                <th>Email</th>
                                <th>Название</th>
                                <th>Цена</th>
                                <th>Валюта</th>
                                <th>Источник кампании</th>
                                <th>Идентификатор объявления</th>
                                <th>Тип трафика</th>
                                <th>Ключевое слово</th>
                                <th>Название кампании</th>
                                <th>Создан</th>
                                <th>Обновлен</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($orders as $order)
                            <tr>
                                <th>{{ $order->user->email }}</th>
                                {{-- <td>{{ $order->product->title }}</td> --}}
                                <td>
                                    @foreach ($order->products as $product)
                                        {{ $product->title }}<br>
                                    @endforeach
                                </td>
                                <td>{{ $order->total }}</td>
                                <th>{{ $order->currency ? $order->currency : 'RUB' }}</th>
                                <td>{{ $order->user->campaign->utm_source }}</td>
                                <td>{{ $order->user->campaign->utm_content }}</td>
                                <td>{{ $order->user->campaign->utm_medium }}</td>
                                <td>{{ $order->user->campaign->utm_term }}</td>
                                <td>{{ $order->user->campaign->utm_campaign }}</td>
                                <td>{{ $order->created_at->timezone('Europe/Moscow') }}</td>
                                <td>{{ $order->updated_at->timezone('Europe/Moscow') }}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </main>
            <div class="col-md-2 col-lg-2"></div>
        </div>
    </div>


    <script src="{{ mix('js/app.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/select2.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/floatingFormLabels.min.js') }}"></script>
    <script src="{{ asset('js/jquery.event.move.js') }}"></script>
    <script src="{{ asset('js/jquery.twentytwenty.js') }}"></script>
    <script src="{{ asset('js/libs/jquery.mousewheel.js') }}"></script>
    <script src="{{ asset('js/libs/SmoothScroll.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/app_custom.js') }}"></script>


</body>

</html>
