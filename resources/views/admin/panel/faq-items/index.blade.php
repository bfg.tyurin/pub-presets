<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta name="robots" content="noindex, nofollow" />
    <meta charset="utf-8">
    <meta content="width=device-width,initial-scale=1" name="viewport">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Пресеты Жени Савиной</title>
    <meta content="Пресеты Жени Савиной" name="description">

    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@400;500;600;700;800;900&display=swap" rel="stylesheet">

    <link href="{{ mix('css/app.css')}}" rel="stylesheet">
</head>

<body>
<nav class="navbar navbar-dark sticky-top bg-dark flex-md-nowrap p-0">
    <a class="navbar-brand col-sm-3 col-md-2 mr-0" href="{{ url('/') }}">Пресеты Жени Савиной - FAQ ({{ $items->count() }})</a>
    <ul class="navbar-nav px-3">
        <li class="nav-item text-nowrap">
            <a class="nav-link" href="{{ route('admin.logout') }}">Выйти</a>
        </li>
    </ul>
</nav>



<div class="container-fluid">
    <div class="row">
        <a href="{{ route('admin.panel.faq-items.create') }}" class="btn btn-primary btn-lg active" role="button">Добавить</a>
    </div>
    <div class="row">
        <main role="main" class="col-md-12 ml-sm-auto col-lg-12 px-4">
            <div class="table-responsive" style=" font-size: .875rem">
                <table class="table table-striped table-sm">
                    <thead class="thead-dark">
                    <tr>
                        <th>Вопрос ru</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($items as $item)
                        <tr>
                            <td>{{ $item->question_ru }}</td>
                            <td><button><a href="{{ route('admin.panel.faq-items.edit', $item) }}">Edit</a></button></td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </main>
    </div>
</div>


<script src="{{ mix('js/app.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/select2.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/floatingFormLabels.min.js') }}"></script>
<script src="{{ asset('js/jquery.event.move.js') }}"></script>
<script src="{{ asset('js/jquery.twentytwenty.js') }}"></script>
<script src="{{ asset('js/libs/jquery.mousewheel.js') }}"></script>
<script src="{{ asset('js/libs/SmoothScroll.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/app_custom.js') }}"></script>

<script>
    $(document).ready(function() {

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
        });

        console.log('dashboard');
        $('.js-find-user-form').on('submit', function(e) {
            e.preventDefault();
            console.log($(this).attr('action'));
            const table = $('.table');
            table.find("tbody tr").remove();

            $.ajax({
                type: 'POST',
                url: $(this).attr('action'),
                data: $(this).serialize(),
                dataType: 'json',
                success: function (data) {
                    console.log(data);
                    data.downloadables.forEach(function (downloadable) {
                        table.append('<tr><td><button class="btn btn-danger btn-sm js-send-mail-btn" data-email="' + downloadable.user_email + '" data-url="' + downloadable.product_page_url + '" data-endpoint="' + data.url + '">Отправить</button></td><td><span class="js-user-email">' + downloadable.user_email + "</span></td><td>" + downloadable.product_title + "</td><td>" + downloadable.is_completed_order + "</td><td>" + '<span class="js-product-page-url">' + downloadable.product_page_url + "</span></td></tr>");
                    });
                },
            });
        });

        $(document).on('click', '.js-faq-item-update-btn', function(e) {
            e.preventDefault();
            const that = $(this);
            that.prop('disabled', true);
            $.ajax({
                type: 'PATCH',
                url: '/support/dashboard/mail',
                data: {
                    faqItemId : that.data('faqItemId'),
                    question_ru: that.data('question_ru'),
                    question_en: that.data('question_en'),
                    answer_ru: that.data('answer_ru'),
                    answer_en: that.data('answer_en'),
                },
                dataType: 'json',
                success: function (data) {
                    console.log(data);
                    that.removeClass('btn-danger');
                    that.addClass('btn-success');
                },
                error: function (data) {
                    that.addClass('btn-success');
                    $(this).prop('disabled', false);
                }
            });
        });

    });
</script>

</body>
</html>
