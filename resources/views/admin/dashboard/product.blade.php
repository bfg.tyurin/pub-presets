<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta name="robots" content="noindex, nofollow" />
    <meta charset="utf-8">
    <meta content="width=device-width,initial-scale=1" name="viewport">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Пресеты Жени Савиной</title>
    <meta content="Пресеты Жени Савиной" name="description">

    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@400;500;600;700;800;900&display=swap" rel="stylesheet">

    <link href="{{ mix('css/app.css')}}" rel="stylesheet">
</head>

<body class="text-center">
    <div class="form-signin card" style="width: 18rem;">
        <div class="card-body">
            <h5 class="card-title">{{ $product->title }}</h5>

        </div>
        <ul class="list-group list-group-flush">
            <li class="list-group-item">Слаг: {{ $product->slug }}</li>
            <li class="list-group-item">Цена: {{ $product->price_original }}</li>
            <li class="list-group-item">Цена со скидкой: {{ $product->price_discount }}</li>
        </ul>
        <div class="card-body">
            <a href="{{ route('products.show', $product) }}" class="card-link">Ссылка на сайт</a>
        </div>
    </div>
</body>
</html>
