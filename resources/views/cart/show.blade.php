{{-- not used in v2 --}}
@extends('layout')

@section('content')


<?php

use Illuminate\Support\Facades\Cookie;
    $ids = ["golden-metallic-wedding","golden-caramel","wedding-pureness","metallic-blues"];
    if(isset($_GET["name"])) $id = trim($_GET["name"]);
    else $id = "golden-metallic-wedding";

    if (!in_array($id, $ids)) $id = "golden-metallic-wedding";

    switch ($id) {
        case "golden-metallic-wedding":
            $price = 5600;
            $class1 = $class2 = $class3 = $style1 = $style2 = $style3 = "";

        break;
        case "golden-caramel":
            $price = 3300;
            $name = "Golden Caramel";
            $class1 = "pt-single";
            $class2 = $class3 = $style1 = "";
            $style2 = $style3 = "display: none";

        break;
        case "metallic-blues":
            $price = 1980;
            $name = "Metallic Blues";
            $class2 = "pt-single";
            $class1 = $class3 = $style2 = "";
            $style1 = $style3 = "display: none";
        break;
        case "wedding-pureness":
            $price = 2420;
            $name = "Wedding Pureness";
            $class3 = "pt-single";
            $class2 = $class1 = $style3  = "";
            $style1 = $style2 = "display: none";
        break;
    }
?>


<div class="preset_page preset_payment_page">
    <section class="page_back">
        <div class="container">
            <a href="" onclick="javascript:history.back(); return false;" class="btn_page_back"></a>
        </div>
    </section>


    <section class="payment_page">
        <div class="container">
            <div class="row d-flex justify-content-between payment_row">
                <div class="col-12 col-md-6 col-xl-4">
                    <div class="payment_text">
                        @lang('Оплатить можно картой и PayPal любой страны и валюты. Также доступны Apple и Google Pay.<br><br>Не закрывайте сайт после оплаты. Вы сразу получите ссылку на скачивание.<br><br>Плюс продублируем её на электронную почту.')
                    </div>
                </div>
                <div class="col-12 col-md-6">
                    <?php if($id=="golden-metallic-wedding") {?>
                    <div class="payment_info">
                        <div>
                            <div class="payment_all_title">Golden Caramel +</div>
                            <div class="payment_all_title">Metallic Blues +</div>
                            <div class="payment_all_title">Wedding Pureness</div>
                            <div class="payment__count">25 @lang('пресетов')</div>
                        </div>
{{--                        <div class="payment_price payment_tarif selected" data-slug="<?=$id;?>" data-cost="{{ $product->price_discount }}">{{ number_format($product->price_discount, 0, ".", " ") }} <i class="far fa-ruble-sign"></i></div>--}}

                        @switch(LaravelLocalization::getCurrentLocale())
                            @case('ru')
                                <div class="payment_price payment_tarif selected" data-slug="<?=$id;?>" data-cost="{{ $product->price_discount }}">{{ number_format($product->price_discount, 0, ".", " ") }} <i class="far fa-ruble-sign"></i></div>
                                @break
                            @case('en')
                                <div class="payment_price payment_tarif selected" data-slug="<?=$id;?>" data-cost="{{ $product->usd_price_discount }}"><i class="fal fa-dollar-sign"></i> {{ number_format($product->usd_price_discount, 0, ".", " ") }}</div>
                                @break
                        @endswitch
                    </div>
                    <?php }
                    else {?>
                        <div class="row payment_tarifs">
                        <div class="col-6">

                            @switch(LaravelLocalization::getCurrentLocale())
                                @case('ru')
                                <div class="payment_tarif selected" data-tarif="1" data-slug="<?=$id;?>" data-cost="{{ $product->price_discount }}">
                                    <div class="payment_tarif_block">
                                        <div class="pt_title_big"><?=$name;?></div>
                                        <div class="pt_quan">10 @lang('пресетов')</div>
                                        <div class="pt_price">{{ number_format($product->price_discount, 0, ".", " ") }} <i class="far fa-ruble-sign"></i></div>
                                        <div class="pt_checkbox"></div>
                                    </div>
                                    <div class="payment_tarif_bottom">
                                        <div class="pt_text pt_text__red">
                                            {{--                                        <span>Вы можете доплатить <?php echo 5600-$price; ?> рублей</span> и получить сразу все <span>3 пака пресетов,</span> сэкономив более 6000 рублей--}}
                                            @lang('<span>Вы можете доплатить :price рублей</span> и получить сразу все <span>3 пака пресетов,</span> сэкономив более 6000 рублей', ['price' => number_format(($bundle->price_discount - $product->price_discount), 0, ".", " ")])
                                        </div>
                                    </div>
                                </div>
                                @break

                                @case('en')
                                <div class="payment_tarif selected" data-tarif="1" data-slug="<?=$id;?>" data-cost="{{ $product->usd_price_discount }}">
                                    <div class="payment_tarif_block">
                                        <div class="pt_title_big"><?=$name;?></div>
                                        <div class="pt_quan">10 @lang('пресетов')</div>
                                        <div class="pt_price"><i class="far fa-dollar-sign"></i> {{ number_format($product->usd_price_discount, 0, ".", " ") }}</div>
                                        <div class="pt_checkbox"></div>
                                    </div>
                                    <div class="payment_tarif_bottom">
                                        <div class="pt_text pt_text__red">
                                            {{--                                        <span>Вы можете доплатить <?php echo 5600-$price; ?> рублей</span> и получить сразу все <span>3 пака пресетов,</span> сэкономив более 6000 рублей--}}
                                            @lang('<span>Вы можете доплатить :price рублей</span> и получить сразу все <span>3 пака пресетов,</span> сэкономив более 6000 рублей', ['price' => number_format(($bundle->usd_price_discount - $product->usd_price_discount), 0, ".", " ")])
                                        </div>
                                    </div>
                                </div>
                                @break
                            @endswitch
                        </div>
                        <div class="col-6">

                            @switch(LaravelLocalization::getCurrentLocale())
                                @case('ru')
                                    <div class="payment_tarif" data-tarif="all" data-slug="golden-metallic-wedding" data-cost="{{ $bundle->price_discount }}">
                                        <div class="payment_tarif_block">
                                            <div class="pt_title_small">Golden Caramel +</div>
                                            <div class="pt_title_small">Metallic Blues +</div>
                                            <div class="pt_title_small">Wedding Pureness</div>
                                            <div class="pt_quan">25 @lang('пресетов')</div>
                                            <div class="pt_price">{{ number_format($bundle->price_discount, 0, ".", " ") }} <i class="far fa-ruble-sign"></i></div>
                                            <div class="pt_checkbox"></div>
                                        </div>
                                        <div class="payment_tarif_bottom">
                                            <div class="pt_text pt_text__green">
                                                @lang('<span>3 пака по цене 2</span><br><span>Экономия более 6000 рублей</span>')
                                            </div>
                                        </div>
                                    </div>
                                    @break
                                @case('en')
                                    <div class="payment_tarif" data-tarif="all" data-slug="golden-metallic-wedding" data-cost="{{ $bundle->usd_price_discount }}">
                                        <div class="payment_tarif_block">
                                            <div class="pt_title_small">Golden Caramel +</div>
                                            <div class="pt_title_small">Metallic Blues +</div>
                                            <div class="pt_title_small">Wedding Pureness</div>
                                            <div class="pt_quan">25 @lang('пресетов')</div>
                                            <div class="pt_price"><i class="fal fa-dollar-sign"></i> {{ number_format($bundle->usd_price_discount, 0, ".", " ") }}</div>
                                            <div class="pt_checkbox"></div>
                                        </div>
                                        <div class="payment_tarif_bottom">
                                            <div class="pt_text pt_text__green">
                                                @lang('<span>3 пака по цене 2</span><br><span>Экономия более 6000 рублей</span>')
                                            </div>
                                        </div>
                                    </div>
                                    @break
                            @endswitch


{{--                            <div class="payment_tarif" data-tarif="all" data-slug="golden-metallic-wedding" data-cost="{{ $bundle->price_discount }}">--}}
{{--                                <div class="payment_tarif_block">--}}
{{--                                    <div class="pt_title_small">Golden Caramel +</div>--}}
{{--                                    <div class="pt_title_small">Metallic Blues +</div>--}}
{{--                                    <div class="pt_title_small">Wedding Pureness</div>--}}
{{--                                    <div class="pt_quan">25 @lang('пресетов')</div>--}}
{{--                                    <div class="pt_price">{{ number_format($bundle->price_discount, 0, ".", " ") }} <i class="far fa-ruble-sign"></i></div>--}}

{{--                                    @switch(LaravelLocalization::getCurrentLocale())--}}
{{--                                        @case('ru')--}}
{{--                                        <div class="payment_price payment_tarif selected" data-slug="<?=$id;?>" data-cost="{{ $product->price_discount }}">{{ number_format($product->price_discount, 0, ".", " ") }} <i class="far fa-ruble-sign"></i></div>--}}
{{--                                        @break--}}
{{--                                        @case('en')--}}
{{--                                        <div class="payment_price payment_tarif selected" data-slug="<?=$id;?>" data-cost="{{ $product->usd_price_discount }}"><i class="fal fa-dollar-sign"></i> {{ number_format($product->usd_price_discount, 0, ".", " ") }}</div>--}}
{{--                                        @break--}}
{{--                                    @endswitch--}}

{{--                                    <div class="pt_checkbox"></div>--}}
{{--                                </div>--}}
{{--                                <div class="payment_tarif_bottom">--}}
{{--                                    <div class="pt_text pt_text__green">--}}
{{--                                        @lang('<span>3 пака по цене 2</span><br><span>Экономия более 6000 рублей</span>')--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                            </div>--}}
                        </div>
                    </div>
                    <?php } ?>
                </div>
            </div>
            <div class="row d-flex justify-content-between">
                <div class="col-12 col-md-6 col-xl-4  ">
                    <div class="home_price_block payment_price_block">
                        <div class="pb_left">
                            <div class="payment_text">
                                @lang('Возникли сложности с оплатой?<br>Свяжитесь с нами и мы поможем')
                            </div>
                            <div class="faq_add__links">
                                <a target="_blank" href="https://tele.gg/denis_ayupov" class="faq_add__link">
                                    <img src="{{ asset('img/svg/telegram.svg') }}" alt="">telegram
                                </a>
                                <a target="_blank" href="https://wa.me/79874749387" class="faq_add__link">
                                    <img src="{{ asset('img/svg/whatsup.svg') }}" alt="">whatsapp
                                </a>
                            </div>
                        </div>
                        <div class="pp_block__images">
                            <img class="<?=$class1;?>" style="<?=$style1;?>" data-image="1" src="{{ asset('images/buy/1.png') }}" alt="">
                            <img class="<?=$class2;?>" style="<?=$style2;?>" data-image="2" src="{{ asset('images/buy/2.png') }}" alt="">
                            <img class="<?=$class3;?>" style="<?=$style3;?>" data-image="3" src="{{ asset('images/buy/3.png') }}" alt="">
                        </div>
                    </div>

                </div>
                <div class="col-12 col-md-6">
                    <div class="input_box">
                    <form action="{{ route('cart.show') }}" method="POST" class="js-cart-form">
                        <div class="input_label">@lang('Введите ваш e-mail')</div>
                        <div class="ffl-wrapper p_input_wrapper">

                            <label class="ffl-label" for="input-1">e-mail</label>
                            <input type="text" required id="email_for_pay" value="{{ $cookie_email ? $cookie_email : $user_email}}">
                            <span class="p_input_error">@lang('Заполните отмеченные поля')</span>
                            <input type="hidden" name="cost" value="<?=$price;?>" id="price_for_pay"/>
                            <input type="hidden" name="slug" value="<?=$id;?>" id="slug"/>

                        </div>
                    </form>
                </div>

                    @if(LaravelLocalization::getCurrentLocale() === 'ru')
                        <a href="" class="button_payment_big" id="pay_by_card">
                            <span>Оплатить <b>Картой, Apple и Google Pay</b></span>
                        </a>
                    @endif
                    <form class="js-paypal-form" action="{{ route('paypal.beforePayment') }}" method="POST">
                        <button type="submit" class="button_payment_big" >
                            <span>@lang('Оплатить <b>paypal</b>')</span>
                        </button>
                    </form>

                    <div class="payment_bottom_text">@lang('Нажимая кнопку оплатить вы даете согласие с <a href="https://willteach.ru/oferta.pdf" style="color: #4F4336" target="_blank">Договором оферты</a> и <a href="https://willteach.ru/politika.pdf" style="color: #4F4336" target="_blank">Политикой конфиденциальности</a> и обработки персональных данных')</div>
                    <br/><br/><br/><br/>
                    <div class="pb_right">
                        <div class="payment_text">
                            @lang('Возникли сложности с оплатой?<br>Свяжитесь с нами и мы поможем')
                        </div>
                        <div class="faq_add__links">
                            <a target="_blank" href="https://tele.gg/denis_ayupov" class="faq_add__link">
                                <img src="{{ asset('img/svg/telegram.svg') }}" alt="">telegram
                            </a>
                            <a target="_blank" href="https://wa.me/79874749387" class="faq_add__link">
                                <img src="{{ asset('img/svg/whatsup.svg') }}" alt="">whatsapp
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>


</div>
@endsection
