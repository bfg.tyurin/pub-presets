<section class="faq">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 d-flex flex-column justify-content-between">
                <h2 class="faq__h2">@lang('Ответы<br>на популярные<br>вопросы')</h2>
                <div class="faq_add">
                    <div class="faq_add__title">
                        @lang('Остались вопросы?<br>Пишите и мы ответим')
                    </div>
                    <div class="faq_add__links">
                        <a target="_blank" href="https://tele.gg/denis_ayupov" class="faq_add__link">
                            <img src="{{ asset('img/svg/telegram.svg') }}" alt="">telegram
                        </a>
                        <a target="_blank" href="https://wa.me/79874749387" class="faq_add__link">
                            <img src="{{ asset('img/svg/whatsup.svg') }}" alt="">whatsapp
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="faq_list">
                    @php
                        $items = \App\FaqItem::orderBy('order')->get();
                    @endphp
                    @foreach($items as $item)
                        <div class="faq_item">
                            <div class="faq_question">{!! $item->question() !!}</div>
                            <div class="faq_answer">
                                {!! $item->answer() !!}
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
            <div class="col-12">
                <div class="faq_add_md">
                    <div class="faq_add__title">
                        @lang('Остались вопросы?<br>Пишите и мы ответим')
                    </div>
                    <div class="faq_add__links">
                        <a target="_blank" href="https://tele.gg/denis_ayupov" class="faq_add__link">
                            <img src="{{ asset('img/svg/telegram.svg') }}" alt="">telegram
                        </a>
                        <a target="_blank" href="https://wa.me/79874749387" class="faq_add__link">
                            <img src="{{ asset('img/svg/whatsup.svg') }}" alt="">whatsapp
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<footer class="footer">
    <div class="container">
        <div class="row footer__row">
            <div class="col-12 col-md-6">
                <div class="row">
                    <div class="col-6 col-lg-4">
                        <div class="footer__info">
                            @lang('ИП Негинский<br>Сергей Евгеньевич')
                        </div>
                    </div>
                    <div class="col-6 col-lg-4">
                        <div class="footer__info">
                            @lang('ИНН 772160492397<br>+7 916 869 41 23')
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-6 d-flex flex-column">
                <a href="https://willteach.ru/politika.pdf" class="footer__link" target="_blank">@lang('Политика конфиденциальности') →</a>
                <a href="https://willteach.ru/oferta.pdf" class="footer__link" target="_blank">@lang('Договор оферты') →</a>
            </div>
        </div>

    </div>
</footer>
