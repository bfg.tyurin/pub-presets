<header class="header_portugal">
    <div class="container">
        <div class="header_home__wrapper">
            <div class="header__wrapper_top">
                <a href="{{ url('/') }}" class="header_home__logo will-fade-in">Zhenya <br />Swan</a>
                <a href="#" class="button__menu button__menu_no_mob js_open_nav will-fade-in"></a>
            </div>

            <div class="header__portugal_bottom">

                <div class="header_home__title will-fade-in">Portugal <br />Morning</div>
                <div class="header_home__subtitle will-fade-in">@lang('Подарок')</div>
            </div>
            <a class="header_home__arrow will-fade-in-top"></a>
        </div>
    </div>
    <div class="header_home__lang will-fade-in">
        <a class="{{ LaravelLocalization::getCurrentLocale() === 'ru' ? 'active' : ''}}" href="{{ LaravelLocalization::getLocalizedURL('ru') }}">Русский</a>
        <a class="{{ LaravelLocalization::getCurrentLocale() === 'en' ? 'active' : ''}}" href="{{ LaravelLocalization::getLocalizedURL('en') }}">English</a>
    </div>
</header>

<a href="#" class="button__menu button__menu_mob button__menu_white js_open_nav will-fade-in" style="top: 40px"></a>
