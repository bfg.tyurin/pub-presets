<header class="header_home">
    <div class="bg"></div>
    <div class="container">
        <div class="header_home__wrapper">
            <div class="header__wrapper_top">
                <a href="{{ url('/') }}" class="header_home__logo will-fade-in">Zhenya <br />Swan</a>
                <x-shopping-cart-button/>
            </div>

            <div class="header__wrapper_bottom">
                <a class="header_home__arrow will-fade-in-top"></a>
                <div class="header_home__title will-fade-in">Presets</div>
                <div class="header_home__subtitle will-fade-in">@lang('Цвет,<br>который продаёт')</div>
            </div>


        </div>
    </div>
    <div class="header_home__lang will-fade-in">
        <a class="{{ LaravelLocalization::getCurrentLocale() === 'ru' ? 'active' : ''}}"
            href="{{ LaravelLocalization::getLocalizedURL('ru') }}">Русский</a>
        <a class="{{ LaravelLocalization::getCurrentLocale() === 'en' ? 'active' : ''}}"
            href="{{ LaravelLocalization::getLocalizedURL('en') }}">English</a>
    </div>
</header>

<x-shopping-cart-block/>
