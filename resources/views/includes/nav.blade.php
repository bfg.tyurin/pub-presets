<nav class="top_menu">
    <div class="top_menu_container">
        <div class="container">
            <div class="top_menu__wrapper">
                <div class="top_menu__top_block">
                    <a class="top_menu__logo">
                        <img src="{{ asset('img/svg/zs.svg') }}" alt="">
                        <img src="{{ asset('img/svg/presets.svg') }}" alt="">
                    </a>
                    <a href="#" class="button__menu_close js_close_nav"></a>
                </div>
                <div class="top_menu__links">
                    <div class="row">
                        <div class="col-12 col-md-6 d-flex justify-content-end top_menu__nav_image_block">
                            <div class="top_menu__nav_image">
                                <img data-img="1" src="{{ asset('images/menu/dusty_caramel.jpg') }}" alt="">
                                <img data-img="2" src="{{ asset('images/menu/wedding_preset_met_blue.jpg') }}" alt="">
                                <img data-img="3" src="{{ asset('images/menu/wedding_preset.jpg') }}" alt="">
                            </div>
                        </div>
                        <div class="col-12 col-md-6">
                            <ul class="top_menu__nav">
                                <li>
                                    <a href="/products/golden-caramel" data-image="1" class="menu_hover_item {{ Request::path() === 'products/golden-caramel' ? 'active' : ''}}">
                                        <div class="top_menu__nav_num">01</div>
                                        <div class="top_menu__nav_title">Golden <br />Caramel</div>
                                    </a>
                                </li>
                                <li>
                                    <a href="/products/metallic-blues" data-image="2" class="menu_hover_item {{ Request::path() === 'products/metallic-blues' ? 'active' : ''}}">
                                        <div class="top_menu__nav_num">02</div>
                                        <div class="top_menu__nav_title">Metallic <br />blues</div>
                                    </a>
                                </li>
                                <li>
                                    <a href="/products/wedding-pureness" data-image="3" class="menu_hover_item {{ Request::path() === 'products/wedding-pureness' ? 'active' : ''}}">
                                        <div class="top_menu__nav_num">03</div>
                                        <div class="top_menu__nav_title">Wedding <br />pureness</div>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="top_menu__lang">
                <a class="{{ LaravelLocalization::getCurrentLocale() === 'ru' ? 'active' : ''}}" href="{{ LaravelLocalization::getLocalizedURL('ru') }}">Русский</a>
                <a class="{{ LaravelLocalization::getCurrentLocale() === 'en' ? 'active' : ''}}" href="{{ LaravelLocalization::getLocalizedURL('en') }}">English</a>
            </div>
        </div>
    </div>
    </div>
</nav>
