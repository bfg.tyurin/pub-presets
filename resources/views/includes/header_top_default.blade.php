<header class="header header_default">
    <div class="container">
        <div class="header__wrapper">
            <a href="{{ url('/') }}" class="header__logo">
                <img src="{{ asset('/img/svg/zs.svg') }}" alt="">
                <img src="{{ asset('/img/svg/presets.svg') }}" alt="">
            </a>
            <x-shopping-cart-button/>
            <x-desktop-lang-switcher/>
        </div>
    </div>
</header>
<div class="header_empty"></div>

<x-shopping-cart-block/>
