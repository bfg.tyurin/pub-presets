<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"><meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0;">
    <title></title>
    <style type="text/css">
        /*<![CDATA[*/
        @import url(https://fonts.googleapis.com/css2?family=Roboto:wght@400;500;700;900&display=swap);
        .ReadMsgBody {
            width: 100%;
            background-color: #ede6e2;
        }
        .ExternalClass{
            width: 100%;
            background-color: #ede6e2;
        }
        body {
            width: 100%;
            background-color: #ede6e2;
            margin:0;
            padding:0;
            -webkit-text-size-adjust: 100%;
            text-size-adjust: 100%;
            font-family: 'Roboto', Arial, sans-serif;
            font-size: 14px;
            color: #4F4336;
        }
        table {
            border-collapse: collapse;
        }
        .deviceWidth {
            width: 540px;
        }
        .text-table1 {
            padding: 55px 40px 30px 40px;
            background: #fff;
            font-size: 16px;
            line-height: 135%;
            font-weight: 500;
            color: #4F4336;
        }
        .headline1 {
            font-weight: 900;
            font-size: 60px;
            color: #EDE6E2;
        }
        .bold-text1 {
            font-weight: 900;
            color: #4F4336;
        }
        .link-button {
            display: block;
            line-height: 52px;
            border-radius: 26px;
            background: #4F4336;
            color: #fff !important;
            text-align: center;
            font-weight: bold;
            font-size: 18px;
            text-decoration: none;
            margin: 40px 0;
        }
        .social-table {
            padding: 0 40px 35px 40px;
            background: #fff;
        }
        .social-link1 {
            margin: 0 100px 0 0;
        }
        .unsubscribe {
            padding: 30px 0 75px 0;
            font-size: 16px;
            line-height: 135%;
            text-align: center;
            color: #B2A8A0;
            font-weight: 500;
        }
        @media only screen and (max-width: 580px) {
            .deviceWidth {
                width: 320px;
                padding: 0;
            }
            .text-table1 {
                padding: 20px 20px 15px 20px;
                font-size: 14px;
            }
            .headline1 {
                font-size: 28px;
            }
            .social-table {
                padding: 0 20px 40px 20px;
            }
            .social-link1 {
                margin: 0 30px 0 0;
            }
            .unsubscribe {
                font-size: 14px;
            }
        }/*]]>*/
    </style>
</head>
<body style="font-family: 'Roboto', Arial, sans-serif; min-width: 320px !important; -webkit-text-size-adjust: 100%; text-size-adjust: 100%;"><!-- Wrapper --><table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" style="-webkit-text-size-adjust: 100% !important; text-size-adjust: 100% !important; color: #4f4336; background: #ede6e2;">
<tbody>
<tr>
    <td>
        <table border="0" cellpadding="0" cellspacing="0" align="center" class="deviceWidth">
            <tbody>
            <tr>
                <td style="padding: 50px 0 40px 0;">
                    <a rel="noopener noreferrer" href="{{ url("/") }}" target="_blank" style="text-decoration: none;"><img src="https://login.sendpulse.com/files/emailservice/userfiles/2fa9fccde1532aa66410cd1fadb1cc987191381/h-logo2.jpg" style="border: 0; max-width: 100%;"></a></td></tr><tr><td><img src="https://login.sendpulse.com/files/emailservice/userfiles/2fa9fccde1532aa66410cd1fadb1cc987191381/banner3.jpg" style="display: block; width: 100%; border: 0;"></td></tr>
            <tr>
                <td class="text-table1"><p><span class="headline1" style="font-family: georgia, palatino;">Привет!</span></p>
                    <p><span style="font-family: georgia, palatino;">Спасибо, что покупали мои пресеты. Я собрала обратную связь и обнаружила, что у некоторых пользователей возникали технические сложности. Хотя в пресеты были включены стандартные лайтрумовские профили, они не всегда корректно отрабатывали.</span></p>
                    <p><span style="font-family: georgia, palatino;">Поэтому я обновила все пресеты. И в качестве бонусов добавила в коллекции Wedding Pureness и Metallic Blues по 1 бонусному пресету.</span></p>
                    <p><span style="font-family: georgia, palatino;">Вот обновленная ссылка на скачивание:</span></p>
                    <p><span style="font-family: georgia, palatino; font-size: 14px;"><a rel="noopener noreferrer" href="{{ $downloadableUrl ?? '' }}" target="_blank" class="link-button">Скачать</a></span></p>
                    <p><span style="font-family: georgia, palatino;"><br> <span style="font-family: georgia, palatino;">Если у вас есть любые вопросы, не стесняйтесь обращаться в поддержку &mdash; мы будем рады помочь.</span></span></p>
                </td>
            </tr>
            <tr>
                <td class="social-table"><span style="font-family: georgia, palatino; font-size: 16px;">
                        <a rel="noopener noreferrer" href="https://tele.gg/denis_ayupov" target="_blank" style="display: inline-block; line-height: 30px; padding: 0px 0px 0px 45px; font-weight: 500; color: #4f4336; text-decoration: none; background-image: url('https://login.sendpulse.com/files/emailservice/userfiles/2fa9fccde1532aa66410cd1fadb1cc987191381/social3.jpg'); background-position: left center; background-repeat: no-repeat;" class="social-link1">telegram</a>
                        <a rel="noopener noreferrer" href="https://wa.me/79874749387" target="_blank" style="display: inline-block; line-height: 30px; padding: 0px 0px 0px 45px; font-weight: 500; color: #4f4336; text-decoration: none; background-repeat: no-repeat; background-image: url('https://login.sendpulse.com/files/emailservice/userfiles/2fa9fccde1532aa66410cd1fadb1cc987191381/social4.jpg'); background-position: left center;">whatsapp</a></span>
                </td>
            </tr>
            <tr>
                <td class="unsubscribe"><span style="font-family: georgia, palatino;">Если вы не хотите получать письма от меня,</span><br> <span style="font-family: georgia, palatino;">нажмите <a rel="noopener noreferrer" href="@{{unsubscribe}}" style="color: #b2a8a0; text-decoration: underline; font-weight: 500;">отписаться&nbsp;</a>
                    </span>
                </td>
            </tr>
            </tbody>
        </table>
    </td>
</tr>
</tbody>
</table>
</body>
</html>
