<div class="desktop-lang-switcher">
    <a class="{{ LaravelLocalization::getCurrentLocale() === 'ru' ? 'active' : ''}}" href="{{ LaravelLocalization::getLocalizedURL('ru') }}">Русский</a>
    <a class="{{ LaravelLocalization::getCurrentLocale() === 'en' ? 'active' : ''}}" href="{{ LaravelLocalization::getLocalizedURL('en') }}">English</a>
</div>
