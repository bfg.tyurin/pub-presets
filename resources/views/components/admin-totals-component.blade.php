<div class="col-md-6 col-lg-6">
    <div class="card-header">
        <li class="far fa-ruble-sign"></li> покупок: {{ $count }}
    </div>
    <div class="card-body">
        <p>Средний чек: {{ $avg }}</p>
        <p>Всего выручка: {{ $total }}</p>
        <p>Выручка за вычетом налогов: {{ $totalWithTax }}</p>
    </div>

</div>
