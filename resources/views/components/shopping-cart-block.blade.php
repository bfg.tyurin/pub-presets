@inject('shoppingCart', 'App\Services\ShoppingCartService')

<div class="basket_block">

    <div class="mobile-lang-switcher button__menu_mob">
        <a class="{{ LaravelLocalization::getCurrentLocale() === 'ru' ? 'active' : ''}}" href="{{ LaravelLocalization::getLocalizedURL('ru') }}">Русский</a>
        <a class="{{ LaravelLocalization::getCurrentLocale() === 'en' ? 'active' : ''}}" href="{{ LaravelLocalization::getLocalizedURL('en') }}">English</a>
    </div>
    <a href="#" class="button__menu button__menu_mob js_close_basket js_opened button__basket_close"></a>

    <div class="basket_block__top">

        @foreach ($shoppingCart->packs() as $pack)

        <div class="basket_item">
            <div class="basket_item__left">
                <a href="{{ route('products.show', $pack->slug) }}" class="basket_item__img">
                    <img src="{{ asset($shoppingCart->images()[$pack->slug]) }}" />
                    <span class="basket_item__link">@lang('basket.see')</span>
                </a>
                <div class="basket_item__info">
                    <div class="basket_item__name">{{ $pack->title }}</div>
                    <div class="basket_item__quan">{{ $pack->quantity }} @lang('пресетов')</div>
                </div>
            </div>
            <div class="basket_item__right">

                @foreach ($pack->products as $product)
                <a href="#"
                    class="basket_item__button js-basket_item__button {{ \Cart::get($product->id) ? 'basket_item__dark' : '' }}"
                    data-product_id="{{ $product->id }}" data-resource="{{ route('shopping_cart_products.store') }}"
                    data-resource_destroy="{{ route('shopping_cart_products.destroy', $product) }}">

                    <div class="basket_item__button_name">
                        {{ $product->category === 'desktop' ? __('basket.desktop') : __('basket.mobile')}}</div>
                    <div class="basket_item__button_prices">
                        @switch(LaravelLocalization::getCurrentLocale())
                        @case('ru')
                        <span class="basket_item__button_price_old js-basket_item__button_price_old"
                            style="text-decoration: line-through">
                            {{ number_format($product->price_original, 0, ".", " ") }}
                            <i class="fal fa-ruble-sign"></i>
                        </span>
                        <span class="basket_item__button_price">
                            {{-- <span class="js-basket_item__button_price" data-value="{{ $product->price_discount }}">
                                {{ \Cart::isEmpty() || \Cart::get($product->id) ? $product->getFormattedRubPriceValueWithMultiplier($shoppingCart->discountPriceMultiplier()) : $product->getNextFormattedRubPriceValueWithMultiplier($shoppingCart->nextDiscountPriceMultiplier(), \Cart::getSubTotal(), \Cart::getTotal()) }}
                            </span> --}}
                            <span data-value="{{ $product->price_discount }}">
                                {{ $product->price_discount }}
                            </span>
                            <i class="fal fa-ruble-sign"></i>
                        </span>
                        @break
                        @default
                        <span class="basket_item__button_price_old js-basket_item__button_price_old"
                            style="text-decoration: line-through"
                            data-value="{{ $product->usd_price_original }}">
                            <i class="fal fa-dollar-sign"></i>
                            {{ number_format($product->usd_price_original, 0, ".", " ") }}
                        </span>
                        <span class="basket_item__button_price">
                            <i class="fal fa-dollar-sign"></i>
                            {{-- <span class="js-basket_item__button_price" data-value="{{ $product->price_discount }}">
                                {{ \Cart::isEmpty() || \Cart::get($product->id) ? $product->getFormattedRubPriceValueWithMultiplier($shoppingCart->discountPriceMultiplier()) : $product->getNextFormattedRubPriceValueWithMultiplier($shoppingCart->nextDiscountPriceMultiplier(), \Cart::getSubTotal(), \Cart::getTotal()) }}
                            </span> --}}
                            <span data-value="{{ $product->usd_price_discount }}">
                                {{ $product->usd_price_discount }}
                            </span>
                        </span>
                        @break
                        @endswitch
                    </div>
                    <div class="basket_item__button_plus"></div>
                </a>
                @endforeach

            </div>
        </div>
        @endforeach

        @switch(LaravelLocalization::getCurrentLocale())
            @case('ru')
            <div class="basket_item_info">
                <x-shopping-cart-message :shoppingCart="$shoppingCart"/>
                {{-- <div class="bi_left">

                    @if ($shoppingCart->getNextDiscountPercent() === $shoppingCart->getDiscountPercent())
                    <span class="js-warn_discount_message" data-regular_message="{{ __('basket.add_another') }}" data-cart_is_full_message="{{ __('basket.max_discount_text') }}">@lang('basket.max_discount_text')</span> <span class="bi_green">
                        <span class="js-next-discount-percent">{{ $shoppingCart->getNextDiscountPercent() }}</span>%
                        @lang('basket.discount')</span>
                    @else
                    <span class="js-warn_discount_message" data-regular_message="{{ __('basket.add_another') }}" data-cart_is_full_message="{{ __('basket.max_discount_text') }}">@lang('basket.add_another')</span> <span class="bi_green">
                        <span class="js-next-discount-percent">{{ $shoppingCart->getDiscountPercent() }}</span>%
                        @lang('basket.discount')</span>
                    @endif

                </div> --}}

                {{-- <div class="bi_right">
                    <div class="bi_info">@lang('basket.selected') <b><span
                                class="js-cart-count">{{ \Cart::getContent()->count() }}</span> @lang('пресета')</b></div>
                    <div class="bi_info">@lang('basket.discount_2') <span
                            class="js-discount-percent">{{ $shoppingCart->getDiscountPercent() }}</span>%
                        <span class="bi_discount_val">
                            <span class="js-discount-value">
                                {{ number_format(ceil($shoppingCart->discountInCurrency()), 0, ".", " ") }}
                            </span>
                            <i class="fal fa-ruble-sign"></i>
                        </span>
                    </div>
                </div> --}}

                {{-- new --}}
                <div class="bi_right">
                    <div class="bi_info">
                        <span class="js-cart-count">
                            <span class="js_basket_collections_choosen_string">{!! trans_choice('basket.collections_choosen', $shoppingCart->getContentCount()) !!}</span> {{ __('basket.sum') }}
                            <span class="bi_info--grey"><span class="js_cart_full_price">{{ number_format($shoppingCart->getFullPrice(), 0, ".", " ") }}</span> <i class="fal fa-ruble-sign"></i></span>
                            <b><span class="js_cart_sub_total">{{ number_format($shoppingCart->getSubTotal(), 0, ".", " ") }}</span> <i class="fal fa-ruble-sign"></i></b>
                        </span>
                    </div>
                    <div class="bi_info">@lang('basket.discount_3') <b><span
                            class="js-discount-percent">{{ $shoppingCart->getDiscountPercent() }}</span>%</b>
                        <span class="bi_discount_val">
                            <span class="js-discount-value">
                                {{ number_format(ceil($shoppingCart->discountInCurrency()), 0, ".", " ") }}
                            </span>
                            <i class="fal fa-ruble-sign"></i>
                        </span>
                    </div>
                </div>
                {{-- new --}}

                <div class="bi_total">@lang('basket.total'): <span class="js-cart-total">
                    {{ $shoppingCart->getTotal() ? number_format(ceil($shoppingCart->getTotal()), 0, ".", " ") : '' }}
                </span> <i class="fal fa-ruble-sign"></i>
                </div>

                {{-- new --}}
                <div class="bi_right bi_info bi_info--sm">
                    {{ __('basket.saving') }} <span class="js_basket_saving">{{ number_format($shoppingCart->getSaving(), 0, ".", " ") }}</span> <i class="fal fa-ruble-sign"></i>
                </div>
                {{-- new --}}

            </div>
            @break
            @default
            <div class="basket_item_info">
                <x-shopping-cart-message :shoppingCart="$shoppingCart"/>
                {{-- <div class="bi_left">

                    @if (\Cart::isEmpty())
                        <span class="js-warn_discount_message" data-regular_message="{{ __('basket.add_another') }}" data-cart_is_full_message="{{ __('basket.max_discount_text') }}"> </span> <span class="bi_green">
                            <span class="js-next-discount-percent"></span>
                        </span>
                    @else
                        @if ($shoppingCart->getNextDiscountPercent() === $shoppingCart->getDiscountPercent())
                        <span class="js-warn_discount_message" data-regular_message="{{ __('basket.add_another') }}" data-cart_is_full_message="{{ __('basket.max_discount_text') }}">@lang('basket.max_discount_text')</span> <span class="bi_green">
                            <span class="js-next-discount-percent">{{ $shoppingCart->getNextDiscountPercent() }}</span>%
                            @lang('basket.discount')</span>
                        @else
                        <span class="js-warn_discount_message" data-regular_message="{{ __('basket.add_another') }}" data-cart_is_full_message="{{ __('basket.max_discount_text') }}">@lang('basket.add_another')</span> <span class="bi_green">
                            <span class="js-next-discount-percent">{{ $shoppingCart->getDiscountPercent() }}</span>%
                            @lang('basket.discount')</span>
                        @endif
                    @endif


                </div> --}}

                {{-- <div class="bi_right">
                    <div class="bi_info">@lang('basket.selected') <b><span
                                class="js-cart-count">{{ \Cart::getContent()->count() }}</span> @lang('пресета')</b></div>
                    <div class="bi_info">@lang('basket.discount_2') <span
                            class="js-discount-percent">{{ $shoppingCart->getDiscountPercent() }}</span>%
                        <span class="bi_discount_val">
                            <i class="fal fa-dollar-sign"></i>
                            <span class="js-discount-value">
                                {{ number_format(ceil($shoppingCart->discountInCurrency()), 0, ".", " ") }}
                            </span>
                        </span>
                    </div>
                </div> --}}

                {{-- new --}}
                <div class="bi_right">
                    <div class="bi_info">
                        <span class="js-cart-count">
                            <span class="js_basket_collections_choosen_string">{!! trans_choice('basket.collections_choosen', $shoppingCart->getContentCount()) !!}</span> {{ __('basket.sum') }}
                            <span class="bi_info--grey"><i class="fal fa-dollar-sign"></i> <span class="js_cart_full_price">{{ number_format($shoppingCart->getFullPrice(), 0, ".", " ") }}</span></span>
                            <b><i class="fal fa-dollar-sign"></i> <span class="js_cart_sub_total">{{ number_format($shoppingCart->getSubTotal(), 0, ".", " ") }}</span></b>
                        </span>
                    </div>
                    <div class="bi_info">@lang('basket.discount_3') <b><span
                            class="js-discount-percent">{{ $shoppingCart->getDiscountPercent() }}</span>%</b>
                        <span class="bi_discount_val">
                            <span class="js-discount-value">
                                <i class="fal fa-dollar-sign"></i> {{ number_format(ceil($shoppingCart->discountInCurrency()), 0, ".", " ") }}
                            </span>
                        </span>
                    </div>
                </div>
                {{-- new --}}

                <div class="bi_total">@lang('basket.total'): <i class="fal fa-dollar-sign"></i> <span class="js-cart-total">
                    {{ \Cart::getTotal() ? number_format(ceil($shoppingCart->getTotal()), 0, ".", " ") : '' }}
                </span>
                </div>

                {{-- new --}}
                <div class="bi_right bi_info bi_info--sm">
                    {{ __('basket.saving') }} <i class="fal fa-dollar-sign"></i> <span class="js_basket_saving">{{ number_format($shoppingCart->getSaving(), 0, ".", " ") }}</span>
                </div>
                {{-- new --}}

            </div>
            @break
        @endswitch


    </div>
    <div class="basket_block__bottom">
        <div class="bi_input_box">
            <div class="ffl-wrapper p_input_wrapper js-input-error-wrapper">
                <label class="ffl-label" for="input-1">Email</label>
                <input type="text" required id="email_for_pay" value="" class="js-user-email js-input-email">
                <span class="p_input_error js-p-input-error">@lang('Введен неверно')</span>
            </div>
        </div>
        @switch(LaravelLocalization::getCurrentLocale())
        @case('ru')
            <a href="#"
                class="button_payment_big js-cloudpayments-payment"
                id=""
                data-orders-resource="{{ route('orders.store') }}"
                data-completed-orders-resource="{{ route('completed-orders.store') }}">
                <span>Оплатить Картой, <br /><b>Apple и Google Pay</b></span>
            </a>
            <a href="#"
                class="button_payment_big js-paypal-payment"
                id=""
                data-orders_resource="{{ route('orders.store') }}"
                data-paypal_resource="{{ route('paypal.store') }}">
                <span>
                    @lang('Оплатить <b>paypal</b>')
                    @switch(LaravelLocalization::getCurrentLocale())
                        @case('ru')
                        <br /><span class="without_russia">(Кроме России)</span>
                        @break
                    @endswitch
                </span>
            </a>
        @break

        @default
            <a href="#"
            class="button_payment_big js-paypal-payment"
            id=""
            data-orders_resource="{{ route('orders.store') }}"
            data-paypal_resource="{{ route('paypal.store') }}">
                <span>
                    @lang('Оплатить <b>paypal</b>')
                    @switch(LaravelLocalization::getCurrentLocale())
                        @case('ru')
                        <br /><span class="without_russia">(Кроме России)</span>
                        @break
                    @endswitch
                </span>
            </a>
        @break
        @endswitch

        {{-- <form class="js-paypal-payment" action="{{ route('completed-orders.store') }}" method="POST">
            <button type="submit" class="button_payment_big">
                <span>@lang('Оплатить <b>paypal</b>')</span>
            </button>
        </form> --}}
        {{-- <form class="js-paypal-form" action="{{ route('paypal.beforePayment') }}" method="POST">
            <button type="submit" class="button_payment_big">
                <span>@lang('Оплатить <b>paypal</b>')</span>
            </button>
        </form> --}}
        <div class="bi_bottom_text">
            @lang('Нажимая кнопку оплатить вы даете согласие с <a href="https://willteach.ru/oferta.pdf" style="color: #4F4336" target="_blank">Договором оферты</a> и <a href="https://willteach.ru/politika.pdf" style="color: #4F4336" target="_blank">Политикой конфиденциальности</a> и обработки персональных данных')
        </div>
    </div>
</div>
