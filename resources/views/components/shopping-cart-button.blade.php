<a href="#" class="button__basket js_toggle_basket will-fade-in">
    @lang('basket.title') <span class="js-red_point {{ \Cart::isEmpty() ? '' : 'red_point' }}"></span>
</a>
