<div class="bi_left" style="min-height: 60px">
    <div class="js-warn_discount_block" style="{{ \Cart::isEmpty() ? 'display:none' : ''}}">
        @if ($shoppingCart->getNextDiscountPercent() === $shoppingCart->getDiscountPercent())
        <span class="js-warn_discount_message" data-regular_message="{{ __('basket.add_another') }}" data-cart_is_full_message="{{ __('basket.max_discount_text') }}">@lang('basket.max_discount_text')</span> <span class="bi_green">
            <span class="js-next-discount-percent">{{ $shoppingCart->getNextDiscountPercent() }}</span>%
            @lang('basket.discount')</span>
        @else
        <span class="js-warn_discount_message" data-regular_message="{{ __('basket.add_another') }}" data-cart_is_full_message="{{ __('basket.max_discount_text') }}">@lang('basket.add_another')</span> <span class="bi_green">
            <span class="js-next-discount-percent">{{ $shoppingCart->getNextDiscountPercent() }}</span>%
            @lang('basket.discount')</span>
        @endif
    </div>
</div>
