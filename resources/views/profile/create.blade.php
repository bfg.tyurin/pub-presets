@extends('layout')

@section('content')
    @include('includes.nav')
    @include('includes.header_top_default')

    <div class="preset_page preset_payment_page">


        <section class="payment_thx_page">
            <div class="container">
                <div class="payment_thx_title">@lang('Спасибо<br>за покупку!')</div>
                <div class="row d-flex justify-content-between">
                    <div class="col-12 col-md-6 col-xl-4">
                        <div class="payment_text">
                            @lang('Мне бы очень хотелось узнать о вас немного больше. Чтобы понимать, кто вы. Кто моя аудитория. Чтобы иногда заходить в ваши аккаунты, смотреть, как вы используете пресеты и вдохновляться вашими успехами.')
                            <br><br>
                            @lang('В благодарность за заполнение этой небольшой анкеты на следующей странице вас ждёт бонус. И, конечно, ссылка на скачивание пресетов!')
                        </div>
                    </div>
                    <div class="col-12 col-md-6">
                        <form class="js-profile-form" action="{{ route('profile.store') }}" >
                            <div class="row">
                                <div class="col-12 col-lg-6">
                                    <div class="input_box input_box_thx">
                                        <div class="ffl-wrapper p_input_wrapper">
                                            <label class="ffl-label" for="input-1">@lang('Имя')</label>
                                            <input name="firstName" type="text" required >
                                            <span class="p_input_error">@lang('Заполните отмеченные поля')</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 col-lg-6">
                                    <div class="input_box input_box_thx">
                                        <div class="ffl-wrapper p_input_wrapper">
                                            <label class="ffl-label" for="input-1">@lang('Фамилия')</label>
                                            <input name="surname" type="text" required >
                                            <span class="p_input_error">@lang('Заполните отмеченные поля')</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 col-lg-6">
                                    <div class="input_box input_box_thx">
                                        <div class="ffl-wrapper p_input_wrapper">
                                            <label class="ffl-label" for="input-1">@lang('Никнейм в instagram')</label>
                                            <div class="inst_input">
                                                <div class="inst_prefix">@</div>
                                                <input name="instagram" id="inst_name" type="text" required >
                                                <span class="p_input_error">@lang('Заполните отмеченные поля')</span>
                                            </div>

                                            <span class="p_input_error">@lang('Заполните отмеченные поля')</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 col-lg-6 d-flex align-items-center justify-content-around radio_box">
                                    <div class="custom-control custom-radio custom_radio">
                                        <input name="gender" type="radio" class="custom-control-input" id="man" value="m">
                                        <label class="custom-control-label" for="man">@lang('мужчина')</label>
                                    </div>
                                    <div class="custom-control custom-radio custom_radio">
                                        <input name="gender" type="radio" class="custom-control-input" id="woman" value="w">
                                        <label class="custom-control-label" for="woman">@lang('женщина')</label>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="input_box input_box_thx">
                                        <div class="ffl-wrapper p_input_wrapper">
                                            <label class="ffl-label" for="input-1">@lang('Номер в whatsapp / telegram')'</label>
                                            <input name="phone" type="text" required >
                                            <span class="p_input_error">@lang('Заполните отмеченные поля')</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="input_box input_box_thx">
                                        <div class="input_label">Вы:</div>
                                        <select name="profession" class="custom_select">
                                            <option value="Профессиональный фотограф (зарабатываю фотографией)">@lang('Профессиональный фотограф (зарабатываю фотографией)')</option>
                                            <option value="Блогер (фотографии как инструмент)">@lang('Блогер (фотографии как инструмент)')</option>
                                            <option value="Увлекаюсь (просто люблю фотографию)">@lang('Увлекаюсь (просто люблю фотографию)')</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-12">
                                    <div class="input_box input_box_thx">
                                        <div class="input_label">@lang('Почему вы купили пресеты:')</div>
                                        <textarea name="reason" rows="4" class="custom_textarea"></textarea>
                                        <span class="p_input_error">@lang('Заполните отмеченные поля')</span>
                                    </div>
                                </div>

                                <div class="col-12">
                                    <button type="button" class="button_payment_big js-profile-form-btn">
                                        @lang('получить пресеты! спасибо!')
                                    </button>
                                </div>

                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </section>
    </div>
    @include('includes.footer')
@endsection
