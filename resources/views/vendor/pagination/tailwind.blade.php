@if ($paginator->hasPages())
<div class="flex items-center m-2">
    {{-- Previous Page Link --}}
    @if ($paginator->onFirstPage())
    <span
        class="rounded-l rounded-sm border border-brand-light sm:px-3 sm:py-2 cursor-not-allowed no-underline text-xs px-1 py-1">&laquo;</span>
    @else
    <a class="rounded-l rounded-sm border-t border-b border-l border-brand-light sm:px-3 sm:py-2 text-brand-dark hover:bg-brand-light no-underline text-xs px-1 py-1"
        href="{{ $paginator->previousPageUrl() }}" rel="prev">
        &laquo;
    </a>
    @endif

    {{-- Pagination Elements --}}
    @foreach ($elements as $element)
    {{-- "Three Dots" Separator --}}
    @if (is_string($element))
    <span
        class="border-t border-b border-l border-brand-light sm:px-3 sm:py-2 cursor-not-allowed no-underline text-xs px-1 py-1">{{ $element }}</span>
    @endif

    {{-- Array Of Links --}}
    @if (is_array($element))
    @foreach ($element as $page => $url)
    @if ($page == $paginator->currentPage())
    <span
        class="border-t border-b border-l border-brand-light sm:px-3 sm:py-2 bg-blue-500 no-underline text-xs px-2 py-1">{{ $page }}</span>
    @else
    <a class="border-t border-b border-l border-brand-light sm:px-3 sm:py-2 hover:bg-brand-light text-brand-dark no-underline text-xs px-2 py-1"
        href="{{ $url }}">{{ $page }}</a>
    @endif
    @endforeach
    @endif
    @endforeach

    {{-- Next Page Link --}}
    @if ($paginator->hasMorePages())
    <a class="rounded-r rounded-sm border border-brand-light sm:px-3 sm:py-2 hover:bg-brand-light text-brand-dark no-underline text-xs px-1 py-1"
        href="{{ $paginator->nextPageUrl() }}" rel="next">&raquo;</a>
    @else
    <span
        class="rounded-r rounded-sm border border-brand-light px-2 py-1 hover:bg-brand-light text-brand-dark no-underline cursor-not-allowed">&raquo;</span>
    @endif
</div>
@endif
