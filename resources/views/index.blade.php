@extends('layout')

@section('content')
{{--  @foreach ($product_packs as $product)--}}
{{--      <h2><a href="{{ route('products.show', $product) }}">{{ $product->title }}</a></h2>--}}
{{--      <p>Количество пресетов: {{ $product->packs->first()->quantity }}</p>--}}
{{--      <p style="text-decoration: line-through">Цена: {{ $product->price_original }}</p>--}}
{{--      <p>Цена скидкой: {{ $product->price_discount }}</p>--}}
{{--  @endforeach--}}
{{--    <h2>Специальное предложение:</h2>--}}
{{--    <p>{{ $product_bundle->title }}</p>--}}
{{--    <p style="text-decoration: line-through">{{ $product_bundle->price_original }}</p>--}}
{{--    <p>{{ $product_bundle->price_discount }}</p>--}}
@include('includes.nav')
@include('includes.header_top_home')
  <div class=" preset_page__home">
      <section class="home_top_block">
          <div class="container">
              <div class="row">
                  <div class="col-12 col-md-6">
                      <div class="ht_title will-fade-in">
                          @lang('Красивая<br>обработка<br>в 1 клик')
                      </div>
                  </div>
                  <div class="col-12 col-md-6 col-xl-4">
                      <div class="ht_text will-fade-in">
                          @lang('Пресеты — это готовые настройки обработки в программе Adobe Lightroom.')
                          <br /><br />
                          @lang('Их легко применять, они экономят ваше время, дарят пространство для творческих экспериментов и придают цельность вашей ленте в Instagram.')
                      </div>
                  </div>
              </div>
          </div>
      </section>

      <section class="home_info_block">
          <div class="container">
              <div class="row">
                  <div class="col-12 col-md-5 col-xl-6 hi_left">
                      <div>
                          <div class="hi_title will-fade-in">
                              @lang('Кому<br>пригодятся<br>мои наборы')
                          </div>
                          <div class="hi_text will-fade-in">
                              @lang('Фотографам, блогерам и всем, кто снимает людей, события из жизни и путешествия.<br>Неважно, для себя или на заказ.')<br /><br />
                              @lang('Смотрите примеры по хештегу <a href="https://www.instagram.com/explore/tags/zhenyaswanpresets/" target="_blank">#zhenyaswanpresets</a>. Вы сможете так же — и даже лучше.')
                          </div>
                      </div>
                  </div>
                  <div class="col-12 col-md-7 col-xl-6">
                      <img class="will-fade-in visible_in_desktop" src="{{ asset('images/whom_retina.jpg') }}"
                           srcset="{{ asset('images/whom.jpg') }}, {{ asset('images/whom_retina.jpg 2x') }} " />
                      <img class="will-fade-in visible_in_tab" src="{{ asset('images/whom_tab_retina.jpg') }}"
                           srcset="{{ asset('images/whom_tab.jpg') }}, {{ asset('images/whom_tab_retina.jpg 2x') }} " />
                      <img class="will-fade-in visible_in_mobile" src="{{ asset('images/whom_mob_retina.jpg') }}"
                           srcset="{{ asset('images/whom_mob.jpg') }}, {{ asset('images/whom_mob_retina.jpg 2x') }} " />
                  </div>
              </div>
          </div>
      </section>

      <section class="home_preset home_preset__golden">
          <div class="container">
              <a href="products/golden-caramel" class="home_preset__wrapper golden-caramel-fix">
{{--              <a href="/golden-caramel" class="home_preset__wrapper">--}}
                  <div class="home_preset__page">01</div>

                  <div class="home_preset__content home_preset__content_gm">

                      <img class="will-fade-in visible_in_desktop" src="{{ asset('images/home_golden_caramel_retina.jpg') }}"
                           srcset="{{ asset('images/home_golden_caramel.jpg') }}, {{ asset('images/home_golden_caramel_retina.jpg 2x') }} " />
                      <img class="will-fade-in visible_in_tab" src="{{ asset('images/home_golden_caramel_tab_retina.jpg') }}"
                           srcset="{{ asset('images/home_golden_caramel_tab.jpg') }}, {{ asset('images/home_golden_caramel_tab_retina.jpg 2x') }} " />
                      <img class="will-fade-in visible_in_mobile" src="{{ asset('images/home_golden_caramel_mob_retina.jpg') }}"
                           srcset="{{ asset('images/home_golden_caramel_mob.jpg') }}, {{ asset('images/home_golden_caramel_mob_retina.jpg 2x') }} " />
                      <div class="home_preset__info ">
                          <div class="home_preset__title">
                              <div class="oh">
                                  <div class="will-fade-in-title">Golden</div>
                              </div>
                              <div class="oh">
                                  <div class="will-fade-in-title">Caramel</div>
                              </div>
                          </div>
                          <div class="home_preset__quan_block will-fade-in ">
                              <div class="home_preset__quan">10 @lang('пресетов')</div>
                              <div class="home_preset__label">@lang('компьютерные и мобильные пресеты')</div>
                          </div>
                          <div class="home_preset__text will-fade-in">
                              @lang('gc.excerpt')
                              <div class="home_preset__button">
                                  <span>@lang('Посмотреть')</span>
                                  <img src="{{ asset('img/svg/arrow_view.svg') }}" alt="">
                              </div>
                          </div>
                      </div>
                  </div>
              </a>
          </div>
      </section>

      <section class="home_preset home_preset__metallic">
          <div class="container">
              <a href="products/metallic-blues" class="home_preset__wrapper metallic-blue-fix">
                  <div class="home_preset__page">02</div>

                  <div class="home_preset__content">
                      <div class="home_preset__info">
                          <div class="home_preset__title">
                              <div class="oh">
                                  <div class="will-fade-in-title">Metallic</div>
                              </div>
                              <div class="oh">
                                  <div class="will-fade-in-title">blues</div>
                              </div>
                          </div>
                          <div class="home_preset__quan_block">
                              <div class="home_preset__quan">5 @lang('пресетов')</div>
                              <div class="home_preset__label">@lang('компьютерные и мобильные пресеты')</div>
                          </div>
                          <div class="home_preset__text will-fade-in">
                              @lang('mb.excerpt')
                              <div class="home_preset__button">
                                  <span>@lang('Посмотреть')</span>
                                  <img src="{{ asset('img/svg/arrow_view.svg') }}" alt="">
                              </div>
                          </div>
                      </div>
                      <img class="will-fade-in visible_in_desktop" src="{{ asset('images/home_metallic_blue_retina.jpg') }}"
                           srcset="{{ asset('images/home_metallic_blue.jpg') }}, {{ asset('images/home_metallic_blue_retina.jpg 2x') }} " />
                      <img class="will-fade-in visible_in_tab" src="{{ asset('images/Metallic-Blues_tablet_retina.jpg') }}'"
                           srcset="{{ asset('images/Metallic-Blues_tablet.jpg') }}, {{ asset('images/Metallic-Blues_tablet_retina.jpg 2x') }} " />
                      <img class="will-fade-in visible_in_mobile" src="{{ asset('images/Photo_Metallic_Blues_mobile_retina.jpg') }}"
                           srcset="{{ asset('images/Photo_Metallic_Blues_mobile.jpg') }}, {{ asset('images/Photo_Metallic_Blues_mobile_retina.jpg 2x') }}' " />
                  </div>
              </a>
          </div>
      </section>

      <section class="home_preset home_preset__wedding">
          <div class="container">
              <a href="products/wedding-pureness" class="home_preset__wrapper wedding-fix">
                  <div class="home_preset__page">03</div>

                  <div class="home_preset__content">
                      <img class="will-fade-in visible_in_desktop" src="{{ asset('images/home_wedding_pureness_retina.jpg') }}"
                           srcset="{{ asset('images/home_wedding_pureness.jpg') }}, {{ asset('images/home_wedding_pureness_retina.jpg 2x') }} " />
                      <img class="will-fade-in visible_in_tab" src="{{ asset('images/home_wedding_pureness_tab_retina.jpg') }}"
                           srcset="{{ asset('images/home_wedding_pureness_tab.jpg') }}, {{ asset('images/home_wedding_pureness_tab_retina.jpg 2x') }} " />
                      <img class="will-fade-in visible_in_mobile" src="{{ asset('images/home_wedding_pureness_mob_retina.jpg') }}"
                           srcset="{{ asset('images/home_wedding_pureness_mob.jpg') }}, {{ asset('images/home_wedding_pureness_mob_retina.jpg 2x') }} " />
                      <div class="home_preset__info">
                          <div class="home_preset__title">
                              <div class="oh">
                                  <div class="will-fade-in-title">Wedding</div>
                              </div>
                              <div class="oh">
                                  <div class="will-fade-in-title">pureness</div>
                              </div>
                          </div>
                          <div class="home_preset__quan_block">
                              <div class="home_preset__quan">10 @lang('пресетов')</div>
                              <div class="home_preset__label">@lang('компьютерные пресеты')</div>
                          </div>
                          <div class="home_preset__text">
                              @lang('wp.excerpt')
                              <div class="home_preset__button home_preset__button_brown">
                                  <span>@lang('Посмотреть')</span>
                                  <img src="{{ asset('img/svg/arrow_view_brown.svg') }}" alt="">
                              </div>
                          </div>
                      </div>
                  </div>
              </a>
          </div>
      </section>

      <section class="home_preset home_preset__metallic home_preset__spring_burst">
          <div class="container">
              <a href="products/spring-burst" class="home_preset__wrapper spring_burst-fix">
                  <div class="home_preset__page">04</div>

                  <div class="home_preset__content">
                      <div class="home_preset__info">
                          <div class="home_preset__title">
                              <div class="oh">
                                  <div class="will-fade-in-title">Spring </div>
                              </div>
                              <div class="oh">
                                  <div class="will-fade-in-title">Burst</div>
                              </div>
                          </div>
                          <div class="home_preset__quan_block home_preset__quan_block_120">
                              <div class="home_preset__quan">7 @lang('пресетов')</div>
                              <div class="home_preset__label">@lang('мобильные пресеты')</div>
                          </div>
                          <div class="home_preset__text home_preset__text_250 will-fade-in">
                              @lang('sb.excerpt')
                              <div class="home_preset__button">
                                  <span>@lang('Посмотреть')</span>
                                  <img src="{{ asset('img/svg/arrow_view.svg') }}" alt="">
                              </div>
                          </div>
                      </div>
                      <img class="will-fade-in visible_in_desktop" src="{{ asset('images/home/spring_burst_retina.jpg') }}"
                           srcset="{{ asset('images/home/spring_burst.jpg') }}, {{ asset('images/home/spring_burst_retina.jpg 2x') }} " />
                      <img class="will-fade-in visible_in_tab" src="{{ asset('images/home/spring_burst_retina.jpg') }}'"
                           srcset="{{ asset('images/home/spring_burst.jpg') }}, {{ asset('images/home/spring_burst_retina.jpg 2x') }} " />
                      <img class="will-fade-in visible_in_mobile mobimg_mb_40" src="{{ asset('images/home/spring_burst_mobile_retina.jpg') }}"
                           srcset="{{ asset('images/home/spring_burst_mobile.jpg') }}, {{ asset('images/home/spring_burst_mobile_retina.jpg 2x') }}' " />
                  </div>
              </a>
          </div>
      </section>

      <section class="home_preset home_preset__wedding">
          <div class="container">
              <a href="products/safari-style" class="home_preset__wrapper home_preset__wrapper_safari safari_style-fix">
                  <div class="home_preset__page">05</div>

                  <div class="home_preset__content home_preset__content_safari">
                      <img class="will-fade-in visible_in_desktop" src="{{ asset('images/home/safari_style_retina.jpg') }}"
                           srcset="{{ asset('images/home/safari_style.jpg') }}, {{ asset('images/home/safari_style_retina.jpg 2x') }} " />
                      <img class="will-fade-in visible_in_tab" src="{{ asset('images/home/safari_style_retina.jpg') }}'"
                           srcset="{{ asset('images/home/safari_style.jpg') }}, {{ asset('images/home/safari_style_retina.jpg 2x') }} " />
                      <img class="will-fade-in visible_in_mobile mobimg_mb_70" src="{{ asset('images/home/safari_style_mobile_retina.jpg') }}"
                           srcset="{{ asset('images/home/safari_style_mobile.jpg') }}, {{ asset('images/home/safari_style_mobile_retina.jpg 2x') }}' " />
                      <div class="home_preset__info">
                          <div class="home_preset__title home_preset__title_safari">
                              <div class="oh">
                                  <div class="will-fade-in-title">Safari</div>
                              </div>
                              <div class="oh">
                                  <div class="will-fade-in-title">Style</div>
                              </div>
                          </div>
                          <div class="home_preset__quan_block">
                              <div class="home_preset__quan">6 @lang('пресетов')</div>
                              <div class="home_preset__label">@lang('мобильные пресеты')</div>
                          </div>
                          <div class="home_preset__text home_preset__text_250">
                              @lang('ss.excerpt')
                              <div class="home_preset__button home_preset__button_brown">
                                  <span>@lang('Посмотреть')</span>
                                  <img src="{{ asset('img/svg/arrow_view_brown.svg') }}" alt="">
                              </div>
                          </div>
                      </div>
                  </div>
              </a>
          </div>
      </section>

      <section class="home_author" >
          <div class="container">
              <div class="row home_author_row">
                  <div class="col-12 col-md-6 ha_item ha_item__first ha_item__text">
                      <div class="ha_wrapper">
                          <div class="ha_top">
                              <div class="ha_label will-fade-in">@lang('author.text')</div>
                              <div>
                                  <div class="ha_title will-fade-in">@lang('author.name')</div>
                              </div>
                          </div>


                          <div class="ha_description will-fade-in">@lang('author.body')</div>
                          <div class="ha_text will-fade-in" style="float: right"><a class="ha_text will-fade-in" target="_blank" href="https://instagram.com/zhenyaswan">@lang('author.inst')</a></div>

                      </div>

                  </div>
                  <div class="col-12 col-md-6 ha_item ha_item__image">
                      <img class="will-fade-in visible_in_desktop" src="{{ asset('images/evgeniya_retina.jpg') }}"
                           srcset="{{ asset('images/evgeniya.jpg') }}, src="{{ asset('images/evgeniya_retina.jpg 2x') }} " />
                      <img class="will-fade-in visible_in_tab" src="{{ asset('images/evgeniya_tab_retina.jpg') }}"
                           srcset="{{ asset('images/evgeniya_tab.jpg') }}, src="{{ asset('images/evgeniya_tab_retina.jpg 2x') }} " />
                      <img class="will-fade-in visible_in_mobile" src="{{ asset('images/evgeniya_mob_retina.jpg') }}"
                           srcset="{{ asset('images/evgeniya_mob.jpg') }}, src="{{ asset('images/evgeniya_mob_retina.jpg 2x') }} " />
                  </div>
                  <div class="col-12 col-md-6 ha_item ha_item__text ha_item__t1">
                      <div class="ha_text will-fade-in">
                          @lang('author.block1')
                      </div>
                  </div>
                  <div class="col-12 col-md-6 ha_item ha_item__text">
                      <div class="ha_text will-fade-in">
                          @lang('author.block2')
                      </div>
                  </div>
              </div>
          </div>
      </section>
{{--      <section class="home_price_block">--}}
{{--          <div class="container">--}}
{{--              <div class="preset_payment_block">--}}
{{--                  <div class="preset_payment_block__top">--}}
{{--                      <div class="row justify-content-between">--}}
{{--                          <div class="col-12 col-md-6 col-xl-6">--}}
{{--                              <div class="pp_block__title_small">Golden Caramel +</div>--}}
{{--                              <div class="pp_block__title_small">Metallic Blues +</div>--}}
{{--                              <div class="pp_block__title_small">Wedding Pureness</div>--}}
{{--                              <div class="pp_block__count">25 пресетов</div>--}}
{{--                          </div>--}}
{{--                          <div class="col-12 col-md-4 col-lg-5 col-xl-4">--}}
{{--                              <div class="row">--}}
{{--                                  <div class="col-6 col-md-12">--}}
{{--                                      <div class="pp_block__price_old">{{ number_format($product_bundle->price_original, 0, ".", " ") }} <i class="fal fa-ruble-sign"></i></div>--}}
{{--                                      <div class="pp_block__price">{{ number_format($product_bundle->price_discount, 0, ".", " ") }} <i class="far fa-ruble-sign"></i></div>--}}
{{--                                  </div>--}}
{{--                                  <div class="col-6 col-md-12">--}}
{{--                                      <div class="pp_block__discount">Скидка <span>65%</span> до <span>10 мая</span><br /><span>3 по цене 2-х</span></div>--}}
{{--                                  </div>--}}
{{--                              </div>--}}
{{--                          </div>--}}
{{--                      </div>--}}
{{--                  </div>--}}
{{--                  <div class="preset_payment_block__bottom">--}}
{{--                      <div class="row align-items-end justify-content-between">--}}
{{--                          <div class="col-12 col-md-6 col-xl-6">--}}
{{--                              <div class="pp_block__images">--}}
{{--                                  <img src="{{ asset('images/buy/1.png') }}" alt="">--}}
{{--                                  <img src="{{ asset('images/buy/2.png') }}" alt="">--}}
{{--                                  <img src="{{ asset('images/buy/3.png') }}" alt="">--}}
{{--                              </div>--}}
{{--                          </div>--}}
{{--                          <div class="col-12 col-md-4 col-lg-5 col-xl-4">--}}
{{--                              <a href="{{ route('cart.show')  . '?name=golden-metallic-wedding' }}" class="button_buy">Купить</a>--}}
{{--                          </div>--}}
{{--                      </div>--}}
{{--                  </div>--}}
{{--              </div>--}}
{{--          </div>--}}
{{--      </section>--}}

  </div>

@include('includes.footer')

@endsection
