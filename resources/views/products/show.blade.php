@extends('layout')

@section('content')
@include('includes.nav')
@include('includes.header_top_default')
    @switch($pack->slug)
        @case('golden-caramel')

        <div class="preset_page" >


            <section class="page_number">
                <div class="page_number__num">01</div>
            </section>

            <section class="preset_block">
                <div class="container">
                    <div class="row justify-content-between">
                        <div class="col-12 col-md-6 col-lg-5 preset_top__info">
                            <div class="preset_top__empty"></div>
                            <div class="preset_top__center">
                                <div class="preset_top__title will-fade-in" >{{ $pack->title }}</div>
                                {{-- <div class="preset_top__count will-fade-in">10 @lang('пресетов')</div> --}}
                                <div class="preset_top__subtitle will-fade-in">@lang('gc.excerpt')</div>
                                <div class="preset_top__description will-fade-in" >
                                    <p>@lang('gc.body1')</p>
                                    <p>@lang('gc.body2')</p>
                                </div>
                            </div>
                            <div class="preset_top__ht will-fade-in" >
                                @lang('gc.tags')
                            </div>
                        </div>
                        <div class="col-12 col-md-6 will-fade-in" >
                            <div class="preset_top_mobile">@lang('Двигайте слайдер для просмотра до/после')</div>
                            <div class="preset_top__photo">
                                <div class="twentytwenty-container">
                                    <img src="{{ asset('images/Golden-Caramel/Amber-1-mob.jpg') }}"
                                         srcset="{{ asset('images/Golden-Caramel/Amber-1-mob-retina.jpg 768w') }}, {{ asset('images/Golden-Caramel/Amber-1-desktop-retina.jpg') }} "/>
                                    <img src="{{ asset('images/Golden-Caramel/Amber-2-mob.jpg') }}"
                                         srcset="{{ asset('images/Golden-Caramel/Amber-2-mob-retina.jpg 768w') }}, {{ asset('images/Golden-Caramel/Amber-2-desktop-retina.jpg') }} "/>
                                </div>
                                <div class="preset_top__nav">
                                    <div class="preset_top__name">Amber</div>
                                    <div class="preset_top__number">01</div>
                                </div>
                            </div>
                            <div class="preset_top_before_after">
                                <div>@lang('до')</div> <div>@lang('после')</div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <section class="preset_block">
                <div class="container">
                    <div class="row">
                        <div class="col-12 col-md-4">
                            <div class="preset_top__photo will-fade-in">
                                <div class="twentytwenty-container">
                                    <img src="{{ asset('images/Golden-Caramel/DustyCaramel-1-mob.jpg') }}"
                                         srcset="{{ asset('images/Golden-Caramel/DustyCaramel-1-mob-retina.jpg 768w') }}, {{ asset('images/Golden-Caramel/DustyCaramel-1-desktop-retina.jpg') }}"/>
                                    <img src="{{ asset('images/Golden-Caramel/DustyCaramel-2-mob.jpg') }}"
                                         srcset="{{ asset('images/Golden-Caramel/DustyCaramel-2-mob-retina.jpg 768w') }}, {{ asset('images/Golden-Caramel/DustyCaramel-2-desktop-retina.jpg') }}"/>
                                </div>
                                <div class="preset_top__nav will-fade-in">
                                    <div class="preset_top__name preset_top__name_20">Dusty<br /> Caramel</div>
                                    <div class="preset_top__number">02</div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 visible_in_mobile">
                            <div class="preset_block_text_mob will-fade-in">@lang('gc.body1')</div>
                        </div>
                        <div class="col-12 col-md-4">
                            <div class="preset_top__photo">
                                <div class="twentytwenty-container will-fade-in">
                                    <img src="{{ asset('images/Golden-Caramel/SandyCaramel-1-mob.jpg') }}"
                                         srcset="{{ asset('images/Golden-Caramel/SandyCaramel-1-mob-retina.jpg 768w') }}, {{ asset('images/Golden-Caramel/SandyCaramel-1-desktop-retina.jpg') }}"/>
                                    <img src="{{ asset('images/Golden-Caramel/SandyCaramel-2-mob.jpg') }}"
                                         srcset="{{ asset('images/Golden-Caramel/SandyCaramel-2-mob-retina.jpg 768w') }}, {{ asset('images/Golden-Caramel/SandyCaramel-2-desktop-retina.jpg') }}"/>
                                </div>
                                <div class="preset_top__nav will-fade-in">
                                    <div class="preset_top__name preset_top__name_20">Sandy<br /> Caramel</div>
                                    <div class="preset_top__number">03</div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-md-4 aos-item" data-aos="fade-up">
                            <div class="preset_top__photo">
                                <div class="twentytwenty-container will-fade-in">
                                    <img src="{{ asset('images/Golden-Caramel/SavannahTouchGlossy-1-mob.jpg') }}"
                                         srcset="{{ asset('images/Golden-Caramel/SavannahTouchGlossy-1-mob-retina.jpg 768w') }}, {{ asset('images/Golden-Caramel/SavannahTouchGlossy-1-desktop-retina.jpg') }}"/>
                                    <img src="{{ asset('images/Golden-Caramel/SavannahTouchGlossy-2-mob.jpg') }}"
                                         srcset="{{ asset('images/Golden-Caramel/SavannahTouchGlossy-2-mob-retina.jpg 768w') }}, {{ asset('images/Golden-Caramel/SavannahTouchGlossy-2-desktop-retina.jpg') }}"/>
                                </div>
                                <div class="preset_top__nav will-fade-in">
                                    <div class="preset_top__name preset_top__name_20">Savannah <br />Touch Glossy</div>
                                    <div class="preset_top__number">04</div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 visible_in_mobile">
                            <div class="preset_block_text_mob will-fade-in">@lang('gc.body2')</div>
                        </div>
                    </div>
                </div>
            </section>

            <section class="preset_block">
                <div class="container">
                    <div class="row">
                        <div class="col-12 aos-item" data-aos="fade-up">
                            <div class="preset_top__photo">
                                <div class="twentytwenty-container will-fade-in">
                                    <img src="{{ asset('images/Golden-Caramel/SavannahTouch-1-mob.jpg') }}"
                                         srcset="{{ asset('images/Golden-Caramel/SavannahTouch-1-mob-retina.jpg 768w') }}, {{ asset('images/Golden-Caramel/SavannahTouch-1-desktop-retina.jpg') }}"/>
                                    <img src="{{ asset('images/Golden-Caramel/SavannahTouch-2-mob.jpg') }}"
                                         srcset="{{ asset('images/Golden-Caramel/SavannahTouch-2-mob-retina.jpg 768w') }}, {{ asset('images/Golden-Caramel/SavannahTouch-2-desktop-retina.jpg') }}"/>
                                </div>
                                <div class="preset_top__nav will-fade-in">
                                    <div class="preset_top__name preset_top__name_40">Savannah Touch</div>
                                    <div class="preset_top__number preset_top__number_25">05</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <section class="preset_block d-lg-none will-fade-in">
                <div class="container">
                    <div class="row">
                        <div class="col-12 col-md-6 preset_block_text hidden_in_mobile">
                            @lang('gc.small_text1')
                        </div>
                        <div class="col-12 col-md-6 preset_block_text hidden_in_mobile">
                            @lang('gc.small_text2')
                        </div>
                    </div>
                </div>
            </section>

            <section class="preset_block">
                <div class="container">
                    <div class="row justify-content-between">
                        <div class="col-12 col-md-6 col-lg-6 aos-item" data-aos="fade-up">
                            <div class="preset_top__photo">
                                <div class="twentytwenty-container will-fade-in">
                                    <img src="{{ asset('images/Golden-Caramel/SoftBrown-1-mob.jpg') }}"
                                         srcset="{{ asset('images/Golden-Caramel/SoftBrown-1-mob-retina.jpg 768w') }}, {{ asset('images/Golden-Caramel/SoftBrown-1-desktop-retina.jpg') }}"/>
                                    <img src="{{ asset('images/Golden-Caramel/SoftBrown-2-mob.jpg') }}"
                                         srcset="{{ asset('images/Golden-Caramel/SoftBrown-2-mob-retina.jpg 768w') }}, {{ asset('images/Golden-Caramel/SoftBrown-2-desktop-retina.jpg') }}"/>
                                </div>
                                <div class="preset_top__nav will-fade-in">
                                    <div class="preset_top__name preset_top__name_20">Soft Brown</div>
                                    <div class="preset_top__number">06</div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-md-6 col-lg-4 flex_vertical_between will-fade-in">
                            <div class="preset_block_text m_b_50 d-none d-lg-block">
                                @lang('gc.small_text1')<br>@lang('gc.small_text2')
                            </div>
                            <div class="preset_top__photo">
                                <div class="twentytwenty-container will-fade-in">
                                    <img src="{{ asset('images/Golden-Caramel/SoftCaramel-1-mob.jpg') }}"
                                         srcset="{{ asset('images/Golden-Caramel/SoftCaramel-1-mob-retina.jpg 768w') }}, {{ asset('images/Golden-Caramel/SoftCaramel-1-desktop-retina.jpg') }}"/>
                                    <img src="{{ asset('images/Golden-Caramel/SoftCaramel-2-mob.jpg') }}"
                                         srcset="{{ asset('images/Golden-Caramel/SoftCaramel-2-mob-retina.jpg 768w') }}, {{ asset('images/Golden-Caramel/SoftCaramel-2-desktop-retina.jpg') }}"/>
                                </div>
                                <div class="preset_top__nav will-fade-in">
                                    <div class="preset_top__name preset_top__name_20">Soft Caramel</div>
                                    <div class="preset_top__number">07</div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 visible_in_mobile">
                            <div class="preset_block_text_mob will-fade-in">@lang('gc.small_text1') @lang('gc.small_text2')</div>
                        </div>
                    </div>
                </div>
            </section>

            <section class="preset_block">
                <div class="container">
                    <div class="row">
                        <div class="col-12 col-md-4">
                            <div class="preset_top__photo">
                                <div class="twentytwenty-container will-fade-in">
                                    <img src="{{ asset('images/Golden-Caramel/SunsetKiss-1-mob.jpg') }}"
                                         srcset="{{ asset('images/Golden-Caramel/SunsetKiss-1-mob-retina.jpg 768w') }}, {{ asset('images/Golden-Caramel/SunsetKiss-1-desktop-retina.jpg') }}"/>
                                    <img src="{{ asset('images/Golden-Caramel/SunsetKiss-2-mob.jpg') }}"
                                         srcset="{{ asset('images/Golden-Caramel/SunsetKiss-2-mob-retina.jpg 768w') }}, {{ asset('images/Golden-Caramel/SunsetKiss-2-desktop-retina.jpg') }}"/>
                                </div>
                                <div class="preset_top__nav will-fade-in">
                                    <div class="preset_top__name preset_top__name_20">Sunset kiss</div>
                                    <div class="preset_top__number">08</div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-md-4">
                            <div class="preset_top__photo">
                                <div class="twentytwenty-container will-fade-in">
                                    <img src="{{ asset('images/Golden-Caramel/WeddingElegance-1-mob.jpg') }}"
                                         srcset="{{ asset('images/Golden-Caramel/WeddingElegance-1-mob-retina.jpg 768w') }}, {{ asset('images/Golden-Caramel/WeddingElegance-1-desktop-retina.jpg') }}"/>
                                    <img src="{{ asset('images/Golden-Caramel/WeddingElegance-2-mob.jpg') }}"
                                         srcset="{{ asset('images/Golden-Caramel/WeddingElegance-2-mob-retina.jpg 768w') }}, {{ asset('images/Golden-Caramel/WeddingElegance-2-desktop-retina.jpg') }}"/>
                                </div>
                                <div class="preset_top__nav will-fade-in">
                                    <div class="preset_top__name preset_top__name_20">Wedding<br /> elegance</div>
                                    <div class="preset_top__number">09</div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-md-4 aos-item" data-aos="fade-up">
                            <div class="preset_top__photo">
                                <div class="twentytwenty-container will-fade-in">
                                    <img src="{{ asset('images/Golden-Caramel/KissNeutral-1-mob.jpg') }}"
                                         srcset="{{ asset('images/Golden-Caramel/KissNeutral-1-mob-retina.jpg 768w') }}, {{ asset('images/Golden-Caramel/KissNeutral-1-desktop-retina.jpg') }}"/>
                                    <img src="{{ asset('images/Golden-Caramel/KissNeutral-2-mob.jpg') }}"
                                         srcset="{{ asset('images/Golden-Caramel/KissNeutral-2-mob-retina.jpg 768w') }}, {{ asset('images/Golden-Caramel/KissNeutral-2-desktop-retina.jpg') }}"/>
                                </div>
                                <div class="preset_top__nav will-fade-in">
                                    <div class="preset_top__name preset_top__name_20">ZS Sunset kiss<br />neutral</div>
                                    <div class="preset_top__number">10</div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-md-6 col-lg-4 will-fade-in" data-aos="fade-up">
                            <div class="preset_block_text p_t_50"><!--Еще больше примеров фотографий с этим пресетом в инстаграме: <a href="#" class="pr_link">#zhenyaswanpresets</a>--></div>
                        </div>
                    </div>
                </div>
            </section>

            <section class="preset_payment_list">
                <div class="container">
                    <div class="pp_tarif_horizontal">
                        <div class="pp_title">Golden Caramel</div>
                        <div class="pp_description">@lang('gc.tarifs')</div>
                    </div>

                    <div class="row">
                        <div class="col-12 col-lg-6 aos-item" data-aos="fade-up">
                            <a href="#" class="preset_payment_block preset_payment_block__new {{ \Cart::get($pack->desktopProduct()->id) ? 'preset_payment_block__selected' : ''  }} js-preset_payment_block ">
                                <div class="preset_payment_block__top will-fade-in">
                                    <div class="row justify-content-between">
                                        <div class="col-12 col-md-6 col-xl-6">
                                            <div class="pp_block__title">Desktop</div>
                                            <div class="pp_block__count pp_block__count_dark">{{ $pack->quantity }} @lang('пресетов')</div>
                                        </div>
                                        <div class="col-12 col-md-4 col-lg-5 col-xl-4">
                                            <div class="row pp_price_right">

                                                @switch(LaravelLocalization::getCurrentLocale())
                                                    @case('ru')
                                                    <div class="col-7 col-md-12">
                                                        <div class="pp_block__price_old">{{ number_format($pack->desktopProduct()->price_original, 0, ".", " ") }} <i class="fal fa-ruble-sign"></i></div>
                                                        <div class="pp_block__price">{{ number_format($pack->desktopProduct()->price_discount, 0, ".", " ") }} <i class="far fa-ruble-sign"></i></div>
                                                    </div>
                                                    <div class="col-5 col-md-12">
                                                        {{-- <div class="pp_block__discount"><span>Скидка <b>50%</b></span> <span>до <b>20 сентября</span></b></div> --}}
                                                    </div>
                                                    @break

                                                    @default
                                                    <div class="col-7 col-md-12">
                                                        <div class="pp_block__price_old"><i class="fal fa-dollar-sign"></i> {{ number_format($pack->desktopProduct()->usd_price_original, 0, ".", " ") }}</div>
                                                        <div class="pp_block__price"><i class="far fa-dollar-sign"></i> {{ number_format($pack->desktopProduct()->usd_price_discount, 0, ".", " ") }}</div>
                                                    </div>
                                                    <div class="col-5 col-md-12">
                                                        {{-- <div class="pp_block__discount"><span>Discount <b>50%</b></span> <span>until <b>september 20</span></b></div> --}}
                                                    </div>
                                                    @break
                                                @endswitch

                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <div class="preset_payment_block__bottom">
                                    <div class="row align-items-end justify-content-between">
                                        <div class="col-12 col-md-6 col-xl-6 will-fade-in">
                                            <div class="pp_block__text">@lang('gc.tarif_2')</div>
                                        </div>
                                        <div class="col-12 col-md-4 col-lg-5 col-xl-4">
                                            <div class="button_buy will-fade-in js-button_buy" data-id="{{ $pack->desktopProduct()->id }}" data-resource="{{ route('shopping_cart_products.store') }}">
                                                {{ \Cart::get($pack->desktopProduct()->id) ? __('В корзине') : __('купить') }}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="col-12 col-lg-6 aos-item" data-aos="fade-up">
                            <a href="#" class="preset_payment_block preset_payment_block__new {{ \Cart::get($pack->mobileProduct()->id) ? 'preset_payment_block__selected' : ''  }} js-preset_payment_block">
                                <div class="preset_payment_block__top will-fade-in">
                                    <div class="row justify-content-between">
                                        <div class="col-12 col-md-7 col-xl-7">
                                            <div class="pp_block__title">Mobile</div>
                                            <div class="pp_block__count pp_block__count_dark">{{ $pack->quantity }} @lang('пресетов')</div>
                                        </div>
                                        <div class="col-12 col-md-4 col-lg-5 col-xl-4">

                                            <div class="row pp_price_right">
                                                @switch(LaravelLocalization::getCurrentLocale())
                                                    @case('ru')
                                                    <div class="col-7 col-md-12">
                                                        <div class="pp_block__price_old">{{ number_format($pack->mobileProduct()->price_original, 0, ".", " ") }} <i class="fal fa-ruble-sign"></i></div>
                                                        <div class="pp_block__price">{{ number_format($pack->mobileProduct()->price_discount, 0, ".", " ") }} <i class="far fa-ruble-sign"></i></div>
                                                    </div>
                                                    <div class="col-5 col-md-12">
                                                        {{-- <div class="pp_block__discount"><span>Скидка <b>50%</b></span> <span>до <b>20 сентября</span></b></div> --}}
                                                    </div>
                                                    @break

                                                    @default
                                                    <div class="col-7 col-md-12">
                                                        <div class="pp_block__price_old"><i class="fal fa-dollar-sign"></i> {{ number_format($pack->mobileProduct()->usd_price_original, 0, ".", " ") }}</div>
                                                        <div class="pp_block__price"><i class="far fa-dollar-sign"></i> {{ number_format($pack->mobileProduct()->usd_price_discount, 0, ".", " ") }}</div>
                                                    </div>
                                                    <div class="col-5 col-md-12">
                                                        {{-- <div class="pp_block__discount"><span>Discount <b>50%</b></span> <span>until <b>september 20</span></b></div> --}}
                                                    </div>
                                                    @break
                                                @endswitch
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <div class="preset_payment_block__bottom">
                                    <div class="row align-items-end justify-content-between">
                                        <div class="col-12 col-md-6 col-xl-6 will-fade-in">
                                            <div class="pp_block__text">@lang('gc.tarif_1')</div>
                                        </div>
                                        <div class="col-12 col-md-4 col-lg-5 col-xl-4">
                                            <div class="button_buy will-fade-in js-button_buy" data-id="{{ $pack->mobileProduct()->id }}" data-resource="{{ route('shopping_cart_products.store') }}">{{ \Cart::get($pack->mobileProduct()->id) ? __('В корзине') : __('купить')  }}</div>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </section>
            {{-- Golden Caramel gc-end--}}

            <section class="presets_other">
                <div class="container">
                    <h2 class="section_h2 will-fade-in">@lang('Другие Коллекции')</h2>
                    <div class="row">
                        <div class="col-12 col-md-6">
                            <a href="../products/metallic-blues" class="po_item will-fade-in">
                                <img src="{{ asset('images/MetalBlue_retina.jpg') }}"
                                     srcset="{{ asset('images/Wedding_PresetMetBlue2-2%201.png') }}, {{ asset('images/MetalBlue_retina.jpg 2x') }} " />
                                <div class="po__name_block">
                                    <div class="po__name_num">02</div>
                                    <div class="po__name_title">Metallic<br /> blues</div>
                                </div>
                                <div class="po_link"></div>
                            </a>
                        </div>
                        <div class="col-12 col-md-6">
                            <a href="../products/wedding-pureness" class="po_item po_item__wedding will-fade-in">
                                <img src="{{ asset('images/Wedding_Preset_retina.jpg') }}"
                                     srcset="{{ asset('images/Wedding_PresetWarmTones-3%201.jpg') }}, {{ asset('images/Wedding_Preset_retina.jpg') }} " />
                                <div class="po__name_block">
                                    <div class="po__name_num">03</div>
                                    <div class="po__name_title">Wedding<br />pureness</div>
                                </div>
                                <div class="po_link"></div>
                            </a>
                        </div>

                    </div>
                </div>
            </section>
        </div>

        @break
        {{-- Metallic blues mb-start--}}
        @case('metallic-blues')

        <div class="preset_page preset__metallic_blues">
            <section class="page_number">
                <div class="page_number__num">02</div>
            </section>

            <section class="preset_block">
                <div class="container">
                    <div class="row justify-content-between">
                        <div class="col-12 col-md-6 col-lg-5 preset_top__info aos-item" data-aos="fade-up">
                            <div class="preset_top__empty"></div>
                            <div class="preset_top__center">
                                <div class="preset_top__title will-fade-in">Metallic blues</div>
                                <div class="preset_top__count will-fade-in">5 @lang('пресетов')</div>
                                <div class="preset_top__subtitle will-fade-in">@lang('mb.excerpt')</div>
                                <div class="preset_top__description will-fade-in">
                                    <p>@lang('mb.body1')</p>
                                    <p>@lang('mb.body2')</p>
                                </div>
                            </div>
                            <div class="preset_top__ht will-fade-in">@lang('mb.tags')</div>
                        </div>
                        <div class="col-12 col-md-6 aos-item" data-aos="fade-up">
                            <div class="preset_top_mobile will-fade-in">@lang('Двигайте слайдер для просмотра до/после')</div>
                            <div class="preset_top__photo">
                                <div class="twentytwenty-container will-fade-in">
                                    <img src="{{ asset('images/Metallic-Blues/ArcticMorning-1-mob.jpg') }}"
                                         srcset="{{ asset('images/Metallic-Blues/ArcticMorning-1-mob-retina.jpg 768w') }}, {{ asset('images/Metallic-Blues/ArcticMorning-1-desktop-retina.jpg') }} "/>
                                    <img src="{{ asset('images/Metallic-Blues/ArcticMorning-2-mob.jpg') }}"
                                         srcset="{{ asset('images/Metallic-Blues/ArcticMorning-2-mob-retina.jpg 768w') }}, {{ asset('images/Metallic-Blues/ArcticMorning-2-desktop-retina.jpg') }} "/>
                                </div>
                                <div class="preset_top__nav will-fade-in">
                                    <div class="preset_top__name">Arctic Morning</div>
                                    <div class="preset_top__number">01</div>
                                </div>
                            </div>
                            <div class="preset_top_before_after will-fade-in">
                                <div>@lang('до')</div> <div>@lang('после')</div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <section class="preset_block">
                <div class="container">
                    <div class="row justify-content-between">
                        <div class="col-12 col-md-4 col-lg-4 flex_vertical_between aos-item" data-aos="fade-up">

                            <div class="preset_top__photo">
                                <div class="twentytwenty-container will-fade-in">
                                    <img src="{{ asset('images/Metallic-Blues/BlueSunset-1-mob.jpg') }}"
                                         srcset="{{ asset('images/Metallic-Blues/BlueSunset-1-mob-retina.jpg 768w') }}, {{ asset('images/Metallic-Blues/BlueSunset-1-desktop-retina.jpg') }}"/>
                                    <img src="{{ asset('images/Metallic-Blues/BlueSunset-2-mob.jpg') }}"
                                         srcset="{{ asset('images/Metallic-Blues/BlueSunset-2-mob-retina.jpg 768w') }}, {{ asset('images/Metallic-Blues/BlueSunset-2-desktop-retina.jpg') }}"/>
                                </div>
                                <div class="preset_top__nav will-fade-in">
                                    <div class="preset_top__name preset_top__name_20">Blue Sunset</div>
                                    <div class="preset_top__number">02</div>
                                </div>
                            </div>

                            <div class="visible_in_mobile preset_top__photo_empty">
                                <div class="preset_block_text_mob will-fade-in aos-item" data-aos="fade-up">
                                    <p>@lang('mb.body1')</p>
                                    <p>@lang('mb.body2')</p>
                                </div>
                            </div>

                            <div class="preset_top__photo">
                                <div class="twentytwenty-container will-fade-in">
                                    <img src="{{ asset('images/Metallic-Blues/NavyDusk-1-mob.jpg') }}"
                                         srcset="{{ asset('images/Metallic-Blues/NavyDusk-1-mob-retina.jpg 768w') }}, {{ asset('images/Metallic-Blues/NavyDusk-1-desktop-retina.jpg') }}"/>
                                    <img src="{{ asset('images/Metallic-Blues/NavyDusk-2-mob.jpg') }}"
                                         srcset="{{ asset('images/Metallic-Blues/NavyDusk-2-mob-retina.jpg 768w') }}, {{ asset('images/Metallic-Blues/NavyDusk-2-desktop-retina.jpg') }}"/>
                                </div>
                                <div class="preset_top__nav will-fade-in">
                                    <div class="preset_top__name preset_top__name_20">Navy Dusk</div>
                                    <div class="preset_top__number">03</div>
                                </div>
                            </div>
                        </div>

                        <div class="col-12 col-md-6 col-lg-6 flex_vertical_between aos-item" data-aos="fade-up">
                            <div class="preset_top__photo">
                                <div class="twentytwenty-container will-fade-in">
                                    <img src="{{ asset('images/Metallic-Blues/CrystalTwilight-1-mob.jpg') }}"
                                         srcset="{{ asset('images/Metallic-Blues/CrystalTwilight-1-mob-retina.jpg 768w') }}, {{ asset('images/Metallic-Blues/CrystalTwilight-1-desktop-retina.jpg') }}"/>
                                    <img src="{{ asset('images/Metallic-Blues/CrystalTwilight-2-mob.jpg') }}"
                                         srcset="{{ asset('images/Metallic-Blues/CrystalTwilight-2-mob-retina.jpg 768w') }}, {{ asset('images/Metallic-Blues/CrystalTwilight-2-desktop-retina.jpg') }}"/>
                                </div>
                                <div class="preset_top__nav will-fade-in">
                                    <div class="preset_top__name preset_top__name_20">Crystal Twilight</div>
                                    <div class="preset_top__number">04</div>
                                </div>
                            </div>

                            <div class="preset_block_text p_t_50 hidden_in_mobile will-fade-in">
                                @lang('mb.small_text1')
                                <!--<p>Еще больше примеров фотографий с этим пресетомв инстаграме: <a href="#" class="pr_link">#zhenyaswanpresets</a></p>-->
                            </div>

                        </div>

                    </div>
                </div>
            </section>

            <section class="preset_block">
                <div class="container">
                    <div class="row">
                        <div class="col-12 aos-item" data-aos="fade-up">
                            <div class="preset_top__photo">
                                <div class="twentytwenty-container will-fade-in">
                                    <img src="{{ asset('images/Metallic-Blues/PurpleMist-1-mob.jpg') }}"
                                         srcset="{{ asset('images/Metallic-Blues/PurpleMist-1-mob-retina.jpg 768w') }}, {{ asset('images/Metallic-Blues/PurpleMist-1-desktop-retina.jpg') }}"/>
                                    <img src="{{ asset('images/Metallic-Blues/PurpleMist-2-mob.jpg') }}"
                                         srcset="{{ asset('images/Metallic-Blues/PurpleMist-2-mob-retina.jpg 768w') }}, {{ asset('images/Metallic-Blues/PurpleMist-2-desktop-retina.jpg') }}"/>
                                </div>
                                <div class="preset_top__nav will-fade-in">
                                    <div class="preset_top__name preset_top__name_40">Purple Mist</div>
                                    <div class="preset_top__number preset_top__number_25">05</div>
                                </div>
                            </div>
                            <div class="preset_block_text_mob visible_in_mobile will-fade-in">
                                @lang('mb.small_text1')
                                <!--<p>Еще больше примеров фотографий с этим пресетомв инстаграме: <a href="#" class="pr_link">#zhenyaswanpresets</a></p>-->
                            </div>
                        </div>

                    </div>
                </div>
            </section>

            <section class="preset_payment_list">
                <div class="container">
                    <div class="pp_tarif_horizontal">
                        <div class="pp_title">Metallic Blues</div>
                        <div class="pp_description">@lang('mb.tarifs')</div>
                    </div>

                    <div class="row">
                        <div class="col-12 col-lg-6 aos-item" data-aos="fade-up">
                            <a href="#" class="preset_payment_block preset_payment_block__new {{ \Cart::get($pack->desktopProduct()->id) ? 'preset_payment_block__selected' : ''  }} js-preset_payment_block">
                                <div class="preset_payment_block__top will-fade-in">
                                    <div class="row justify-content-between">
                                        <div class="col-12 col-md-6 col-xl-6">
                                            <div class="pp_block__title">Desktop</div>
                                            <div class="pp_block__count pp_block__count_dark">6 @lang('пресетов')</div>
                                        </div>
                                        <div class="col-12 col-md-4 col-lg-5 col-xl-4">
                                            <div class="row pp_price_right">

                                                @switch(LaravelLocalization::getCurrentLocale())
                                                    @case('ru')
                                                    <div class="col-7 col-md-12">
                                                        <div class="pp_block__price_old">{{ number_format($pack->desktopProduct()->price_original, 0, ".", " ") }} <i class="fal fa-ruble-sign"></i></div>
                                                        <div class="pp_block__price">{{ number_format($pack->desktopProduct()->price_discount, 0, ".", " ") }} <i class="far fa-ruble-sign"></i></div>
                                                    </div>
                                                    <div class="col-5 col-md-12">
                                                        {{-- <div class="pp_block__discount"><span>Скидка <b>50%</b></span> <span>до <b>20 сентября</span></b></div> --}}
                                                    </div>
                                                    @break

                                                    @default
                                                    <div class="col-7 col-md-12">
                                                        <div class="pp_block__price_old"><i class="fal fa-dollar-sign"></i> {{ number_format($pack->desktopProduct()->usd_price_original, 0, ".", " ") }}</div>
                                                        <div class="pp_block__price"><i class="far fa-dollar-sign"></i> {{ number_format($pack->desktopProduct()->usd_price_discount, 0, ".", " ") }}</div>
                                                    </div>
                                                    <div class="col-5 col-md-12">
                                                        {{-- <div class="pp_block__discount"><span>Discount <b>50%</b></span> <span>until <b>september 20</span></b></div> --}}
                                                    </div>
                                                    @break
                                                @endswitch

                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <div class="preset_payment_block__bottom">
                                    <div class="row align-items-end justify-content-between">
                                        <div class="col-12 col-md-6 col-xl-6 will-fade-in">
                                            <div class="pp_block__text">@lang('mb.tarif_2')</div>
                                        </div>
                                        <div class="col-12 col-md-4 col-lg-5 col-xl-4">
                                            <div class="button_buy will-fade-in js-button_buy" data-id="{{ $pack->desktopProduct()->id }}" data-resource="{{ route('shopping_cart_products.store') }}">{{ \Cart::get($pack->desktopProduct()->id) ? __('В корзине') : __('купить')  }}</div>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="col-12 col-lg-6 aos-item" data-aos="fade-up">
                            <a href="#" class="preset_payment_block preset_payment_block__new {{ \Cart::get($pack->mobileProduct()->id) ? 'preset_payment_block__selected' : ''  }} js-preset_payment_block">
                                <div class="preset_payment_block__top will-fade-in">
                                    <div class="row justify-content-between">
                                        <div class="col-12 col-md-7 col-xl-7">
                                            <div class="pp_block__title">Mobile</div>
                                            <div class="pp_block__count pp_block__count_dark">6 @lang('пресетов')</div>
                                        </div>
                                        <div class="col-12 col-md-4 col-lg-5 col-xl-4">

                                            <div class="row pp_price_right">
                                                @switch(LaravelLocalization::getCurrentLocale())
                                                    @case('ru')
                                                    <div class="col-7 col-md-12">
                                                        <div class="pp_block__price_old">{{ number_format($pack->mobileProduct()->price_original, 0, ".", " ") }} <i class="fal fa-ruble-sign"></i></div>
                                                        <div class="pp_block__price">{{ number_format($pack->mobileProduct()->price_discount, 0, ".", " ") }} <i class="far fa-ruble-sign"></i></div>
                                                    </div>
                                                    <div class="col-5 col-md-12">
                                                        {{-- <div class="pp_block__discount"><span>Скидка <b>50%</b></span> <span>до <b>20 сентября</span></b></div> --}}
                                                    </div>
                                                    @break

                                                    @default
                                                    <div class="col-7 col-md-12">
                                                        <div class="pp_block__price_old"><i class="fal fa-dollar-sign"></i> {{ number_format($pack->mobileProduct()->usd_price_original, 0, ".", " ") }}</div>
                                                        <div class="pp_block__price"><i class="far fa-dollar-sign"></i> {{ number_format($pack->mobileProduct()->usd_price_discount, 0, ".", " ") }}</div>
                                                    </div>
                                                    <div class="col-5 col-md-12">
                                                        {{-- <div class="pp_block__discount"><span>Discount <b>50%</b></span> <span>until <b>september 20</span></b></div> --}}
                                                    </div>
                                                    @break
                                                @endswitch
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <div class="preset_payment_block__bottom">
                                    <div class="row align-items-end justify-content-between">
                                        <div class="col-12 col-md-6 col-xl-6 will-fade-in">
                                            <div class="pp_block__text">@lang('mb.tarif_1')</div>
                                        </div>
                                        <div class="col-12 col-md-4 col-lg-5 col-xl-4">
                                            <div class="button_buy will-fade-in js-button_buy" data-id="{{ $pack->mobileProduct()->id }}" data-resource="{{ route('shopping_cart_products.store') }}">{{ \Cart::get($pack->mobileProduct()->id) ? __('В корзине') : __('купить')  }}</div>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </section>

            <section class="presets_other">
                <div class="container">
                    <h2 class="section_h2 will-fade-in">@lang('Другие Коллекции')</h2>
                    <div class="row">
                        <div class="col-12 col-md-6 aos-item" data-aos="fade-up">
                            <a href="../products/golden-caramel" class="po_item po_item__gc will-fade-in">
                                <img src="{{ asset('images/DustyCaramel_retina.jpg') }}"
                                     srcset="{{ asset('images/DustyCaramel.jpg') }}, {{ asset('images/DustyCaramel_retina.jpg 2x') }} " />
                                <div class="po__name_block">
                                    <div class="po__name_num">01</div>
                                    <div class="po__name_title">Golden <br />Caramel</div>
                                </div>
                                <div class="po_link"></div>
                            </a>
                        </div>
                        <div class="col-12 col-md-6 aos-item" data-aos="fade-up">
                            <a href="../products/wedding-pureness" class="po_item po_item__wedding will-fade-in">
                                <img src="{{ asset('images/Wedding_Preset_retina.jpg') }}"
                                     srcset="{{ asset('images/Wedding_PresetWarmTones-3%201.jpg') }}, {{ asset('images/Wedding_Preset_retina.jpg') }} " />
                                <div class="po__name_block">
                                    <div class="po__name_num">03</div>
                                    <div class="po__name_title">Wedding<br />pureness</div>
                                </div>
                                <div class="po_link"></div>
                            </a>
                        </div>

                    </div>
                </div>
            </section>
        </div>

        @break

        @case('wedding-pureness')

        <div class="preset_page preset__wedding_pureness">
            <section class="page_number">
                <div class="page_number__num">03</div>
            </section>

            <section class="preset_block">
                <div class="container">
                    <div class="row justify-content-between">
                        <div class="col-12 col-md-6 col-lg-5 preset_top__info aos-item" data-aos="fade-up">
                            <div class="preset_top__empty"></div>
                            <div class="preset_top__center">
                                <div class="preset_top__title will-fade-in">Wedding pureness</div>
                                <div class="preset_top__count will-fade-in">10 @lang('пресетов')</div>
                                <div class="preset_top__subtitle will-fade-in">@lang('wp.excerpt')</div>
                                <div class="preset_top__description will-fade-in">
                                    <p>@lang('wp.body1')</p>
                                </div>
                            </div>
                            <div class="preset_top__ht will-fade-in">
                                @lang('wp.tags')
                            </div>
                        </div>
                        <div class="col-12 col-md-6 aos-item" data-aos="fade-up">
                            <div class="preset_top_mobile will-fade-in">@lang('Двигайте слайдер для просмотра до/после')</div>
                            <div class="preset_top__photo">
                                <div class="twentytwenty-container will-fade-in">
                                    <img src="{{ asset('images/Wedding-Pureness/SoftandNatural-1-mob.jpg') }}"
                                         srcset="{{ asset('images/Wedding-Pureness/SoftandNatural-1-mob-retina.jpg 768w') }}, {{ asset('images/Wedding-Pureness/SoftandNatural-1-desktop-retina.jpg') }} "/>
                                    <img src="{{ asset('images/Wedding-Pureness/SoftandNatural-2-mob.jpg') }}"
                                         srcset="{{ asset('images/Wedding-Pureness/SoftandNatural-2-mob-retina.jpg 768w') }}, {{ asset('images/Wedding-Pureness/SoftandNatural-2-desktop-retina.jpg') }} "/>
                                </div>
                                <div class="preset_top__nav will-fade-in">
                                    <div class="preset_top__name">Soft and Natural</div>
                                    <div class="preset_top__number">01</div>
                                </div>
                            </div>
                            <div class="preset_top_before_after will-fade-in">
                                <div>@lang('до')</div> <div>@lang('после')</div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <section class="preset_block">
                <div class="container">
                    <div class="row">
                        <div class="col-12 col-md-4 aos-item" data-aos="fade-up">
                            <div class="preset_top__photo">
                                <div class="twentytwenty-container will-fade-in">
                                    <img src="{{ asset('images/Wedding-Pureness/WeddingBright-1-mob.jpg') }}"
                                         srcset="{{ asset('images/Wedding-Pureness/WeddingBright-1-mob-retina.jpg 768w') }}, {{ asset('images/Wedding-Pureness/WeddingBright-1-desktop-retina.jpg') }}"/>
                                    <img src="{{ asset('images/Wedding-Pureness/WeddingBright-2-mob.jpg') }}"
                                         srcset="{{ asset('images/Wedding-Pureness/WeddingBright-2-mob-retina.jpg 768w') }}, {{ asset('images/Wedding-Pureness/WeddingBright-2-desktop-retina.jpg') }}"/>
                                </div>
                                <div class="preset_top__nav will-fade-in">
                                    <div class="preset_top__name preset_top__name_20">Wedding <br />Bright</div>
                                    <div class="preset_top__number">02</div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 visible_in_mobile ">
                            <div class="preset_block_text_mob will-fade-in aos-item" data-aos="fade-up">@lang('wp.body1')</div>
                        </div>
                        <div class="col-12 col-md-4 aos-item" data-aos="fade-up">
                            <div class="preset_top__photo">
                                <div class="twentytwenty-container will-fade-in">
                                    <img src="{{ asset('images/Wedding-Pureness/WeddingCleanColor-1-mob.jpg') }}"
                                         srcset="{{ asset('images/Wedding-Pureness/WeddingCleanColor-1-mob-retina.jpg 768w') }}, {{ asset('images/Wedding-Pureness/WeddingCleanColor-1-desktop-retina.jpg') }}"/>
                                    <img src="{{ asset('images/Wedding-Pureness/WeddingCleanColor-2-mob.jpg') }}"
                                         srcset="{{ asset('images/Wedding-Pureness/WeddingCleanColor-2-mob-retina.jpg 768w') }}, {{ asset('images/Wedding-Pureness/WeddingCleanColor-2-desktop-retina.jpg') }}"/>
                                </div>
                                <div class="preset_top__nav will-fade-in">
                                    <div class="preset_top__name preset_top__name_20">Wedding <br /> Clean Color</div>
                                    <div class="preset_top__number">03</div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-md-4 aos-item" data-aos="fade-up">
                            <div class="preset_top__photo">
                                <div class="twentytwenty-container will-fade-in">
                                    <img src="{{ asset('images/Wedding-Pureness/WeddingDayTime-1-mob.jpg') }}"
                                         srcset="{{ asset('images/Wedding-Pureness/WeddingDayTime-1-mob-retina.jpg 768w') }}, {{ asset('images/Wedding-Pureness/WeddingDayTime-1-desktop-retina.jpg') }}"/>
                                    <img src="{{ asset('images/Wedding-Pureness/WeddingDayTime-2-mob.jpg') }}"
                                         srcset="{{ asset('images/Wedding-Pureness/WeddingDayTime-2-mob-retina.jpg 768w') }}, {{ asset('images/Wedding-Pureness/WeddingDayTime-2-desktop-retina.jpg') }}"/>
                                </div>
                                <div class="preset_top__nav will-fade-in">
                                    <div class="preset_top__name preset_top__name_20">Wedding <br />Daytime</div>
                                    <div class="preset_top__number">04</div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 visible_in_mobile">
                            <div class="preset_block_text_mob aos-item" data-aos="fade-up">@lang('wp.small_text1')</div>
                        </div>
                    </div>
                </div>
            </section>

            <section class="preset_block">
                <div class="container">
                    <div class="row">
                        <div class="col-12 aos-item" data-aos="fade-up">
                            <div class="preset_top__photo">
                                <div class="twentytwenty-container will-fade-in">
                                    <img src="{{ asset('images/Wedding-Pureness/WeddingDayTimeSoft-1-mob.jpg') }}"
                                         srcset="{{ asset('images/Wedding-Pureness/WeddingDayTimeSoft-1-mob-retina.jpg 768w') }}, {{ asset('images/Wedding-Pureness/WeddingDayTimeSoft-1-desktop-retina.jpg') }}"/>
                                    <img src="{{ asset('images/Wedding-Pureness/WeddingDayTimeSoft-2-mob.jpg') }}"
                                         srcset="{{ asset('images/Wedding-Pureness/WeddingDayTimeSoft-2-mob-retina.jpg 768w') }}, {{ asset('images/Wedding-Pureness/WeddingDayTimeSoft-2-desktop-retina.jpg') }}"/>
                                </div>
                                <div class="preset_top__nav will-fade-in">
                                    <div class="preset_top__name preset_top__name_40">Wedding Daytime Soft</div>
                                    <div class="preset_top__number preset_top__number_25">05</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <section class="preset_block d-lg-none">
                <div class="container">
                    <div class="row aos-item" data-aos="fade-up">
                        <div class="col-12 col-md-6 preset_block_text hidden_in_mobile">
                            @lang('wp.small_text1')
                        </div>
                        <div class="col-12 col-md-6 preset_block_text hidden_in_mobile">
                            @lang('wp.small_text2')
                        </div>
                    </div>
                </div>
            </section>

            <section class="preset_block">
                <div class="container">
                    <div class="row justify-content-between">
                        <div class="col-12 col-md-6 col-lg-6 aos-item" data-aos="fade-up">
                            <div class="preset_top__photo">
                                <div class="twentytwenty-container will-fade-in ">
                                    <img src="{{ asset('images/Wedding-Pureness/WeddingEvening-1-mob.jpg') }}"
                                         srcset="{{ asset('images/Wedding-Pureness/WeddingEvening-1-mob-retina.jpg 768w') }}, {{ asset('images/Wedding-Pureness/WeddingEvening-1-desktop-retina.jpg') }}"/>
                                    <img src="{{ asset('images/Wedding-Pureness/WeddingEvening-2-mob.jpg') }}"
                                         srcset="{{ asset('images/Wedding-Pureness/WeddingEvening-2-mob-retina.jpg 768w') }}, {{ asset('images/Wedding-Pureness/WeddingEvening-2-desktop-retina.jpg') }}"/>
                                </div>
                                <div class="preset_top__nav will-fade-in">
                                    <div class="preset_top__name preset_top__name_20">Wedding Evening</div>
                                    <div class="preset_top__number">06</div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-md-6 col-lg-4 flex_vertical_between aos-item" data-aos="fade-up">
                            <div class="preset_block_text m_b_50 d-none d-lg-block will-fade-in">
                                <p>@lang('wp.small_text1')</p>
                                <p>@lang('wp.small_text2')</p>
                            </div>
                            <div class="preset_top__photo">
                                <div class="twentytwenty-container will-fade-in">
                                    <img src="{{ asset('images/Wedding-Pureness/WeddingFineArt-1-mob.jpg') }}"
                                         srcset="{{ asset('images/Wedding-Pureness/WeddingFineArt-1-mob-retina.jpg 768w') }}, {{ asset('images/Wedding-Pureness/WeddingFineArt-1-desktop-retina.jpg') }}"/>
                                    <img src="{{ asset('images/Wedding-Pureness/WeddingFineArt-2-mob.jpg') }}"
                                         srcset="{{ asset('images/Wedding-Pureness/WeddingFineArt-2-mob-retina.jpg 768w') }}, {{ asset('images/Wedding-Pureness/WeddingFineArt-2-desktop-retina.jpg') }}"/>
                                </div>
                                <div class="preset_top__nav will-fade-in">
                                    <div class="preset_top__name preset_top__name_20">Wedding Fine Art</div>
                                    <div class="preset_top__number">07</div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 visible_in_mobile">
                            <div class="preset_block_text m_b_50 d-none d-lg-block will-fade-in" data-aos="fade-up">
                                @lang('wp.small_text1')
                                <br />
                                @lang('wp.small_text2')
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <section class="preset_block">
                <div class="container">
                    <div class="row">
                        <div class="col-12 col-md-4 aos-item" data-aos="fade-up">
                            <div class="preset_top__photo">
                                <div class="twentytwenty-container will-fade-in">
                                    <img src="{{ asset('images/Wedding-Pureness/WeddingNeutralTones-1-mob.jpg') }}"
                                         srcset="{{ asset('images/Wedding-Pureness/WeddingNeutralTones-1-mob-retina.jpg 768w') }}, {{ asset('images/Wedding-Pureness/WeddingNeutralTones-1-desktop-retina.jpg') }}"/>
                                    <img src="{{ asset('images/Wedding-Pureness/WeddingNeutralTones-2-mob.jpg') }}"
                                         srcset="{{ asset('images/Wedding-Pureness/WeddingNeutralTones-2-mob-retina.jpg 768w') }}, {{ asset('images/Wedding-Pureness/WeddingNeutralTones-2-desktop-retina.jpg') }}"/>
                                </div>
                                <div class="preset_top__nav will-fade-in">
                                    <div class="preset_top__name preset_top__name_20">Wedding <br />Neutral Tones</div>
                                    <div class="preset_top__number">08</div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-md-4 aos-item" data-aos="fade-up">
                            <div class="preset_top__photo">
                                <div class="twentytwenty-container will-fade-in">
                                    <img src="{{ asset('images/Wedding-Pureness/WeddingInsidePortraits-1-mob.jpg') }}"
                                         srcset="{{ asset('images/Wedding-Pureness/WeddingInsidePortraits-1-mob-retina.jpg 768w') }}, {{ asset('images/Wedding-Pureness/WeddingInsidePortraits-1-desktop-retina.jpg') }}"/>
                                    <img src="{{ asset('images/Wedding-Pureness/WeddingInsidePortraits-2-mob.jpg') }}"
                                         srcset="{{ asset('images/Wedding-Pureness/WeddingInsidePortraits-2-mob-retina.jpg 768w') }}, {{ asset('images/Wedding-Pureness/WeddingInsidePortraits-2-desktop-retina.jpg') }}"/>
                                </div>
                                <div class="preset_top__nav will-fade-in">
                                    <div class="preset_top__name preset_top__name_20">Wedding Inside <br />Portraits</div>
                                    <div class="preset_top__number">09</div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-md-4 aos-item" data-aos="fade-up">
                            <div class="preset_top__photo">
                                <div class="twentytwenty-container will-fade-in">
                                    <img src="{{ asset('images/Wedding-Pureness/Wedding_PresetWarmTones-1-mob.jpg') }}"
                                         srcset="{{ asset('images/Wedding-Pureness/Wedding_PresetWarmTones-1-mob-retina.jpg 768w') }}, {{ asset('images/Wedding-Pureness/Wedding_PresetWarmTones-1-desktop-retina.jpg') }}"/>
                                    <img src="{{ asset('images/Wedding-Pureness/Wedding_PresetWarmTones-2-mob.jpg') }}"
                                         srcset="{{ asset('images/Wedding-Pureness/Wedding_PresetWarmTones-2-mob-retina.jpg 768w') }}, {{ asset('images/Wedding-Pureness/Wedding_PresetWarmTones-2-desktop-retina.jpg') }}"/>
                                </div>
                                <div class="preset_top__nav will-fade-in">
                                    <div class="preset_top__name preset_top__name_20">Wedding <br />Warm Tones</div>
                                    <div class="preset_top__number">10</div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-md-6 col-lg-8 aos-item" data-aos="fade-up">
                            <div class="preset_block_text p_t_50 will-fade-in">
                                @lang('wp.small_text3')
                            </div>
                        </div>
                        <div class="col-12 col-md-6 col-lg-8 aos-item" data-aos="fade-up">
                            <div class="preset_block_text p_t_50 will-fade-in">
                                <!--Еще больше примеров фотографий с этим пресетом в инстаграме: <a href="#" class="pr_link">#zhenyaswanpresets</a>-->
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <section class="preset_payment_list">
                <div class="container">
                    <div class="pp_title">Wedding <br />pureness</div>
                    <div class="pp_description">@lang('wp.tarifs')</div>
                    <div class="row">
                        <div class="col-12 col-lg-6 aos-item" data-aos="fade-up">
                            <a href="#" class="preset_payment_block preset_payment_block__new {{ \Cart::get($pack->desktopProduct()->id) ? 'preset_payment_block__selected' : ''  }} js-preset_payment_block">
                                <div class="preset_payment_block__top will-fade-in">
                                    <div class="row justify-content-between">
                                        <div class="col-12 col-md-6 col-xl-6">
                                            <div class="pp_block__title">Desktop</div>
                                            <div class="pp_block__count pp_block__count_dark">6 @lang('пресетов')</div>
                                        </div>
                                        <div class="col-12 col-md-4 col-lg-5 col-xl-4">
                                            <div class="row pp_price_right">

                                                @switch(LaravelLocalization::getCurrentLocale())
                                                    @case('ru')
                                                    <div class="col-7 col-md-12">
                                                        <div class="pp_block__price_old">{{ number_format($pack->desktopProduct()->price_original, 0, ".", " ") }} <i class="fal fa-ruble-sign"></i></div>
                                                        <div class="pp_block__price">{{ number_format($pack->desktopProduct()->price_discount, 0, ".", " ") }} <i class="far fa-ruble-sign"></i></div>
                                                    </div>
                                                    <div class="col-5 col-md-12">
                                                        {{-- <div class="pp_block__discount"><span>Скидка <b>50%</b></span> <span>до <b>20 сентября</span></b></div> --}}
                                                    </div>
                                                    @break

                                                    @default
                                                    <div class="col-7 col-md-12">
                                                        <div class="pp_block__price_old"><i class="fal fa-dollar-sign"></i> {{ number_format($pack->desktopProduct()->usd_price_original, 0, ".", " ") }}</div>
                                                        <div class="pp_block__price"><i class="far fa-dollar-sign"></i> {{ number_format($pack->desktopProduct()->usd_price_discount, 0, ".", " ") }}</div>
                                                    </div>
                                                    <div class="col-5 col-md-12">
                                                        {{-- <div class="pp_block__discount"><span>Discount <b>50%</b></span> <span>until <b>september 20</span></b></div> --}}
                                                    </div>
                                                    @break
                                                @endswitch

                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <div class="preset_payment_block__bottom">
                                    <div class="row align-items-end justify-content-between">
                                        <div class="col-12 col-md-6 col-xl-6 will-fade-in">
                                            <div class="pp_block__text">@lang('wp.tarif_1')</div>
                                        </div>
                                        <div class="col-12 col-md-4 col-lg-5 col-xl-4">
                                            <div class="button_buy will-fade-in js-button_buy" data-id="{{ $pack->desktopProduct()->id }}" data-resource="{{ route('shopping_cart_products.store') }}">{{ \Cart::get($pack->desktopProduct()->id) ? __('В корзине') : __('купить')  }}</div>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>

                        {{-- Desktop All block --}}
                        <div class="col-12 col-lg-6 aos-item" data-aos="fade-up">
                            <a href="#" class="preset_payment_block preset_payment_block__new {{  $isAllDesktopProductInCart ? 'preset_payment_block__selected' : ''  }} js-button_buy_list" data-ids="{{ $desktopProductIdsJson }}" data-resource="{{ route('shopping_cart_product_list.store') }}">
                                <div class="preset_payment_block__top will-fade-in">
                                    <div class="row justify-content-between">
                                        <div class="col-12 col-md-7 col-xl-7">
                                            <div class="pp_block__title">Desktop All</div>
                                            <div class="pp_block__count pp_block__count_dark">{{ $desktopProductsCount }} @lang('коллекции')</div>
                                        </div>
                                        <div class="col-12 col-md-4 col-lg-5 col-xl-4">

                                            <div class="row pp_price_right">
                                                @switch(LaravelLocalization::getCurrentLocale())
                                                    @case('ru')
                                                    <div class="col-7 col-md-12">
                                                        <div class="pp_block__price_old">{{ number_format($allDesktopRubPriceOriginal, 0, ".", " ") }} <i class="fal fa-ruble-sign"></i></div>
                                                        <div class="pp_block__price">{{ number_format($allDesktopRubPriceDiscount, 0, ".", " ") }} <i class="far fa-ruble-sign"></i></div>
                                                    </div>
                                                    <div class="col-5 col-md-12">
                                                        {{-- <div class="pp_block__discount"><span>Скидка <b>50%</b></span> <span>до <b>20 сентября</span></b></div> --}}
                                                    </div>
                                                    @break

                                                    @default
                                                    <div class="col-7 col-md-12">
                                                        <div class="pp_block__price_old"><i class="fal fa-dollar-sign"></i> {{ number_format($allDesktopUsdPriceOriginal, 0, ".", " ") }}</div>
                                                        <div class="pp_block__price"><i class="far fa-dollar-sign"></i> {{ number_format($allDesktopUsdPriceDiscount, 0, ".", " ") }}</div>
                                                    </div>
                                                    <div class="col-5 col-md-12">
                                                        {{-- <div class="pp_block__discount"><span>Discount <b>50%</b></span> <span>until <b>september 20</span></b></div> --}}
                                                    </div>
                                                    @break
                                                @endswitch
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <div class="preset_payment_block__bottom">
                                    <div class="row align-items-end justify-content-between">
                                        <div class="col-12 col-md-6 col-xl-6 will-fade-in">
                                            <div class="pp_block__text">@lang('wp.tarif_2')</div>
                                        </div>
                                        <div class="col-12 col-md-4 col-lg-5 col-xl-4">
                                            <div class="button_buy will-fade-in js_buy_buton_text">{{ $isAllDesktopProductInCart ? __('В корзине') : __('купить')  }}</div>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>

                    </div>
                </div>
            </section>

            <section class="presets_other">
                <div class="container">
                    <h2 class="section_h2 will-fade-in">@lang('Другие Коллекции')</h2>

                    <div class="row">

                        <div class="col-12 col-md-6 aos-item" data-aos="fade-up">
                            <a href="/products/golden-caramel" class="po_item po_item__gc will-fade-in">
                                <img src="{{ asset('images/DustyCaramel_retina.jpg') }}"
                                     srcset="{{ asset('images/DustyCaramel.jpg') }}, {{ asset('images/DustyCaramel_retina.jpg 2x') }} " />
                                <div class="po__name_block">
                                    <div class="po__name_num">01</div>
                                    <div class="po__name_title">Golden <br />Caramel</div>
                                </div>
                                <div class="po_link"></div>
                            </a>
                        </div>
                        <div class="col-12 col-md-6 aos-item" data-aos="fade-up">
                            <a href="../products/metallic-blues" class="po_item will-fade-in">
                                <img src="{{ asset('images/MetalBlue_retina.jpg') }}"
                                     srcset="{{ asset('images/Wedding_PresetMetBlue2-2%201.png') }}, {{ asset('images/MetalBlue_retina.jpg 2x') }} " />
                                <div class="po__name_block">
                                    <div class="po__name_num">02</div>
                                    <div class="po__name_title">Metallic<br /> blues</div>
                                </div>
                                <div class="po_link"></div>
                            </a>
                        </div>

                    </div>
                </div>
            </section>
        </div>

        @break
    @endswitch

    @include('includes.footer')
@endsection
