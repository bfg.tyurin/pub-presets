@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <p>You are logged in!</p>

                    <ul>Ваши покупки:
                    @foreach ($orders as $order)
                        <li>Заказ {{ $order->product->title }}</li>
                    @endforeach
                    @php  unset($order) @endphp

                    </ul>
                    <ul>Ваши ссылки на скачивание:
                    @foreach ($orders as $order)
                            @foreach ($order->product->packs as $pack)
                                <li>Ссылка на пак пресетов: <a href="{{ $pack->title }}">{{ $pack->title }}</a></li>
                            @endforeach
                    @endforeach
                    </ul>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
