<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta content="width=device-width,initial-scale=1" name="viewport">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Пресеты Жени Савиной</title>
    <meta content="Пресеты Жени Савиной" name="description">

    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@400;500;600;700;800;900&display=swap" rel="stylesheet">

    <link href="{{ mix('css/app.css')}}" rel="stylesheet">
    <script src="https://widget.cloudpayments.ru/bundles/cloudpayments"></script>
    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
                new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
            j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
            'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-WQ3HH6S');</script>
    <!-- End Google Tag Manager -->
</head>

    <body>
        <!-- Google Tag Manager (noscript) -->
        <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-WQ3HH6S"
                          height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
        <!-- End Google Tag Manager (noscript) -->

        <?php if(!isset($_COOKIE['lang'])){ ?>
            <div class="show_in_mobile">
                <div class="change_lang">
                    <a class="js_write_lang_cookie change_lang_btn_c {{ LaravelLocalization::getCurrentLocale() === 'ru' ? 'active' : ''}}" href="{{ LaravelLocalization::getLocalizedURL('ru') }}">РУС</a>
                    <a class="js_write_lang_cookie change_lang_btn {{ LaravelLocalization::getCurrentLocale() === 'en' ? 'active' : ''}}" href="{{ LaravelLocalization::getLocalizedURL('en') }}">ENG</a>
                    <div class="change_lang_text">
                        Выберите язык<br />
                        Choose language
                    </div>
                </div>
            </div>
        <?php } ?>

        @yield('content')



        <script src="{{ mix('js/app.js') }}"></script>
        <script type="text/javascript" src="{{ asset('js/select2.js') }}"></script>
        <script type="text/javascript" src="{{ asset('js/floatingFormLabels.min.js') }}"></script>
        <script src="{{ asset('js/jquery.event.move.js') }}"></script>
        <script src="{{ asset('js/jquery.twentytwenty.js') }}"></script>
        <script src="{{ asset('js/libs/jquery.mousewheel.js') }}"></script>
        <script src="{{ asset('js/libs/SmoothScroll.js') }}"></script>
        <script type="text/javascript" src="{{ asset('js/app_custom.js') }}"></script>


    </body>
</html>
