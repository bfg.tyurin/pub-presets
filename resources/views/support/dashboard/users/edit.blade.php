@extends('support.layout')

@section('content')
<div class="flex flex-col">
    <div class="my-2 py-2 overflow-x-auto sm:-mx-6 sm:px-6 lg:-mx-8 lg:px-8">
        <div class="flex justify-center align-middle min-w-full overflow-hidden sm:rounded-lg">
            <div class="w-full max-w-sm">
                <h1 class="text-lg px-8 pt-6 pb-8">Редактирование пользователя</h1>
                @if ($errors->any())
                <div>
                    <ul>
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                @endif

                <form class="bg-white shadow-md rounded px-8 pt-6" method="POST"
                    action="{{ route('support.dashboard.users.update', $user) }}">
                    @csrf
                    @method('PATCH')
                    <div class="mb-4">
                        <label class="block text-gray-700 text-sm font-bold mb-2" for="user_email">
                            Email
                        </label>
                        <input name="user_email" value="{{ $user->email }}" required
                            class="mb-4 shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                            id="email" type="email" placeholder="email">

                        <div class="pb-4 flex items-center justify-center">
                            <button data-confirm="Вы уверены?"
                                class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline"
                                type="submit">
                                Обновить данные пользователя
                            </button>
                        </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
