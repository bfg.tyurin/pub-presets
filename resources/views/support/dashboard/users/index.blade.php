@extends('support.layout')

@section('content')
<div class="flex flex-col items-center ">
    <form class="flex flex-col w-full sm:w-1/5 items-center py-4">
        <div class="w-full px-3 mb-2 md:mb-0">
            <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" for="user_email">
                Email:
            </label>
            <input name="user_email" type="email"
                class="appearance-none block w-full bg-gray-200 text-gray-700 border rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white"
                id="grid-first-name" type="text" placeholder="email" value="{{ $user_email ?? '' }}">
            <button href="{{ route('support.dashboard.users.index') }}"
                class="bg-blue-500 hover:bg-blue-700 text-white font-bold text-center py-2 px-4">
                Найти
            </button>
        </div>
    </form>
    <div class="flex flex-col my-2 py-2  overflow-x-auto sm:-mx-6 sm:px-6 lg:-mx-8 lg:px-8">
        <div class="align-middle inline-block  shadow overflow-hidden sm:rounded-lg border-b border-gray-200">
            <table class="divide-y divide-gray-200">
                <thead>
                    <tr>
                        <th
                            class="px-6 py-3 border-b border-gray-200 bg-gray-50 text-left text-xs leading-4 font-medium text-gray-500 uppercase tracking-wider">
                            Пользователь
                        </th>
                        <th class="px-6 py-3 border-b border-gray-200 bg-gray-50"></th>
                    </tr>
                </thead>
                <tbody class="bg-white">
                    @foreach ($users as $user)
                    <tr>
                        <td class="px-6 py-4 whitespace-no-wrap border-b border-gray-200">
                            <div class="flex items-center">
                                <div class="ml-4">
                                    <div class="text-sm leading-5 font-medium text-gray-900">
                                        {{ $user->email }}
                                    </div>
                                    <div class="text-sm leading-5 text-gray-500">
                                        {{ $user->profile->name ?? '-' }} {{ $user->profile->surname ?? '-' }}
                                    </div>
                                    @if (count($user->completedOrders) > 0)
                                    @php
                                    @endphp
                                    <a href="{{ route('support.dashboard.orders.index', ['user_email' => $user->email]) }}"
                                        class="text-xs text-teal-500">
                                        Посмотреть выполненные заказы
                                    </a>
                                    @endif

                                </div>
                            </div>
                        </td>
                        <td
                            class="px-6 py-4 whitespace-no-wrap text-right border-b border-gray-200 text-sm leading-5 font-medium">
                            <a href="{{ route('support.dashboard.users.edit', $user) }}"
                                class="mr-5 text-indigo-600 hover:text-indigo-900">
                                <svg class="fill-current text-indigo-500 inline-block h-4 w-4"
                                    xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
                                    <path d="M12.3 3.7l4 4L4 20H0v-4L12.3 3.7zm1.4-1.4L16 0l4 4-2.3 2.3-4-4z" />
                                </svg>
                            </a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
    {{ $users->links('vendor.pagination.tailwind') }}
</div>

@endsection
