<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta name="robots" content="noindex, nofollow" />
    <meta charset="utf-8">
    <meta content="width=device-width,initial-scale=1" name="viewport">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Пресеты Жени Савиной</title>
    <meta content="Пресеты Жени Савиной" name="description">

    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@400;500;600;700;800;900&display=swap"
        rel="stylesheet">

    <link href="{{ mix('css/app.css')}}" rel="stylesheet">
</head>

<body>
    <nav class="navbar navbar-expand-md navbar-dark sticky-top bg-dark">
        <a class="navbar-brand" href="{{ url('/') }}">Сайт</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse"
            aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarCollapse">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('support.dashboard.index') }}">Dashboard</a>
                </li>
                <li class="nav-item active">
                    <a class="nav-link" href="{{ route('support.dashboard.gifts') }}">Бесплатные пресеты
                        ({{ $gifts ? $gifts->count() : '' }})</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('support.dashboard.discounts.index') }}">Скидки</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('support.dashboard.orders.index') }}">Ручные заказы</a>
                </li>
                <li class="nav-item">
                    <a href="{{ route('support.dashboard.users.index') }}" class="nav-link">Пользователи</a>
                </li>
            </ul>
            <ul class="navbar-nav px-3">
                <li class="nav-item text-nowrap">
                    <a class="nav-link" href="{{ route('admin.logout') }}">Выйти</a>
                </li>
            </ul>
        </div>
    </nav>



    <div class="container-fluid">
        <div class="row">
            <div class="col-md-2 col-lg-8"></div>
            <main role="main" class="col-md-8 ml-sm-auto col-lg-8 px-4">
                <div class="table-responsive" style=" font-size: .875rem">
                    <table class="table table-striped table-sm">
                        <thead class="thead-dark">
                            <tr>
                                <th>Email</th>
                                <th>Instagram</th>
                                <th>Телефон</th>
                                <th>Создан</th>
                                <th>Обновлен</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($gifts as $gift)
                            <tr>
                                <th>{{ $gift->user->email }}</th>
                                <td>{{ $gift->instagram }}</td>
                                <td>{{ $gift->phone }}</td>
                                <td>{{ $gift->created_at->timezone('Europe/Moscow') }}</td>
                                <td>{{ $gift->updated_at->timezone('Europe/Moscow') }}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </main>
            <div class="col-md-2 col-lg-2"></div>
        </div>
    </div>


    <script src="{{ mix('js/app.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/select2.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/floatingFormLabels.min.js') }}"></script>
    <script src="{{ asset('js/jquery.event.move.js') }}"></script>
    <script src="{{ asset('js/jquery.twentytwenty.js') }}"></script>
    <script src="{{ asset('js/libs/jquery.mousewheel.js') }}"></script>
    <script src="{{ asset('js/libs/SmoothScroll.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/app_custom.js') }}"></script>


</body>

</html>
