<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta name="robots" content="noindex, nofollow" />
    <meta charset="utf-8">
    <meta content="width=device-width,initial-scale=1" name="viewport">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Пресеты Жени Савиной</title>
    <meta content="Пресеты Жени Савиной" name="description">

    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@400;500;600;700;800;900&display=swap"
        rel="stylesheet">

    <link href="{{ mix('css/app.css')}}" rel="stylesheet">
</head>

<body style="padding-top: 4.5rem">

    <nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
        <a class="navbar-brand" href="{{ url('/') }}">Сайт</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse"
            aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarCollapse">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item active">
                    <a class="nav-link" href="{{ route('support.dashboard.index') }}">Dashboard</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('support.dashboard.gifts') }}">Бесплатные пресеты</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('support.dashboard.discounts.index') }}">Скидки</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('support.dashboard.orders.index') }}">Ручные заказы</a>
                </li>
                <li class="nav-item">
                    <a href="{{ route('support.dashboard.users.index') }}" class="nav-link">Пользователи</a>
                </li>
            </ul>
            <ul class="navbar-nav px-3">
                <li class="nav-item text-nowrap">
                    <a class="nav-link" href="{{ route('admin.logout') }}">Выйти</a>
                </li>
            </ul>
        </div>
    </nav>

    <div class="container-fluid">
        <div class="row">
            <form class="form-inline js-find-user-form" action="{{ route('support.dashboard.d') }}">
                <div class="form-group mx-sm-3 mb-2">
                    <label for="userEmail" class="sr-only">Email</label>
                    <input name="email" type="email" class="form-control" id="userEmail" placeholder="Email" required>
                </div>
                <button type="submit" class="btn btn-primary mb-2">Найти</button>
            </form>
        </div>
        <div class="row">
            <div class="table-responsive">
                <table class="table table-sm">
                    <thead>
                        <tr>
                            <th scope="col"></th>
                            <th scope="col">Email</th>
                            <th scope="col">Товар</th>
                            <th scope="col">Заказ выполнен?</th>
                            <th scope="col">Ссылка</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>

    </div>


    <script src="{{ mix('js/app.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/select2.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/floatingFormLabels.min.js') }}"></script>
    <script src="{{ asset('js/jquery.event.move.js') }}"></script>
    <script src="{{ asset('js/jquery.twentytwenty.js') }}"></script>
    <script src="{{ asset('js/libs/jquery.mousewheel.js') }}"></script>
    <script src="{{ asset('js/libs/SmoothScroll.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/app_custom.js') }}"></script>

    <script>
        $(document).ready(function() {

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            , });

            console.log('dashboard');
            $('.js-find-user-form').on('submit', function(e) {
                e.preventDefault();
                console.log($(this).attr('action'));
                const table = $('.table');
                table.find("tbody tr").remove();

                $.ajax({
                    type: 'POST'
                    , url: $(this).attr('action')
                    , data: $(this).serialize()
                    , dataType: 'json'
                    , success: function(data) {
                        console.log(data);
                        data.downloadables.forEach(function(downloadable) {
                            // TO-DO !!!
                            let productTitles = '';
                            console.log(downloadable.product_id);
                            if (downloadable.product_id) {
                                productTitles = downloadable.product_title;
                            } else {
                                productTitles = downloadable.order.products.map($item => $item.title).join('<br>');
                            }
                            // data.product_id
                            // downloadable.product_title
                            table.append('<tr><td><button class="btn btn-danger btn-sm js-send-mail-btn" data-email="' + downloadable.user_email + '" data-url="' + downloadable.product_page_url + '" data-endpoint="' + data.url + '">Отправить</button></td><td><span class="js-user-email">' + downloadable.user_email + "</span></td><td>" + productTitles + "</td><td>" + downloadable.is_completed_order + "</td><td>" + '<span class="js-product-page-url">' + downloadable.product_page_url + "</span></td></tr>");
                        });
                    }
                , });
            });

            $(document).on('click', '.js-send-mail-btn', function(e) {
                e.preventDefault();
                const that = $(this);
                that.prop('disabled', true);
                $.ajax({
                    type: 'POST'
                    , url: '/support/dashboard/mail'
                    , data: {
                        email: that.data('email')
                        , url: that.data('url')
                    }
                    , dataType: 'json'
                    , success: function(data) {
                        console.log(data);
                        that.removeClass('btn-danger');
                        that.addClass('btn-success');
                    }
                    , error: function(data) {
                        that.addClass('btn-success');
                        $(this).prop('disabled', false);
                    }
                });
            });

        });

    </script>
</body>

</html>
