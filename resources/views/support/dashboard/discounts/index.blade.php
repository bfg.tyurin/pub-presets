<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta name="robots" content="noindex, nofollow" />
    <meta charset="utf-8">
    <meta content="width=device-width,initial-scale=1" name="viewport">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Пресеты Жени Савиной</title>
    <meta content="Пресеты Жени Савиной" name="description">

    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@400;500;600;700;800;900&display=swap"
        rel="stylesheet">

    <link href="{{ mix('css/app.css')}}" rel="stylesheet">
</head>

<body style="padding-top: 4.5rem">
    <nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
        <a class="navbar-brand" href="{{ url('/') }}">Сайт</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse"
            aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarCollapse">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item active">
                    <a class="nav-link" href="{{ route('support.dashboard.index') }}">Dashboard</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('support.dashboard.gifts') }}">Бесплатные пресеты</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('support.dashboard.discounts.index') }}">Скидки</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('support.dashboard.discounts.create') }}">Добавить скидку</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('support.dashboard.orders.index') }}">Ручные заказы</a>
                </li>
                <li class="nav-item">
                    <a href="{{ route('support.dashboard.users.index') }}" class="nav-link">Пользователи</a>
                </li>
            </ul>
            <ul class="navbar-nav px-3">
                <li class="nav-item text-nowrap">
                    <a class="nav-link" href="{{ route('admin.logout') }}">Выйти</a>
                </li>
            </ul>
        </div>
    </nav>

    <div class="container-fluid">
        <div class="row">
            <div class="col-md-2 d-md-block bg-light sidebar">
                <div class="sidebar-sticky">
                    {{Form::open(['url' => route('support.dashboard.discounts.index'), 'method' => 'GET'])}}
                    {{-- Имя пользователя передано из экшена --}}
                    {{Form::label('user_email', 'E-Mail пользователя:') }}
                    {{Form::email('user_email')}}
                    {{Form::submit('Найти')}}
                    {{Form::close()}}
                    <a href="{{  route('support.dashboard.discounts.index') }}">Очистить</a>
                </div>
            </div>
            <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
                <div class="table-responsive" style="font-size: .875rem">
                    <table class="table table-striped table-sm">
                        <thead class="thead-dark">
                            <tr>
                                <th>Пользователь</th>
                                <th>Продукт</th>
                                <th>Скидочная цена в рублях</th>
                                <th>Скидочная цена в долларах</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($discounts as $discount)
                            <tr>
                                <td>
                                    {{ $discount->user->email }}
                                <td>
                                    <a href="{{ route('support.dashboard.discounts.index', ['product_id' => $discount->product->id]) }}"
                                        data-method="get" rel="nofollow">{{ $discount->product->title }}</a>
                                    <p>
                                        <li class="far fa-ruble-sign"> {{ $discount->product->price_discount }}</li>
                                        <li class="far fa-dollar-sign"> {{ $discount->product->usd_price_discount }}
                                        </li>
                                    </p>
                                </td>
                                <td>
                                    {{ $discount->rub_price }}
                                </td>
                                <td>
                                    {{ $discount->usd_price }}
                                </td>
                                <td>
                                    {{-- <a href="#" class="btn btn-primary btn-sm" role="button" aria-pressed="true">Редактировать</a> --}}
                                    <form action="{{ route('support.dashboard.discounts.destroy', $discount->id) }}"
                                        method="post" class="d-inline">
                                        @csrf
                                        @method('delete')
                                        <button type="submit" class="btn btn-danger btn-sm" role="button"
                                            aria-pressed="true">Удалить</button>
                                    </form>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>

                    </table>
                    {{ $discounts->withQueryString()->links() }}
                </div>

            </main>
        </div>
    </div>


    <script src="{{ mix('js/app.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/select2.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/floatingFormLabels.min.js') }}"></script>
    <script src="{{ asset('js/jquery.event.move.js') }}"></script>
    <script src="{{ asset('js/jquery.twentytwenty.js') }}"></script>
    <script src="{{ asset('js/libs/jquery.mousewheel.js') }}"></script>
    <script src="{{ asset('js/libs/SmoothScroll.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/app_custom.js') }}"></script>

</body>

</html>
