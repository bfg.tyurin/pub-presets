<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta name="robots" content="noindex, nofollow" />
    <meta charset="utf-8">
    <meta content="width=device-width,initial-scale=1" name="viewport">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Пресеты Жени Савиной</title>
    <meta content="Пресеты Жени Савиной" name="description">

    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@400;500;600;700;800;900&display=swap" rel="stylesheet">

    <link href="{{ mix('css/app.css')}}" rel="stylesheet">
</head>

<body>
<nav class="navbar navbar-dark sticky-top bg-dark flex-md-nowrap p-0">
    <a class="navbar-brand col-sm-3 col-md-2 mr-0" href="{{ url('/') }}">Пресеты Жени Савиной</a>
    <a class="navbar-brand col-sm-3 col-md-2 mr-0" href="{{ route('support.dashboard.discounts.index') }}">Скидки</a>
    <ul class="navbar-nav px-3">
        <li class="nav-item text-nowrap">
            <a class="nav-link" href="{{ route('admin.logout') }}">Выйти</a>
        </li>
    </ul>
</nav>



<div class="container-fluid">
    <div class="row">
        <div class="col-md-2 d-md-block bg-light sidebar">
            <div class="sidebar-sticky">

            </div>
        </div>
        <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
            <form method="post" action="{{ route('support.dashboard.discounts.store') }}">
                @csrf
                <div class="form-row js-discount-form">
                    <div class="form-group col-md-6">
                        <label for="inputEmail4">Email пользователя</label>
                        <input name="user_email" type="email" class="form-control @error('user_email') is-invalid @enderror" id="inputEmail4" placeholder="Email" required>
                    </div>
                    <div class="form-group col-md-6">
                        <label for="inputState">Продукт</label>
                        <select name="product_id" class="form-control @error('product_id') is-invalid @enderror js-product-select" id="inlineFormCustomSelectPref">
                            <option class="js-option-empty" selected>Выбрать...</option>
                            @foreach($products as $product)
                                <option value="{{ $product->id }}" data-url="{{ route('support.dashboard.products.show', $product->id) }}">{{ $product->title }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-row js-product" style="display: none">
                    <div class="col-md-6 mb-3">
                        <label for="validationDefault03">Цена в рублях</label>
                        <input type="text" class="form-control js-product-rub" id="validationDefault03" value="5500" disabled>
                    </div>
                    <div class="col-md-3 mb-3">
                        <label for="validationDefault04">Цена в долларах</label>
                        <input type="text" class="form-control js-product-usd" id="validationDefault04" value="55" disabled>
                    </div>
                </div>
                <div class="form-row js-discount" style="display: none">
                    <div class="col-md-6 mb-3">
                        <label for="validationDefault03">Новая цена в рублях</label>
                        @error('rub_price')
                        <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                        <input name="rub_price" type="text" class="form-control js-discount-rub" id="validationDefault05" required>
                    </div>
                    <div class="col-md-3 mb-3">
                        <label for="validationDefault04">Новая цена в долларах</label>
                        @error('usd_price')
                        <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                        <input name="usd_price" type="text" class="form-control js-discount-usd" id="validationDefault06" required>
                    </div>
                </div>
                <button type="submit" class="btn btn-primary js-submit-btn">Добавить скидку</button>
            </form>
        </main>
    </div>
</div>


<script src="{{ mix('js/app.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/select2.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/floatingFormLabels.min.js') }}"></script>
<script src="{{ asset('js/jquery.event.move.js') }}"></script>
<script src="{{ asset('js/jquery.twentytwenty.js') }}"></script>
<script src="{{ asset('js/libs/jquery.mousewheel.js') }}"></script>
<script src="{{ asset('js/libs/SmoothScroll.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/app_custom.js') }}"></script>

<script>
    $(document).ready(function() {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
        });

        $('.js-discount-form :input, .js-discount-form select').each(function() {
            const input = $(this);
            input.focus(function() {
                $(this).removeClass('is-invalid');
            });
        });

        $('.js-product-select').change(function() {
            if($('select[name="product_id"] :selected').hasClass('js-option-empty')) {
                console.log('empty');
                return;
            }

            const submitBtn = $('.js-submit-btn');
            submitBtn.prop('disabled', true);

            console.log($(this).val());
            const url = $('select[name="product_id"] :selected').data('url');
            console.log(url);


            $.ajax({
                type: 'GET',
                url: url,
                success: function (data) {
                    console.log(data);
                    submitBtn.prop('disabled', false);
                    const product = $('.js-product');
                    const discount = $('.js-discount');
                    $('.js-product-rub').val(data.price_discount);
                    $('.js-product-usd').val(data.usd_price_discount);
                    $('.js-discount-rub').val(data.price_discount);
                    $('.js-discount-usd').val(data.usd_price_discount);
                    product.show();
                    discount.show();
                },
            });
        });
    });
</script>

</body>
</html>

