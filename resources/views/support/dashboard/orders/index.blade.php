@extends('support.layout')

@section('content')
<div class="flex flex-col">
    <div class="w-full px-6 py-4">
        <a href="{{ route('support.dashboard.orders.create') }}" class="bg-blue-500 hover:bg-blue-700 text-white font-bold text-center py-2 px-4">
            Добавить выполненный заказ
        </a>
    </div>
    <div class="my-2 py-2 overflow-x-auto sm:-mx-6 sm:px-6 lg:-mx-8 lg:px-8">
        <div class="align-middle inline-block min-w-full shadow overflow-hidden sm:rounded-lg border-b border-gray-200">
            <table class="min-w-full">
                <thead>
                    <tr>
                        <th class="px-6 py-3 border-b border-gray-200 bg-gray-50 text-left text-xs leading-4 font-medium text-gray-500 uppercase tracking-wider">
                            Заказ
                        </th>
                        <th class="px-6 py-3 border-b border-gray-200 bg-gray-50 text-left text-xs leading-4 font-medium text-gray-500 uppercase tracking-wider">
                            Цена
                        </th>
                        <th class="px-6 py-3 border-b border-gray-200 bg-gray-50 text-left text-xs leading-4 font-medium text-gray-500 uppercase tracking-wider">
                            Пользователь
                        </th>
                        <th class="px-6 py-3 border-b border-gray-200 bg-gray-50 text-left text-xs leading-4 font-medium text-gray-500 uppercase tracking-wider">
                            Статус
                        </th>
                        <th class="px-6 py-3 border-b border-gray-200 bg-gray-50"></th>
                    </tr>
                </thead>
                <tbody class="bg-white">
                    @foreach ($orders as $order)
                    <tr>
                        <td class="px-6 py-4 whitespace-no-wrap border-b border-gray-200">
                            <div class="flex items-center">
                                <div class="ml-4">
                                    <div class="text-sm leading-5 font-medium text-gray-900">
                                        @foreach ($order->products as $product)
                                            {{ $product->title ?? '' }}<br>
                                        @endforeach</div>
                                    <div class="text-sm leading-5 text-gray-500">{{ $order->id }}</div>

                                    {{-- TO-DO v1 compatibility --}}
                                    @isset($order->product_id)
                                        <div class="text-xs leading-5 text-gray-500"><a href="{{ $order->user->downloadables()->where('product_id', $order->product_id)->latest()->first() ? $order->user->downloadables()->where('product_id', $order->product_id)->latest()->first()->getUrl() : '' }}">Ссылка на скачку</a></div>
                                    @endisset

                                    @isset($order->downloadable)
                                        <div class="text-xs leading-5 text-gray-500"><a href="{{ $order->downloadable->getUrl() }}">Ссылка на скачку</a></div>
                                    @endisset

                                </div>
                            </div>
                        </td>
                        <td class="px-6 py-4 whitespace-no-wrap border-b border-gray-200 text-sm leading-5 text-gray-500">
                            {{ $order->total }} {{ $order->currency }}
                        </td>
                        <td class="px-6 py-4 whitespace-no-wrap border-b border-gray-200">
                            <div class="text-sm leading-5 text-gray-900">{{ $order->user->email }}</div>
                            <div class="text-sm leading-5 text-gray-500">{{ $order->user->profile->name ?? '-' }} {{ $order->user->profile->surname ?? '-' }}</div>
                        </td>
                        <td class="px-6 py-4 whitespace-no-wrap border-b border-gray-200">
                            <span class="px-2 inline-flex text-xs leading-5 font-semibold rounded-full {{ $order->status === 'completed' ? 'bg-green-100 text-green-800' : 'bg-red-100 text-red-800' }} ">
                                {{ $order->status }}
                            </span>
                        </td>
                        <td class="px-6 py-4 whitespace-no-wrap text-right border-b border-gray-200 text-sm leading-5 font-medium">
                            <a href="{{ route('support.dashboard.orders.edit', $order) }}" class="mr-5 text-indigo-600 hover:text-indigo-900">
                                <svg class="fill-current text-indigo-500 inline-block h-4 w-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
                                    <path d="M12.3 3.7l4 4L4 20H0v-4L12.3 3.7zm1.4-1.4L16 0l4 4-2.3 2.3-4-4z" />
                                </svg>
                            </a>
                            <a href="{{ route('support.dashboard.orders.destroy', $order) }}" data-confirm="Вы уверены?" data-method="delete" rel="nofollow" class="text-red-600 hover:text-red-900">
                                <svg class="fill-current text-red-500 inline-block h-4 w-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
                                    <path d="M6 2l2-2h4l2 2h4v2H2V2h4zM3 6h14l-1 14H4L3 6zm5 2v10h1V8H8zm3 0v10h1V8h-1z" />
                                </svg>
                            </a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            {{ $orders->links('vendor.pagination.tailwind') }}
        </div>
    </div>
</div>
@endsection
