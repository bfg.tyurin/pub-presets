@extends('support.layout')

@section('content')
<div class="flex flex-col">
    <div class="my-2 py-2 overflow-x-auto sm:-mx-6 sm:px-6 lg:-mx-8 lg:px-8">
        <div class="flex justify-center align-middle min-w-full overflow-hidden sm:rounded-lg">
            <div class="w-full max-w-sm">
                <h1 class="text-lg px-8 pt-6 pb-8">Редактирование выполненного заказа</h1>
                @if ($errors->any())
                <div>
                    <ul>
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                @endif

                <form class="bg-white shadow-md rounded px-8 pt-6 pb-8 mb-4" method="POST"
                    action="{{ route('support.dashboard.orders.update', $order) }}">
                    @csrf
                    @method('PATCH')
                    <div class="mb-4">
                        <label class="block text-gray-700 text-sm font-bold mb-2" for="email">
                            Email пользователя
                        </label>
                        <input name="email" value="{{ $order->user->email }}" required
                            class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                            id="email" type="email" placeholder="email">
                    </div>
                    <div class="relative mb-6">
                        <select name="product_id" required
                            class="block appearance-none w-full bg-gray-200 border border-gray-200 text-gray-700 py-3 px-4 pr-8 rounded leading-tight focus:outline-none focus:bg-white focus:border-gray-500"
                            id="grid-state">
                            <option selected disabled value="Выбрать продукт">Выбрать продукт</option>
                            @foreach ($products as $product)
                            <option {{ $order->product->id === $product->id ? 'selected' : '' }}
                                value="{{ $product->id }}">{{ $product->title }}</option>
                            @endforeach
                        </select>
                        <div
                            class="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-700">
                            <svg class="fill-current h-4 w-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
                                <path d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z" />
                            </svg>
                        </div>
                    </div>
                    <div class="flex flex-wrap -mx-3 mb-6">
                        <div class="w-full md:w-1/2 px-3 mb-6 md:mb-0 relative">
                            <label class=" block text-g ray-700 text-sm font-bold mb-2" for="total">
                                Валюта
                            </label>
                            <select name="currency" value="{{ $order->currency }}" required
                                class="shadow appearance-none border border-gray-200 rounded w-full py-2 px-3 text-gray-700 mb-3 leading-tight focus:outline-none focus:shadow-outline"
                                name="currency" id="currency" type="select" placeholder="0">
                                <option {{ $order->currency === 'RUB' ? 'selected' : '' }} value="RUB">RUB</option>
                                <option {{ $order->currency === 'USD' ? 'selected' : '' }} value="USD">USD</option>
                            </select>
                        </div>
                        <div class="w-full md:w-1/2 px-3 mb-6 md:mb-0">
                            <label class=" block text-g ray-700 text-sm font-bold mb-2" for="total">
                                Цена
                            </label>
                            <input name="total" value="{{ number_format($order->total, 0, '', '') }}" required
                                class="shadow appearance-none border border-gray-200 rounded w-full py-2 px-3 text-gray-700 mb-3 leading-tight focus:outline-none focus:shadow-outline"
                                id="total" type="number">
                        </div>
                    </div>
                    <div class="flex flex-wrap -mx-3 mb-6">
                        <label class="w-full px-3 mb-6 md:mb-0 relative text-gray-500 font-bold">
                            <input name="send_email" class="mr-2 leading-tight" type="checkbox">
                            <span class="text-sm">
                                Отправить пресет
                            </span>
                            <div class="text-xs">* Пользователь получить ссылку на скачивание выбранного пресета</div>
                        </label>
                    </div>
                    <div class="flex items-center justify-center">
                        <button data-confirm="Вы уверены?"
                            class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline"
                            type="submit">
                            Обновить выполненный заказ
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
