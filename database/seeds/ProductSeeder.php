<?php

use App\Pack;
use App\Price;
use App\Product;
use Illuminate\Database\Seeder;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Product::addProduct(
            Pack::where('slug', 'metallic-blues')->first(),
            new Price(3600, 1800),
            'pack'
        );
        Product::addProduct(
            Pack::where('slug', 'golden-caramel')->first(),
            new Price(6000, 3000),
            'pack'
        );
        Product::addProduct(
            Pack::where('slug', 'wedding-pureness')->first(),
            new Price(4400, 2200),
            'pack'
        );

        Product::addBundle(
            [
                Pack::where('slug', 'golden-caramel')->first()->id,
                Pack::where('slug', 'metallic-blues')->first()->id,
                Pack::where('slug', 'wedding-pureness')->first()->id,
            ],
            new Price(14000, 4900),
            'Golden + Metallic + Wedding',
            'golden-metallic-wedding',
            'bundle'
        );
    }
}
