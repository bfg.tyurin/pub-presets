<?php


use App\Product;
use Illuminate\Database\Seeder;

class UsdProductPricesSeeder extends Seeder
{
    public function run()
    {
        $p = Product::firstWhere('slug', 'golden-metallic-wedding');
        // -60%
        $p->usd_price_original = 240;
        $p->usd_price_discount = 96;
        $p->save();

        // -45%
        $p = Product::firstWhere('slug', 'golden-caramel');
        $p->usd_price_original = 100;
        $p->usd_price_discount = 55;
        $p->save();

        $p = Product::firstWhere('slug', 'metallic-blues');
        $p->usd_price_original = 60;
        $p->usd_price_discount = 33;
        $p->save();

        $p = Product::firstWhere('slug', 'wedding-pureness');
        $p->usd_price_original = 80;
        $p->usd_price_discount = 44;
        $p->save();

        $p = Product::firstWhere('slug', 'metallic-wedding');
        $p->usd_price_original = 140;
        $p->usd_price_discount = 96 - 55;
        $p->save();

        $p = Product::firstWhere('slug', 'golden-metallic');
        $p->usd_price_original = 160;
        $p->usd_price_discount = 96 - 44;
        $p->save();

        $p = Product::firstWhere('slug', 'golden-wedding');
        $p->usd_price_original = 180;
        $p->usd_price_discount = 96 - 33;
        $p->save();
    }
}
