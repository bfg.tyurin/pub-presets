<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UserSeeder::class);
        $this->call(PackSeeder::class);
        $this->call(ProductSeeder::class);
        $this->call(OrderSeeder::class);
        $this->call(RelatedProductSeeder::class);
        $this->call(FaqItemSeeder::class);
        $this->call(OrderTotalSeeder::class);
        $this->call(UsdProductPricesSeeder::class);
        $this->call(EmailCampaignSeeder::class);
        $this->call(PromotionAvalableUntilSeeder::class);
        $this->call(MobilePacksSeeder::class);
        $this->call(MobileProductsSeeder::class);
        $this->call(V2PricesSeeder::class);
    }
}
