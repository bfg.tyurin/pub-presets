<?php


use Illuminate\Database\Seeder;

class PromotionAvalableUntilSeeder extends Seeder
{
    public function run()
    {
        foreach(\App\Promotion::all() as $promotion) {
            $promotion->available_until = $promotion->created_at;
            $promotion->save();
        }
    }
}
