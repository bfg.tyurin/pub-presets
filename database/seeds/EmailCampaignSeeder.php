<?php


use App\EmailCampaign;
use Illuminate\Database\Seeder;

class EmailCampaignSeeder extends Seeder
{
    public function run()
    {
        EmailCampaign::create(['title' => 'Хорошие новости! Обновление пресетов']);
    }
}
