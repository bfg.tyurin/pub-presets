<?php

use App\Pack;
use App\Price;
use App\Product;
use App\RelatedProduct;
use Illuminate\Database\Seeder;

class RelatedProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
//        $product = Product::addBundle(
//            [
//                Pack::where('slug', 'golden-caramel')->first()->id,
//                Pack::where('slug', 'metallic-blues')->first()->id,
//            ],
//            new Price(2000, 2000),
//            'Акция Golden + Metallic',
//            'golden-metallic',
//            'promotion'
//        );

//        $related = Product::firtstWhere('slug', 'wedding-pureness')->id;

        RelatedProduct::create(
            [
                'product_id' => Product::addBundle(
                    [
                        Pack::where('slug', 'golden-caramel')->first()->id,
                        Pack::where('slug', 'metallic-blues')->first()->id,
                    ],
                    new Price(2000, 2000),
                    'Акция Golden + Metallic',
                    'golden-metallic',
                    'promotion'
                )->id,
                'related_product_id' => Product::firstWhere('slug', 'wedding-pureness')->id,
            ]
        );

        RelatedProduct::create(
            [
                'product_id' => Product::addBundle(
                    [
                        Pack::where('slug', 'golden-caramel')->first()->id,
                        Pack::where('slug', 'wedding-pureness')->first()->id,
                    ],
                    new Price(2000, 2000),
                    'Акция Golden +  Wedding',
                    'golden-wedding',
                    'promotion'
                )->id,
                'related_product_id' => Product::firstWhere('slug', 'metallic-blues')->id,
            ]
        );

        RelatedProduct::create(
            [
                'product_id' => Product::addBundle(
                    [
                        Pack::where('slug', 'metallic-blues')->first()->id,
                        Pack::where('slug', 'wedding-pureness')->first()->id,
                    ],
                    new Price(2000, 2000),
                    'Акция Metallic + Wedding',
                    'metallic-wedding',
                    'promotion'
                )->id,
                'related_product_id' => Product::firstWhere('slug', 'golden-caramel')->id,
            ]
        );


    }
}
