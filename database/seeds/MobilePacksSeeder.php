<?php

use App\Pack;
use Illuminate\Database\Seeder;

class MobilePacksSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Pack::addPack(
            'Spring Burst',
            'spring-burst',
            7
        );
        Pack::addPack(
            'Safari Style',
            'safari-style',
            6
        );
    }
}
