<?php

use App\Pack;
use App\Product;
use Illuminate\Database\Seeder;

class MobileProductsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $goldenCaramelPack = Pack::firstWhere('slug', 'golden-caramel');
        $metallicBluesPack = Pack::firstWhere('slug', 'metallic-blues');
        $springBurstPack = Pack::firstWhere('slug', 'spring-burst');
        $safariStylePack = Pack::firstWhere('slug', 'safari-style');

        $goldenCaramelMobileProduct = Product::create([
            'title'              => 'Golden Caramel Mobile',
            'slug'               => 'golden-caramel-mobile',
            'type'               => 'pack',
            'price_original'     => 4000,
            'price_discount'     => 1999,
            'usd_price_original' => 68,
            'usd_price_discount' => 33,
            'category'           => 'mobile',
        ]);
        $goldenCaramelPack->products()->attach($goldenCaramelMobileProduct->id);

        $metallicBluesMobileProduct = Product::create([
            'title'              => 'Metallic Blues Mobile',
            'slug'               => 'metallic-blues-mobile',
            'type'               => 'pack',
            'price_original'     => 3000,
            'price_discount'     => 1499,
            'usd_price_original' => 48,
            'usd_price_discount' => 23,
            'category'           => 'mobile',
        ]);
        $metallicBluesPack->products()->attach($metallicBluesMobileProduct->id);

        $springBurstMobileProduct = Product::create([
            'title'              => 'Spring Burst',
            'slug'               => 'spring-burst',
            'type'               => 'pack',
            'price_original'     => 3800,
            'price_discount'     => 1899,
            'usd_price_original' => 64,
            'usd_price_discount' => 31,
            'category'           => 'mobile',
        ]);
        $springBurstPack->products()->attach($springBurstMobileProduct->id);

        $safariStyleMobileProduct = Product::create([
            'title'              => 'Safari Style',
            'slug'               => 'safari-style',
            'type'               => 'pack',
            'price_original'     => 3600,
            'price_discount'     => 1799,
            'usd_price_original' => 60,
            'usd_price_discount' => 29,
            'category'           => 'mobile',
        ]);
        $safariStylePack->products()->attach($safariStyleMobileProduct->id);
    }
}
