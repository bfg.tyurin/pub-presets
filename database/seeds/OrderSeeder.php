<?php

use App\Order;
use App\Product;
use App\User;
use Illuminate\Database\Seeder;

class OrderSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $order = new Order();
        $order->user_id = User::where('email', 'test@example.com')->first()->id;
        $order->product_id = Product::where('slug', 'golden-metallic-wedding')->first()->id;
        $order->status = 'completed';
        $order->save();

        $order = new Order();
        $order->user_id = User::where('email', 'test@example.com')->first()->id;
        $order->product_id = Product::where('slug', 'wedding-pureness')->first()->id;
        $order->status = 'completed';
        $order->save();
    }
}
