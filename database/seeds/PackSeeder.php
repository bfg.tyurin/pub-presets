<?php

use App\Pack;
use Illuminate\Database\Seeder;

class PackSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Pack::addPack(
            'Golden Caramel',
            'golden-caramel',
            10
        );
        Pack::addPack(
            'Metallic blues',
            'metallic-blues',
            6
        );
        Pack::addPack(
            'Wedding pureness',
            'wedding-pureness',
            9
        );
    }
}
