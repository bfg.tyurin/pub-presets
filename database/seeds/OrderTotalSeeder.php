<?php


use App\Order;
use Illuminate\Database\Seeder;
use Illuminate\Support\Carbon;

class OrderTotalSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $orders = Order::where('created_at', '<=', Carbon::createFromTimestamp('1591045765'))->get();
        foreach ($orders as $order) {
            switch ($order->product->slug) {
                case 'golden-caramel':
                    $order->total = 3000;
                    break;
                case'metallic-blues':
                    $order->total = 1800;
                    break;
                case('wedding-pureness'):
                    $order->total = 2200;
                    break;
                case('golden-metallic-wedding'):
                    $order->total = 4900;
                    break;
            }
            $order->save();
        }
    }
}
