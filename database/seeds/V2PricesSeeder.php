<?php

use App\Product;
use Illuminate\Database\Seeder;

class V2PricesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $goldenCaramelProduct = Product::firstWhere('slug', 'golden-caramel');
        $goldenCaramelProduct->price_original = 6000;
        $goldenCaramelProduct->price_discount = 2999;
        $goldenCaramelProduct->usd_price_original = 100;
        $goldenCaramelProduct->usd_price_discount = 49;
        $goldenCaramelProduct->save();

        $metallicBluesProduct = Product::firstWhere('slug', 'metallic-blues');
        $metallicBluesProduct->price_original = 3600;
        $metallicBluesProduct->price_discount = 1799;
        $metallicBluesProduct->usd_price_original = 60;
        $metallicBluesProduct->usd_price_discount = 29;
        $metallicBluesProduct->save();

        $weddingPurinessProduct = Product::firstWhere('slug', 'wedding-pureness');
        $weddingPurinessProduct->price_original = 4400;
        $weddingPurinessProduct->price_discount = 2199;
        $weddingPurinessProduct->usd_price_original = 80;
        $weddingPurinessProduct->usd_price_discount = 39;
        $weddingPurinessProduct->save();
    }
}
