<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Product;
use App\User;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(Product::class, function (Faker $faker) {
    return [
        'title' => $faker->sentence(2),
        'slug' => $faker->word(),
        'price_original' => $faker->randomNumber(5, true),
        'price_discount' => $faker->randomNumber(4, true),
        'usd_price_original' => $faker->randomNumber(3, true),
        'usd_price_discount' => $faker->randomNumber(2, true),
        'type' => $faker->word(),
    ];
});
