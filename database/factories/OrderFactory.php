<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Pack;
use App\User;
use App\Order;
use App\Product;
use App\Discount;
use Illuminate\Support\Str;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(Order::class, function (Faker $faker) {
    return [
        'user_id'      => factory(User::class)->create()->id,
        'product_id'   => factory(Product::class)->create()->id,
        'status'       => Order::STATUS_PROCESSING,
        'total'        => $faker->randomNumber(5, true),
        'payment_type' => Order::PAYMENT_TYPE_CLOUDPAYMENTS,
        'currency'     => Order::CURRENCY_RUB,
    ];
});
