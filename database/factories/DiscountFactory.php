<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Discount;
use App\Pack;
use App\User;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(Discount::class, function (Faker $faker) {
    return [
        'user_id' => $faker->uuid,
        'product_id' => $faker->uuid,
        'rub_price' => $faker->randomNumber(4, true),
        'usd_price' => $faker->randomNumber(2, true),
    ];
});
