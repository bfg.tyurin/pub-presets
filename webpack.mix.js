const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js')
  // .sass('resources/sass/app.scss', 'public/css')
  .styles([
    'resources/css/bootstrap.min.css',
    'resources/css/select2.css',
    'resources/css/fontawesome.css',
    'resources/css/twentytwenty-no-compass.css',
    'resources/css/fonts.css',
    'resources/css/styles.css',
    'resources/css/responsive.css'
  ], 'public/css/app.css')
  .copy('resources/images', 'public/images', false)
  .copy('resources/img', 'public/img', false);

if (mix.inProduction()) {
  mix.version();
}

mix.postCss('resources/css/admin/main.css', 'public/admin/css', [
  require('autoprefixer'),
  require('tailwindcss'),
])
