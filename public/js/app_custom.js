$(document).ready(function () {
    JS.init();


});

var isMobile = {
    Android: function() {
        return navigator.userAgent.match(/Android/i);
    },
    BlackBerry: function() {
        return navigator.userAgent.match(/BlackBerry/i);
    },
    iOS: function() {
        return navigator.userAgent.match(/iPhone|iPad|iPod/i);
    },
    Opera: function() {
        return navigator.userAgent.match(/Opera Mini/i);
    },
    Windows: function() {
        return navigator.userAgent.match(/IEMobile/i);
    },
    any: function() {
        return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
    }
};

var JS = {
    clickEvent: '',
    hoverEvent: '',
    startInterval: 0,

    init: function () {
        JS.clickEvent = isMobile.any() ? 'touchstart' : 'click';
        JS.hoverEvent = isMobile.any() ? 'touchstart' : 'hover';


        if($(window).width()*1 > 767) {
            /*AOS.init({
                easing: 'ease-in-out-sine'
            });*/

            SmoothScroll({
                keyboardSupport: false,
                animationTime    : 1200,
                stepSize         : 70
            });
        }

        if($(window).width()*1 < 768) {
            if($('.header_home').length > 0) {
                var wh = $(window).height()*1;
                if(isMobile.iOS()){
                    wh = wh - 50;
                }

                $('.header_home').height(wh);
            }
        }


        if($(window).width()*1 >= 1280) {
            JS.animateContent();
        }


        //if($(window).width()*1 > 767){

        /*}else{
            $('.twentytwenty-container').addClass('twentytwenty-container-mobile').removeClass('twentytwenty-container');

            $('body').on('touchstart', '.twentytwenty-container-mobile', function(e){
                e.preventDefault();
                $(this).addClass('active');
                return false;
            });
            $('body').on('touchend', '.twentytwenty-container-mobile', function(e){
                e.preventDefault();
                $(this).removeClass('active');
                return false;
            });
        }*/

        JS.initPreset();
        setTimeout(function(){
            JS.initPreset();
        }, 1000);

        JS.toggleFAQ();
        JS.navbar();
        JS.validate();
        JS.selectTarif();
        JS.instName();
        JS.getPreset();
        JS.animateContent();
        JS.headerChangeTheme();
        JS.menuHover();

        $('.ffl-wrapper').floatingFormLabels();

        if($('.custom_select').length > 0){
            $('.custom_select').select2({
                width: '100%',
                minimumResultsForSearch: -1
            });
        }

        if($('.header_home').length > 0){
            $(window).scroll(function(){
                var top = $(window).scrollTop();
                var scale = 1 + top/4000;
                $('.header_home .bg').css('transform', 'scale('+scale+')');
            });
        }


    },

    menuHover: function(){
        $('body').on('mouseenter', '.menu_hover_item', function(){
            $('.top_menu__nav_image img').removeClass('active');
            var image = $(this).attr('data-image');
            $('.top_menu__nav_image img[data-img="'+image+'"]').addClass('active');
        })
    },

    headerChangeTheme: function(){

        changeTheme();
        $(window).scroll(function () {
            changeTheme();
        });


        var lastScrollTop = 0;
        function changeTheme(){

            var st = $(this).scrollTop();
            if (st > lastScrollTop){
                var top = $(window).scrollTop();
                if (top > 100) {
                    $('.header').addClass('header_hidden');
                    $('.js_show_header').show();
                }
            } else {
                $('.header').removeClass('header_hidden');
                $('.js_show_header').hide();
            }
            lastScrollTop = st;
        }

        $('body').on('click', '.js_show_header', function(){
            $('.header').removeClass('header_hidden');
            $('.js_show_header').hide();
            return false;
        });
    },

    getPreset: function(){
        $('body').on('click','.js_get_preset', function(){
            $('.pm_form').hide();
            $('.pm_thanks').show();

            return false;
        })
    },

    instName:function () {
        if($('#inst_name').length > 0){
            var input = document.getElementById('inst_name')
                ,value = input.value;

            input.addEventListener('input', onInput);

            function onInput(e){
                var newValue = e.target.value;
                if( newValue.match(/[^a-zA-Z_.0-9]/g)) {
                    input.value = value;
                    return;
                }
                value = newValue;
            }
        }

    },

    initPreset: function(){
        $(".twentytwenty-container[data-orientation!='vertical']").twentytwenty({
            default_offset_pct: 0.5,
            move_slider_on_hover: true
        });
    },

    toggleFAQ: function(){
        $('body').on('click', '.faq_question', function(){
            var $this = $(this);
            var item = $this.closest('.faq_item');
            var answer = item.find('.faq_answer');

            if(item.hasClass('open')){
                item.removeClass('open');
                answer.slideUp();
            }else{
                item.addClass('open');
                answer.slideDown();
            }
        });
    },

    navbar: function(){
        if($(window).width()*1 > 767) {
            $('body').on('click', '.js_open_nav', function () {
                $('.top_menu').addClass('open');
                $('body').addClass('body_fixed');

                return false;
            });

            $('body').on('click', '.js_close_nav', function () {
                $('.top_menu').removeClass('open');
                $('body').removeClass('body_fixed');

                return false;
            });
        } else {
            $('body').on('click', '.js_open_nav', function () {
                if($(this).hasClass('js_opened')){
                    $('.top_menu').removeClass('open');
                    $('body').removeClass('body_fixed');
                    $(this).removeClass('js_opened');
                }else{
                    $('.top_menu').addClass('open');
                    $('body').addClass('body_fixed');
                    $(this).addClass('js_opened');
                }


                return false;
            });
        }

    },

    validate: function(){
        $('body').on('blur', 'input[required]', function(){
            var $this = $(this);
            var input_wrapper = $this.closest('.p_input_wrapper');
            if($this.val() == ''){
                input_wrapper.addClass('input_error');
                input_wrapper.append();
            }
        });

        $('body').on('keyup', 'input[required]', function(){
            var $this = $(this);
            var input_wrapper = $this.closest('.p_input_wrapper');
            if($this.val() == ''){
                input_wrapper.addClass('input_error');
            }else{
                input_wrapper.removeClass('input_error');
            }
        });

        $('body').on('click', '.p_input_wrapper', function(){
            $(this).find('input').focus();

        })
    },

    selectTarif: function(){
        $('body').on('click', '.payment_tarif_block', function(){
            var $this = $(this);
            var tarif = $this.closest('.payment_tarif');
            var t = tarif.attr('data-tarif');
            if(t == 'all'){
                $('.pp_block__images img').removeClass('pt_single').show();
            }else{
                $('.pp_block__images img').removeClass('pt_single').hide();
                $('.pp_block__images img[data-image="'+t+'"]').show().addClass('pt_single');
            }

            $('.payment_tarif').removeClass('selected');
            tarif.addClass('selected');

            return false;
        })
    },

    animateContent: function(){
        function hello() {
            JS.startInterval = 0;
            $('.will-fade-in').each(function (i) {
                var $this = $(this);
                var topOfItem = $this.offset().top - 100,
                    bottomOfItem = $this.offset().top + 100 + $this.outerHeight(),
                    windowoff = $(window).scrollTop(),
                    windowOffset = $(window).scrollTop() + $(window).height();


                if ((windowOffset > topOfItem && windowoff < topOfItem) || (windowOffset > bottomOfItem && windowoff < bottomOfItem) || topOfItem < windowoff && bottomOfItem > windowOffset) {
                    JS.startInterval += 80;
                    setTimeout(function(){
                        $this.css({
                            'opacity': 1,
                            'transform': 'translate3d(0,0,0)'
                        });
                    }, JS.startInterval);
                } else {
                    //$(this).attr('style', '');
                }

            });

            $('.will-fade-in-top').each(function (i) {
                var $this = $(this);
                var topOfItem = $this.offset().top - 100,
                    bottomOfItem = $this.offset().top + 100 + $this.outerHeight(),
                    windowoff = $(window).scrollTop(),
                    windowOffset = $(window).scrollTop() + $(window).height();


                if ((windowOffset > topOfItem && windowoff < topOfItem) || (windowOffset > bottomOfItem && windowoff < bottomOfItem) || topOfItem < windowoff && bottomOfItem > windowOffset) {
                    JS.startInterval += 80;
                    setTimeout(function(){
                        $this.css({
                            'opacity': 1,
                            'transform': 'translate3d(0,0,0)'
                        });
                    }, JS.startInterval);
                } else {
                    // $(this).attr('style', '');
                }

            });

            $('.will-fade-in-title').each(function (i) {
                var $this = $(this);
                var topOfItem = $this.offset().top - 100,
                    bottomOfItem = $this.offset().top + 100 + $this.outerHeight(),
                    windowoff = $(window).scrollTop(),
                    windowOffset = $(window).scrollTop() + $(window).height();


                if ((windowOffset > topOfItem && windowoff < topOfItem) || (windowOffset > bottomOfItem && windowoff < bottomOfItem) || topOfItem < windowoff && bottomOfItem > windowOffset) {
                    JS.startInterval += 80;
                    setTimeout(function(){
                        $this.css({
                            'opacity': 1,
                            'transform': 'translate3d(0,0,0)'
                        });
                    }, JS.startInterval);
                } else {
                    //$(this).attr('style', '');
                }

            });

        }

        hello();
        $(window).scroll(hello);
    }


};
