$(document).ready(function () {
    JS.init();


});

var isMobile = {
    Android: function() {
        return navigator.userAgent.match(/Android/i);
    },
    BlackBerry: function() {
        return navigator.userAgent.match(/BlackBerry/i);
    },
    iOS: function() {
        return navigator.userAgent.match(/iPhone|iPad|iPod/i);
    },
    Opera: function() {
        return navigator.userAgent.match(/Opera Mini/i);
    },
    Windows: function() {
        return navigator.userAgent.match(/IEMobile/i);
    },
    any: function() {
        return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
    }
};

var JS = {
    clickEvent: '',
    hoverEvent: '',

    init: function () {
        JS.clickEvent = isMobile.any() ? 'touchstart' : 'click';
        JS.hoverEvent = isMobile.any() ? 'touchstart' : 'hover';


        /*AOS.init({
            easing: 'ease-in-out-sine'
        });*/

        if($(window).width()*1 > 767){
            $(".twentytwenty-container[data-orientation!='vertical']").twentytwenty({
                default_offset_pct: 0.5,
                move_slider_on_hover: true
            });
        }else{
            $('.twentytwenty-container').addClass('twentytwenty-container-mobile').removeClass('twentytwenty-container');

            $('body').on('touchstart', '.twentytwenty-container-mobile', function(e){
                e.preventDefault();
                $('.twentytwenty-container-mobile').removeClass('active');
                $(this).addClass('active');
                return false;
            });
        }

        $('img').bind('contextmenu', function(e) {
            return false;
        });

        function longClickHandler(e){
            e.preventDefault();
        }

        $("img").longclick(250, longClickHandler);

    }


};
